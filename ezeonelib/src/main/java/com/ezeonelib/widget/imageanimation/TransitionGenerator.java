
package com.ezeonelib.widget.imageanimation;

import android.graphics.RectF;

public interface TransitionGenerator {

    /**
     * Generates the next transition to be played by the {@link AnimateImageView}.
     *
     * @param drawableBounds the bounds of the drawable to be shown in the {@link AnimateImageView}.
     * @param viewport       the rect that represents the viewport where
     *                       the transition will be played in. This is usually the bounds of the
     *                       {@link AnimateImageView}.
     * @return a {@link Transition} object to be played by the {@link AnimateImageView}.
     */
    public Transition generateNextTransition(RectF drawableBounds, RectF viewport);

}
