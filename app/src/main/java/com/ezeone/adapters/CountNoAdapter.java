package com.ezeone.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.pojo.CountNo;

import java.util.List;

public class CountNoAdapter extends
		RecyclerView.Adapter<CountNoAdapter.CountViewHolder> {

	private List<CountNo> countList;

	public CountNoAdapter(List<CountNo> contactList) {
		this.countList = contactList;
	}

	@Override
	public int getItemCount() {
		return countList.size();
	}

	@Override
	public void onBindViewHolder(CountViewHolder countViewHolder, int i) {

		CountNo countNo = countList.get(i);
		countViewHolder.countText.setText(countNo.count);

	}

	@Override
	public CountViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {

		View countView = LayoutInflater.from(viewGroup.getContext()).inflate(
				R.layout.row_ezeid_count, viewGroup, false);
		return new CountViewHolder(countView);

	}

	public static class CountViewHolder extends RecyclerView.ViewHolder {

		protected TextView countText;

		public CountViewHolder(View v) {
			super(v);
			countText = (TextView) v.findViewById(R.id.countText);
		}
	}
}
