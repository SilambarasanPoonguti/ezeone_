package com.ezeone.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.pojo.OtherLocation;

import java.util.List;

public class OtherLocationsAdapter extends ArrayAdapter<OtherLocation>{

	private Context context;
	private List<OtherLocation> locations;
	private int resource;
	
	
	public OtherLocationsAdapter(Context context, int resource,
			List<OtherLocation> locations) {
		super(context, resource, locations);
		this.context = context;
		this.locations = locations;
		this.resource = resource;
				
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = null;
		final OtherLocation location = getItem(position);
		
		if(convertView==null){
			convertView = mInflater.inflate(resource, null);
			holder = new ViewHolder();
			holder.locationName = (TextView) convertView.findViewById(R.id.locationName);
			holder.deleteImage = (ImageView) convertView.findViewById(R.id.deleteLocation);
			holder.locationSubtext = (TextView) convertView.findViewById(R.id.locationSubText);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.locationName.setText(location.getLocTitle());
		holder.locationSubtext.setText(location.getAddressLine1()+","+location.getAddressLine2());
		return convertView;
	}
	
	private class ViewHolder{
		TextView locationName,locationSubtext;
		ImageView deleteImage;
	}
}
