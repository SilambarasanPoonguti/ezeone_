package com.ezeone.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.pojo.Document;

import java.util.List;

public class DocumentAdapter extends ArrayAdapter<Document>{
	private Context context;
	private List<Document> docs;
	private int resource;
	
	
	public DocumentAdapter(Context context, int resource,
			List<Document> docs) {
		super(context, resource, docs);
		this.context = context;
		this.docs = docs;
		this.resource = resource;
				
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = null;
		final Document doc = getItem(position);
		
		if(convertView==null){
			convertView = mInflater.inflate(resource, null);
			holder = new ViewHolder();
			holder.documentType = (TextView) convertView.findViewById(R.id.documentType);
			holder.documentLink = (TextView) convertView.findViewById(R.id.documentLink);
			
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.documentType.setText(doc.getDocumentType());
		holder.documentLink.setText(doc.getLink());
		
		return convertView;
	}
	
	private class ViewHolder{
		TextView documentType,documentLink;
	}
}
