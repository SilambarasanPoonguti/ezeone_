package com.ezeone.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ezeone.R;
import com.ezeone.pojo.SearchListItem;

import java.util.List;

public class KeySearchListAdapter extends
		RecyclerView.Adapter<KeySearchListAdapter.ListItemViewHolder> {

	private List<SearchListItem> searchListItems;
	private Context context;

	public KeySearchListAdapter(List<SearchListItem> trItems) {

		if (null == trItems) {
			throw new IllegalArgumentException("search list  items not be null");
		}
		this.searchListItems = trItems;

	}

	@Override
	public int getItemCount() {
		// TODO Auto-generated method stub
		return searchListItems.size();
	}

	@Override
	public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {

		SearchListItem items = searchListItems.get(position);
		viewHolder.search_title.setText(String.valueOf(items.search_title));
		viewHolder.search_tid.setText(String.valueOf(items.search_tid));
		viewHolder.search_lati.setText(String.valueOf(items.search_lati));
		viewHolder.search_longi.setText(String.valueOf(items.search_longi));
		viewHolder.search_roletype.setText(String
				.valueOf(items.search_roletype));
		viewHolder.search_openstatus.setText(String
				.valueOf(items.search_openstatus));
		viewHolder.search_distance.setText(String
				.valueOf(items.search_distance) + " km");
		viewHolder.search_ezeid.setText(String
				.valueOf(items.search_EZEID));
		viewHolder.search_pin.setText(String
				.valueOf(items.search_pin));
		viewHolder.search_seqno.setText(String
				.valueOf(items.search_seqno));

//		if (null != items.search_openstatus) {
//			if (!viewHolder.search_openstatus.getText().toString()
//					.equalsIgnoreCase("")) {
//				if (viewHolder.search_openstatus.getText().toString()
//						.equalsIgnoreCase("1")) {
//					viewHolder.search_comp_img
//							.setImageResource(R.drawable.ic_action_open);
//				}
//				if (viewHolder.search_openstatus.getText().toString()
//						.equalsIgnoreCase("2")) {
//					viewHolder.search_comp_img
//							.setImageResource(R.drawable.ic_action_close);
//				}
//			}
//
//		} else {
//			viewHolder.search_comp_img
//					.setImageResource(R.drawable.ic_action_close);
//		}

//		setAnimation(context, viewHolder.search_comp_root, position);
	}

	@Override
	public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int arg1) {

		View itemView;
		itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
				R.layout.row_key_search_result_list, viewGroup, false);
		this.context = viewGroup.getContext();
		return new ListItemViewHolder(itemView);
	}

	public final static class ListItemViewHolder extends
			RecyclerView.ViewHolder implements View.OnClickListener {

		private TextView search_title, search_tid, search_lati, search_longi,
				search_roletype, search_openstatus, search_distance,search_ezeid,search_seqno,search_pin;

		private RelativeLayout search_comp_root;
//		private ImageView search_comp_img;
		
//		 private int originalHeight = 0;
//		    private boolean isViewExpanded = false;

		public ListItemViewHolder(View itemView) {
			super(itemView);
			search_comp_root = (RelativeLayout) itemView
					.findViewById(R.id.search_comp_root);
			search_title = (TextView) itemView
					.findViewById(R.id.search_comp_name);
			search_tid = (TextView) itemView.findViewById(R.id.search_comp_tid);
			search_lati = (TextView) itemView
					.findViewById(R.id.search_comp_lati);
			search_longi = (TextView) itemView
					.findViewById(R.id.search_comp_longi);
			search_roletype = (TextView) itemView
					.findViewById(R.id.search_comp_roletype);
			search_openstatus = (TextView) itemView
					.findViewById(R.id.search_comp_openstatus);
//			search_comp_img = (ImageView) itemView
//					.findViewById(R.id.search_comp_img);
			search_distance = (TextView) itemView
					.findViewById(R.id.search_comp_dis);
			search_ezeid = (TextView) itemView
					.findViewById(R.id.search_comp_ezeid);
			search_pin = (TextView) itemView
					.findViewById(R.id.search_comp_pin);
			search_seqno = (TextView) itemView
					.findViewById(R.id.search_comp_seqno);
			
//			itemView.setOnClickListener(this);
//			 if (isViewExpanded == false) {
//		         // Set Views to View.GONE and .setEnabled(false)
//		         yourCustomView.setVisibility(View.GONE);
//		         yourCustomView.setEnabled(false);
//		     }
		}

		@Override
		public void onClick(View view) {
			Toast.makeText(view.getContext(), "position = " + getPosition(),
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Here is the key method to apply the animation
	 */
//	private void setAnimation(Context context, View viewToAnimate, int position) {
//
//		Animation animation = AnimationUtils.loadAnimation(context,
//				R.anim.card_animation);
//		viewToAnimate.startAnimation(animation);
//
//	}

	// method to access in activity after updating selection
	public List<SearchListItem> getItemsList() {
		return searchListItems;
	}
	
}
