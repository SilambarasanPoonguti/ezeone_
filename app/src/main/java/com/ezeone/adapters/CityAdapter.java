package com.ezeone.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.pojo.City;

import java.util.List;

public class CityAdapter extends ArrayAdapter<City>{

	private Context context;
	private int resource;
	private List<City> city;
	
	
	public CityAdapter(Context context, int resource,
			List<City> city) {
		super(context, resource, city);
		this.context = context;
		this.resource = resource;
		this.city = city;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = null;
		
		City city = getItem(position);
		if(convertView==null){
			convertView = mInflater.inflate(resource, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.categoryTitle);
			holder.id = (TextView) convertView.findViewById(R.id.categoryId);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.title.setText(city.getCityName());
		holder.id.setText(city.getCityID());
		
		return convertView;
		
	}
	
	private class ViewHolder{
		TextView title;
		TextView id;
	}
}
