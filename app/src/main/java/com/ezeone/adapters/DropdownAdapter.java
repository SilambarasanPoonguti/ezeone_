package com.ezeone.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ezeone.R;


public class DropdownAdapter extends ArrayAdapter<String> {

	private Context context;
	private int resource;
	private String[] titles;

	public DropdownAdapter(Context context, int resource, String[] titles) {
		super(context, resource, titles);
		this.context = context;
		this.resource = resource;
		this.titles = titles;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt);
	}

	@Override
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt);
	}

	public View getCustomView(int position, View convertView,
			ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View mySpinner = inflater.inflate(R.layout.category_list_item,
				parent, false);
		TextView masterTag = (TextView) mySpinner
				.findViewById(R.id.categoryTitle);
		masterTag.setText(titles[position]);
		return mySpinner;
	}

}
