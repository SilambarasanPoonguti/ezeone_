package com.ezeone.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.pojo.Title;

import java.util.List;

public class TitleSpinnerAdapter extends ArrayAdapter<Title> {

	private Context context;
	private int resource;
	private List<Title> titles;
	Title titleObj;

	public TitleSpinnerAdapter(Context context, int resource, List<Title> titles) {
		super(context, resource, titles);
		this.context = context;
		this.resource = resource;
		this.titles = titles;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		titleObj = titles.get(position);

		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(resource, null);

			holder = new ViewHolder();

			holder.title = (TextView) convertView.findViewById(R.id.titleText);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.title.setText(titleObj.getTitle());

		return convertView;
	}

	private class ViewHolder {
		TextView title;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(
					R.layout.row_spinner_list_item, null);
			
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.titleText);
			convertView.setTag(holder);
		}else {
			holder = (ViewHolder) convertView.getTag();
		}
		titleObj = titles.get(position);
		holder.title.setText(titleObj.getTitle());
		holder.title.setTextColor(Color.WHITE);

		/*TextView label = new TextView(context);
		label.setTextColor(Color.WHITE);
		label.setText(titleObj.getTitle());*/
		return convertView;
	}

}
