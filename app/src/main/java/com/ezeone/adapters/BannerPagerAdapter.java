package com.ezeone.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ezeone.R;

import java.util.ArrayList;

public class BannerPagerAdapter extends PagerAdapter {
	Context mContext;
	LayoutInflater mLayoutInflater;
	ArrayList<Bitmap> banners;

	public BannerPagerAdapter(Context context,ArrayList<Bitmap> banners) {
		mContext = context;
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.banners = banners;
	}

	@Override
	public int getCount() {
		return banners.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((LinearLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View itemView = mLayoutInflater.inflate(R.layout.pager_item, container,
				false);
		ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
		imageView.setImageBitmap(banners.get(position));
		container.addView(itemView);
		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}

}
