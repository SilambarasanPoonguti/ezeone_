package com.ezeone.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.pojo.MessagesNavItem;

import java.util.ArrayList;

public class MessageListNavAdapter extends BaseAdapter {

	private ImageView imgIcon;
	private TextView txtTitle;
	private ArrayList<MessagesNavItem> messagesNavItem;
	private Context context;

	public MessageListNavAdapter(Context context,
			ArrayList<MessagesNavItem> messagesNavItem) {
		this.messagesNavItem = messagesNavItem;
		this.context = context;
	}

	@Override
	public int getCount() {
		return messagesNavItem.size();
	}

	@Override
	public Object getItem(int index) {
		return messagesNavItem.get(index);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(
					R.layout.message_list_item_title_navigation, null);
		}

		imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
		txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);

		imgIcon.setImageResource(messagesNavItem.get(position).getIcon());
		imgIcon.setVisibility(View.GONE);
		txtTitle.setText(messagesNavItem.get(position).getTitle());
		return convertView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(
					R.layout.message_list_item_title_navigation, null);
		}

		imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);
		txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);

		imgIcon.setImageResource(messagesNavItem.get(position).getIcon());
		txtTitle.setText(messagesNavItem.get(position).getTitle());
		return convertView;
	}

}
