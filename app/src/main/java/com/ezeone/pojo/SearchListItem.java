package com.ezeone.pojo;

public class SearchListItem {

    public String search_title;
    public String search_tid;
    public String search_lati;
    public String search_longi;
    public String search_roletype;
    public String search_openstatus;
    public String search_distance;
    public String search_EZEID;
    public String search_seqno;
    public String search_pin;

    public String getSearch_seqno() {
        return search_seqno;
    }

    public void setSearch_seqno(String search_seqno) {
        this.search_seqno = search_seqno;
    }

    public String getSearch_pin() {
        return search_pin;
    }

    public void setSearch_pin(String search_pin) {
        this.search_pin = search_pin;
    }


    public String getSearch_EZEID() {
        return search_EZEID;
    }

    public void setSearch_EZEID(String search_EZEID) {
        this.search_EZEID = search_EZEID;
    }

    public String getSearch_title() {
        return search_title;
    }

    public void setSearch_title(String search_title) {
        this.search_title = search_title;
    }

    public String getSearch_tid() {
        return search_tid;
    }

    public void setSearch_tid(String search_tid) {
        this.search_tid = search_tid;
    }

    public String getSearch_lati() {
        return search_lati;
    }

    public void setSearch_lati(String search_lati) {
        this.search_lati = search_lati;
    }

    public String getSearch_longi() {
        return search_longi;
    }

    public void setSearch_longi(String search_longi) {
        this.search_longi = search_longi;
    }

    public String getSearch_roletype() {
        return search_roletype;
    }

    public void setSearch_roletype(String search_roletype) {
        this.search_roletype = search_roletype;
    }

    public String getSearch_openstatus() {
        return search_openstatus;
    }

    public void setSearch_openstatus(String search_openstatus) {
        this.search_openstatus = search_openstatus;
    }

    public String getSearch_distance() {
        return search_distance;
    }

    public void setSearch_distance(String search_distance) {
        this.search_distance = search_distance;
    }
}
