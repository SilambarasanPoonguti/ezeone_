package com.ezeone.pojo;

public class City {
	private String CityID;
	private String StateID;
	private String CityName;
	public String getCityID() {
		return CityID;
	}
	public void setCityID(String cityID) {
		CityID = cityID;
	}
	public String getStateID() {
		return StateID;
	}
	public void setStateID(String stateID) {
		StateID = stateID;
	}
	public String getCityName() {
		return CityName;
	}
	public void setCityName(String cityName) {
		CityName = cityName;
	}
}
