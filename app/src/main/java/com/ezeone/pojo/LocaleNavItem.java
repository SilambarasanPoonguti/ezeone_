package com.ezeone.pojo;

public class LocaleNavItem {

	private String title;
	private int icon;

	public LocaleNavItem(String title, int icon) {
		this.title = title;
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public int getIcon() {
		return icon;
	}

}
