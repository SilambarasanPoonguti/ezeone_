package com.ezeone.pojo;

public class MessagesNavItem {
	private String title;
	private int icon;
	
	public MessagesNavItem(String title, int icon){
		this.title = title;
		this.icon = icon;
	}
	
	public String getTitle(){
		return this.title;		
	}
	
	public int getIcon(){
		return this.icon;
	}
}
