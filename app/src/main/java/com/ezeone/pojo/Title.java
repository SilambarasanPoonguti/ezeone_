package com.ezeone.pojo;

public class Title{
	
	private String TitleID;
	private String Title;
	public String getTitleID() {
		return TitleID;
	}
	public void setTitleID(String titleID) {
		TitleID = titleID;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	
}
