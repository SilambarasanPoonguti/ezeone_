package com.ezeone.pojo;

public class ProfilePickerModel {

	public int icon;
	public String title;

	public ProfilePickerModel() {
		super();
	}

	public ProfilePickerModel(int icon, String title) {
		super();
		this.icon = icon;
		this.title = title;
	}
}
