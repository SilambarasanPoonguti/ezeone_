package com.ezeone.pojo;

public class OtherLocation {
	
	private String AddressLine1;
	private String AddressLine2;
	private String CityTitle;
	private int CountryID;
	private String EMailID;
	private String LaptopSLNO;
	private String Latitude;
	private String LocTitle;
	private String Longitude;
	private String MobileNumber;
	private int PIN;
	private String PhoneNumber;
	private String Picture;
	private String PostalCode;
	private int StateID;
	private String VehicleNumber;
	private String Website;
	private String PictureFileName;
	private String TID;
	private String WorkingHours;
	private String SalesEnquiryMailID;
	private String HomeDeliveryMailID;
	private String ReservationMailID;
	private String SupportMailID;
	private String CVMailID;
	private int ParkingStatus;
	private int OpenStatus;
	private String Icon;
	private String ISDMobileNumber;
	private String ISDPhoneNumber;
	private int SalesEnquiryButton;
	private int HomeDeliveryButton;
	private int ReservationButton;
	private int SupportButton;
	private int CVButton;
	
	public String getTID() {
		return TID;
	}
	public void setTID(String tID) {
		TID = tID;
	}
	
	public String getLocTitle() {
		return LocTitle;
	}
	public void setLocTitle(String locTitle) {
		LocTitle = locTitle;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	
	public String getAddressLine1() {
		return AddressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		AddressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return AddressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		AddressLine2 = addressLine2;
	}
	public int getStateID() {
		return StateID;
	}
	public void setStateID(int stateID) {
		StateID = stateID;
	}
	public int getCountryID() {
		return CountryID;
	}
	public void setCountryID(int countryID) {
		CountryID = countryID;
	}
	public String getPostalCode() {
		return PostalCode;
	}
	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}
	public int getPIN() {
		return PIN;
	}
	public void setPIN(int pIN) {
		PIN = pIN;
	}
	public String getEMailID() {
		return EMailID;
	}
	public void setEMailID(String eMailID) {
		EMailID = eMailID;
	}
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	public String getMobileNumber() {
		return MobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}
	public String getLaptopSLNO() {
		return LaptopSLNO;
	}
	public void setLaptopSLNO(String laptopSLNO) {
		LaptopSLNO = laptopSLNO;
	}
	public String getVehicleNumber() {
		return VehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		VehicleNumber = vehicleNumber;
	}
	public String getWebsite() {
		return Website;
	}
	public void setWebsite(String website) {
		Website = website;
	}
	
	public String getPictureFileName() {
		return PictureFileName;
	}
	public void setPictureFileName(String pictureFileName) {
		PictureFileName = pictureFileName;
	}
	
	public String getPicture() {
		return Picture;
	}
	public void setPicture(String picture) {
		Picture = picture;
	}
	public String getCityTitle() {
		return CityTitle;
	}
	public void setCityTitle(String cityTitle) {
		CityTitle = cityTitle;
	}
	public String getWorkingHours() {
		return WorkingHours;
	}
	public void setWorkingHours(String workingHours) {
		WorkingHours = workingHours;
	}
	public String getSalesEnquiryMailID() {
		return SalesEnquiryMailID;
	}
	public void setSalesEnquiryMailID(String salesEnquiryMailID) {
		SalesEnquiryMailID = salesEnquiryMailID;
	}
	public String getHomeDeliveryMailID() {
		return HomeDeliveryMailID;
	}
	public void setHomeDeliveryMailID(String homeDeliveryMailID) {
		HomeDeliveryMailID = homeDeliveryMailID;
	}
	public String getReservationMailID() {
		return ReservationMailID;
	}
	public void setReservationMailID(String reservationMailID) {
		ReservationMailID = reservationMailID;
	}
	public String getSupportMailID() {
		return SupportMailID;
	}
	public void setSupportMailID(String supportMailID) {
		SupportMailID = supportMailID;
	}
	public String getCVMailID() {
		return CVMailID;
	}
	public void setCVMailID(String cVMailID) {
		CVMailID = cVMailID;
	}
	public int getParkingStatus() {
		return ParkingStatus;
	}
	public void setParkingStatus(int parkingStatus) {
		ParkingStatus = parkingStatus;
	}
	public int getOpenStatus() {
		return OpenStatus;
	}
	public void setOpenStatus(int openStatus) {
		OpenStatus = openStatus;
	}
	public String getIcon() {
		return Icon;
	}
	public void setIcon(String icon) {
		Icon = icon;
	}
	public String getISDMobileNumber() {
		return ISDMobileNumber;
	}
	public void setISDMobileNumber(String iSDMobileNumber) {
		ISDMobileNumber = iSDMobileNumber;
	}
	public String getISDPhoneNumber() {
		return ISDPhoneNumber;
	}
	public void setISDPhoneNumber(String iSDPhoneNumber) {
		ISDPhoneNumber = iSDPhoneNumber;
	}
	public int getSalesEnquiryButton() {
		return SalesEnquiryButton;
	}
	public void setSalesEnquiryButton(int salesEnquiryButton) {
		SalesEnquiryButton = salesEnquiryButton;
	}
	public int getHomeDeliveryButton() {
		return HomeDeliveryButton;
	}
	public void setHomeDeliveryButton(int homeDeliveryButton) {
		HomeDeliveryButton = homeDeliveryButton;
	}
	public int getReservationButton() {
		return ReservationButton;
	}
	public void setReservationButton(int reservationButton) {
		ReservationButton = reservationButton;
	}
	public int getSupportButton() {
		return SupportButton;
	}
	public void setSupportButton(int supportButton) {
		SupportButton = supportButton;
	}
	public int getCVButton() {
		return CVButton;
	}
	public void setCVButton(int cVButton) {
		CVButton = cVButton;
	}
}
