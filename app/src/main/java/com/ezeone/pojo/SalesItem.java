package com.ezeone.pojo;

public class SalesItem {

	String tid = null;
	String functionType = null;
	String itemName = null;
	String description = null;
	String pic = null;
	String rate = null;
	String status = null;
	boolean selected = false;

	public SalesItem(String tid, String functionType, String itemName,
			String description, String pic, String rate, String status,
			boolean selected) {
		super();
		this.tid = tid;
		this.functionType = functionType;
		this.itemName = itemName;
		this.description = description;
		this.pic = pic;
		this.rate = rate;
		this.status = status;
		this.selected = selected;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getFunctionType() {
		return functionType;
	}

	public void setFunctionType(String functionType) {
		this.functionType = functionType;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
