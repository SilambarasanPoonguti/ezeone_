package com.ezeone;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.ezeone.adapters.EzeidFindByAdapter;
import com.ezeone.adapters.KeySearchListAdapter;
import com.ezeone.helpers.Constant;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.helpers.MapSearchResults;
import com.ezeone.listeners.MapSearchResultsListener;
import com.ezeone.listeners.RecyclerItemClickListener;
import com.ezeone.location.FusedLocationService;
import com.ezeone.location.GoogleMapUtis;
import com.ezeone.manager.BusinessManager;
import com.ezeone.manager.helpdesk.HelpDeskEnquiry;
import com.ezeone.manager.homedelivery.HDAddDeliveryItems;
import com.ezeone.manager.homedelivery.HDAddDeliveryMsgs;
import com.ezeone.manager.homedelivery.HDAddItems;
import com.ezeone.manager.humanresource.HREnquiry;
import com.ezeone.manager.reservations.AppointmentCalendarActivity;
import com.ezeone.manager.salesenquiry.EzeidAddItems;
import com.ezeone.manager.salesenquiry.EzeidAddSalesItems;
import com.ezeone.manager.salesenquiry.EzeidAddSalesMessages;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.networkhandler.VolleyErrorHelper;
import com.ezeone.pojo.SearchListItem;
import com.ezeone.signup.SignUp;
import com.ezeone.utils.DividerItemDecoration;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeone.utils.GoogleDirection;
import com.ezeonelib.dialog.MaterialDialog;
import com.ezeonelib.floatingaction.ButtonFloatSmall;
import com.ezeonelib.floatingaction.FloatingActionButton;
import com.ezeonelib.floatingaction.FloatingActionsMenu;
import com.ezeonelib.slidepanel.EZEOneSlideUpPanel;
import com.ezeonelib.utils.Utils;
import com.ezeonelib.widget.CircleImageView;
import com.ezeonelib.widget.cruton.Crouton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener;


public class LandingPage extends AppCompatActivity implements
        OnEditorActionListener, OnMarkerClickListener, EZEOneSlideUpPanel.PanelSlideListener,
        OnCameraChangeListener, MapSearchResultsListener,
        OnCheckedChangeListener, ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener, OnItemSelectedListener,
        AnimationListener {
    private static final String TAG = LandingPage.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    /**
     * These settings are the same as the settings for the map. They will in
     * fact give you updates at the maximal rates currently possible.
     */
    /**
     * The desired interval for location updates. Inexact. Updates may be more
     * or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    /**
     * The fastest rate for active location updates. Exact. Updates will never
     * be more frequent than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private static final LocationRequest REQUEST = LocationRequest.create()
            .setSmallestDisplacement(100)// 300 mtrs
            .setInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS) // 5 seconds
                    // .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS) //
                    // 16ms
            .setFastestInterval(16) // =
                    // 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    private boolean isMarkerAnimating = false;
    private boolean timer_flag = false;
    private Animation animRotate;
    // private final long startTime = 50000;
    private long startTime = TimeUnit.MINUTES.toMillis(60);
    private final long interval = 1000;
    private TrackLimit trackLimit;
    private Timer timer;
    private TimerTask timerTask;
    private LatLng camLocation;
    private Spinner proximity;
    private String proximityStr = "0";
    private LinearLayout contactBar;
    private TextView panelRedFlag, hideLabel, nav_count_text;
    private Location location = null;
    private GoogleMap googleMap;
    private SupportMapFragment mapFragment;
    private Marker myMarker = null, ezeidMarker = null, rMyMarker = null;
    private Polyline polyline = null;
    private FusedLocationService fusedLocationService;
    private EditText map_search;
    //    private TextInputLayout searchInputslayout;
    private EditText searchInputs;
    public static int findSelection = 0;
    private Button search, aSearchCancel, aSearchOk, aSearchReset;
    private boolean rate1Clicked = false, rate2Clicked = false,
            rate3Clicked = false, rate4Clicked = false, rate5Clicked = false,
            isRedFlag = false, onTrackingState = false,
            isLocationChanged = false, isSearchPalelExpand = false;
    private String dynamicLati = "", dynamicLongi = "";
    public static String searchType = "ezeid";
    private ArrayList<HashMap<String, String>> keywordList = null;
    private HashMap<String, HashMap<String, String>> searchExtraInfo = null;
    private EzeidPreferenceHelper ezeidPreferenceHelper;
    private Dialog overlayInfo;
    private List<Marker> markers;
    private View kewordSearchView;
    private Marker trackingMarker;
    private EzeidLoadingProgress ezeidLoadingProgress;
    private Handler handlerDialog;
    private Runnable runnableDialog;
    private RelativeLayout searchPanelLayout;
    private int PAGE_COUNT = 0, ISPAGINATION = 1, PAGE_SIZE = 10,
            SEARCH_TOTAL = 0, SEARCH_COUNT = 0;
    private String addressl1S = "", addressl2S = "", cityS = "", stateS = "",
            countryS = "", postalCodeS = "", emailS = "", mobileS = "",
            phoneS = "", websiteS = "", latitude = "", longitude = "",
            picture = "", address = "", openStatusS = "", roleTypeOfUser = "",
            verify = "", toMasterIdMsg = "", locationID = "",
            parseRate = "1,2,3,4,5", iSDPhoneNumber = "", iSDMobileNumber = "",
            visibleModules = "", parkingCheckStr = "0",
            hDeliveryCheckStr = "0", openStatusStr = "0", markerTitle = "",
            searchTypeinfo = "", business_navigation = "";
    private boolean setValidSearch = false, upPanelLayoutStatus = false,
            isPreLoading = false;
    private TextView panelTitle, panelContent, panelSubContent, panelMobileNo,
            panelPhone, panelEmailid, panelBetweenPM, resultText, website,
            ezeid;
    private Button activeButton = null;
    private ViewGroup buttonsContainer;

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private static JSONArray search_list_array = null;
    private LinearLayout emailBar, websiteBar, nav_layout;
    private ImageView panelPhDial, panelMobDial, panelChat, panelEmail,
            panelWebIcon, verifyStatusIcon, panelParkingImg, staticMarker,
            roleStatusImg, hideLblImg, panelWhrs;
    private CircleImageView panelImg;
    private ImageView rateIconOne, rateIconTwo, rateIconThree, rateIconFour,
            rateIconFive, rate_one, rate_two, rate_three, rate_four, rate_five,
            left_arrow, right_arrow;
    private FloatingActionsMenu businessMenu;
    private FloatingActionButton command_one, command_two, command_three,
            command_four, command_five;
    private ButtonFloatSmall locationFound, command_six, command_navigation;
    private Button advancedSearch;
    private RelativeLayout clearRoute, addressSearchLayout;
    private String keywords = "", token = "", tid = "", tlatitude = "",
            tlongitude = "", roleType = "", ezeidS = "", parkingStatusS = "",

    panelRate = "", currentDate = "", eZEIDVerifiedID = "",
            salesUrl = "", homeDeliveryUrl = "", reservationUrl = "",
            supportUrl = "", cvUrl = "";
    private String[] proxiTags = {"1 miles(1.6 km)", "2 miles(3.2 km)",
            "5 miles(8.0 km)", "10 miles(16 km)", "25 miles(1.6 km)",
            "50 miles(80 km)", "Any"};
    private String[] proxiIds = {"1", "2", "5", "10", "25", "50", "0"};
    private List<SearchListItem> searchkeyListItems;
    String[] workStatusTags = {"Any", "Open", "Closed"};
    String[] workStatusIds = {"0", "2", "5"};
    private ToggleButton switch_toggle_result;
    private boolean isCurLocCreated = false;
    private ProgressBar mapProgressBar, routeProgress;
    private LatLng EzeidRoute = null, currentLatalng = null;
    private ArrayList<Bitmap> bannerDrawables;
    private ArrayList<LatLng> routePoints;
    public static EZEOneSlideUpPanel upPanelLayout, searchPanel;
    public static final int CORNER_RADIUS = 4; // dips
    public static final int MARGIN = 2; // dips
    final int SENT_MAIL = 1;
    private SwitchCompat switchTrack;
    private CheckBox switchPark, switchDelivery, switchStatus;
    private Calendar calendar;
    private LatLng ORIGIN, DEST;
    private String rMarkerStatus = "", rMarkerRole = "", rMarkerTitle = "";
    private GoogleDirection gd;
    private Document mDoc;
//    public static Spinner findBySpinner;
    private EzeidFindByAdapter spinAdapter;
    private MediaPlayer mediaPlayer;
    private View panelViewChild, mainContentView;
    private RecyclerView searchFragmentListView;
    private LinearLayout mapContainerView;
    private Button ezeOneBtn, keywordSrchBtn,jobKeywordSrchBtn;


    private ProgressBar progressProfile;
    private View menuHeaderView;
    private TextView menuUsername, menuEmail;
    private CircleImageView profileImg;
    private android.support.design.widget.FloatingActionButton edit_profile;
    private MaterialDialog changPwdDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isGooglePlayServicesAvailable()) {
            finish();
        }

        setContentView(R.layout.activity_landing_page);
        // Initializing Toolbar and setting it as the actionbar
        mainContentView = findViewById(R.id.landing_page_content);
//        toolbar = (Toolbar) mainContentView.findViewById(R.id.toolbar_landing_page);
//        View toolbarview = findViewById(R.id.landing_page_content);
        toolbar = (Toolbar) findViewById(R.id.toolbar_landing_page);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        fusedLocationService = new FusedLocationService(LandingPage.this,
                LandingPage.this);
        mGoogleApiClient = new GoogleApiClient.Builder(LandingPage.this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        InitComponents(savedInstanceState);
    }

    private NavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem menuItem) {
            //Checking if the item is in checked state or not, if not make it in checked state
            if (menuItem.isChecked()) menuItem.setChecked(false);
            else menuItem.setChecked(true);

            //Closing drawer on item click
            drawerLayout.closeDrawers();


            //Check to see which item was being clicked and perform appropriate action
            switch (menuItem.getItemId()) {


                //Replacing the main content with ContentFragment Which is our Inbox View;
                case R.id.menu_business_mgr:
                    if (ezeidPreferenceHelper.GetValueFromSharedPrefs("USER_STATUS")
                            .equals("2")) {
                        BusinessDesk();
                    } else {
                        EzeidUtil.showToast(LandingPage.this,
                                "This feature will be available after verification");
                    }
                    return true;

                case R.id.menu_contact_us:
                    ContactDesk();
                    return true;

                case R.id.menu_help:
                    HelpDesk();
                    return true;

                case R.id.menu_change_pwd:
                    ChangePwdForgotPwd();
                    return true;

                case R.id.menu_sign_out:
                    QuitApp();
                    return true;

                default:
                    Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                    return true;
            }
        }

    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_landing_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void InitComponents(Bundle savedInstanceState) {

        fusedLocationService = new FusedLocationService(LandingPage.this,
                this);
        mGoogleApiClient = new GoogleApiClient.Builder(LandingPage.this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(onNavigationItemSelectedListener);

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        menuUsername = (TextView) navigationView.findViewById(R.id.profName);
        menuEmail = (TextView) navigationView.findViewById(R.id.profEmail);
        profileImg = (CircleImageView) navigationView.findViewById(R.id.profileImg);
        edit_profile = (android.support.design.widget.FloatingActionButton) navigationView.findViewById(R.id.edit_profile);
        edit_profile.setOnClickListener(mOnClickListener);

        progressProfile = (ProgressBar) navigationView.findViewById(
                R.id.progressProfile);
        progressProfile.setVisibility(View.INVISIBLE);

        BindComponents();
        GetUserInfo();

    }


    private void BindComponents() {

        ezeidPreferenceHelper = new EzeidPreferenceHelper(LandingPage.this);

        fusedLocationService = new FusedLocationService(LandingPage.this,
                LandingPage.this);

        kewordSearchView = mainContentView.findViewById(R.id.searchView_layout);
        panelViewChild = mainContentView.findViewById(R.id.panelView_layout);
        /**
         * Based on the user type have to change the visibility of the menu icon
         */
        String user = ezeidPreferenceHelper.GetValueFromSharedPrefs("Type");
        EzeidGlobal.SALES_TYPE = user;

        if (user.equalsIgnoreCase("1") || user.equalsIgnoreCase("3")) {

            navigationView.getMenu().findItem(R.id.menu_business_mgr).setVisible(false);

        } else {
            navigationView.getMenu().findItem(R.id.menu_business_mgr).setVisible(true);
        }


        keywordList = new ArrayList<>();
        markers = new ArrayList<>();

        bannerDrawables = new ArrayList<>();
        searchExtraInfo = new HashMap<>();
        upPanelLayout = (EZEOneSlideUpPanel) findViewById(
                R.id.sliding_layout);
        searchPanel = (EZEOneSlideUpPanel) findViewById(
                R.id.sliding_layoutchild);
        upPanelLayout.setPanelSlideListener(this);
        searchPanel.setPanelSlideListener(this);
        mapProgressBar = (ProgressBar) findViewById(
                R.id.mapProgressBar);
        routeProgress = (ProgressBar) findViewById(
                R.id.routeProgress);
        routeProgress.setVisibility(View.GONE);
        switchTrack = (SwitchCompat) kewordSearchView
                .findViewById(R.id.switchTrack);

        addSearchButtons();

//        ezeOneBtn= (Button) kewordSearchView
//                .findViewById(R.id.button1);
//        keywordSrchBtn= (Button) kewordSearchView
//                .findViewById(R.id.button2);
//        jobKeywordSrchBtn= (Button) kewordSearchView
//                .findViewById(R.id.button3);
//        ezeOneBtn.setOnClickListener(mOnClickListener);
//        keywordSrchBtn.setOnClickListener(mOnClickListener);
//        jobKeywordSrchBtn.setOnClickListener(mOnClickListener);
        switch_toggle_result = (ToggleButton) panelViewChild
                .findViewById(R.id.switch_toggle_result);
        switch_toggle_result.setVisibility(View.INVISIBLE);
        mapContainerView = (LinearLayout) findViewById(
                R.id.mapViewContainer);
        switch_toggle_result
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            mapContainerView.setVisibility(View.INVISIBLE);
                            staticMarker.setVisibility(View.INVISIBLE);
                            map_search.setVisibility(View.INVISIBLE);
                            BindSearchListItems(search_list_array);
                        } else {
                            mapContainerView.setVisibility(View.VISIBLE);
                            staticMarker.setVisibility(View.VISIBLE);
                            BindSearchListItems(search_list_array);
                            map_search.setVisibility(View.VISIBLE);
                        }
                    }
                });

        searchFragmentListView = (RecyclerView) findViewById(
                R.id.searchFragmentList);

        searchFragmentListView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                LandingPage.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        searchFragmentListView.setLayoutManager(layoutManager);
        staticMarker = (ImageView)
                findViewById(R.id.staticMarker);
        trackLimit = new TrackLimit(startTime, interval);

        if (switchTrack.isChecked() == true) {
            mGoogleApiClient.connect();
            onTrackingState = true;
            trackLimit.start();
            timer_flag = true;
        } else {
            mGoogleApiClient.disconnect();
            onTrackingState = false;
            trackLimit.cancel();
            timer_flag = false;
            ClearCaches();
        }

        clearRoute = (RelativeLayout) findViewById(
                R.id.clearRoute);
        addressSearchLayout = (RelativeLayout) findViewById(
                R.id.map_searchLayout);
        clearRoute.setVisibility(View.GONE);
        clearRoute.setOnClickListener(mOnClickListener);

        searchPanelLayout = (RelativeLayout) findViewById(
                R.id.searchPanel);

        contactBar = (LinearLayout) panelViewChild
                .findViewById(R.id.contactBar);
        panelRedFlag = (TextView) panelViewChild
                .findViewById(R.id.panelRedFlag);

        panelRedFlag.setVisibility(View.GONE);
        emailBar = (LinearLayout) panelViewChild.findViewById(R.id.emailBar);
        websiteBar = (LinearLayout) panelViewChild
                .findViewById(R.id.websiteBar);

//        findBySpinner = (Spinner) kewordSearchView
//                .findViewById(R.id.findBySpinner);
        search = (Button) kewordSearchView.findViewById(R.id.searchBtn);
        locationFound = (ButtonFloatSmall) panelViewChild
                .findViewById(R.id.location_found);
        businessMenu = (FloatingActionsMenu) panelViewChild
                .findViewById(R.id.business_actions);
        command_one = (FloatingActionButton) panelViewChild
                .findViewById(R.id.command_one);
        command_two = (FloatingActionButton) panelViewChild
                .findViewById(R.id.command_two);
        command_three = (FloatingActionButton) panelViewChild
                .findViewById(R.id.command_three);
        command_four = (FloatingActionButton) panelViewChild
                .findViewById(R.id.command_four);
        command_five = (FloatingActionButton) panelViewChild
                .findViewById(R.id.command_five);
        command_one.setVisibility(View.GONE);
        command_two.setVisibility(View.GONE);
        command_three.setVisibility(View.GONE);
        command_four.setVisibility(View.GONE);
        command_five.setVisibility(View.GONE);

        panelPhDial = (ImageView) panelViewChild.findViewById(R.id.panelPhDial);
        panelMobDial = (ImageView) panelViewChild
                .findViewById(R.id.panelMobDial);
        panelEmail = (ImageView) panelViewChild.findViewById(R.id.panelEmail);
        rate_one = (ImageView) panelViewChild.findViewById(R.id.rate_icon1);
        rate_two = (ImageView) panelViewChild.findViewById(R.id.rate_icon2);
        rate_three = (ImageView) panelViewChild.findViewById(R.id.rate_icon3);
        rate_four = (ImageView) panelViewChild.findViewById(R.id.rate_icon4);
        rate_five = (ImageView) panelViewChild.findViewById(R.id.rate_icon5);
        roleStatusImg = (ImageView) panelViewChild
                .findViewById(R.id.panelStatusImg);
        panelWhrs = (ImageView) panelViewChild.findViewById(R.id.panelWhrsImg);
        roleStatusImg.setOnClickListener(mOnClickListener);
        hideLblImg = (ImageView) findViewById(R.id.hideSearchImg);
        left_arrow = (ImageView) findViewById(R.id.left_arrow);
        right_arrow = (ImageView) findViewById(R.id.right_arrow);
        panelWebIcon = (ImageView) panelViewChild
                .findViewById(R.id.panelWebsiteIcon);
        verifyStatusIcon = (ImageView) panelViewChild
                .findViewById(R.id.verifyStatusIcon);
        panelChat = (ImageView) panelViewChild.findViewById(R.id.panelChat);
        panelImg = (CircleImageView) panelViewChild
                .findViewById(R.id.panelImage);
        panelParkingImg = (ImageView) panelViewChild
                .findViewById(R.id.panelParkingImg);
        hideLabel = (TextView) findViewById(R.id.hideLabel);
        hideLabel.setText("Search keywords");
//        searchInputslayout = (TextInputLayout) kewordSearchView
//                .findViewById(R.id.searchQueriesL);
        searchInputs = (AppCompatEditText) findViewById(R.id.searchQueries);
        advancedSearch = (Button) kewordSearchView
                .findViewById(R.id.advancedSearch);
        searchInputs.setHintTextColor(getResources().getColor(R.color.primary));
        map_search = (EditText) findViewById(R.id.map_search);

        panelTitle = (TextView) panelViewChild.findViewById(R.id.panelTitle);
        ezeid = (TextView) panelViewChild.findViewById(R.id.ezeid);
        nav_count_text = (TextView) findViewById(
                R.id.nav_count_text);
        nav_layout = (LinearLayout) findViewById(R.id.nav_layout);
        nav_layout.setVisibility(View.INVISIBLE);
        panelContent = (TextView) panelViewChild
                .findViewById(R.id.panelContent);
        panelSubContent = (TextView) panelViewChild
                .findViewById(R.id.panelSubContent);
        panelMobileNo = (TextView) panelViewChild
                .findViewById(R.id.panelMobileNumber);
        panelPhone = (TextView) panelViewChild
                .findViewById(R.id.panelPhoneNumber);
        panelEmailid = (TextView) panelViewChild
                .findViewById(R.id.panelEmailId);
        panelBetweenPM = (TextView) panelViewChild.findViewById(R.id.betweenPM);
        resultText = (TextView) panelViewChild.findViewById(R.id.resultText);
        website = (TextView) panelViewChild.findViewById(R.id.panelWebsite);

        command_six = (ButtonFloatSmall) panelViewChild
                .findViewById(R.id.command_six);
        command_navigation = (ButtonFloatSmall) panelViewChild
                .findViewById(R.id.command_navigation);

        command_six.setOnClickListener(mOnClickListener);
        command_navigation.setOnClickListener(mOnClickListener);
        advancedSearch.setOnClickListener(mOnClickListener);
        right_arrow.setOnClickListener(mOnClickListener);
        left_arrow.setOnClickListener(mOnClickListener);

        resultText.setText("");
//        searchInputs.setHint("Type EZEOne ID to view information");

        command_six.setVisibility(View.INVISIBLE);
        command_navigation.setVisibility(View.INVISIBLE);

        searchInputs.setOnEditorActionListener(onEditorActionListener);
//        searchInputslayout.getEditText().setOnEditorActionListener(onEditorActionListener);
        locationFound.setOnClickListener(mOnClickListener);

//        addItemsToSpinner();
        search.setOnClickListener(mOnClickListener);
        panelPhDial.setOnClickListener(mOnClickListener);
        panelMobDial.setOnClickListener(mOnClickListener);
        panelChat.setOnClickListener(mOnClickListener);
        panelEmail.setOnClickListener(mOnClickListener);
        panelWebIcon.setOnClickListener(mOnClickListener);
        panelPhone.setOnClickListener(mOnClickListener);
        panelMobileNo.setOnClickListener(mOnClickListener);
        panelWhrs.setOnClickListener(mOnClickListener);
        website.setOnClickListener(mOnClickListener);

        map_search.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_SEARCH)) {
                    if (!map_search.getText().toString().equalsIgnoreCase("")
                            && !latitude.equalsIgnoreCase("")
                            && !longitude.equalsIgnoreCase("")) {
                        GetSearch();
                    } else {
                        GetSearch();
                    }
                    return true;
                }
                return false;
            }
        });

        SetMapProgress();

        SetKeywordChecked();
        if (findSelection != 0) {
            // radioKey.setChecked(true);
            SetKeywordChecked();
        } else {
            // EzeidUtil.collapse(filterLayout);
            advancedSearch.setVisibility(View.GONE);
            // EzeidUtil.collapse(switchLayout);
            findSelection = 0;
            searchInputs.setHint("Type EZEOne ID to view information");

            searchType = "ezeid";
        }

        GetUserInfo();

    }

//    public void addItemsToSpinner() {
//
//        ArrayList<String> list = new ArrayList<String>();
//        list.add("EZEOne ID");
//        list.add("Keywords");
//        list.add("Job Keywords");
//        spinAdapter = new EzeidFindByAdapter(LandingPage.this, list);
//
//        findBySpinner.setAdapter(spinAdapter);
//        findBySpinner.setSelection(1);
//        findBySpinner.setOnItemSelectedListener(this);
//
//    }

    private void SetMapProgress() {
        mapProgressBar.getIndeterminateDrawable().setColorFilter(
                Color.rgb(0, 150, 136),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        routeProgress.getIndeterminateDrawable().setColorFilter(
                Color.rgb(0, 150, 136),
                android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    private void SetKeywordChecked() {
        advancedSearch.setVisibility(View.VISIBLE);
        findSelection = 1;
        searchInputs.setHint("Type Product/Service names to find Business(s)");
        searchType = "keyword";
    }


    private void GetAdvancedSearch() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(
                R.layout.ezeid_advanced_search_view, (ViewGroup)
                        findViewById(R.id.requesterRoot));
        final Dialog dialog = new Dialog(LandingPage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialoglayout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));

        switchPark = (CheckBox) dialog.findViewById(R.id.switchPark);
        switchDelivery = (CheckBox) dialog.findViewById(R.id.switchDelivery);
        switchStatus = (CheckBox) dialog.findViewById(R.id.switchStatus);
        proximity = (Spinner) dialog.findViewById(R.id.proximity);
        rateIconOne = (ImageView) dialog.findViewById(R.id.rateIconOne);
        rateIconTwo = (ImageView) dialog.findViewById(R.id.rateIconTwo);
        rateIconThree = (ImageView) dialog.findViewById(R.id.rateIconThree);
        rateIconFour = (ImageView) dialog.findViewById(R.id.rateIconFour);
        rateIconFive = (ImageView) dialog.findViewById(R.id.rateIconFive);
        aSearchCancel = (Button) dialog.findViewById(R.id.aSearchCancel);
        aSearchReset = (Button) dialog.findViewById(R.id.aSearchReset);
        aSearchOk = (Button) dialog.findViewById(R.id.aSearchOk);
        proximity.setOnItemSelectedListener(this);
        BindProximities();

        if (parkingCheckStr.equalsIgnoreCase("0")) {
            switchPark.setChecked(false);
        } else {
            switchPark.setChecked(true);
        }
        if (hDeliveryCheckStr.equalsIgnoreCase("0")) {
            switchDelivery.setChecked(false);
        } else {
            switchDelivery.setChecked(true);
        }

        if (openStatusStr.equalsIgnoreCase("0")) {
            switchStatus.setChecked(false);
        } else {
            switchStatus.setChecked(true);
        }

        if (parseRate.equalsIgnoreCase("0")) {
            rateIconOne
                    .setImageResource(R.drawable.ic_action_star_enable_large_);
            rate1Clicked = true;
            rateIconTwo
                    .setImageResource(R.drawable.ic_action_star_enable_large_);
            rate2Clicked = true;
            rateIconThree
                    .setImageResource(R.drawable.ic_action_star_enable_large_);
            rate3Clicked = true;
            rateIconFour
                    .setImageResource(R.drawable.ic_action_star_enable_large_);
            rate4Clicked = true;
            rateIconFive
                    .setImageResource(R.drawable.ic_action_star_enable_large_);
            rate5Clicked = true;
        } else {
            int len = parseRate.length();
            if (len > 0) {

                char[] rateArray = parseRate.toCharArray();
                for (char temp : rateArray) {

                    if (temp == '1') {
                        rate1Clicked = true;
                    } else if (temp == '2') {
                        rate2Clicked = true;
                    } else if (temp == '3') {
                        rate3Clicked = true;
                    } else if (temp == '4') {
                        rate4Clicked = true;
                    } else if (temp == '5') {
                        rate5Clicked = true;
                    }
                }

                if (rate1Clicked == true) {
                    rateIconOne
                            .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                } else {
                    rate1Clicked = false;
                    rateIconOne
                            .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                }

                if (rate2Clicked == true) {
                    rateIconTwo
                            .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                } else {
                    rate2Clicked = false;
                    rateIconTwo
                            .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                }

                if (rate3Clicked == true) {
                    rateIconThree
                            .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                } else {
                    rate3Clicked = false;
                    rateIconThree
                            .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                }

                if (rate4Clicked == true) {
                    rateIconFour
                            .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                } else {
                    rate4Clicked = false;
                    rateIconFour
                            .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                }

                if (rate5Clicked == true) {
                    rateIconFive
                            .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                } else {
                    rate5Clicked = false;
                    rateIconFive
                            .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                }
            }
        }

        switchTrack.setOnCheckedChangeListener(this);
        switchPark.setOnCheckedChangeListener(this);
        switchDelivery.setOnCheckedChangeListener(this);
        switchStatus.setOnCheckedChangeListener(this);
        rateIconOne.setOnClickListener(mOnClickListener);
        rateIconTwo.setOnClickListener(mOnClickListener);
        rateIconThree.setOnClickListener(mOnClickListener);
        rateIconFour.setOnClickListener(mOnClickListener);
        rateIconFive.setOnClickListener(mOnClickListener);
        aSearchReset.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                rateIconOne
                        .setImageResource(R.drawable.ic_action_star_enable_large_);
                rate1Clicked = true;
                rateIconTwo
                        .setImageResource(R.drawable.ic_action_star_enable_large_);
                rate2Clicked = true;
                rateIconThree
                        .setImageResource(R.drawable.ic_action_star_enable_large_);
                rate3Clicked = true;
                rateIconFour
                        .setImageResource(R.drawable.ic_action_star_enable_large_);
                rate4Clicked = true;
                rateIconFive
                        .setImageResource(R.drawable.ic_action_star_enable_large_);
                rate5Clicked = true;

                switchPark.setChecked(false);
                switchDelivery.setChecked(false);
                switchStatus.setChecked(false);
            }
        });
        aSearchCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        aSearchOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (searchType.equalsIgnoreCase("keyword")) {
                    GetEzeidSearchResult("keyword", "", "");
                } else if (searchType.equalsIgnoreCase("jobkeyword")) {
                    GetEzeidSearchResult("keyword", "", "");
                } else if (searchType.equalsIgnoreCase("ezeid")) {
                    GetEzeidSearchResult("ezeid", "", "");
                } else {
                    GetEzeidSearchResult("keyword", "", "");
                }
                upPanelLayout.collapsePanel();
            }
        });

        dialog.show();
    }

    private void SetJobKeywordChecked() {
        // filterLayout.setVisibility(View.VISIBLE);
        // // switchLayout.setVisibility(View.VISIBLE);
        // EzeidUtil.expand(filterLayout);
        advancedSearch.setVisibility(View.VISIBLE);
        // EzeidUtil.collapse(switchLayout);
        findSelection = 1;
        searchInputs.setHint("Type your Skills to locate nearest Employers");
        searchType = "jobkeyword";
    }


    @Override
    public void onStart() {
        super.onStart();

        ActivityStatus();
        // show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            LandingPage.this.finish();
        } else {
            location = fusedLocationService.getLocation();
        }

        if (onTrackingState == true) {
            mGoogleApiClient.connect();
            addressSearchLayout.setVisibility(View.GONE);
            onTrackingState = true;
            // staticMarker.setVisibility(View.GONE);
        } else {
            mGoogleApiClient.disconnect();
            addressSearchLayout.setVisibility(View.VISIBLE);
            onTrackingState = false;
            // staticMarker.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }

        ezeidPreferenceHelper.SaveValueToSharedPrefs("RATE", "");
        ezeidPreferenceHelper.SaveValueToSharedPrefs("RATE_COUNT", "");
        isPreLoading = false;
    }

    private void ActivityStatus() {

        try {
            // Loading map
            InitilizeMap();
            if (isPreLoading == false) {
                PreLoading();
            } else {
                ResumeMyLocation();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // if (getConfigStatus == false)
        // {
        // RequestTabTitles();
        // }

    }

    /**
     * Function to load map. If map is not created it will create it for you
     */
    private void InitilizeMap() {

        if (googleMap == null) {

            fusedLocationService = new FusedLocationService(LandingPage.this,
                    LandingPage.this);
            FragmentManager fm = getSupportFragmentManager();
            mapFragment = (SupportMapFragment) fm
                    .findFragmentById(R.id.ezeidHomeMap);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap map) {
                    if (map == null) {
                        EzeidUtil.showToast(LandingPage.this,
                                "Sorry! unable to create maps");
                    } else {
                        googleMap = map;
                        SetUpMap();
                    }
                }
            });

            // check if map is created successfully or not

        }

    }

    private void SetUpMap() {
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        // googleMap.setMyLocationEnabled(true);
        googleMap.setOnCameraChangeListener(this);
        googleMap.setOnMarkerClickListener(this);
    }

    private void PreLoading() {
        isPreLoading = true;
        locationFound.setVisibility(View.INVISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                GetMyLocation();
                // staticMarker.setVisibility(View.VISIBLE);
                locationFound.setVisibility(View.VISIBLE);
                if (isSearchPalelExpand == false) {
                    searchPanel.expandPanel();
                }
                mapProgressBar.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    private OnClickListener mOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            selectButton((Button) v);
            switch (v.getId()) {

                case R.id.command_six:
                    RouteDialog();
                    break;

                case R.id.command_navigation:
                    GetNavigation();
                    break;

                case R.id.location_found:
                    if (isCurLocCreated == true) {
                        isCurLocCreated = false;
                    }
                    GetMyLocation();
                    break;

                case R.id.searchBtn:
                    if (!isValidSearch()) {
//                        onSearchFailed();
//                        searchPanel.expandPanel();
//                        EzeidUtil.showToast(LandingPage.this, "Your search keywords shouldn't be empty!");
                        searchPanel.expandPanel();
                        searchInputs.requestFocus();
                    } else {
                        if (searchType.equalsIgnoreCase("keyword")) {
                            GetEzeidSearchResult("keyword", "", "");
                        } else if (searchType.equalsIgnoreCase("jobkeyword")) {
                            GetEzeidSearchResult("keyword", "", "");
                        } else if (searchType.equalsIgnoreCase("ezeid")) {
                            GetEzeidSearchResult("ezeid", "", "");
                        } else {
                            GetEzeidSearchResult("keyword", "", "");
                        }
                        searchPanel.collapsePanel();
                    }
                    break;

                case R.id.panelMobileNumber:
                    Call(mobileS);
                    break;

                case R.id.panelPhoneNumber:
                    Call(phoneS);
                    break;

                case R.id.panelPhDial:
                    Call(phoneS);
                    break;

                case R.id.panelWebsiteIcon:
                    Web();
                    break;

                case R.id.panelWebsite:
                    Web();
                    break;

                case R.id.panelMobDial:
                    Call(mobileS);
                    break;

                case R.id.panelChat:
                    Messaging();
                    break;

                case R.id.panelEmail:
                    sendEmail("Product Enquiry", "I would like to...");
                    break;

                case R.id.clearRoute:
                    ClearRoute();
                    break;

                // case R.id.dirDesc:
                // routeDescription.setVisibility(View.GONE);
                // break;

                case R.id.rateIconOne:
                    if (rate1Clicked) {
                        rateIconOne
                                .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                        rate1Clicked = false;
                    } else {
                        rateIconOne
                                .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                        rate1Clicked = true;
                    }

                    break;

                case R.id.rateIconTwo:
                    if (rate2Clicked) {
                        rateIconTwo
                                .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                        rate2Clicked = false;
                    } else {
                        rateIconTwo
                                .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                        rate2Clicked = true;
                    }

                    break;

                case R.id.rateIconThree:
                    if (rate3Clicked) {
                        rateIconThree
                                .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                        rate3Clicked = false;
                    } else {
                        rateIconThree
                                .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                        rate3Clicked = true;
                    }

                    break;

                case R.id.rateIconFour:
                    if (rate4Clicked) {
                        rateIconFour
                                .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                        rate4Clicked = false;
                    } else {
                        rateIconFour
                                .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                        rate4Clicked = true;
                    }

                    break;

                case R.id.rateIconFive:
                    if (rate5Clicked) {
                        rateIconFive
                                .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha_green);
                        rate5Clicked = false;
                    } else {

                        rateIconFive
                                .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha_green);
                        rate5Clicked = true;
                    }

                    break;

                case R.id.advancedSearch:
                    searchPanel.collapsePanel();
                    GetAdvancedSearch();
                    break;

                case R.id.left_arrow:

                    if (SEARCH_TOTAL > PAGE_COUNT) {
                        int count = 0, n = SEARCH_TOTAL - PAGE_COUNT;
                        if (PAGE_COUNT != 0) {

                            if (0 < n) {
                                count = PAGE_COUNT - 10;
                                PAGE_COUNT = count;
                                GetEzeidSearchResult("keyword", "", "");
                            }
                        } else {
                            PAGE_COUNT = 0;
                            GetEzeidSearchResult("keyword", "", "");
                        }
                    }

                    break;

                case R.id.right_arrow:

                    if (PAGE_COUNT < SEARCH_TOTAL) {
                        int count = 0, n = SEARCH_TOTAL - PAGE_COUNT;
                        if (n > 10) {
                            count = PAGE_COUNT + 10;
                            PAGE_COUNT = count;
                            GetEzeidSearchResult("keyword", "", "");
                        }

                    }

                    break;

                case R.id.panelWhrsImg:
                    getWorkingHours();
                    break;

                case R.id.edit_profile:
                    EditProfile();
                    break;

                case R.id.button1:
                    selectButton(ezeOneBtn);
                    break;
                case R.id.button2:
                    selectButton(keywordSrchBtn);
                    break;
                case R.id.button3:
                    selectButton(jobKeywordSrchBtn);
                    break;

            }

        }


    };


    private void addSearchButtons(){
        this.buttonsContainer = (ViewGroup) findViewById(R.id.buttonsContainer);

        int buttonsSpacing = (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
        int buttonSize = (int) getResources().getDimension(R.dimen.button_size);

        for (int i = 0; i < 3; i++) {
            Button button = (Button) getLayoutInflater().inflate(R.layout.circular_button_layout, buttonsContainer, false);
            button.setText("Test " + i);
            button.setOnClickListener(mOnClickListener);
            buttonsContainer.addView(button);

            //Add margin between buttons manually
            if (i != 3 - 1) {
                buttonsContainer.addView(new Space(this), new ViewGroup.LayoutParams(buttonsSpacing, buttonSize));
            }
        }
        selectButton((Button) buttonsContainer.getChildAt(0));
    }

    private void selectButton(Button button) {
        if (activeButton != null) {
            activeButton.setSelected(false);
            activeButton = null;
        }

        activeButton = button;
        button.setSelected(true);
        String item = button.getText().toString();

        if (item.equalsIgnoreCase(getResources().getString(R.string.findByKey))) {
                    SetKeywordChecked();
                } else if (item.equalsIgnoreCase(getResources().getString(R.string.findJobKey))) {
                    SetJobKeywordChecked();
                } else {
                    if (findSelection == 1) {
                        advancedSearch.setVisibility(View.GONE);
                        findSelection = 0;
                        searchInputs.setHint("Type EZEOne ID to view information");
                        searchType = "ezeid";
                    }
                }
    }


    public void onSearchFailed() {
//        Drawable error_drawable;
//        error_drawable = getResources().getDrawable(R.drawable.error_icon);
//        error_drawable.setBounds(0, 0, error_drawable.getIntrinsicWidth(), error_drawable.getIntrinsicHeight());
//        searchInputs.setCompoundDrawables(null, null, error_drawable, null);
//        searchInputs.setError(Html.fromHtml("<font color='#00e3cd'>Your search keywords shouldn't be empty</font>"));
//        searchInputslayout.setErrorEnabled(true);
//        searchInputs.setCompoundDrawables(null, null, null, null);
//        searchInputslayout.setErrorEnabled(false);
        EzeidUtil.showToast(LandingPage.this, "Your search keywords shouldn't be empty");
    }

    public boolean isValidSearch() {
        boolean valid = true;
        String keywords = searchInputs.getText().toString();

        if (keywords.isEmpty() && keywords.toString().length() < 1) {
            valid = false;
            onSearchFailed();
        } else {
            searchInputs.setCompoundDrawables(null, null, null, null);
//          searchInputslayout.setErrorEnabled(false);
        }

        return valid;
    }

    private TextView.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                if (!isValidSearch()) {
//                    onSearchFailed();
//                    searchPanel.expandPanel();
                    EzeidUtil.showToast(LandingPage.this, "Your search keywords shouldn't be empty!");
                    searchPanel.expandPanel();
                    searchInputs.requestFocus();
                } else {

                    if (searchType.equalsIgnoreCase("keyword")) {
                        GetEzeidSearchResult("keyword", "", "");
                    } else if (searchType.equalsIgnoreCase("jobkeyword")) {
                        GetEzeidSearchResult("keyword", "", "");
                    } else if (searchType.equalsIgnoreCase("ezeid")) {
                        GetEzeidSearchResult("ezeid", "", "");
                    } else {
                        GetEzeidSearchResult("keyword", "", "");
                    }
                    searchPanel.collapsePanel();
                }
            }
            return false;
        }

    };


    private void ValidateUserStatus() {

        if (!eZEIDVerifiedID.equalsIgnoreCase("")) {
            if (eZEIDVerifiedID.equalsIgnoreCase("1")) {
                EzeidUtil.showToast(LandingPage.this, "No Access");
            } else {
                GetConfigurations();
            }
        } else {
            EzeidUtil.showToast(LandingPage.this, "No Access");
        }
    }

    private void GetNavigation() {
        if (EzeidRoute != null) {
            upPanelLayout.collapsePanel();

            // Uri gmmIntentUri =
            // Uri.parse("google.navigation:q=Taronga+Zoo,+Sydney+Australia");
            // Uri gmmIntentUri = Uri
            // .parse("google.navigation:q=43,KR rd, Tata Silk Farm,jaya nagar,bengaluru");
            String url = "http://maps.google.com/maps?saddr="
                    + String.valueOf(currentLatalng.latitude) + ","
                    + String.valueOf(currentLatalng.longitude) + "&daddr="
                    + String.valueOf(EzeidRoute.latitude) + ","
                    + String.valueOf(EzeidRoute.longitude);
            Uri gmmIntentUri = Uri.parse(url);
            // Log.i(TAG, "Parsing navigation latlngs : " + url);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            // mapIntent.setPackage("com.google.android.apps.maps");
            mapIntent.setClassName("com.google.android.apps.maps",
                    "com.google.android.maps.MapsActivity");
            try {
                startActivity(mapIntent);

            } catch (ActivityNotFoundException e) {
                try {
                    Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url));
                    startActivity(unrestrictedIntent);
                } catch (ActivityNotFoundException innerEx) {
                    GetMapApp("Please install Google Maps application. Do you want to download now?");
                }
            }

        }
    }

    private void GetMapApp(String msg) {
        final MaterialDialog materialDialog = new MaterialDialog(LandingPage.this);
        materialDialog
                .setBackgroundResource(R.drawable.abc_cab_background_internal_bg);

        materialDialog
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setPositiveButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        try {
                            startActivity(new Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps")));
                        } catch (ActivityNotFoundException anfe) {
                            EzeidUtil.showToast(LandingPage.this,
                                    "ActivityNotFound");
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        materialDialog.dismiss();
                    }
                });
        materialDialog.setCanceledOnTouchOutside(true).show();
    }

    private void SetRateDrawable() {
        StringBuilder builder = new StringBuilder();
        // i = rateIconFive.
        if (rate1Clicked) {
            builder.append("1,");
        } else {
            builder.append("");
            rate1Clicked = false;
        }
        if (rate2Clicked) {
            if (rate1Clicked == true) {
                builder.append("2,");
            } else {
                builder.append("2,");
            }
        } else {
            builder.append("");
        }
        if (rate3Clicked) {
            if (rate2Clicked == true) {
                builder.append("3,");
            } else {
                builder.append("3,");
            }
        } else {
            builder.append("");
        }
        if (rate4Clicked) {
            if (rate3Clicked == true) {
                builder.append("4,");
            } else {
                builder.append("4,");
            }
        } else {
            builder.append("");
        }
        if (rate5Clicked == true) {
            builder.append("5");
        } else {
            builder.append("");
        }
        parseRate = builder.toString();
        if (parseRate.length() > 0
                && parseRate.charAt(parseRate.length() - 1) == ',') {
            parseRate = parseRate.substring(0, parseRate.length() - 1);
        }

    }

    private void GetSearch() {

        if (!map_search.getText().toString().equalsIgnoreCase("")) {
            mapProgressBar.setVisibility(View.VISIBLE);
            new MapSearchResults(getResources().getString(
                    R.string.map_textsearch_api), LandingPage.this, this,
                    map_search.getText().toString()).execute();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EzeidUtil.LOCATION_CODE) {
            fusedLocationService = new FusedLocationService(LandingPage.this,
                    LandingPage.this);
            GetMyLocation();
        }
        if (requestCode == SENT_MAIL && resultCode == Activity.RESULT_OK) {
            EzeidUtil.showToast(LandingPage.this, "Location found.");
        } else {
            EzeidUtil.showToast(LandingPage.this, "Try again.");
        }
    }

    private void GetMyLocation() {

        if (onTrackingState == false) {
            location = fusedLocationService.getLocation();
        }
        if (isSearchPalelExpand == false) {
            searchPanel.expandPanel();
        } else {
            searchPanel.collapsePanel();
        }
        location = fusedLocationService.getLocation();
        if (null != location) {
            // if (switchTrack.isChecked() == true)
            // {
            // staticMarker.setVisibility(View.GONE);
            // } else
            // {
            // staticMarker.setVisibility(View.VISIBLE);
            // }
            // Log.i(TAG, "location: " + location);
            map_search.setText("");
            double lati = location.getLatitude();
            double longi = location.getLongitude();
            ezeidPreferenceHelper.SaveValueToSharedPrefs("Latitude",
                    String.valueOf(lati));
            ezeidPreferenceHelper.SaveValueToSharedPrefs("Longitude",
                    String.valueOf(longi));
            latitude = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Latitude");
            longitude = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Longitude");

            currentLatalng = new LatLng(lati, longi);
            CameraUpdate center = CameraUpdateFactory.newLatLng(currentLatalng);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
            if (isCurLocCreated == false) {
                googleMap.moveCamera(center);
                googleMap.animateCamera(zoom);
                isCurLocCreated = true;
            }
            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);

        }
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(LandingPage.this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, LandingPage.this, 0)
                    .show();
            return false;
        }
    }

    @Override
    public void onStop() {
        isSearchPalelExpand = false;

        if (upPanelLayout != null && upPanelLayout.isPanelExpanded()
                || upPanelLayout.isPanelAnchored()) {
            upPanelLayout.collapsePanel();
        } else if (searchPanel != null && searchPanel.isPanelExpanded()
                || searchPanel.isPanelAnchored()) {
            searchPanel.collapsePanel();
        } else {
            ezeidPreferenceHelper.SaveValueToSharedPrefs("RATE", "");
            ezeidPreferenceHelper.SaveValueToSharedPrefs("RATE_COUNT", "");
        }
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        EzeidGlobal.getInstance().getRequestQueue().cancelAll(TAG);
    }

    private void BindProximities() {
        try {
            proximity.setAdapter(new ProximityAdapter(LandingPage.this,
                    R.layout.ezeid_tagid_row, proxiTags));
            proximity.setSelection(6);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ProximityAdapter extends ArrayAdapter<String> {
        public ProximityAdapter(Context ctx, int txtViewResourceId,
                                String[] objects) {
            super(ctx, txtViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
            return getCustomView(position, cnvtView, prnt);
        }

        @Override
        public View getView(int pos, View cnvtView, ViewGroup prnt) {
            return getCustomView(pos, cnvtView, prnt);
        }

        public View getCustomView(int position, View convertView,
                                  ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.ezeid_tagid_row, parent,
                    false);
            TextView masterTag = (TextView) mySpinner
                    .findViewById(R.id.masterTag);
            masterTag.setText(proxiTags[position]);
            TextView masterId = (TextView) mySpinner
                    .findViewById(R.id.masterId);
            masterId.setText(proxiIds[position]);
            return mySpinner;
        }
    }

    private void GetSelectedItem() {
        int j = proximity.getSelectedItemPosition();

        switch (j) {
            case 0:
                proximityStr = "1";
                break;
            case 1:
                proximityStr = "2";
                break;
            case 2:
                proximityStr = "5";
                break;
            case 3:
                proximityStr = "10";
                break;
            case 4:
                proximityStr = "25";
                break;
            case 5:
                proximityStr = "50";
                break;

            default:
                proximityStr = "0";
                break;
        }
    }

    @SuppressLint("SimpleDateFormat")
    public void GetEzeidSearchResult(String searchType, String searchFrom,
                                     String searchKeyword) {

        resultText.setVisibility(View.INVISIBLE);
        switch_toggle_result.setVisibility(View.INVISIBLE);

        ClearRoute();
        if (searchFrom.equalsIgnoreCase("Slide")) {
            keywords = searchKeyword;
        } else {

            if (!searchInputs.getText().toString()
                    .equalsIgnoreCase(keywords)) {
                keywordList.clear();
                markers.clear();
                searchExtraInfo.clear();
                googleMap.clear();
                PAGE_COUNT = 0;
            }

            keywords = searchInputs.getText().toString().trim();

            calendar = Calendar.getInstance();
            currentDate = (String) DateFormat.format("MM/dd/yyyy kk:mm:ss",
                    calendar.getTime());
            // keywords = keywords.replaceAll(" ", "");
            token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
            if (null != proximity) {
                GetSelectedItem();
            }
            SetRateDrawable();

            if (onTrackingState == true) {
                GetTrackSearchResponse();
            } else {
                if (searchType.equalsIgnoreCase("ezeid")) {
                    searchTypeinfo = "1";
                    if (!keywords.equalsIgnoreCase("")
                            && !token.equalsIgnoreCase("")) {
                        // eProgress.show();
                        ShowEzeidDialog("Searching...");
                        upPanelLayout.collapsePanel();
                        upPanelLayoutStatus = false;
                        latitude = ezeidPreferenceHelper
                                .GetValueFromSharedPrefs("Latitude");
                        longitude = ezeidPreferenceHelper
                                .GetValueFromSharedPrefs("Longitude");
                        StringRequest myReq = new StringRequest(
                                Method.POST, EzeidGlobal.EzeidUrl
                                + "ewSearchByKeywords",
                                createMyReqSuccessListener(),
                                createMyReqErrorListener()) {

                            protected Map<String, String> getParams()
                                    throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("SearchType", "1");
                                params.put("Keywords", keywords);
                                params.put("Token", token);
                                params.put("SCategory", "0");
                                params.put("Proximity", "0");
                                params.put("Latitude", latitude);
                                params.put("Longitude", longitude);
                                params.put("ParkingStatus", "0");
                                params.put("OpenStatus", "0");
                                params.put("HomeDelivery", "0");
                                params.put("CurrentDate", currentDate);
                                params.put("isPagination", "1");
                                params.put("pagesize", "0");
                                params.put("pagecount", "0");
                                // Log.i("TAG", "Params: " + params);
                                return params;
                            }

                            ;
                        };
                        // disable cache
                        myReq.setShouldCache(false);
                        myReq.setRetryPolicy(new DefaultRetryPolicy(
                                5 * 1000, 5, 1.0f));
                        EzeidGlobal.getInstance().addToRequestQueue(myReq,
                                TAG);
                    } else {
                        // eProgress.dismiss();
                        DismissEzeidDialog();
                    }
                } else if (searchType.equalsIgnoreCase("keyword")) {
                    // GetMyLocation();
                    // eProgress.show();
                    searchTypeinfo = "2";
                    ShowEzeidDialog("Searching...");
                    upPanelLayout.collapsePanel();
                    upPanelLayoutStatus = false;
                    latitude = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Latitude");
                    longitude = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Longitude");

                    if (null != switchPark) {

                        if (switchPark.isChecked() == true) {
                            parkingCheckStr = "1";
                        } else {
                            parkingCheckStr = "0";
                        }
                    }
                    if (null != switchDelivery) {
                        if (switchDelivery.isChecked() == true) {
                            hDeliveryCheckStr = "1";
                        } else {
                            hDeliveryCheckStr = "0";
                        }
                    }
                    if (null != switchStatus) {
                        if (switchStatus.isChecked() == true) {
                            openStatusStr = "1";
                        } else {
                            openStatusStr = "0";
                        }
                    }
                    if (parseRate.equalsIgnoreCase("")) {
                        parseRate = "1,2,3,4,5";
                    }

                    // Log.i("TAG", "parseRate: " + parseRate);

                    if (!keywords.equalsIgnoreCase("")
                            && !token.equalsIgnoreCase("")
                            && !latitude.equalsIgnoreCase("null")
                            && !longitude.equalsIgnoreCase("null")) {
                        searchTypeinfo = "2";
                        StringRequest myReq = new StringRequest(
                                Method.POST, EzeidGlobal.EzeidUrl
                                + "ewSearchByKeywords",
                                createMyReqSuccessListener(),
                                createMyReqErrorListener()) {

                            protected Map<String, String> getParams()
                                    throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("SearchType", "2");
                                params.put("Keywords", keywords);
                                params.put("Token", token);
                                params.put("SCategory", "0");
                                params.put("Proximity", proximityStr);
                                params.put("Latitude", latitude);
                                params.put("Longitude", longitude);
                                params.put("ParkingStatus", parkingCheckStr);
                                params.put("OpenStatus", openStatusStr);
                                params.put("Rating", parseRate);
                                params.put("HomeDelivery",
                                        hDeliveryCheckStr);
                                params.put("CurrentDate", currentDate);
                                params.put("isPagination",
                                        String.valueOf(ISPAGINATION));
                                params.put("pagesize",
                                        String.valueOf(PAGE_SIZE));
                                params.put("pagecount",
                                        String.valueOf(PAGE_COUNT));
                                // Log.i("TAG", "Params: " + params);
                                return params;
                            }

                            ;
                        };
                        // disable cache
                        myReq.setShouldCache(false);
                        myReq.setRetryPolicy(new DefaultRetryPolicy(
                                5 * 1000, 5, 1.0f));
                        EzeidGlobal.getInstance().addToRequestQueue(myReq,
                                TAG);

                    } else {
                        // eProgress.dismiss();
                        DismissEzeidDialog();
                    }
                } else if (searchType.equalsIgnoreCase("jobkeyword")) {
                    // GetMyLocation();
                    // eProgress.show();
                    searchTypeinfo = "3";
                    ShowEzeidDialog("Searching...");
                    upPanelLayout.collapsePanel();
                    upPanelLayoutStatus = false;
                    latitude = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Latitude");
                    longitude = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Longitude");

                    if (switchPark.isChecked() == true) {
                        parkingCheckStr = "1,2";
                    } else {
                        parkingCheckStr = "0";
                    }
                    if (parseRate.equalsIgnoreCase("")) {
                        parseRate = "1,2,3,4,5";
                    }

                    // Log.i("TAG", "parseRate: " + parseRate);

                    if (!keywords.equalsIgnoreCase("")
                            && !token.equalsIgnoreCase("")
                            && !latitude.equalsIgnoreCase("null")
                            && !longitude.equalsIgnoreCase("null")) {

                        StringRequest myReq = new StringRequest(
                                Method.POST, EzeidGlobal.EzeidUrl
                                + "ewSearchByKeywords",
                                createMyReqSuccessListener(),
                                createMyReqErrorListener()) {

                            protected Map<String, String> getParams()
                                    throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("SearchType", "3");
                                params.put("Keywords", keywords);
                                params.put("Token", token);
                                params.put("SCategory", "0");
                                params.put("Proximity", proximityStr);
                                params.put("Latitude", latitude);
                                params.put("Longitude", longitude);
                                params.put("ParkingStatus", parkingCheckStr);
                                params.put("OpenStatus", "0");
                                params.put("Rating", parseRate);
                                params.put("HomeDelivery", "0");
                                params.put("CurrentDate", currentDate);

                                params.put("isPagination",
                                        String.valueOf(ISPAGINATION));
                                params.put("pagesize",
                                        String.valueOf(PAGE_SIZE));
                                params.put("pagecount",
                                        String.valueOf(PAGE_COUNT));

                                // Log.i("TAG", "Params: " + params);
                                return params;
                            }

                            ;
                        };
                        // disable cache
                        myReq.setShouldCache(false);
                        myReq.setRetryPolicy(new DefaultRetryPolicy(
                                5 * 1000, 5, 1.0f));
                        EzeidGlobal.getInstance().addToRequestQueue(myReq,
                                TAG);
                    }
                } else {
                    // eProgress.dismiss();
                    DismissEzeidDialog();
                }

            }
        }

    }

    private Response.Listener<String> createMyReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // eProgress.dismiss();
                // pfsearch.setClickable(true);
                DismissEzeidDialog();
                if (searchType.equalsIgnoreCase("ezeid")) {
                    ParseEzeidResult(response);
                    EzeidLatLngBounds();
                } else {
                    ParseSearchResult(response);
                    EzeidLatLngBounds();
                }
            }
        };
    }

    private Response.ErrorListener createMyReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // eProgress.dismiss();
                DismissEzeidDialog();
                // pfsearch.setClickable(false);
                // Log.i("TAG", "ERROR:" + error.getMessage().toString());
                // nav_layout.setVisibility(View.INVISIBLE);
                if (error instanceof TimeoutError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Connection timeout. Please try again");
                } else if (error instanceof ServerError
                        || error instanceof AuthFailureError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Unable to connect server. Please try later");
                } else if (error instanceof NetworkError
                        || error instanceof NoConnectionError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Network is unreachable ");
                } else {
                    EzeidUtil.showToast(LandingPage.this, "No Results.");
                }

            }
        };
    }

    protected void ParseEzeidResult(String response) {
        // Log.i(TAG, "ParseEzeidResult() Response :" + response);
        try {

            if (!response.equalsIgnoreCase("null")) {
                nav_layout.setVisibility(View.INVISIBLE);
                resultText.setVisibility(View.INVISIBLE);
                switch_toggle_result.setVisibility(View.INVISIBLE);
                upPanelLayoutStatus = true;
                UpdateMyLocation();
                keywordList.clear();
                markers.clear();
                searchExtraInfo.clear();
                JSONObject ojb = new JSONObject(response);

                JSONArray jsonArray = ojb.getJSONArray("Result");
                String ezeid = "", pin = "", seqno = "";
                if (jsonArray.length() != 0) {
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    tid = jsonObject.getString("TID");
                    tlatitude = jsonObject.getString("Latitude");
                    tlongitude = jsonObject.getString("Longitude");
                    roleType = jsonObject.getString("IDTypeID");
                    ezeid = jsonObject.getString("EZEID");
                    pin = jsonObject.getString("PIN");
                    seqno = jsonObject.getString("SeqNo");
                    if (!searchType.equalsIgnoreCase("ezeid")) {
                        openStatusS = jsonObject.getString("OpenStatus");
                    }

                    if (roleType.equalsIgnoreCase("2")) {
                        markerTitle = jsonObject.getString("CompanyName");
                        if (markerTitle.equalsIgnoreCase("")) {
                            markerTitle = jsonObject.getString("Name");
                        }
                    } else if (roleType.equalsIgnoreCase("3")) {
                        markerTitle = jsonObject.getString("CompanyName");
                        if (markerTitle.equalsIgnoreCase("")) {
                            markerTitle = jsonObject.getString("Name");
                        }
                    } else {
                        markerTitle = jsonObject.getString("Name");
                        if (markerTitle.equalsIgnoreCase("")) {
                            markerTitle = jsonObject.getString("CompanyName");
                        }
                    }
                } else {
                    EzeidUtil.showToast(LandingPage.this,
                            "No Results. Try with different EZEOne ID");
                    googleMap.clear();
                    GetMyLocation();
                }

                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("TID", tid);
                hashMap.put("Latitude", tlatitude);
                hashMap.put("Longitude", tlongitude);
                hashMap.put("IDTypeID", roleType);
                hashMap.put("EZEID", ezeid);
                hashMap.put("PIN", pin);
                hashMap.put("SeqNo", seqno);
                hashMap.put("OpenStatus", openStatusS);
                hashMap.put("MarkerTitle", markerTitle);
                keywordList.add(hashMap);
                if (!tlatitude.equalsIgnoreCase("")
                        && !tlongitude.equalsIgnoreCase("")) {
                    AddSearchMarkers(Double.parseDouble(tlatitude),
                            Double.parseDouble(tlongitude), hashMap,
                            markerTitle, openStatusS, roleType);
                }
                EzeidRoute = new LatLng(Double.parseDouble(tlatitude),
                        Double.parseDouble(tlongitude));
                routePoints = new ArrayList<LatLng>();
                routePoints.add(EzeidRoute);
                routePoints.add(currentLatalng);
                GoogleMapUtis.fixZoomForLatLngs(googleMap, routePoints);
                GetSearchInformation(tid, searchTypeinfo, ezeid, pin, seqno);

            } else {
                upPanelLayout.collapsePanel();
                if (ezeidMarker != null) {
                    ezeidMarker.remove();
                }
                if (map_search.getText().toString().length() != 0) {
                    map_search.setText("");
                }
                EzeidUtil.showToast(LandingPage.this,
                        "No Results. Try with different EZEOne ID");
                setValidSearch = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void ParseSearchResult(String response) {
        try {
            if (!response.equalsIgnoreCase("null")) {
                upPanelLayoutStatus = false;
                resultText.setVisibility(View.VISIBLE);
                switch_toggle_result.setVisibility(View.VISIBLE);
                JSONObject ojb = new JSONObject(response);
                String count = ojb.getString("totalcount");
                if (!count.equalsIgnoreCase("0")) {
                    SEARCH_COUNT = Integer.parseInt(count) / 10;
                    String c = String.valueOf(SEARCH_COUNT) + 0;
                    SEARCH_COUNT = Integer.parseInt(c);
                    if (!count.equalsIgnoreCase("")) {
                        SEARCH_TOTAL = Integer.parseInt(count);
                        if (SEARCH_TOTAL > 0) {

                            if (SEARCH_TOTAL <= 10) {
                                nav_layout.setVisibility(View.GONE);

                            } else {
                                nav_layout.setVisibility(View.VISIBLE);
                                int from = PAGE_COUNT + ISPAGINATION;
                                int to;
                                int cou = SEARCH_TOTAL - PAGE_COUNT;
                                if (cou < 10) {
                                    to = SEARCH_TOTAL;
                                } else {
                                    to = PAGE_COUNT + PAGE_SIZE;
                                }
                                nav_count_text.setText(from + " ... " + to);
                            }
                            if (SEARCH_TOTAL == 1) {
                                resultText.setText(SEARCH_TOTAL + " Result");
                            } else {
                                resultText.setText(SEARCH_TOTAL + " Results");
                            }
                        } else {
                            nav_layout.setVisibility(View.GONE);
                            resultText.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        EzeidUtil
                                .showToast(LandingPage.this,
                                        "No Results. Try with different search preferences");
                        SEARCH_TOTAL = 0;
                        nav_layout.setVisibility(View.GONE);
                    }
                } else {
                    resultText.setVisibility(View.INVISIBLE);
                    EzeidUtil
                            .showToast(LandingPage.this,
                                    "No Results. Try with different search preferences");
                    SEARCH_TOTAL = 0;
                    nav_layout.setVisibility(View.GONE);
                }

                JSONArray jsonArray = ojb.getJSONArray("Result");
                search_list_array = jsonArray;
                String switch_ = switch_toggle_result.getText().toString();
                if (switch_.equalsIgnoreCase("MAP")) {
                    BindSearchListItems(search_list_array);
                }

                // int i = jsonArray.length();

                // BindSearchListItems(search_list_array);
                if (jsonArray.length() != 0) {
                    keywordList.clear();
                    markers.clear();
                    searchExtraInfo.clear();
                    UpdateMyLocation();
                    String ezeid = "", pin = "", seqno = "";
                    for (int k = 0; k < jsonArray.length(); k++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(k);
                        tid = jsonObject.getString("TID");
                        tlatitude = jsonObject.getString("Latitude");
                        tlongitude = jsonObject.getString("Longitude");
                        roleType = jsonObject.getString("IDTypeID");
                        ezeid = jsonObject.getString("EZEID");
                        pin = jsonObject.getString("PIN");
                        seqno = jsonObject.getString("SeqNo");
                        openStatusS = jsonObject.getString("OpenStatus");
                        if (roleType.equalsIgnoreCase("2")) {
                            markerTitle = jsonObject.getString("CompanyName");
                            if (markerTitle.equalsIgnoreCase("")) {
                                markerTitle = jsonObject.getString("Name");
                            }
                        } else if (roleType.equalsIgnoreCase("3")) {
                            markerTitle = jsonObject.getString("CompanyName");
                            if (markerTitle.equalsIgnoreCase("")) {
                                markerTitle = jsonObject.getString("Name");
                            }
                        } else {
                            markerTitle = jsonObject.getString("Name");
                            if (markerTitle.equalsIgnoreCase("")) {
                                markerTitle = jsonObject
                                        .getString("CompanyName");
                            }
                        }

                        EzeidRoute = new LatLng(Double.parseDouble(tlatitude),
                                Double.parseDouble(tlongitude));

                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("TID", tid);
                        hashMap.put("Latitude", tlatitude);
                        hashMap.put("Longitude", tlongitude);
                        hashMap.put("IDTypeID", roleType);
                        hashMap.put("EZEID", ezeid);
                        hashMap.put("PIN", pin);
                        hashMap.put("SeqNo", seqno);
                        hashMap.put("OpenStatus", openStatusS);
                        hashMap.put("MarkerTitle", markerTitle);
                        keywordList.add(hashMap);
                        if (!tlatitude.equalsIgnoreCase("")
                                && !tlongitude.equalsIgnoreCase("")) {
                            AddSearchMarkers(Double.parseDouble(tlatitude),
                                    Double.parseDouble(tlongitude), hashMap,
                                    markerTitle, openStatusS, roleType);
                        }
                    }
                } else {
                    googleMap.clear();
                    GetMyLocation();
                }
            } else {
                nav_layout.setVisibility(View.INVISIBLE);
                upPanelLayout.collapsePanel();
                if (map_search.getText().toString().length() != 0) {
                    map_search.setText("");
                }
                setValidSearch = false;
                if (ezeidMarker != null) {
                    ezeidMarker.remove();
                }
                googleMap.clear();
                GetMyLocation();
                EzeidUtil.showToast(LandingPage.this,
                        "No Results. Try with different search preferences");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddSearchMarkers(double lati, double longi,
                                  HashMap<String, String> hashMap, String markerTitle, String status,
                                  String roleType) {
        if (!tlatitude.equalsIgnoreCase("") && !tlongitude.equalsIgnoreCase("")) {
            Bitmap res = EzeidUtil.DynamicMarker(LandingPage.this, markerTitle,
                    status, roleType);
            LatLng newlatLng = new LatLng(Double.parseDouble(tlatitude),
                    Double.parseDouble(tlongitude));
            ezeidMarker = googleMap.addMarker(new MarkerOptions()
                    .position(newlatLng).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromBitmap(res)));
            if (onTrackingState == true) {
                DropPinEffect(ezeidMarker);
            }else{
                dropPinEffect(ezeidMarker);
            }
//            Utils.bounceMarker(googleMap, marker);
            markers.add(ezeidMarker);
            searchExtraInfo.put(ezeidMarker.getId(), hashMap);
        }
    }


    private void GetSearchInformation(String tid, String search_type,
                                      String tEzeid, String pin, String seqno) {

        String pins = "";
        if (pin.equalsIgnoreCase("")) {
            pins = "";
        } else if (pin.equalsIgnoreCase(null)) {
            pins = "";
        } else if (pin.equalsIgnoreCase("null")) {
            pins = "";
        } else if (pin.equalsIgnoreCase("0")) {
            pins = "";
        } else {
            pins = "." + pin;
        }
        if (seqno.equalsIgnoreCase("")) {
            seqno = "0";
        }
        ShowEzeidDialog("Loading...");
        upPanelLayout.collapsePanel();
        String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");

        calendar = Calendar.getInstance();
        currentDate = (String) DateFormat.format("MM/dd/yyyy kk:mm:ss",
                calendar.getTime());
        JsonArrayRequest arrayRequest = new JsonArrayRequest(
                EzeidGlobal.EzeidUrl + "ewtGetSearchInformationNew?Token="
                        + token + "&ezeTerm=" + tEzeid + ".l" + seqno + pins
                        + "&CurrentDate=" + URLEncoder.encode(currentDate),
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        DismissEzeidDialog();
                        if (searchType.equalsIgnoreCase("ezeid")) {
                            SetPanelInformation(response);
                        } else {
                            SetPanelSearchInformation(response);
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                DismissEzeidDialog();
                setValidSearch = false;
                if (error instanceof TimeoutError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Connection timeout. Please try again");
                } else if (error instanceof ServerError
                        || error instanceof AuthFailureError) {
                    EzeidUtil
                            .showToast(LandingPage.this,
                                    "Unable to connect server. Please try later");
                } else if (error instanceof NetworkError
                        || error instanceof NoConnectionError) {

                    error.printStackTrace();
                    EzeidUtil.showToast(LandingPage.this,
                            "Network is unreachable");

                } else {
                    EzeidUtil.showToast(LandingPage.this, "No Results");
                }

            }
        });
        arrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EzeidGlobal.getInstance().addToRequestQueue(arrayRequest, TAG);

    }

    private void SetPanelInformation(JSONArray response) {

        try {
            int length = response.length();
            if (length != 0) {
                upPanelLayoutStatus = true;
                upPanelLayout.expandPanel();
                resultText.setVisibility(View.INVISIBLE);
                switch_toggle_result.setVisibility(View.INVISIBLE);
                SwipeOverlay();
                setValidSearch = true;
                JSONObject jsonObject = response.getJSONObject(0);
                roleType = jsonObject.getString("IDTypeID");
                EzeidGlobal.EZEID_TYPE = roleType;
                if (roleType.equalsIgnoreCase("1")) {
                    panelTitle.setText(jsonObject.getString("FirstName") + " "
                            + jsonObject.getString("LastName"));
                } else {

                    if (jsonObject.getString("CompanyName")
                            .equalsIgnoreCase("")) {
                        panelTitle.setText(jsonObject.getString("FirstName")
                                + " " + jsonObject.getString("LastName"));
                    } else {
                        panelTitle.setText(jsonObject.getString("CompanyName"));
                    }
                }
                panelContent.setVisibility(View.VISIBLE);
                panelContent.setText(jsonObject.getString("JobTitle") + ", \n "
                        + jsonObject.getString("AboutCompany"));
                addressl1S = jsonObject.getString("AddressLine1");
                addressl2S = jsonObject.getString("AddressLine2");
                toMasterIdMsg = jsonObject.getString("TID");
                cityS = jsonObject.getString("CityTitle");
                stateS = jsonObject.getString("StateTitle");
                countryS = jsonObject.getString("CountryTitle");
                postalCodeS = jsonObject.getString("PostalCode");
                parkingStatusS = jsonObject.getString("ParkingStatus");
                openStatusS = jsonObject.getString("OpenStatus");
                // workHrsS = jsonObject.getString("WorkingHours");
                ezeidS = jsonObject.getString("EZEID");
                eZEIDVerifiedID = jsonObject.getString("EZEIDVerifiedID");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("BusinessEZEID",
                        ezeidS);
                locationID = jsonObject.getString("LocID");
                iSDPhoneNumber = jsonObject.getString("ISDPhoneNumber");
                iSDMobileNumber = jsonObject.getString("ISDMobileNumber");
                salesUrl = jsonObject.getString("SalesURL");
                homeDeliveryUrl = jsonObject.getString("HomeDeliveryURL");
                reservationUrl = jsonObject.getString("ReservationURL");
                supportUrl = jsonObject.getString("ServiceURL");
                cvUrl = jsonObject.getString("CVURL");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("LocID",
                        locationID);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("TID",
                        toMasterIdMsg);
                rMarkerTitle = panelTitle.getText().toString();
                rMarkerRole = roleType;
                rMarkerStatus = openStatusS;
                String visible_module = jsonObject.getString("VisibleModules");
                setBusinessMenu(eZEIDVerifiedID, visible_module, roleType);
                if (!ezeidS.equalsIgnoreCase("")) {
                    ezeid.setVisibility(View.VISIBLE);
                    ezeid.setText(ezeidS.substring(1));
                } else {
                    ezeid.setVisibility(View.GONE);
                }

                if (!addressl2S.equalsIgnoreCase("")) {
                    addressl2S = ", " + addressl2S;
                } else {
                    addressl2S = "";
                }

                if (!cityS.equalsIgnoreCase("")) {
                    cityS = ", " + cityS;
                } else {
                    cityS = "";
                }

                if (!stateS.equalsIgnoreCase("")) {
                    stateS = ", " + stateS;
                } else {
                    stateS = "";
                }

                if (!countryS.equalsIgnoreCase("")) {
                    countryS = ", " + countryS;
                } else {
                    countryS = "";
                }
                if (!postalCodeS.equalsIgnoreCase("")) {
                    postalCodeS = " - " + postalCodeS;
                } else {
                    postalCodeS = "";
                }

                address = addressl1S + addressl2S + cityS + stateS + countryS
                        + postalCodeS;
                panelSubContent.setText(address);
                if (panelContent.getText().toString().equalsIgnoreCase("")) {
                    panelContent.setVisibility(View.GONE);
                }
                if (!roleType.equalsIgnoreCase("1")) {
                    if (!parkingStatusS.equalsIgnoreCase("")) {
                        panelParkingImg.setVisibility(View.VISIBLE);
                        if (parkingStatusS.equalsIgnoreCase("1")) {
                            panelParkingImg
                                    .setImageResource(R.drawable.ic_action_parking);
                        } else if (parkingStatusS.equalsIgnoreCase("2")) {
                            panelParkingImg
                                    .setImageResource(R.drawable.ic_action_vparking);
                        } else {
                            panelParkingImg
                                    .setImageResource(R.drawable.ic_action_noparking);
                        }
                    } else {
                        panelParkingImg.setVisibility(View.GONE);
                    }
                    if (!openStatusS.equalsIgnoreCase("")) {
                        if (openStatusS.equalsIgnoreCase("1")) {
                            roleStatusImg
                                    .setImageResource(R.drawable.ic_action_open);
                        }
                        if (openStatusS.equalsIgnoreCase("2")) {
                            roleStatusImg
                                    .setImageResource(R.drawable.ic_action_close);
                        }
                    }
                }
                picture = "";
                mobileS = jsonObject.getString("MobileNumber");
                phoneS = jsonObject.getString("PhoneNumber");
                emailS = jsonObject.getString("EMailID");
                websiteS = jsonObject.getString("Website");
                picture = jsonObject.getString("Picture");
                verify = jsonObject.getString("EZEIDVerifiedID");


                String redflagstatus = jsonObject.getString("Redflagstatus");
                if (redflagstatus.equalsIgnoreCase("1")) {
                    panelRedFlag.setVisibility(View.VISIBLE);
                    isRedFlag = true;
                } else {
                    isRedFlag = false;
                    panelRedFlag.setVisibility(View.GONE);
                }

                if (!verify.equalsIgnoreCase("")) {
                    if (verify.equalsIgnoreCase("1")) {
                        verifyStatusIcon
                                .setImageResource(R.drawable.ic_action_not_verified);
                    } else {
                        verifyStatusIcon
                                .setImageResource(R.drawable.ic_action_verified);
                    }

                } else {
                    verifyStatusIcon.setVisibility(View.GONE);
                }

                if (!picture.equalsIgnoreCase("")) {
                    String pic = EzeidUtil.GetFileType(picture);
                    Bitmap panelImg = EzeidUtil.ByteTOBitmap(pic);
                    SetImagePanelImage(panelImg);
                } else {
                    SetImagePanelImage(null);
                }
                if (mobileS.equalsIgnoreCase("") && phoneS.equalsIgnoreCase("")
                        && emailS.equalsIgnoreCase("")) {
                    contactBar.setVisibility(View.GONE);
                }

                if (phoneS.equalsIgnoreCase("")) {
                    panelPhone.setVisibility(View.GONE);
                    panelPhDial.setVisibility(View.GONE);
                    panelBetweenPM.setVisibility(View.GONE);
                } else {
                    panelBetweenPM.setVisibility(View.VISIBLE);
                    panelPhone.setText("P: " + iSDPhoneNumber + " " + phoneS);
                }

                if (mobileS.equalsIgnoreCase("")) {
                    panelMobileNo.setVisibility(View.GONE);
                    panelMobDial.setVisibility(View.GONE);
                    panelChat.setVisibility(View.GONE);
                    panelBetweenPM.setVisibility(View.GONE);
                } else {
                    if (!phoneS.equalsIgnoreCase("")) {
                        panelBetweenPM.setVisibility(View.GONE);
                    }
                    panelMobileNo.setText("M: " + iSDMobileNumber + " "
                            + mobileS);

                }

                if (emailS.equalsIgnoreCase("")) {
                    emailBar.setVisibility(View.GONE);
                } else {
                    panelEmailid.setText("E: " + emailS);
                }

                if (websiteS.equalsIgnoreCase("")) {
                    websiteBar.setVisibility(View.GONE);
                } else {
                    website.setText(" " + websiteS);
                }

                panelRate = jsonObject.getString("Rating");
                SetRate(panelRate);

            } else {
                upPanelLayout.collapsePanel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setBusinessMenu(String eZEIDVerifiedID, String visible_module,
                                 String roleType) {

        roleTypeOfUser = roleType;
        // individual

        if (eZEIDVerifiedID.equalsIgnoreCase("2")) {
            businessMenu.setVisibility(View.VISIBLE);
            if (roleTypeOfUser.equalsIgnoreCase("1")) {
                command_one.setVisibility(View.VISIBLE);
                command_two.setVisibility(View.VISIBLE);
                command_three.setVisibility(View.GONE);
                command_four.setVisibility(View.GONE);
                command_five.setVisibility(View.GONE);
                // command_one = new FloatingActionButton(getActivity());
                // command_two = new FloatingActionButton(getActivity());
                command_one.setIcon(R.drawable.ic_ezeid_notification_one);
                command_two.setIcon(R.drawable.ic_ezeid_appoinment_one);
                command_one.setTitle("Sales Enquiry");
                command_two.setTitle("Home Delivery");
                command_one.setClickable(true);
                command_two.setClickable(true);
                // businessMenu.addButton(command_one);
                // businessMenu.addButton(command_two);
                businessMenu.expand();
                command_one.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (command_one.isClickable() == true) {
                            if (salesUrl.equalsIgnoreCase("")) {
                                business_navigation = "sales_enquiry";
                                ValidateUserStatus();
                            } else {
                                Web(salesUrl);
                            }
                        }
                    }
                });
                command_two.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (command_two.isClickable() == true) {
                            if (homeDeliveryUrl.equalsIgnoreCase("")) {
                                business_navigation = "home_delivery";
                                ValidateUserStatus();
                            } else {
                                Web(homeDeliveryUrl);
                            }
                        }
                    }
                });
            } else if (roleTypeOfUser.equalsIgnoreCase("2")) {
                if (!visible_module.equalsIgnoreCase("0")) {
                    if (visible_module.length() > 0) {
                        businessMenu.setVisibility(View.VISIBLE);
                        char sales = visible_module.charAt(0);
                        char home = visible_module.charAt(1);
                        char reserve = visible_module.charAt(2);
                        char support = visible_module.charAt(3);
                        char cv = visible_module.charAt(4);
                        String salesEnquiryAction = String.valueOf(sales);
                        String homeDeliveryAction = String.valueOf(home);
                        String reservationAction = String.valueOf(reserve);
                        String supportAction = String.valueOf(support);
                        String cvAction = String.valueOf(cv);

                        command_one.setVisibility(View.VISIBLE);
                        command_two.setVisibility(View.VISIBLE);
                        command_three.setVisibility(View.VISIBLE);
                        command_four.setVisibility(View.VISIBLE);
                        command_five.setVisibility(View.VISIBLE);

                        command_one.setTitle("Sales Enquiry");
                        command_two.setTitle("Home Delivery");
                        command_three.setTitle("Reservation");
                        command_four.setTitle("Support Request");
                        command_five.setTitle("Send CV");

                        if (salesEnquiryAction.equalsIgnoreCase("1")) {
                            command_one.setVisibility(View.VISIBLE);

                        } else {
                            command_one.setVisibility(View.GONE);
                        }

                        if (homeDeliveryAction.equalsIgnoreCase("1")) {
                            command_two.setVisibility(View.VISIBLE);
                        } else {
                            command_two.setVisibility(View.GONE);
                        }
                        if (reservationAction.equalsIgnoreCase("1")) {
                            command_three.setVisibility(View.VISIBLE);
                        } else {
                            command_three.setVisibility(View.GONE);
                        }
                        if (supportAction.equalsIgnoreCase("1")) {
                            command_four.setVisibility(View.VISIBLE);
                        } else {
                            command_four.setVisibility(View.GONE);
                        }
                        if (cvAction.equalsIgnoreCase("1")) {
                            command_five.setVisibility(View.VISIBLE);
                        } else {
                            command_five.setVisibility(View.GONE);
                        }

                        command_one.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (salesUrl.equalsIgnoreCase("")) {
                                    business_navigation = "sales_enquiry";
                                    ValidateUserStatus();
                                } else {
                                    Web(salesUrl);
                                }
                            }
                        });
                        command_two.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (homeDeliveryUrl.equalsIgnoreCase("")) {
                                    business_navigation = "home_delivery";
                                    ValidateUserStatus();
                                } else {
                                    Web(homeDeliveryUrl);
                                }
                            }
                        });

                        command_three.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (reservationUrl.equalsIgnoreCase("")) {
                                    business_navigation = "reservation";
                                    ValidateUserStatus();
                                } else {
                                    Web(reservationUrl);
                                }
                            }
                        });

                        command_four.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (supportUrl.equalsIgnoreCase("")) {
                                    business_navigation = "service_request";
                                    ValidateUserStatus();
                                } else {
                                    Web(supportUrl);
                                }
                            }
                        });

                        command_five.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (cvUrl.equalsIgnoreCase("")) {
                                    business_navigation = "send_cv";
                                    ValidateUserStatus();
                                } else {
                                    Web(cvUrl);
                                }
                            }
                        });
                        businessMenu.expand();
                    }
                } else {
                    businessMenu.setVisibility(View.GONE);
                }
            } else {
                businessMenu.setVisibility(View.GONE);
            }
        } else {
            businessMenu.setVisibility(View.GONE);
        }

    }

    private void SetRate(String panelRate) {

        if (!panelRate.equalsIgnoreCase("")
                || !panelRate.equalsIgnoreCase("null")) {

            int rate = Integer.parseInt(panelRate);
            if (rate == 1) {
                rate_one.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_two.setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_three
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_four
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_five
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            } else if (rate == 2) {
                rate_one.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_two.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_three
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_four
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_five
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            } else if (rate == 3) {
                rate_one.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_two.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_three
                        .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_four
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_five
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            } else if (rate == 4) {
                rate_one.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_two.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_three
                        .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_four
                        .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_five
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            } else if (rate == 5) {
                rate_one.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_two.setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_three
                        .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_four
                        .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                rate_five
                        .setImageResource(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
            } else {
                rate_one.setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_two.setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_three
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_four
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                rate_five
                        .setImageResource(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
            }
        }
    }

    private void SetPanelSearchInformation(JSONArray response) {
        // Log.d(TAG, response.toString());

        try {
            int length = response.length();
            if (length != 0) {
                upPanelLayoutStatus = true;
                upPanelLayout.expandPanel();
                resultText.setVisibility(View.INVISIBLE);
                switch_toggle_result.setVisibility(View.INVISIBLE);
                SwipeOverlay();
                setValidSearch = true;
                JSONObject jsonObject = response.getJSONObject(0);
                roleType = jsonObject.getString("IDTypeID");
                EzeidGlobal.EZEID_TYPE = roleType;
                if (roleType.equalsIgnoreCase("1")) {
                    panelTitle.setText(jsonObject.getString("FirstName") + " "
                            + jsonObject.getString("LastName"));

                } else {

                    if (jsonObject.getString("CompanyName")
                            .equalsIgnoreCase("")) {
                        panelTitle.setText(jsonObject.getString("FirstName")
                                + " " + jsonObject.getString("LastName"));
                    } else {
                        panelTitle.setText(jsonObject.getString("CompanyName"));
                    }
                }
                panelContent.setVisibility(View.VISIBLE);
                panelContent.setText(jsonObject.getString("JobTitle") + ",\n"

                        + jsonObject.getString("AboutCompany"));
                toMasterIdMsg = jsonObject.getString("TID");
                addressl1S = jsonObject.getString("AddressLine1");
                addressl2S = jsonObject.getString("AddressLine2");
                cityS = jsonObject.getString("CityTitle");
                stateS = jsonObject.getString("StateTitle");
                countryS = jsonObject.getString("CountryTitle");
                postalCodeS = jsonObject.getString("PostalCode");
                parkingStatusS = jsonObject.getString("ParkingStatus");
                openStatusS = jsonObject.getString("OpenStatus");
                // workHrsS = jsonObject.getString("WorkingHours");
                ezeidS = jsonObject.getString("EZEID");
                eZEIDVerifiedID = jsonObject.getString("EZEIDVerifiedID");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("BusinessEZEID",
                        ezeidS);
                locationID = jsonObject.getString("LocID");
                iSDPhoneNumber = jsonObject.getString("ISDPhoneNumber");
                iSDMobileNumber = jsonObject.getString("ISDMobileNumber");
                visibleModules = jsonObject.getString("VisibleModules");
                salesUrl = jsonObject.getString("SalesURL");
                homeDeliveryUrl = jsonObject.getString("HomeDeliveryURL");
                reservationUrl = jsonObject.getString("ReservationURL");
                supportUrl = jsonObject.getString("ServiceURL");
                cvUrl = jsonObject.getString("CVURL");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("LocID",
                        locationID);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("TID",
                        toMasterIdMsg);

                rMarkerTitle = panelTitle.getText().toString();
                rMarkerRole = roleType;
                rMarkerStatus = openStatusS;
                setBusinessMenu(eZEIDVerifiedID, visibleModules, roleType);

                if (!ezeidS.equalsIgnoreCase("")) {
                    ezeid.setVisibility(View.VISIBLE);
                    ezeid.setText(ezeidS.substring(1));
                } else {
                    ezeid.setVisibility(View.GONE);
                }
                picture = "";
                // icon = "";
                picture = jsonObject.getString("Picture");
                // icon = jsonObject.getString("Icon");
                verify = jsonObject.getString("EZEIDVerifiedID");


                String redflagstatus = jsonObject.getString("Redflagstatus");
                if (redflagstatus.equalsIgnoreCase("1")) {
                    panelRedFlag.setVisibility(View.VISIBLE);
                    isRedFlag = true;
                } else {
                    isRedFlag = false;
                    panelRedFlag.setVisibility(View.GONE);
                }

                if (!verify.equalsIgnoreCase("")) {
                    if (verify.equalsIgnoreCase("1")) {
                        verifyStatusIcon
                                .setImageResource(R.drawable.ic_action_not_verified);
                    } else {
                        verifyStatusIcon
                                .setImageResource(R.drawable.ic_action_verified);
                    }

                } else {
                    verifyStatusIcon.setVisibility(View.GONE);
                }
                if (!picture.equalsIgnoreCase("")) {
                    String pic = EzeidUtil.GetFileType(picture);
                    Bitmap panelImg = EzeidUtil.ByteTOBitmap(pic);
                    SetImagePanelImage(panelImg);
                } else {
                    SetImagePanelImage(null);
                }

                if (!addressl2S.equalsIgnoreCase("")) {
                    addressl2S = ", " + addressl2S;
                } else {
                    addressl2S = "";
                }

                if (!cityS.equalsIgnoreCase("")) {
                    cityS = ", " + cityS;
                } else {
                    cityS = "";
                }

                if (!stateS.equalsIgnoreCase("")) {
                    stateS = ", " + stateS;
                } else {
                    stateS = "";
                }

                if (!countryS.equalsIgnoreCase("")) {
                    countryS = ", " + countryS;
                } else {
                    countryS = "";
                }
                if (!postalCodeS.equalsIgnoreCase("")) {
                    postalCodeS = " - " + postalCodeS;
                } else {
                    postalCodeS = "";
                }

                address = addressl1S + addressl2S + cityS + stateS + countryS
                        + postalCodeS;

                panelSubContent.setText(address);
                if (panelContent.getText().toString().equalsIgnoreCase("")) {
                    panelContent.setVisibility(View.GONE);
                }

                if (!roleType.equalsIgnoreCase("1")) {
                    if (!parkingStatusS.equalsIgnoreCase("")) {
                        panelParkingImg.setVisibility(View.VISIBLE);
                        if (parkingStatusS.equalsIgnoreCase("1")) {
                            panelParkingImg
                                    .setImageResource(R.drawable.ic_action_parking);
                        } else if (parkingStatusS.equalsIgnoreCase("2")) {
                            panelParkingImg
                                    .setImageResource(R.drawable.ic_action_vparking);
                        } else {
                            panelParkingImg
                                    .setImageResource(R.drawable.ic_action_noparking);
                        }
                    } else {
                        panelParkingImg.setVisibility(View.GONE);
                    }
                    if (!openStatusS.equalsIgnoreCase("")) {
                        if (openStatusS.equalsIgnoreCase("1")) {
                            roleStatusImg
                                    .setImageResource(R.drawable.ic_action_open);
                        }
                        if (openStatusS.equalsIgnoreCase("2")) {
                            roleStatusImg
                                    .setImageResource(R.drawable.ic_action_close);
                        }
                    }

                }

                mobileS = jsonObject.getString("MobileNumber");
                phoneS = jsonObject.getString("PhoneNumber");
                emailS = jsonObject.getString("EMailID");
                websiteS = jsonObject.getString("Website");
                if (mobileS.equalsIgnoreCase("") && phoneS.equalsIgnoreCase("")
                        && emailS.equalsIgnoreCase("")) {
                    contactBar.setVisibility(View.GONE);
                }
                if (phoneS.equalsIgnoreCase("")) {
                    panelPhone.setVisibility(View.GONE);
                    panelPhDial.setVisibility(View.GONE);
                    panelBetweenPM.setVisibility(View.GONE);
                } else {
                    panelBetweenPM.setVisibility(View.VISIBLE);
                    panelPhone.setText("P: " + iSDPhoneNumber + " " + phoneS);
                }

                if (mobileS.equalsIgnoreCase("")) {
                    panelMobileNo.setVisibility(View.GONE);
                    panelMobDial.setVisibility(View.GONE);
                    panelChat.setVisibility(View.GONE);
                    panelBetweenPM.setVisibility(View.GONE);
                } else {
                    if (!phoneS.equalsIgnoreCase("")) {
                        panelBetweenPM.setVisibility(View.GONE);
                    }
                    panelMobileNo.setText("M: " + iSDMobileNumber + " "
                            + mobileS);
                }

                if (emailS.equalsIgnoreCase("")) {
                    emailBar.setVisibility(View.GONE);
                } else {
                    panelEmailid.setText("E: " + emailS);
                }

                if (websiteS.equalsIgnoreCase("")) {
                    websiteBar.setVisibility(View.GONE);
                } else {
                    website.setText(" " + websiteS);
                }

                panelRate = jsonObject.getString("Rating");
                SetRate(panelRate);
                if (roleType.equalsIgnoreCase("2")) {
                    // bannerParent.setVisibility(View.VISIBLE);
                    // bannerCount = jsonObject.getString("Banners");
                    // stateTitle = jsonObject.getString("StateTitle");
                    // frameProgressBar.setVisibility(View.VISIBLE);
                    if (bannerDrawables.size() > 0) {
                        bannerDrawables.clear();
                    }
                }

            } else {
                upPanelLayout.collapsePanel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void EzeidLatLngBounds() {
        if (markers.size() != 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int padding = 24;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(
                    bounds, padding);
            googleMap.animateCamera(cameraUpdate);
        }

    }

    private void SetImagePanelImage(Bitmap b) {
        panelImg.setVisibility(View.VISIBLE);
        if (b == null) {
            b = BitmapFactory.decodeResource(getResources(),
                    R.drawable.profile_pic_ezeid);
        }
        panelImg.setImageBitmap(EzeidUtil.GetRoundedRectBitmap(b, 12));

    }

    protected void Call(String mobileS) {
        if (!mobileS.equalsIgnoreCase("")) {
            try {
                Intent phoneCallIntent = new Intent(Intent.ACTION_CALL);
                phoneCallIntent.setData(Uri.parse("tel:" + mobileS));
                // startActivity(phoneCallIntent);
                phoneCallIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(Intent.createChooser(phoneCallIntent, "EZEOne"));

            } catch (Exception bug) {
                EzeidUtil.showToast(LandingPage.this, "Call failed!");
                bug.printStackTrace();
            }
        }
    }

    protected void Web() {
        if (!websiteS.equalsIgnoreCase("")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if (!websiteS.startsWith("http://")
                        && !websiteS.startsWith("https://")) {
                    websiteS = "http://" + websiteS;
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.setData(Uri.parse(websiteS));
                startActivity(Intent.createChooser(intent, "EZEOne"));
                // startActivity(intent);
            } catch (Exception bug) {
                EzeidUtil.showToast(LandingPage.this, "Failed to open the site");
                bug.printStackTrace();
            }
        }
    }

    protected void Web(String url) {
        if (!url.equalsIgnoreCase("")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                    url = "http://" + url;
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.setData(Uri.parse(url));
                startActivity(Intent.createChooser(intent, "EZEOne"));
                // startActivity(intent);
            } catch (Exception bug) {
                EzeidUtil.showToast(LandingPage.this, "Failed to open the site");
                bug.printStackTrace();
            }
        }
    }

    protected void Messaging() {
        if (!mobileS.equalsIgnoreCase("")) {
            try {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", mobileS);
                smsIntent.putExtra("sms_body", "Hi....");
                // startActivity(smsIntent);
                smsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                startActivity(Intent.createChooser(smsIntent, "EZEOne"));
            } catch (Exception bug) {
                EzeidUtil.showToast(LandingPage.this, "Send failed!");
                bug.printStackTrace();
            }
        }
    }

    protected void sendEmail(String objective, String message) {

        String[] TO = {"www.hirecraft.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, objective);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        try {
            startActivityForResult(
                    Intent.createChooser(emailIntent, "Send mail..."),
                    SENT_MAIL);
            // Log.i("Finished sending email...", "");
        } catch (ActivityNotFoundException ex) {
            EzeidUtil.showToast(LandingPage.this,
                    "There is no email client installed.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
            if (timer_flag == true) {
                startTime = 0;
                trackLimit.cancel();
                timer_flag = false;
            }
        }
        ezeidPreferenceHelper.SaveValueToSharedPrefs("RATE", "");
        ezeidPreferenceHelper.SaveValueToSharedPrefs("RATE_COUNT", "");
        Crouton.cancelAllCroutons();
    }


    private void UpdateMyLocation() {
        googleMap.clear();
        location = fusedLocationService.getLocation();
        if (null != location) {
            double lati = location.getLatitude();
            double longi = location.getLongitude();
            if (myMarker != null) {
                myMarker.setPosition(new LatLng(lati, longi));
            } else {
                myMarker = googleMap.addMarker(new MarkerOptions().position(
                        new LatLng(lati, longi)).icon(
                        BitmapDescriptorFactory
                                .fromResource(R.drawable.transparent)));
                // Show the current location in Google Map
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
                        lati, longi)));
                // Zoom in the Google Map
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            }
        }
    }

    private void ResumeMyLocation() {
        location = fusedLocationService.getLocation();
        if (null != location) {
            double lati = location.getLatitude();
            double longi = location.getLongitude();
            if (myMarker != null) {
                myMarker.setPosition(new LatLng(lati, longi));
            } else {
                myMarker = googleMap.addMarker(new MarkerOptions().position(
                        new LatLng(lati, longi)).icon(
                        BitmapDescriptorFactory
                                .fromResource(R.drawable.transparent)));
                // Show the current location in Google Map
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(
                        lati, longi)));
                // Zoom in the Google Map
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));

            }
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((actionId == EditorInfo.IME_ACTION_SEARCH)) {
            GetEzeidSearchResult(searchType, "", "");
            searchPanel.collapsePanel();
            return true;
        }
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        HashMap<String, String> searchMarkerData = null;
        if (!marker.equals(myMarker)) {

            searchMarkerData = searchExtraInfo.get(marker.getId());
            if (searchMarkerData != null) {
                if (searchMarkerData.get("TID") != null) {
                    // resultText.setText("");
                    String tlatitude = searchMarkerData.get("Latitude");
                    String tlongitude = searchMarkerData.get("Longitude");
                    String tEzeid = searchMarkerData.get("EZEID");
                    String tSeqNo = searchMarkerData.get("SeqNo");
                    String tPin = searchMarkerData.get("PIN");
                    if (!tlatitude.equalsIgnoreCase("")
                            && !tlongitude.equalsIgnoreCase("")) {
                        EzeidRoute = new LatLng(Double.parseDouble(tlatitude),
                                Double.parseDouble(tlongitude));
                    }

                    String tid = searchMarkerData.get("TID");
                    if (!tid.equalsIgnoreCase("")) {
                        GetSearchInformation(tid, searchTypeinfo, tEzeid, tPin,
                                tSeqNo);
                    }
                }
            }

        }
        return false;
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {
        switch (panel.getId()) {
            case R.id.panelViewChild:
                // do to
                break;
            case R.id.panelView:
                if (setValidSearch == false || upPanelLayoutStatus == false) {
                    upPanelLayout.collapsePanel();
                } else if (setValidSearch == true || upPanelLayoutStatus == true) {
                    command_six.setVisibility(View.VISIBLE);
                    command_navigation.setVisibility(View.VISIBLE);
                }
                break;

            default:
                break;
        }
        // Log.i("debug tag", "onPanelSlide():" );

    }

    @Override
    public void onPanelCollapsed(View panel) {
        // Log.i("debug tag", "onPanelCollapsed()");

        switch (panel.getId()) {
            case R.id.panelViewChild:
                // do to
                // searchPanelLayout.setVisibility(View.VISIBLE);
                ChangeSearchEffect(searchPanelLayout, "collopse");
                hideLabel.setVisibility(View.VISIBLE);
                hideLblImg.setVisibility(View.VISIBLE);
                // hideLblImg.startAnimation(animRotate);
                // hideLblImg.setImageResource(R.drawable.abc_ic_search_api_mtrl_alpha);
                SetHideLabel();
                break;

            case R.id.panelView:
                if (panelRedFlag.getVisibility() == View.VISIBLE) {
                    panelRedFlag.setVisibility(View.GONE);
                }
                resultText.setVisibility(View.VISIBLE);
                switch_toggle_result.setVisibility(View.VISIBLE);
                businessMenu.collapse();
                break;

            default:
                break;
        }
    }

    @Override
    public void onPanelExpanded(View panel) {
        // Log.i("debug tag", "onPanelExpanded()");
        isSearchPalelExpand = true;
        switch (panel.getId()) {
            case R.id.panelViewChild:
                // do to
                // searchPanelLayout.setVisibility(View.GONE);
                // EzeidUtil.collapse(searchPanelLayout);
                ChangeSearchEffect(searchPanelLayout, "expand");
                hideLabel.setVisibility(View.GONE);
                // hideLblImg.startAnimation(animRotate);
                // hideLblImg.setImageResource(R.drawable.arrowdown);
                // hideLblImg.setVisibility(View.INVISIBLE);
                // hideLblImg.startAnimation(animRotate);
                GetMyLocation();
                break;
            case R.id.panelView:
                if (isRedFlag == true) {
                    panelRedFlag.setVisibility(View.VISIBLE);
                }
                resultText.setVisibility(View.INVISIBLE);
                switch_toggle_result.setVisibility(View.INVISIBLE);
                businessMenu.expand();
                break;

            default:
                break;
        }
    }

    @Override
    public void onPanelAnchored(View panel) {
        // Log.i("debug tag", "onPanelAnchored()");

    }

    @Override
    public void onPanelHidden(View panel) {
        // Log.i("debug tag", "onPanelHidden()");
    }

    private void SwipeOverlay() {

        String overlay = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("OVERLAY_SEARCH");
        if (!overlay.equalsIgnoreCase("false")) {

            overlayInfo = new Dialog(LandingPage.this);
            overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
            overlayInfo.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            overlayInfo.getWindow().setFlags(
                    WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
            overlayInfo.setContentView(R.layout.ezeone_searchinfo_overlay_view);
            overlayInfo.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            if (overlayInfo != null) {
                overlayInfo.show();
            }
            TextView overlayText = (TextView) overlayInfo
                    .findViewById(R.id.overlayText);
            FloatingActionButton btn = (FloatingActionButton) overlayInfo
                    .findViewById(R.id.overlay_cmdbtn);

            // overlayText.setText("Tap to select or unselect.");

            overlayText.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    overlayInfo.cancel();
                    // overlayInfo.dismiss();
                    ezeidPreferenceHelper.SaveValueToSharedPrefs(
                            "OVERLAY_SEARCH", "false");
                }
            });
            btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    overlayInfo.cancel();
                    // overlayInfo.dismiss();
                    ezeidPreferenceHelper.SaveValueToSharedPrefs(
                            "OVERLAY_SEARCH", "false");
                }
            });

        }
    }

    private void ChangeSearchEffect(final RelativeLayout relativeLayout,
                                    String state) {

        Integer colorFrom, colorTo;

        if (state.equalsIgnoreCase("collopse")) {
            colorFrom = getResources().getColor(R.color.primary);
            colorTo = getResources().getColor(R.color.primary_level_three);
        } else {
            colorFrom = getResources().getColor(R.color.primary);
            colorTo = getResources().getColor(R.color.primary_level_three);
        }

        ValueAnimator valueAnimator = ValueAnimator.ofObject(
                new ArgbEvaluator(), colorFrom, colorTo);
        valueAnimator.setDuration(500);
        valueAnimator.addUpdateListener(new AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                // TODO Auto-generated method stub
                relativeLayout.setBackgroundColor((Integer) animator
                        .getAnimatedValue());
            }
        });
        valueAnimator.start();
    }

    private void SetHideLabel() {



//        switch (findBySpinner.getSelectedItemPosition()) {
//            case 0:
//                hideLabel.setText("Search EZEOne ID");
//                break;
//            case 1:
//                hideLabel.setText("Search Keywords");
//                break;
//            case 2:
//                hideLabel.setText("Search Job Keywords");
//                break;
//
//            default:
//                hideLabel.setText("Search Keywords");
//                break;
//        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

        camLocation = googleMap.getCameraPosition().target;
        double latitude = camLocation.latitude;
        double longitude = camLocation.longitude;
        dynamicLati = String.valueOf(latitude);
        dynamicLongi = String.valueOf(longitude);

        if (null != camLocation) {
            // GetAddress(latitude, longitude);
            ezeidPreferenceHelper.SaveValueToSharedPrefs("Latitude",
                    dynamicLati);
            ezeidPreferenceHelper.SaveValueToSharedPrefs("Longitude",
                    dynamicLongi);
        }
    }

    @Override
    public void onSearchResults(JSONArray array) {

        try {
            int length = array.length();
            if (length != 0) {
                upPanelLayoutStatus = false;
                InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(map_search.getWindowToken(), 0);
                JSONObject jsonObject = array.getJSONObject(0);
                String formatted_address = jsonObject
                        .getString("formatted_address");
                map_search.setText(formatted_address);
                String geometry = jsonObject.getString("geometry");
                JSONObject jsonGeometry = new JSONObject(geometry);
                String location = jsonGeometry.getString("location");
                JSONObject jsonLocation = new JSONObject(location);
                dynamicLati = jsonLocation.getString("lat");
                dynamicLongi = jsonLocation.getString("lng");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("Latitude",
                        dynamicLati);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("Longitude",
                        dynamicLongi);
                // GetEzeidSearchResult(2);
                searchType = "dynamicSearch";
                // GetSearchResult(dynamicLati, dynamicLongi);
                // Log.i(TAG, "Data:" + formatted_address + " " + dynamicLati
                // + " " + dynamicLongi);
                LatLng latLng = new LatLng(Double.parseDouble(dynamicLati),
                        Double.parseDouble(dynamicLongi));
                // GetMyLocation();
                myMarker = googleMap.addMarker(new MarkerOptions().position(
                        latLng).icon(
                        BitmapDescriptorFactory
                                .fromResource(R.drawable.transparent)));
                // Show the current location in Google Map
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                // Zoom in the Google Map
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                mapProgressBar.setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    private String CurrentDate(String date) {// 02/25/2015 03:22:29 PM GMT+05:30
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                "MM/dd/yyyy HH:mm:ss a");
        // simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parsed;
        try {
            parsed = simpleDateFormat.parse(date);// Tue Feb 24 17:30:57
            // GMT+05:30 2015
            SimpleDateFormat destFormat = new SimpleDateFormat(// Thu Feb 26
                    // 00:04:48
                    // GMT+05:30
                    // 2015
                    "dd MMM yyyy HH:mm:ss a");
            // destFormat.setTimeZone(TimeZone.getDefault());
            return destFormat.format(parsed);// 24 Feb 2015 17:30:57 PM
        } catch (ParseException e) {
            return null;
        }

    }

    private void RouteDialog() {
        if (EzeidRoute != null && currentLatalng != null) {
            upPanelLayout.collapsePanel();
            ORIGIN = new LatLng(currentLatalng.latitude,
                    currentLatalng.longitude);
            DEST = new LatLng(EzeidRoute.latitude, EzeidRoute.longitude);
            GetRouteData();
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {

            case R.id.switchPark:
                if (isChecked == true) {
                    parkingCheckStr = "1,2";
                } else {
                    parkingCheckStr = "0";
                }
                break;
            case R.id.switchStatus:

                if (isChecked == true) {
                    openStatusStr = "1";
                } else {
                    openStatusStr = "0";
                }
                break;

            case R.id.switchDelivery:
                if (isChecked == true) {
                    hDeliveryCheckStr = "1";
                } else {
                    hDeliveryCheckStr = "0";
                }
                break;

            case R.id.switchTrack:
                if (isChecked == true) {
                    // staticMarker.setVisibility(View.GONE);
                    googleMap.clear();
                    SetTrackingVisibility();
                    StartTimer();
                } else {
                    resultText.setText("");
                    // staticMarker.setVisibility(View.VISIBLE);
                    SetTrackingInvisibility();
                    StopTimer();
                }
                break;

            default:
                break;
        }
    }

    private void SetTrackingVisibility() {
        mGoogleApiClient.connect();
        addressSearchLayout.setVisibility(View.GONE);
        onTrackingState = true;
        GetMyLocation();
        if (timer_flag == false) {
            startTime = TimeUnit.MINUTES.toMillis(60);
            trackLimit = new TrackLimit(startTime, interval);
            trackLimit.start();
            timer_flag = true;
        }
        resultText.setText("");
        ClearCaches();

    }

    private void SetTrackingInvisibility() {
        mGoogleApiClient.disconnect();
        if (timer_flag == true) {
            startTime = 0;
            trackLimit.cancel();
            timer_flag = false;
        }
        GetMyLocation();
        addressSearchLayout.setVisibility(View.VISIBLE);
        // UpdateMyLocation();
        if (null != trackingMarker) {
            trackingMarker.remove();
        }
        ClearCaches();
        // if (routePoints.size() > 0)
        // routePoints.clear();
        onTrackingState = false;
    }

    Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.arg1) {


                case Constant.MessageState.REQUEST_USERDETAIL_SUCCESS:
                    DismissEzeidDialog();
                    EzeidGlobal.userDetailsObj = ((JSONArray) msg.obj);
                    break;

                case Constant.MessageState.REQUEST_USERDETAIL_FAIL:
                    DismissEzeidDialog();
                    VolleyErrorHelper.getMessage(msg.obj, LandingPage.this, false);
                    break;

                case Constant.MessageState.WORKING_HOURS_AVAILABLE:
                    DismissEzeidDialog();
                    ShowWorkingHours((JSONObject) msg.obj);
                    break;

                case Constant.MessageState.WORKING_HOURS_UNAVAILABLE:
                    DismissEzeidDialog();
                    VolleyErrorHelper.getMessage(msg.obj, LandingPage.this, false);
                    break;

                case Constant.MessageState.PASSWORD_CHANGED:
                    DismissEzeidDialog();
                    parsePasswordChangeResponse((JSONObject) msg.obj);
                    break;

                case Constant.MessageState.PASSWORD_CHANGE_FAILED:
                    DismissEzeidDialog();
                    break;

                default:
                    break;
            }
            return false;
        }
    });

    private void GetRouteData() {
        if (null != ORIGIN && null != DEST) {
            // Log.d("TAG", "Data:" + ORIGIN + DEST + icon + status + roleType);
            DrawRoute(ORIGIN, DEST, rMarkerTitle, rMarkerStatus, rMarkerRole);
        }
    }

    private void DrawRoute(final LatLng ORIGIN, final LatLng DEST,
                           final String markerTitle, final String typeStatus,
                           final String roleType) {
        routeProgress.setVisibility(View.VISIBLE);
        // eProgress.show();

        if (null != polyline) {
            polyline.remove();
        }
        if (null != rMyMarker) {
            rMyMarker.remove();
        }
        gd = new GoogleDirection(LandingPage.this);
        gd.setOnDirectionResponseListener(new GoogleDirection.OnDirectionResponseListener() {

            public void onResponse(String status, Document doc,
                                   GoogleDirection gd) {
                if (status.equalsIgnoreCase("OK")) {
                    mDoc = doc;
                    polyline = googleMap.addPolyline(gd.getPolyline(doc, 7,
                            getResources().getColor(R.color.menuBgColor)));
                    rMyMarker = googleMap.addMarker(new MarkerOptions()
                            .position(ORIGIN)
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ic_action_root_circle)));
                    clearRoute.setVisibility(View.VISIBLE);
                    String dur = gd.getTotalDurationText(mDoc);
                    String dis = gd.getTotalDistanceText(mDoc);
                    resultText.setText("" + dur + ", " + dis + ".");
                    fixZoom();
                    routeProgress.setVisibility(View.GONE);
                } else {
                    EzeidUtil
                            .showToast(LandingPage.this, "Error in getting route");
                    routeProgress.setVisibility(View.GONE);
                }

            }

        });
        gd.setLogging(true);
        gd.request(ORIGIN, DEST, GoogleDirection.MODE_WALKING);
    }

    private void ClearRoute() {

        resultText.setText("");
        if (null != polyline) {
            polyline.remove();
        }
        if (null != rMyMarker) {
            rMyMarker.remove();
        }
        clearRoute.setVisibility(View.GONE);
        isPreLoading = false;
        ResumeMyLocation();

    }

    private void fixZoom() {
        List<LatLng> points = new ArrayList<LatLng>(); // route is instance of
        // PolylineOptions
        points.add(ORIGIN);
        points.add(DEST);
        LatLngBounds.Builder bc = new LatLngBounds.Builder();

        for (LatLng item : points) {
            bc.include(item);
        }
        googleMap
                .moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
    }

    private Location markerCurrentPos = null;

    @Override
    public void onLocationChanged(Location loc) {
        if (null != loc) {
            isLocationChanged = true;
            location = loc;
            markerCurrentPos = loc;
            LatLng markerPos = new LatLng(loc.getLatitude(), loc.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(markerPos).zoom(15).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

        }

    }

    private void GetTrackSearchResponse() {

        if (null != markerCurrentPos && isLocationChanged == true) {
            resultText.setText("");
            // Toast.makeText(getApplicationContext(), "Tracking",
            // Toast.LENGTH_SHORT).show();
            final String lati = String.valueOf(markerCurrentPos.getLatitude());
            final String longi = String
                    .valueOf(markerCurrentPos.getLongitude());
            token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
            if (!searchInputs.getText().toString().equalsIgnoreCase("")) {
                StringRequest stringRequest = new StringRequest(
                        Method.POST, EzeidGlobal.EzeidUrl
                        + "ewtSearchForTracker",
                        TrackReqSuccessListener(), TrackReqErrorListener()) {
                    protected Map<String, String> getParams()
                            throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Keyword", searchInputs.getText().toString());
                        params.put("Token", token);
                        params.put("Proximity", proximityStr);
                        params.put("Latitude", lati);
                        params.put("Longitude", longi);

                        // Log.i("TAG", "Track Params: " + params);
                        return params;
                    }

                    ;
                };
                stringRequest.setShouldCache(false);
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                EzeidGlobal.getInstance().addToRequestQueue(stringRequest, TAG);
            }
        } else {
            // Log.i("TAG", "device not moving...");
        }
    }

    private Response.Listener<String> TrackReqSuccessListener() {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ParseTrackResult(response);
            }
        };
    }

    protected void ParseTrackResult(String response) {

        try {
            if (!response.equalsIgnoreCase("")) {
                // Log.i("TAG", "ParseTrackResult() response: " + response);

                if (!response.equalsIgnoreCase("null")) {
                    JSONArray jsonArray = new JSONArray(response);
                    int len = jsonArray.length();
                    if (len > 0) {
                        if (len == 1) {
                            resultText.setText(len + " Result");
                        } else {
                            resultText.setText(len + " Results");
                        }
                        isLocationChanged = false;
                        SetMediaPlayer();
                        mediaPlayer.start();
                        RemoveMarkers();
                        ClearCaches();
                        for (int k = 0; k < len; k++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(k);
                            tid = jsonObject.getString("TID");
                            tlatitude = jsonObject.getString("Latitude");
                            tlongitude = jsonObject.getString("Longitude");
                            roleType = jsonObject.getString("IDTypeID");
                            EzeidGlobal.EZEID_TYPE = roleType;
                            openStatusS = jsonObject
                                    .getString("OpenCloseStatus");
                            if (roleType.equalsIgnoreCase("2")) {
                                markerTitle = jsonObject
                                        .getString("CompanyName");
                                if (markerTitle.equalsIgnoreCase("")) {
                                    markerTitle = jsonObject.getString("Name");
                                }
                            } else {
                                markerTitle = jsonObject.getString("Name");
                                if (markerTitle.equalsIgnoreCase("")) {
                                    markerTitle = jsonObject
                                            .getString("CompanyName");
                                }
                            }

                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put("TID", tid);
                            hashMap.put("Latitude", tlatitude);
                            hashMap.put("Longitude", tlongitude);
                            hashMap.put("IDTypeID", roleType);
                            hashMap.put("OpenStatus", openStatusS);
                            hashMap.put("MarkerTitle", markerTitle);
                            keywordList.add(hashMap);
                            if (!tlatitude.equalsIgnoreCase("")
                                    && !tlongitude.equalsIgnoreCase("")) {
                                AddSearchMarkers(Double.parseDouble(tlatitude),
                                        Double.parseDouble(tlongitude),
                                        hashMap, markerTitle, openStatusS,
                                        roleType);
                            }
                            EzeidRoute = new LatLng(
                                    Double.parseDouble(tlatitude),
                                    Double.parseDouble(tlongitude));
                            routePoints = new ArrayList<LatLng>();
                            routePoints.add(EzeidRoute);
                            routePoints.add(currentLatalng);
                        }
                    }
                } else {
                    upPanelLayout.collapsePanel();
                    if (ezeidMarker != null) {
                        ezeidMarker.remove();
                    }
                    if (map_search.getText().toString().length() != 0) {
                        map_search.setText("");
                    }
                    RemoveMarkers();
                    ClearCaches();
                    setValidSearch = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void ClearCaches() {
        if (keywordList.size() > 0)
            keywordList.clear();
        if (markers.size() > 0)
            markers.clear();
        if (searchExtraInfo.size() > 0)
            searchExtraInfo.clear();
    }

    private void SetMediaPlayer() {
        mediaPlayer = MediaPlayer.create(LandingPage.this, R.raw.found);
        // try {
        // mediaPlayer.prepare();
        // } catch (IllegalStateException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // } catch (IOException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        //
        // }
        mediaPlayer.setOnCompletionListener(onCompletionListener);
    }

    private void RemoveMarkers() {
        if (markers.size() > 0) {
            for (Marker marker : markers) {
                marker.remove();
            }
        }
    }

    private OnCompletionListener onCompletionListener = new OnCompletionListener() {

        @Override
        public void onCompletion(MediaPlayer mp) {
            // TODO Auto-generated method stub
            mediaPlayer.release();
        }
    };

    private Response.ErrorListener TrackReqErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.i("TAG", "ERROR:" + error.getMessage().toString());
                if (error instanceof TimeoutError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Connection timeout. Please try again");
                } else if (error instanceof ServerError
                        || error instanceof AuthFailureError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Unable to connect server. Please try later");
                } else if (error instanceof NetworkError
                        || error instanceof NoConnectionError) {
                    EzeidUtil
                            .showToast(LandingPage.this, "Network is unreachable");
                } else {
                    EzeidUtil.showToast(LandingPage.this, "No Results");
                }
            }
        };
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // TODO Auto-generated method stub
        // Refer to the javadoc for ConnectionResult to see what error codes
        // might be returned in
        // onConnectionFailed.
        // Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
        // + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, REQUEST, this);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        // TODO Auto-generated method stub
        // Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    private void StartTimer() {
        timer = new Timer();
        TimerInitialize();
        timer.schedule(timerTask, 6000, 15000);
    }

    private void StopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    protected void TimerInitialize() {
        timerTask = new TimerTask() {

            @Override
            public void run() {
                LandingPage.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // ws call
                        GetTrackSearchResponse();
                    }
                });

            }
        };
    }

    private class TrackLimit extends CountDownTimer {

        public TrackLimit(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onTick(long millisUntilFinished) {
            // TODO Auto-generated method stub
            // Log.i(TAG, "track Time remain:" + millisUntilFinished);
        }

        @Override
        public void onFinish() {
            // TODO Auto-generated method stub
            // Log.i(TAG, "track finished!");
            switchTrack.setChecked(false);
            startTime = TimeUnit.MINUTES.toMillis(60);
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {

        switch (parent.getId()) {
//            case R.id.findBySpinner:
//                // On selecting a spinner item
//                String item = parent.getItemAtPosition(position).toString();
//                String keyword = getResources().getString(R.string.findByKey);
//                String jobkeyword = getResources().getString(R.string.findJobKey);
//                if (item.equalsIgnoreCase(keyword)) {
//                    SetKeywordChecked();
//                } else if (item.equalsIgnoreCase(jobkeyword)) {
//                    SetJobKeywordChecked();
//                } else {
//                    if (findSelection == 1) {
//                        // EzeidUtil.collapse(filterLayout);
//                        advancedSearch.setVisibility(View.GONE);
//                        findSelection = 0;
//                        searchInputs.setHint("Type EZEOne ID to view information");
//                        searchType = "ezeid";
//                    }
//                }
//                break;

            case R.id.proximity:

                switch (position) {
                    case 0:
                        proximityStr = "1";
                        break;
                    case 1:
                        proximityStr = "2";
                        break;
                    case 2:
                        proximityStr = "5";
                        break;
                    case 3:
                        proximityStr = "10";
                        break;
                    case 4:
                        proximityStr = "25";
                        break;
                    case 5:
                        proximityStr = "50";
                        break;

                    default:
                        proximityStr = "50";
                        break;

                }
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub
        proximityStr = "50";
    }

    private void GetConfigurations() {
        ezeidPreferenceHelper.SaveValueToSharedPrefs("ITEM_TO_ADD_USER",
                "consumer");

        ezeidPreferenceHelper.SaveValueToSharedPrefs("BUSINESS_NAVIGATION",
                business_navigation);

        if (business_navigation.equalsIgnoreCase("sales_enquiry")) {
            String salesItemListType = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("SalesItemListType");
            EzeidGlobal.SALES_TYPE = salesItemListType;

            if (salesItemListType.equalsIgnoreCase("0")) {
                startActivity(new Intent(LandingPage.this,
                        EzeidAddSalesMessages.class));
            } else if (salesItemListType.equalsIgnoreCase("2")) {
                startActivity(new Intent(LandingPage.this, EzeidAddItems.class));
            } else {
                startActivity(new Intent(LandingPage.this,
                        EzeidAddSalesItems.class));
            }
        } else if (business_navigation.equalsIgnoreCase("home_delivery")) {

            String homeDeliveryItemListType = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("HomeDeliveryItemListType");
            EzeidGlobal.SALES_TYPE = homeDeliveryItemListType;

            if (homeDeliveryItemListType.equalsIgnoreCase("0")) {
                startActivity(new Intent(LandingPage.this, HDAddDeliveryMsgs.class));
            } else if (homeDeliveryItemListType.equalsIgnoreCase("2")) {
                startActivity(new Intent(LandingPage.this, HDAddItems.class));
            } else {
                startActivity(new Intent(LandingPage.this,
                        HDAddDeliveryItems.class));
            }
        } else if (business_navigation.equalsIgnoreCase("reservation")) {
            startActivity(new Intent(LandingPage.this,
                    AppointmentCalendarActivity.class));
        } else if (business_navigation.equalsIgnoreCase("service_request")) {
            startActivity(new Intent(LandingPage.this, HelpDeskEnquiry.class));
        } else if (business_navigation.equalsIgnoreCase("send_cv")) {
            startActivity(new Intent(LandingPage.this, HREnquiry.class));
        }

    }

    private Animator animator = new Animator();
    private final Handler mHandler = new Handler();
    int currentPt;

    CancelableCallback MyCancelableCallback = new CancelableCallback() {

        @Override
        public void onCancel() {
            System.out.println("onCancelled called");
        }

        @Override
        public void onFinish() {

            if (++currentPt < markers.size()) {

                float targetBearing = bearingBetweenLatLngs(
                        googleMap.getCameraPosition().target,
                        markers.get(currentPt).getPosition());

                LatLng targetLatLng = markers.get(currentPt).getPosition();
                // float targetZoom = zoomBar.getProgress();

                System.out.println("currentPt  = " + currentPt);
                System.out.println("size  = " + markers.size());
                // Create a new CameraPosition
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(targetLatLng)
                        .tilt(currentPt < markers.size() - 1 ? 90 : 0)
                        .bearing(targetBearing)
                        .zoom(googleMap.getCameraPosition().zoom).build();

                googleMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(cameraPosition),
                        3000, MyCancelableCallback);
                System.out.println("Animate to: "
                        + markers.get(currentPt).getPosition() + "\n"
                        + "Bearing: " + targetBearing);

                markers.get(currentPt).showInfoWindow();




            } else {
                // info.setText("onFinish()");
            }

        }

    };

    public class Animator implements Runnable {

        private static final int ANIMATE_SPEEED = 1500;
        // private static final int ANIMATE_SPEEED_TURN = 1000;
        // private static final int BEARING_OFFSET = 20;

        private final Interpolator interpolator = new LinearInterpolator();

        int currentIndex = 0;

        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        LatLng endLatLng = null;
        LatLng beginLatLng = null;

        boolean showPolyline = false;

        private Marker trackingMarker;

        public void reset() {
            resetMarkers();
            start = SystemClock.uptimeMillis();
            currentIndex = 0;
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        public void stop() {
            trackingMarker.remove();
            mHandler.removeCallbacks(animator);

        }

        public void initialize(boolean showPolyLine) {
            reset();
            this.showPolyline = showPolyLine;

            // highLightMarker(0);

            if (showPolyLine) {
                polyLine = initializePolyLine();
            }

            // We first need to put the camera in the correct position for the
            // first run (we need 2 markers for this).....
            LatLng markerPos = markers.get(0).getPosition();
            LatLng secondPos = markers.get(1).getPosition();

            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(LatLng markerPos,
                                                    LatLng secondPos) {

            // float bearing = bearingBetweenLatLngs(markerPos, secondPos);

            trackingMarker = googleMap.addMarker(new MarkerOptions().position(
                    markerPos).icon(
                    BitmapDescriptorFactory
                            .fromResource(R.drawable.transparent)));

            // CameraPosition cameraPosition = new CameraPosition.Builder()
            // .target(markerPos)
            // .bearing(bearing + BEARING_OFFSET)
            // .tilt(90)
            // .zoom(googleMap.getCameraPosition().zoom >= 16 ? googleMap
            // .getCameraPosition().zoom : 16).build();

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(markerPos).zoom(14).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            // googleMap.animateCamera(
            // CameraUpdateFactory.newCameraPosition(cameraPosition),
            // ANIMATE_SPEEED_TURN, new CancelableCallback()
            // {
            //
            // @Override
            // public void onFinish()
            // {
            // System.out.println("finished camera");
            // animator.reset();
            // Handler handler = new Handler();
            // handler.post(animator);
            // }
            //
            // @Override
            // public void onCancel()
            // {
            // System.out.println("cancelling camera");
            // }
            // });
        }

        private Polyline polyLine;
        private PolylineOptions rectOptions = new PolylineOptions();

        private Polyline initializePolyLine() {
            // polyLinePoints = new ArrayList<LatLng>();
            rectOptions.add(markers.get(0).getPosition());
            return googleMap.addPolyline(rectOptions);
        }

        /**
         * Add the marker to the polyline.
         */
        private void updatePolyLine(LatLng latLng) {
            List<LatLng> points = polyLine.getPoints();
            points.add(latLng);
            polyLine.setPoints(points);
        }

        public void stopAnimation() {
            // Log.i(TAG,
            // "stopAnimation()== clear cache markers:" + markers.size());
            // RemoveAnimatedMarkers();
            // Log.i(TAG,
            // "stopAnimation()== clear cache markers:" + markers.size());
            isMarkerAnimating = false;
            animator.stop();
        }

        // private void RemoveAnimatedMarkers() {
        // if (markers.size() > 0) {
        // for (Marker marker : markers) {
        // marker.remove();
        // }
        // markers.clear();
        // }
        // }

        public void startAnimation(boolean showPolyLine) {
            if (markers.size() > 2) {
                isMarkerAnimating = true;
                animator.initialize(showPolyLine);
            }
        }

        @Override
        public void run() {

            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed
                    / ANIMATE_SPEEED);

            // LatLng endLatLng = getEndLatLng();
            // LatLng beginLatLng = getBeginLatLng();

            double lat = t * endLatLng.latitude + (1 - t)
                    * beginLatLng.latitude;
            double lng = t * endLatLng.longitude + (1 - t)
                    * beginLatLng.longitude;
            LatLng newPosition = new LatLng(lat, lng);

            trackingMarker.setPosition(newPosition);

            if (showPolyline) {
                updatePolyLine(newPosition);
            }

            // It's not possible to move the marker + center it through a
            // cameraposition update while another camerapostioning was already
            // happening.
            // navigateToPoint(newPosition,tilt,bearing,currentZoom,false);
            // navigateToPoint(newPosition,false);

            if (t < 1) {
                mHandler.postDelayed(this, 16);
            } else {

                System.out.println("Move to next marker.... current = "
                        + currentIndex + " and size = " + markers.size());
                // imagine 5 elements - 0|1|2|3|4 currentindex must be smaller
                // than 4
                if (currentIndex < markers.size() - 2) {

                    currentIndex++;

                    endLatLng = getEndLatLng();
                    beginLatLng = getBeginLatLng();

                    start = SystemClock.uptimeMillis();

                    // LatLng begin = getBeginLatLng();
                    LatLng end = getEndLatLng();

                    // float bearingL = bearingBetweenLatLngs(begin, end);

                    // highLightMarker(currentIndex);

                    // CameraPosition cameraPosition = new
                    // CameraPosition.Builder()
                    // .target(end)
                    // // changed this...
                    // .bearing(bearingL + BEARING_OFFSET).tilt(tilt)
                    // .zoom(googleMap.getCameraPosition().zoom).build();
                    //
                    // googleMap.animateCamera(CameraUpdateFactory
                    // .newCameraPosition(cameraPosition),
                    // ANIMATE_SPEEED_TURN, null);

                    // CameraUpdate center =
                    // CameraUpdateFactory.newLatLng(currentLatalng);
                    // CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);
                    // if (isCurLocCreated == false)
                    // {
                    // googleMap.moveCamera(center);
                    // googleMap.animateCamera(zoom);
                    // isCurLocCreated = true;
                    // }

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(end).zoom(14).build();
                    // CameraUpdate cameraUpdate = CameraUpdateFactory
                    // .newLatLng(end);
                    // CameraUpdate cameraZoom = CameraUpdateFactory.zoomTo(14);
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));

                    start = SystemClock.uptimeMillis();
                    mHandler.postDelayed(animator, 16);

                } else {
                    currentIndex++;
                    // highLightMarker(currentIndex);
                    stopAnimation();
                }

            }
        }

        private LatLng getEndLatLng() {
            return markers.get(currentIndex + 1).getPosition();
        }

        private LatLng getBeginLatLng() {
            return markers.get(currentIndex).getPosition();
        }

        // private void adjustCameraPosition() {
        // // System.out.println("tilt = " + tilt);
        // // System.out.println("upward = " + upward);
        // // System.out.println("zoom = " + zoom);
        // if (upward) {
        //
        // if (tilt < 90) {
        // tilt++;
        // zoom -= 0.01f;
        // } else {
        // upward = false;
        // }
        //
        // } else {
        // if (tilt > 0) {
        // tilt--;
        // zoom += 0.01f;
        // } else {
        // upward = true;
        // }
        // }
        // }
    }

    ;

    /**
     * Allows us to navigate to a certain point.
     */
    public void navigateToPoint(LatLng latLng, float tilt, float bearing,
                                float zoom, boolean animate) {
        CameraPosition position = new CameraPosition.Builder().target(latLng)
                .zoom(zoom).bearing(bearing).tilt(tilt).build();

        changeCameraPosition(position, animate);

    }

    public void navigateToPoint(LatLng latLng, boolean animate) {
        CameraPosition position = new CameraPosition.Builder().target(latLng)
                .build();
        changeCameraPosition(position, animate);
    }

    private void changeCameraPosition(CameraPosition cameraPosition,
                                      boolean animate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory
                .newCameraPosition(cameraPosition);

        if (animate) {
            googleMap.animateCamera(cameraUpdate);
        } else {
            googleMap.moveCamera(cameraUpdate);
        }

    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location loc = new Location("someLoc");
        loc.setLatitude(latLng.latitude);
        loc.setLongitude(latLng.longitude);
        return loc;
    }

    private float bearingBetweenLatLngs(LatLng begin, LatLng end) {
        Location beginL = convertLatLngToLocation(begin);
        Location endL = convertLatLngToLocation(end);

        return beginL.bearingTo(endL);
    }

    public void toggleStyle() {
        if (GoogleMap.MAP_TYPE_NORMAL == googleMap.getMapType()) {
            googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else {
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    /**
     * Adds a marker to the map.
     */
    public void addMarkerToMap(LatLng latLng) {
        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(latLng).icon(
                        BitmapDescriptorFactory
                                .fromResource(R.drawable.transparent)));
        markers.add(marker);

    }

    /**
     * Adds a marker to the map.
     */
    public void addMarkerToMapCustom(LatLng latLng) {
        List<Marker> cacheLoc = new ArrayList<Marker>();
        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(latLng).icon(
                        BitmapDescriptorFactory
                                .fromResource(R.drawable.transparent)));

        if (isMarkerAnimating == true) {
            cacheLoc.add(marker);
        } else {
            if (cacheLoc.size() > 1) {
                markers = cacheLoc;
                cacheLoc.clear();
            }
            markers.add(marker);
        }

        if (markers.size() > 4) {
            if (isMarkerAnimating == false) {
                animator.startAnimation(false);
            }
        }

    }

    /**
     * Clears all markers from the map.
     */
    public void clearMarkers() {
        googleMap.clear();
        markers.clear();
    }


    private void resetMarkers() {
        for (Marker marker : this.markers) {
            marker.setIcon(BitmapDescriptorFactory
                    .fromResource(R.drawable.transparent));
        }
    }

    private void dropPinEffect(final Marker marker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 500;

        final Interpolator interpolator = new BounceInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = Math.max(
                        1 - interpolator.getInterpolation((float) elapsed
                                / duration), 0);
                marker.setAnchor(0.5f, 1.0f + 9 * t);

                if (t > 0.0) {
                    // Post again 5ms later.
                    handler.postDelayed(this, 10);
                }
            }
        });
    }

    private void DropPinEffect(final Marker marker) {
        // final Handler handler = new Handler();
        // final long start = SystemClock.uptimeMillis();
        // final long duration = 300;
        //
        // final Interpolator interpolator = new
        // AnticipateOvershootInterpolator();
        //
        // handler.post(new Runnable()
        // {
        // @Override
        // public void run()
        // {
        // long elapsed = SystemClock.uptimeMillis() - start;
        // float t = Math.max(
        // 1 - interpolator.getInterpolation((float) elapsed
        // / duration), 0);
        // marker.setAnchor(0.5f, 1.0f + 7 * t);
        //
        // if (t > 0.0)
        // {
        // // Post again 15ms later.
        // handler.postDelayed(this, 50);
        // }
        // }
        // });

        final Handler handler = new Handler();

        final long startTime = SystemClock.uptimeMillis();
        final long duration = 2000;

        Projection proj = googleMap.getProjection();
        final LatLng markerLatLng = marker.getPosition();
        Point startPoint = proj.toScreenLocation(markerLatLng);
        startPoint.offset(0, -100);
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);

        final Interpolator interpolator = new BounceInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - startTime;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * markerLatLng.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * markerLatLng.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });

    }

    private void ShowEzeidDialog(String content) {
        ezeidLoadingProgress = new EzeidLoadingProgress(LandingPage.this, content);
        handlerDialog = new Handler();
        runnableDialog = new Runnable() {

            @Override
            public void run() {
                if (ezeidLoadingProgress != null) {
                    if (ezeidLoadingProgress.isShowing()) {
                        ezeidLoadingProgress.dismiss();
                    }
                }
            }
        };
        ezeidLoadingProgress.show();
    }

    private void DismissEzeidDialog() {
        if (null != runnableDialog) {
            handlerDialog.removeCallbacks(runnableDialog);
            if (ezeidLoadingProgress.isShowing()) {
                ezeidLoadingProgress.dismiss();
            }
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == animRotate) {
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void BindSearchListItems(JSONArray search_liat_array) {
        try {
            searchkeyListItems = new ArrayList<>();
            if (null != search_liat_array) {
                int len = search_liat_array.length();
                if (len != 0) {
                    searchPanel.collapsePanel();
                    searchkeyListItems.clear();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = search_liat_array
                                .getJSONObject(i);
                        tid = jsonObject.getString("TID");
                        tlatitude = jsonObject.getString("Latitude");
                        tlongitude = jsonObject.getString("Longitude");
                        roleType = jsonObject.getString("IDTypeID");
                        String ezeid = jsonObject.getString("EZEID");
                        openStatusS = jsonObject.getString("OpenStatus");
                        String distance = jsonObject.getString("distance");
                        if (roleType.equalsIgnoreCase("2")) {
                            markerTitle = jsonObject.getString("CompanyName");
                            if (markerTitle.equalsIgnoreCase("")) {
                                markerTitle = jsonObject.getString("Name");
                            }
                        } else if (roleType.equalsIgnoreCase("3")) {
                            markerTitle = jsonObject.getString("CompanyName");
                            if (markerTitle.equalsIgnoreCase("")) {
                                markerTitle = jsonObject.getString("Name");
                            }
                        } else {
                            markerTitle = jsonObject.getString("Name");
                            if (markerTitle.equalsIgnoreCase("")) {
                                markerTitle = jsonObject
                                        .getString("CompanyName");
                            }
                        }

                        SearchListItem searchListItem = new SearchListItem();
                        searchListItem.search_title = markerTitle;
                        searchListItem.search_tid = tid;
                        searchListItem.search_lati = tlatitude;
                        searchListItem.search_longi = tlongitude;
                        searchListItem.search_roletype = roleType;
                        searchListItem.search_openstatus = openStatusS;
                        searchListItem.search_distance = distance;
                        searchListItem.search_EZEID = ezeid;
                        searchkeyListItems.add(searchListItem);

                        EzeidRoute = new LatLng(Double.parseDouble(tlatitude),
                                Double.parseDouble(tlongitude));
                    }

                    if (searchkeyListItems.size() > 0) {
                        // searchFragmentListView.setVisibility(View.VISIBLE);
                        KeySearchListAdapter adapter = new KeySearchListAdapter(
                                searchkeyListItems);
                        // AlphaInAnimationAdapter alphaAdapter = new
                        // AlphaInAnimationAdapter(
                        // trItemsAdapter);
                        // searchFragmentListView
                        // .setAdapter(new ScaleInAnimationAdapter(
                        // alphaAdapter));
                        adapter.notifyDataSetChanged();
                        searchFragmentListView.setAdapter(adapter);
                        searchFragmentListView
                                .addItemDecoration(new DividerItemDecoration(
                                        LandingPage.this, null));
                        searchFragmentListView
                                .addOnItemTouchListener(new RecyclerItemClickListener(
                                        LandingPage.this,
                                        searchFragmentListView,
                                        new RecyclerItemClickListener.OnItemClickListener() {
                                            @Override
                                            public void onItemLongClick(
                                                    View view, int position) {
                                            }

                                            @Override
                                            public void onItemClick(View view,
                                                                    int position) {

                                                String tid = ((TextView) view
                                                        .findViewById(R.id.search_comp_tid))
                                                        .getText().toString();
                                                String ezeid = ((TextView) view
                                                        .findViewById(R.id.search_comp_ezeid))
                                                        .getText().toString();
                                                String seqno = ((TextView) view
                                                        .findViewById(R.id.search_comp_seqno))
                                                        .getText().toString();
                                                String pin = ((TextView) view
                                                        .findViewById(R.id.search_comp_pin))
                                                        .getText().toString();
                                                GetSearchInformation(tid,
                                                        searchTypeinfo, ezeid,
                                                        pin, seqno);
                                            }
                                        }));
                    }
                }
            } else {
                switch_toggle_result.setText("LIST VIEW");
                EzeidUtil.showToast(LandingPage.this, "No Results");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowWorkingHours(JSONObject obj) {
        try {
            if (obj != null) {
                JSONArray workingHoursArray = obj.getJSONArray("WorkingHours");
                if (workingHoursArray.length() != 0) {
                    for (int i = 0; i < workingHoursArray.length(); i++) {
                        final JSONObject workingHoursObj = workingHoursArray
                                .getJSONObject(i);

                        final View layout = View.inflate(LandingPage.this,
                                R.layout.working_hours_dialog, null);

                        TextView monday, tuesday, wednesday, thursday, friday, saturday, sunday;

                        monday = (TextView) layout
                                .findViewById(R.id.monday_time);
                        tuesday = (TextView) layout
                                .findViewById(R.id.tuesday_time);
                        wednesday = (TextView) layout
                                .findViewById(R.id.wednesday_time);
                        thursday = (TextView) layout
                                .findViewById(R.id.thursday_time);
                        friday = (TextView) layout
                                .findViewById(R.id.friday_time);
                        saturday = (TextView) layout
                                .findViewById(R.id.saturday_time);
                        sunday = (TextView) layout
                                .findViewById(R.id.sunday_time);

                        try {
                            monday.setText(workingHoursObj.getString("MO1")
                                    + " to" + workingHoursObj.getString("MO2")
                                    + " & " + workingHoursObj.getString("MO3")
                                    + "to " + workingHoursObj.getString("MO4"));
                            tuesday.setText(workingHoursObj.getString("TU1")
                                    + " to" + workingHoursObj.getString("TU2")
                                    + " & " + workingHoursObj.getString("TU3")
                                    + "to " + workingHoursObj.getString("TU4"));
                            wednesday
                                    .setText(workingHoursObj.getString("WE1")
                                            + " to"
                                            + workingHoursObj.getString("WE2")
                                            + " & "
                                            + workingHoursObj.getString("WE3")
                                            + " to "
                                            + workingHoursObj.getString("WE4"));
                            thursday.setText(workingHoursObj.getString("TH1")
                                    + " to " + workingHoursObj.getString("TH2")
                                    + " & " + workingHoursObj.getString("TH3")
                                    + " to " + workingHoursObj.getString("TH4"));
                            friday.setText(workingHoursObj.getString("FR1")
                                    + " to " + workingHoursObj.getString("FR2")
                                    + " & " + workingHoursObj.getString("FR3")
                                    + " to " + workingHoursObj.getString("FR4"));
                            saturday.setText(workingHoursObj.getString("SA1")
                                    + " to " + workingHoursObj.getString("SA2")
                                    + " & " + workingHoursObj.getString("SA3")
                                    + " to " + workingHoursObj.getString("SA4"));
                            sunday.setText(workingHoursObj.getString("SU1")
                                    + " to " + workingHoursObj.getString("SU2")
                                    + " & " + workingHoursObj.getString("SU3")
                                    + " to " + workingHoursObj.getString("SU4"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                LandingPage.this);
                        builder.setTitle(Html
                                .fromHtml("<font color=\"#009688\"><bold>Working Hours</bold></font"));
                        builder.setView(layout);
                        builder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.dismiss();
                                    }
                                });

                        AlertDialog alert = builder.create();
                        alert.show();

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getWorkingHours() {
        String locId = ezeidPreferenceHelper.GetValueFromSharedPrefs("LocID");
        ShowEzeidDialog("Loading...");
        if (!locId.equalsIgnoreCase("")) {
            if (EzeidUtil.isConnectedToInternet(LandingPage.this)) {
                NetworkHandler.GetWorkingHours(TAG, handler, token, locationID);
            } else {
                EzeidUtil.showToast(LandingPage.this,
                        "Please check your internet connection");
            }
        }
    }

    private void EditProfile() {
        try {
            Intent editIntent = new Intent(LandingPage.this, SignUp.class);
            editIntent.putExtra("edit", true);
            startActivity(editIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void ChangePwdForgotPwd() {

        final MaterialDialog changPwdDialog = new MaterialDialog(LandingPage.this);
        if (changPwdDialog != null) {
            RelativeLayout root = new RelativeLayout(LandingPage.this);
            View view = LayoutInflater.from(LandingPage.this).inflate(
                    R.layout.ezeid_change_pwd_layout, root);
            final EditText oldPwd = (EditText) view
                    .findViewById(R.id.oldPassword);
            final EditText newPwd = (EditText) view
                    .findViewById(R.id.newPassword);
            Button okCpwd = (Button) view.findViewById(R.id.okCpwd);
            Button cancelCpwd = (Button) view.findViewById(R.id.cancelCpwd);
            okCpwd.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    changPwdDialog.dismiss();
                    // action for change password
                    String oldPwdStr = oldPwd.getText().toString();
                    String newPwdStr = newPwd.getText().toString();

                    Map<String, String> params = new HashMap<>();
                    params.put("OldPassword", oldPwdStr);
                    params.put("NewPassword", newPwdStr);
                    params.put("Token", ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Token"));

                    // Log.e("Response", "" + params);

                    NetworkHandler.ChangePassword(TAG, handler, params);

                }
            });
            cancelCpwd.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    changPwdDialog.dismiss();
                }
            });

            changPwdDialog.setView(view).show();
        }
    }

    private void QuitApp() {

        ShowEzeidDialog("Sign Out...");
        String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, EZEIDUrlManager.getAPIUrl() + "ewLogout?Token="
                + token, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                // Log.d(TAG, response.toString());
                DismissEzeidDialog();
                QuitAppResponse(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // VolleyLog.d(TAG, "Error: " + error.getMessage());
                DismissEzeidDialog();
                if (error instanceof TimeoutError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Connection timeout. Please try again");
                } else if (error instanceof ServerError
                        || error instanceof AuthFailureError) {
                    EzeidUtil
                            .showToast(LandingPage.this,
                                    "Unable to connect server. Please try later");
                } else if (error instanceof NetworkError
                        || error instanceof NoConnectionError) {
                    EzeidUtil.showToast(LandingPage.this,
                            "Network is unreachable");
                } else {
                    EzeidUtil.showToast(LandingPage.this, "No Results");
                }
            }
        });
        EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest,
                TAG);
    }

    private void QuitAppResponse(JSONObject response) {
        try {
            String token = response.getString("Token");
            String auth = response.getString("IsAuthenticate");
            if (token.equalsIgnoreCase("") && auth.equalsIgnoreCase("false")) {
                ezeidPreferenceHelper.SaveValueToSharedPrefs("Token", "");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("LOGOUT", "true");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("FirstName", "");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("IDTypeID", "1");
                EzeidGlobal.userDetailsObj = null;
                finish();
            } else {
                ezeidPreferenceHelper.SaveValueToSharedPrefs("LOGOUT", "false");
                EzeidUtil.showToast(LandingPage.this, "Unable to logout");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void parsePasswordChangeResponse(JSONObject obj) {
        try {

            if (obj.getBoolean("IsChanged")) {
                EzeidUtil.showToast(LandingPage.this, "Password Changed");
            } else {
                EzeidUtil.showToast(LandingPage.this, "Password Change Failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ContactDesk() {
        try {

            GetEzeidSearchResult("ezeid", "Slide", "EZEID");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void HelpDesk() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(getString(R.string.help_url)));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void BusinessDesk() {
        try {
            startActivity(new Intent(LandingPage.this, BusinessManager.class));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void GetUserInfo() {
        try {
            if (EzeidGlobal.userDetailsObj == null) {
                String token = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("Token");
                JsonArrayRequest req = new JsonArrayRequest(EZEIDUrlManager.getAPIUrl()
                        + "ewtGetUserDetails?Token=" + token,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                // Log.d(TAG, response.toString());
                                EzeidGlobal.userDetailsObj = response;
                                BindUserInfo(response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        EzeidGlobal.userDetailsObj = null;
                        if (error instanceof TimeoutError) {
                            EzeidUtil.showToast(LandingPage.this,
                                    "Connection timeout. Please try again");
                        } else if (error instanceof ServerError
                                || error instanceof AuthFailureError) {
                            EzeidUtil
                                    .showToast(LandingPage.this,
                                            "Unable to connect server. Please try later");
                        } else if (error instanceof NetworkError
                                || error instanceof NoConnectionError) {
                            EzeidUtil.showToast(LandingPage.this,
                                    "Network is unreachable");
                        } else {
                            EzeidUtil
                                    .showToast(LandingPage.this,
                                            "Something went wrong. Please try later");
                        }
                    }
                });
                req.setShouldCache(false);
                req.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
                EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
            } else {
                BindUserInfo(EzeidGlobal.userDetailsObj);
            }

        } catch (Exception bug) {
            bug.printStackTrace();
        }
    }

    private void BindUserInfo(JSONArray jsonArray) {

        try {
            int lan = jsonArray.length();
            if (lan != 0) {
                progressProfile.setVisibility(View.INVISIBLE);
                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String masetrId = jsonObject.getString("MasterID");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("MasterID",
                        masetrId);
                String firstNameS = jsonObject.getString("FirstName");
                String lastNameS = jsonObject.getString("LastName");
                String EZEID = jsonObject.getString("EZEID");
//                String PIN = jsonObject.getString("PIN");
                String userType = jsonObject.getString("IDTypeID");
                emailS = jsonObject.getString("EMailID");
                picture = jsonObject.getString("Picture");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("IDTypeID",
                        userType);
                menuEmail.setText(emailS);
                menuUsername.setText(firstNameS + " " + lastNameS);
                String mobile = jsonObject.getString("MobileNumber");
                String phone = jsonObject.getString("PhoneNumber");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("EZEID", EZEID);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("USER_MOBILE",
                        mobile);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("USER_PHONE",
                        phone);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("USER_NAME",
                        firstNameS + " " + lastNameS);

                // if (userType.equalsIgnoreCase("1")) {
                // businessLayout.setVisibility(View.GONE);
                // } else if (userType.equalsIgnoreCase("2")) {
                // businessLayout.setVisibility(View.VISIBLE);
                // }

                if (!picture.equalsIgnoreCase("") && picture.length() > 15) {
                    Bitmap panelImg = EzeidUtil
                            .ByteTOBitmap(GetFileType(picture));
                    if (panelImg != null) {
                        profileImg.setImageBitmap(panelImg);
                    } else {
                        profileImg.setVisibility(View.INVISIBLE);
                    }
                } else {
                    profileImg.setVisibility(View.INVISIBLE);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String GetFileType(String picture) {
        String base64 = "";
        try {
            StringTokenizer tokenizer = new StringTokenizer(picture, "/");
            String srctype1 = tokenizer.nextToken();
            String srctype2 = tokenizer.nextToken();
            StringTokenizer tokenizer2 = new StringTokenizer(srctype2, ";");
            String type1 = tokenizer2.nextToken();
            if (type1.length() == 3) {
                base64 = picture.substring(22, picture.length());
            } else if (type1.length() == 4) {
                base64 = picture.substring(23, picture.length());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return base64;
    }


}