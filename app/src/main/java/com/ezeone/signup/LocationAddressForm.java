package com.ezeone.signup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.LandingPage;
import com.ezeone.R;
import com.ezeone.adapters.CropOptionAdapter;
import com.ezeone.backend.EzeidDbHelper;
import com.ezeone.helpers.Constant;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.helpers.GetMyAddress;
import com.ezeone.listeners.AddressResultListener;
import com.ezeone.listeners.GetMyAddressListener;
import com.ezeone.listeners.LocationResultListener;
import com.ezeone.location.FusedLocationService;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.pojo.CropOption;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.widget.cruton.Crouton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;

@SuppressLint("HandlerLeak") public class LocationAddressForm extends AppCompatActivity implements
		OnCameraChangeListener, AddressResultListener, LocationResultListener,
		GetMyAddressListener {

	private GoogleMap map;
	private Bundle bundle;
	private EditText locationTitle, address1, address2, postalCode, mobile,
			email, phone, pinCode, website;
	private FusedLocationService fusedLocationService;
	private int sessionRetry = 0;
	private Spinner mobileISDCode, phoneISDCode,parkingStatus;
	private boolean isSessionDialogShown = false;
	private LatLng myLatLng = null;
	private String[] parking = new String[] { "Parking Status",
			"Public Parking", "Valet Parking", "No Parking" };
	private TextView countryId, stateId, cityId, pictureText, iconText;
	private AutoCompleteTextView locationSearch, country, state, city;
	private ImageView clearLocationField, picture, icon;
	private CheckBox enablePin;
	private Button searchButton;
//	private EzeidProgress eProgress;
	public static int resumeValue = 0;
	private Location location = null;
	private Pattern pattern;
	private Matcher matcher;
	private LocationManager locationManager;
	private byte[] imageByteArray, compressedImageByteArray;
	private String lat, lon, imgPath, fileName, selectedImagePath,
			autocompleteText, url, TID, TAG, imageBase64String = "",
			compressedImageBase64String = "",numberRegex = "\\d+";
	private ScrollView scrollView;
	private CameraPosition centerPosVal;
	private GetMyAddress address;
	private List<String> placesList = new ArrayList<String>();
	private ArrayList<HashMap<String, String>> countries = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> states = new ArrayList<HashMap<String, String>>();
	private Map<String, String> params;
	private Address mLastKnownAddress;
	private AddressResultListener mAddressResultListener;
	private EzeidPreferenceHelper mgr;
	private Toolbar mToolbar;
	private LinearLayout pictureIconLayout;
	private double altitude = 0;
	private Uri mImageCaptureUri;
	private static final int CROP_FROM_CAMERA = 10;
//	private AlertDialog.Builder builder1;
	private String ezeid,password,title,firstName,lastName,companyName,jobTitle,aboutCompany;
	private int selectionType,accountType;
	private EzeidDbHelper helper;
//	private TextInputLayout locationTitleTI,address1TI,address2TI,countryTI,stateTI,cityTI,postalCodeTI,mobileTI,phoneTI,emailTI,websiteTI,pinTI;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location_address_form);

		initUiComponents();
		setUpMap();
		imageBase64String = "";
		helper = new EzeidDbHelper(getApplicationContext());
		countries = helper.getAllCountries();
		
		final SimpleAdapter spinnerAdapter = new SimpleAdapter(
				getApplicationContext(), countries,
				R.layout.isdcode_spinner_item, new String[] {
						"ISDCode", "CountryID" }, new int[] {
						R.id.isd_code, R.id.countryCodeISD });

		final SimpleAdapter adapter = new SimpleAdapter(
				getApplicationContext(), countries,
				R.layout.country_list_item, new String[] {
						"CountryName", "CountryID" }, new int[] {
						R.id.countryTitle, R.id.countryId });
		country.setAdapter(adapter);
		country.setThreshold(1);
		mobileISDCode.setAdapter(spinnerAdapter);
		phoneISDCode.setAdapter(spinnerAdapter);
		
		if (bundle != null) {
			for (int i = 0; i < countries.size(); i++) {
				if (countries.get(i).get("ISDCode")
						.equals(bundle.getString("mobileISD"))) {
					mobileISDCode.setSelection(i);
				}

				if (countries.get(i).get("ISDCode")
						.equals(bundle.getString("phoneISD"))) {
					phoneISDCode.setSelection(i);
				}
			}
			
			if (bundle != null) {
				if (bundle.getInt("CountryID") != 0) {
					if (String.valueOf(bundle.getInt("CountryID")) != null) {
						HashMap<String, String> countryObj = countries
								.get(bundle.getInt("CountryID") - 1);
						country.setText(countryObj.get("CountryName"));
						countryId.setText(String.valueOf(bundle
								.getInt("CountryID")));
						mgr.SaveValueToSharedPrefs("countryId",
								"" + bundle.getInt("CountryID"));
//						eProgress.show();
						EzeidUtil.ShowEzeidDialog(LocationAddressForm.this, "Loading");
						NetworkHandler.GetStates(TAG, locationsHandler,
								params, getApplicationContext());
					}
				}
			}
		}

		country.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int arg2, long arg3) {

				String id = ((TextView) view
						.findViewById(R.id.countryId)).getText()
						.toString();
				String title = ((TextView) view
						.findViewById(R.id.countryTitle)).getText()
						.toString();

				country.setText(title);
				countryId.setText(id);

				for (int i = 0; i < countries.size(); i++) {
					if (countries.get(i).get("CountryID").equals(id)) {
						mobileISDCode.setSelection(i);
						phoneISDCode.setSelection(i);
					}
				}

				mgr.SaveValueToSharedPrefs("countryId", countryId
						.getText().toString());
//				eProgress.show();
				EzeidUtil.ShowEzeidDialog(LocationAddressForm.this, "Loading");
				NetworkHandler.GetStates(TAG, locationsHandler, params,
						getApplicationContext());
			}
		});
		
		if (EzeidUtil.isConnectedToInternet(LocationAddressForm.this)) {
			params = new HashMap<String, String>();
//			NetworkHandler.GetCountries(TAG, locationsHandler, params);
		} else {
			EzeidUtil.showToast(LocationAddressForm.this, getResources()
					.getString(R.string.internet_warning));
		}

		clearLocationField.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (locationSearch.getText().toString().length() > 0) {
					locationSearch.setText("");
				}
			}
		});
		
		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (EzeidUtil.isConnectedToInternet(LocationAddressForm.this)) {
					String place = locationSearch.getText().toString();
//					eProgress.show();
					EzeidUtil.ShowEzeidDialog(LocationAddressForm.this, "Loading");
					getGeoLocation(place);
				} else {
					EzeidUtil
							.showToast(LocationAddressForm.this, getResources()
									.getString(R.string.internet_warning));
				}
			}
		});

		picture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// startDialog();
				if (mgr.GetValueFromSharedPrefs("IDTypeID").equals("2")) {
					if (mgr.GetValueFromSharedPrefs("EZEIDVerifiedID").equals(
							"2")) {
						selectImage();
					} else {
						EzeidUtil.showToast(LocationAddressForm.this,
								"You can add photo after verification");
					}
				} else {
					selectImage();
				}
			}
		});

		if (mgr.GetValueFromSharedPrefs("IDTypeID").equals("2")) {
			if (mgr.GetValueFromSharedPrefs("EZEIDVerifiedID").equals("2")) {
				icon.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						selectImage();
					}
				});
			}
		}

		locationSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence charseq, int start,
					int before, int count) {
				if (locationSearch.getText().toString().length() == 0) {
					clearLocationField.setVisibility(View.INVISIBLE);
				} else {
					clearLocationField.setVisibility(View.VISIBLE);
					if (charseq.length() > 5) {
						handler.removeMessages(EzeidGlobal.EZEID_SEARCH_LOC);
						handler.sendEmptyMessageDelayed(
								EzeidGlobal.EZEID_SEARCH_LOC,
								EzeidGlobal.EZEID_DELAY_IN_MILLIS);

						autocompleteText = charseq.toString().replace(" ",
								"%20");

						final Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
							}
						}, 2000);
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}

			@Override
			public void afterTextChanged(final Editable charseq) {}
		});

		if (bundle != null) {
			if (bundle.getInt("primary") == 0) {
				locationTitle.setText("Primary location");
				if (bundle.getInt("Type") == 2) {
					if (mgr.GetValueFromSharedPrefs("IDTypeID").equals("2")) {
						picture.setVisibility(View.VISIBLE);
						pictureText.setVisibility(View.VISIBLE);
//						iconText.setVisibility(View.VISIBLE);
						icon.setVisibility(View.VISIBLE);
						pictureIconLayout.setVisibility(View.VISIBLE);
						parkingStatus.setVisibility(View.VISIBLE);
					}else{
						picture.setVisibility(View.GONE);
						pictureText.setVisibility(View.GONE);
						iconText.setVisibility(View.GONE);
						icon.setVisibility(View.GONE);
						pictureIconLayout.setVisibility(View.GONE);
						parkingStatus.setVisibility(View.VISIBLE);
					}
				}
				locationTitle.setEnabled(false);
				accountType = bundle.getInt("Type");
				selectionType = bundle.getInt("selectionType");

				address1.setText(bundle.getString("address1"));
				address2.setText(bundle.getString("address2"));

				postalCode.setText(bundle.getString("pincode"));
				email.setText(bundle.getString("email"));
				phone.setText(bundle.getString("phoneNumber"));
				mobile.setText(bundle.getString("mobileNumber"));
				website.setText(bundle.getString("website"));
				pinCode.setText(bundle.getString("PIN"));
				lat = bundle.getString("lat");
				lon = bundle.getString("lon");
				imageBase64String = bundle.getString("picture");
				city.setText(bundle.getString("cityName"));
				stateId.setText(bundle.getString("stateId"));
//				phoneISDCode.setSelection(bundle.getInt("phoneIsdPos"));
//				mobileISDCode.setSelection(bundle.getInt("mobileIsdPos"));
				countryId.setText(bundle.getString("countryId"));
				country.setText(bundle.getString("countryName"));
				state.setText(bundle.getString("stateName"));
				
				/*Log.e("Parking", ""+bundle.getString("parkingStatus"));*/
				
				if(bundle.getString("parkingStatus")!=null){
					parkingStatus.setSelection(Integer.parseInt(bundle.getString("parkingStatus")));
				}
				
				if (imageBase64String != null && !imageBase64String.isEmpty()) {
					convertImagefromBase64(EzeidUtil
							.GetFileType(imageBase64String));
				}
				if (lat != null && lon != null) {
					SetMapPoint(lat, lon);
				}

				ezeid = bundle.getString("ezeid");
				password = bundle.getString("password");
				title = bundle.getString("title");
				firstName= bundle.getString("firstName");
				lastName = bundle.getString("lastname");
				companyName = bundle.getString("companyName");
				jobTitle = bundle.getString("jobTitle");
				aboutCompany = bundle.getString("aboutCompany");
				
			} else if (bundle.getInt("primary") == 2) {
				
				address1.setText(bundle.getString("address1"));
				address2.setText(bundle.getString("address2"));

				postalCode.setText(bundle.getString("pincode"));
				email.setText(bundle.getString("email"));
				phone.setText(bundle.getString("phoneNumber"));
				mobile.setText(bundle.getString("mobileNumber"));
				website.setText(bundle.getString("Website"));
				
//				Log.e("Parking 2", ""+bundle.getString("parkingStatus"));
				
				if (mgr.GetValueFromSharedPrefs("IDTypeID").equals("2")) {
					picture.setVisibility(View.VISIBLE);
				}

				if (mgr.GetValueFromSharedPrefs("IDTypeID").equals("2")) {
						parkingStatus.setVisibility(View.VISIBLE);
				}
				
				lat = bundle.getString("lat");
				lon = bundle.getString("lon");
				locationTitle.setText(bundle.getString("locTitle"));
				imageBase64String = bundle.getString("picture");
				TID = bundle.getString("locationId");
				city.setText(bundle.getString("cityName"));
				compressedImageBase64String = "";
				if (bundle.getString("icon") != null) {
					compressedImageBase64String = bundle.getString("icon");
				}
				
				if(!imageBase64String.isEmpty()){
					convertImagefromBase64(EzeidUtil.GetFileType(imageBase64String));
				}
				
				if (!compressedImageBase64String.isEmpty()) {
					convertIconfromBase64(EzeidUtil
							.GetFileType(compressedImageBase64String));
				}

				SetMapPoint(lat, lon);

			} else if (bundle.getInt("primary") == 3) {
				
				address1.setText(bundle.getString("AddressLine1"));
				address2.setText(bundle.getString("AddressLine2"));
				TID = bundle.getString("TID");

				postalCode.setText(bundle.getString("PostalCode"));
				email.setText(bundle.getString("EMailID"));
				phone.setText(bundle.getString("PhoneNumber"));
				mobile.setText(bundle.getString("MobileNumber"));
				website.setText(bundle.getString("WebSite"));
				lat = bundle.getString("Latitude");
				lon = bundle.getString("Longitude");
				locationTitle.setText(bundle.getString("LocTitle"));
				imageBase64String = bundle.getString("picture");
				countryId.setText(String.valueOf(bundle.getInt("CountryID")));
				stateId.setText(String.valueOf(bundle.getInt("StateID")));
				cityId.setText(String.valueOf(bundle.getInt("CityID")));
				city.setText(bundle.getString("cityName"));
				compressedImageBase64String = "";
				if (bundle.getString("icon") != null) {
					compressedImageBase64String = bundle.getString("icon");
				}
				
				if (!imageBase64String.isEmpty()) {
					convertImagefromBase64(EzeidUtil.GetFileType(imageBase64String));
				}
				
				if (!compressedImageBase64String.isEmpty()) {
					convertIconfromBase64(EzeidUtil
							.GetFileType(compressedImageBase64String));
				}
				SetMapPoint(lat, lon);
			} else {
				TID = bundle.getString("TID");
			}
		}

		enablePin.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean enabled) {
				if (enabled) {
					pinCode.setVisibility(View.VISIBLE);
				} else {
					pinCode.setVisibility(View.INVISIBLE);
				}
			}
		});

		parkingStatus.setAdapter(new ParkingAdapter(LocationAddressForm.this,
				R.layout.category_list_item, parking));
		
		if(bundle!=null){
			if(bundle.getString("parkingStatus")!=null){
				parkingStatus.setSelection(Integer.parseInt(bundle.getString("parkingStatus")));
			}
		}

		ArrayAdapter<String> locadapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, placesList);
		locationSearch.setAdapter(locadapter);
		mAddressResultListener = (AddressResultListener) this;
		mAddressResultListener.onAddressAvailable(mLastKnownAddress);
	}

	private void setUpMap() {
		SupportMapFragment fm = (ScrollableMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.mapFragment);
		map = fm.getMap();
		map.setMyLocationEnabled(true);
		map.getUiSettings().setZoomControlsEnabled(true);
		map.getUiSettings().setRotateGesturesEnabled(false);
		map.setOnCameraChangeListener(this);

		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//		Criteria criteria = new Criteria();
//		String provider = locationManager.getBestProvider(criteria, true);
//		Location location = locationManager.getLastKnownLocation(provider);
		/*
		 * if (location != null) { onLocationChanged(location); }
		 */
		// locationManager.requestLocationUpdates(provider, 20000, 0, this);

		((ScrollableMapFragment) getSupportFragmentManager().findFragmentById(
				R.id.mapFragment))
				.setListener(new ScrollableMapFragment.OnTouchListener() {
					@Override
					public void onTouch() {
						scrollView.requestDisallowInterceptTouchEvent(true);
					}
				});
	}

	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		getMenuInflater().inflate(R.menu.locations_menu, menu);
		return true;
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.save_location) {
			if (EzeidUtil.isConnectedToInternet(LocationAddressForm.this)) {
				if (bundle.getInt("primary") == 0) {
					/* Save Primary location */
					if (validateFields()) {
						saveData();
					}
				}
				else if (bundle.getInt("primary") == 2) {
					saveData();
				}
				else {
					/* Save other locations */
					if (!TID.equalsIgnoreCase("0")) {
						if (validateFields()) {
							if (!locationTitle.getText().toString().isEmpty()) {
								params.put("TID", TID);
								params.put("Token",
										mgr.GetValueFromSharedPrefs("Token"));
								params.put("LocTitle", locationTitle.getText()
										.toString());
								params.put("Latitude", lat);
								params.put("Longitude", lon);
								params.put("Altitude", ""+altitude);
								params.put("AddressLine1", address1.getText()
										.toString());
								params.put("AddressLine2", address2.getText()
										.toString());
								params.put("CityTitle", city.getText()
										.toString());
								params.put("StateID", stateId.getText()
										.toString());
								params.put("CountryID", countryId.getText()
										.toString());
								params.put("PostalCode", postalCode.getText()
										.toString());
								if (pinCode.getText().toString().isEmpty()) {
									params.put("PIN", "");
								} else {
									params.put("PIN", pinCode.getText()
											.toString());
								}

								params.put("PhoneNumber", phone.getText()
										.toString());
								params.put("MobileNumber", mobile.getText()
										.toString());
								params.put("EMailID", email.getText()
										.toString());
								params.put("Picture",
										imageBase64String.toString());

								if (imageBase64String.isEmpty()) {
									params.put("PictureFileName", fileName);
								} else {
									params.put("PictureFileName", "");
								}

								params.put("Website", website.getText()
										.toString());

								params.put(
										"ISDMobileNumber",
										countries
												.get(mobileISDCode
														.getSelectedItemPosition())
												.get("ISDCode"));
								params.put(
										"ISDPhoneNumber",
										countries
												.get(phoneISDCode
														.getSelectedItemPosition())
												.get("ISDCode"));

								params.put(
										"ParkingStatus",
										""
												+ parkingStatus
														.getSelectedItemPosition());

								NetworkHandler.SaveOtherLocations(TAG,
										locationsHandler, params);
							} else {
								locationTitle.setError(getResources()
										.getString(R.string.required));
//								locationTitleTI.setError(getResources()
//										.getString(R.string.required));
							}
						}
						// Log.e("location save params --1-- ", "" + params);
					} else {
						if (validateFields()) {
							if (!locationTitle.getText().toString().isEmpty()) {
								params.put("TID", "0");
								params.put("Token",
										mgr.GetValueFromSharedPrefs("Token"));
								params.put("LocTitle", locationTitle.getText()
										.toString());
								params.put("Latitude", lat);
								params.put("Longitude", lon);
								params.put("Altitude", ""+altitude);
								params.put("AddressLine1", address1.getText()
										.toString());
								params.put("AddressLine2", address2.getText()
										.toString());
								params.put("CityTitle", city.getText()
										.toString());
								params.put("StateID", stateId.getText()
										.toString());
								params.put("CountryID", countryId.getText()
										.toString());
								params.put("PostalCode", postalCode.getText()
										.toString());
								if (pinCode.getText().toString().isEmpty()) {
									params.put("PIN", "");
								} else {
									params.put("PIN", pinCode.getText()
											.toString());
								}

								params.put("PhoneNumber", phone.getText()
										.toString());
								params.put("MobileNumber", mobile.getText()
										.toString());
								params.put("EMailID", email.getText()
										.toString());
								params.put("Picture",
										imageBase64String.toString());
								if (imageBase64String.isEmpty()) {
									params.put("PictureFileName", "");
								} else {
									params.put("PictureFileName", fileName);
								}

								params.put("Website", website.getText()
										.toString());

								params.put(
										"ISDMobileNumber",
										countries
												.get(mobileISDCode
														.getSelectedItemPosition())
												.get("ISDCode"));
								params.put(
										"ISDPhoneNumber",
										countries
												.get(phoneISDCode
														.getSelectedItemPosition())
												.get("ISDCode"));

								params.put(
										"ParkingStatus",
										""
												+ parkingStatus
														.getSelectedItemPosition());

								NetworkHandler.SaveOtherLocations(TAG,
										locationsHandler, params);
							} else {
								locationTitle.setError(getResources()
										.getString(R.string.required));
//								locationTitleTI.setError(getResources()
//										.getString(R.string.required));
							}
						}
						// Log.e("location save params --2-- ", "" + params);
					}
				}
			} else {
				EzeidUtil.showToast(LocationAddressForm.this, getResources()
						.getString(R.string.internet_warning));
			}
		}
		return false;
	}

	Handler locationsHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.arg1) {
			case Constant.MessageState.LOCATION_AVAILABLE:
				parseResponse((JSONObject) msg.obj);
				break;

			case Constant.MessageState.LOCATION_UNAVAILABLE:
				EzeidUtil.showError(LocationAddressForm.this, (VolleyError)msg.obj);
				break;

			case Constant.MessageState.STATES_AVAILABLE:
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				parseStates((JSONArray) msg.obj);

				if (bundle != null) {
					if (String.valueOf(bundle.getInt("StateID")) != null) {
						if (bundle.getInt("StateID") != 0) {
							for (int i = 0; i < states.size(); i++) {
								HashMap<String, String> stateObj = states
										.get(i);
								if (stateObj.get("StateID")
										.equals(String.valueOf(bundle
												.getInt("StateID")))) {
									state.setText(stateObj.get("StateName"));
									stateId.setText(String.valueOf(bundle
											.getInt("StateID")));
								}
							}
						}
					}
				}
				break;

			case Constant.MessageState.STATES_UNAVAILABLE:
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(LocationAddressForm.this, (VolleyError)msg.obj);
				break;
				
				
				
				case Constant.MessageState.PRIMARY_DATA_SAVE_SUCCESS:
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				parseSaveResponse((JSONObject) msg.obj);
				break;

			case Constant.MessageState.PRIMARY_DATA_SAVE_FAIL:
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(LocationAddressForm.this, (VolleyError)msg.obj);
				break;

			/*
			 * case Constant.MessageState.CITIES_AVAILABLE:
			 * parseCities((JSONArray) msg.obj); break;
			 * 
			 * case Constant.MessageState.CITIES_UNAVAILABLE: break;
			 */

			case Constant.MessageState.LOCATION_SAVE_SUCCESS:
				parseLocationSaveResponse((String) msg.obj);
				break;

			case Constant.MessageState.LOCATION_SAVE_FAILED:
				EzeidUtil.showError(LocationAddressForm.this, (VolleyError)msg.obj);
				break;

			case Constant.MessageState.SESSION_EXPIRED:
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				break;

			case Constant.MessageState.SESSION_EXPIRED_ERROR:
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(LocationAddressForm.this, (VolleyError)msg.obj);
				break;
			}
		}
	};

	public void parseResponse(JSONObject obj) {
		try {
			if (obj.has("status")) {
				if (obj.getString("status").equalsIgnoreCase("OK")) {
					if (placesList.size() > 0) {
						placesList.clear();
					}

					if (obj.has("predictions")) {
						JSONArray places = obj.getJSONArray("predictions");
						for (int i = 0; i < places.length(); i++) {
							String placeObj = places.getJSONObject(i)
									.getString("description");
							placesList.add(placeObj);
						}
					}

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							this, android.R.layout.simple_dropdown_item_1line,
							placesList);

					locationSearch.setAdapter(adapter);
					locationSearch
							.setOnItemClickListener(new OnItemClickListener() {
								@Override
								public void onItemClick(AdapterView<?> arg0,
										View arg1, int arg2, long arg3) {
									getGeoLocation(locationSearch.getText()
											.toString());
								}
							});
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void parseStates(JSONArray obj) {
		try {
			if (obj.length() > 0) {
				states.clear();
//				eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();

				for (int i = 0; i < obj.length(); i++) {
					JSONObject stateObj = obj.getJSONObject(i);

					HashMap<String, String> map = new HashMap<String, String>();
					map.put("StateName", stateObj.getString("StateName"));
					map.put("StateID", stateObj.getString("StateID"));
					states.add(map);
				}

				final SimpleAdapter adapter = new SimpleAdapter(
						getApplicationContext(), states,
						R.layout.state_list_item, new String[] { "StateName",
								"StateID" }, new int[] { R.id.stateTitle,
								R.id.stateId });
				state.setThreshold(1);
				state.setAdapter(adapter);

				state.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View view,
							int arg2, long arg3) {

						state.setText(((TextView) view
								.findViewById(R.id.stateTitle)).getText()
								.toString());
						stateId.setText(((TextView) view
								.findViewById(R.id.stateId)).getText()
								.toString());

						mgr.SaveValueToSharedPrefs("stateId", stateId.getText()
								.toString());
						NetworkHandler.GetCities(TAG, locationsHandler, params,
								getApplicationContext());
					}
				});
			}
		} catch (Exception e) {
//			eProgress.dismiss();
			EzeidUtil.DismissEzeidDialog();
		}
	}

	/*
	 * private void parseCities(JSONArray obj) { try { if (obj.length() > 0) {
	 * for (int i = 0; i < obj.length(); i++) { JSONObject cityObj =
	 * obj.getJSONObject(i); City city = new City();
	 * city.setCityID(cityObj.getString("CityID"));
	 * city.setCityName(cityObj.getString("CityName")); cities.add(city); }
	 * 
	 * final CityAdapter adapter = new CityAdapter( getApplicationContext(),
	 * R.layout.category_list_item, cities); city.setThreshold(1);
	 * city.setAdapter(adapter);
	 * 
	 * city.setOnItemClickListener(new OnItemClickListener() {
	 * 
	 * @Override public void onItemClick(AdapterView<?> arg0, View arg1, int
	 * arg2, long arg3) { city.setText(adapter.getItem(arg2).getCityName());
	 * cityId.setText(adapter.getItem(arg2).getCityID()); } }); } } catch
	 * (Exception e) {
	 * 
	 * } }
	 */

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == EzeidGlobal.EZEID_SEARCH_LOC) {
				String dropStr = locationSearch.getText().toString();
				if (dropStr.trim().length() > 5) {
					url = EzeidGlobal.PLACES_API_BASE + EzeidGlobal.OUT_JSON
							+ "input=" + autocompleteText.toString() + "&key="
							+ getResources().getString(R.string.location_api);
					Map<String, String> params = new HashMap<String, String>();
					params.put("location", autocompleteText.toString());
					NetworkHandler.PlacesAutocomplete(TAG, locationsHandler,
							params, url);
				}
			}

			switch (msg.arg1) {
			case EzeidGlobal.CAMERA_CHANGE_CALLBACK:
				CameraPosition pos = (CameraPosition) msg.obj;
				centerPosVal = pos;
				setOnCamChanged(pos);
				break;
			}
		};
	};

	private void getGeoLocation(String location) {
//		eProgress.dismiss();
		EzeidUtil.DismissEzeidDialog();
		Geocoder geocoder = new Geocoder(getApplicationContext());
		try {
			List<Address> locations = geocoder.getFromLocationName(location, 1);
			map.clear();
			for (Address a : locations) {
				double lati = a.getLatitude();
				double longi = a.getLongitude();
				LatLng pos = new LatLng(lati, longi);
				map.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));
				String latStr = String.valueOf(lati);
				String longStr = String.valueOf(longi);
				SetPickpoint(latStr, longStr);
			}
		} catch (Exception e) {
			e.printStackTrace();
			EzeidUtil.showToast(LocationAddressForm.this,
					"That place couldn't be located");
		}
	}

	@Override
	public void onCameraChange(CameraPosition position) {
		handler.removeCallbacksAndMessages(null);
		Message msg = new Message();
		msg.arg1 = EzeidGlobal.CAMERA_CHANGE_CALLBACK;
		msg.obj = position;
		handler.sendMessageDelayed(msg, EzeidGlobal.CAMERA_DELAY);
	}

	private void setOnCamChanged(CameraPosition position) {
		if (position != null) {
			try {
				LatLng loc = position.target;
				double lati = loc.latitude;
				double longi = loc.longitude;

				lat = "" + lati;
				lon = "" + longi;

				if (address == null) {
					address = new GetMyAddress(getApplicationContext(), lati,
							longi, this);
					address.execute();
				}

				lat = String.valueOf(loc.latitude);
				lon = String.valueOf(loc.longitude);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void MyAddress(String address) {
		if (!TextUtils.isEmpty(address)) {
			locationSearch.setText(address + ".");
		} else {
			handler.removeCallbacksAndMessages(null);
			Message msg = new Message();
			msg.arg1 = EzeidGlobal.CAMERA_CHANGE_CALLBACK;
			msg.obj = centerPosVal;
			handler.sendMessageDelayed(msg, EzeidGlobal.CAMERA_DELAY);
		}
		this.address = null;
	}

	@Override
	public void onAddressAvailable(Address address) {
	}

	private void SetPickpoint(String lati, String longi) {
		try {
			LatLng target = new LatLng(Double.parseDouble(lati),
					Double.parseDouble(longi));
			Builder builder = new Builder();
			builder.zoom(15);
			builder.target(target);

			map.animateCamera(
					CameraUpdateFactory.newCameraPosition(builder.build()),
					new CancelableCallback() {
						@Override
						public void onFinish() {
							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									map.setOnCameraChangeListener(LocationAddressForm.this);
								}
							}, 300);
						}

						@Override
						public void onCancel() {
							new Handler().postDelayed(new Runnable() {
								@Override
								public void run() {
									map.setOnCameraChangeListener(LocationAddressForm.this);
								}
							}, 300);
						}
					});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Uri setImageUri() {
		// Store image in dcim
		File file = new File(Environment.getExternalStorageDirectory()
				+ "/DCIM/", "image" + fileName);
		Uri imgUri = Uri.fromFile(file);
		this.imgPath = file.getAbsolutePath();
		return imgUri;
	}

	public String getImagePath() {
		return imgPath;
	}

	public String getAbsolutePath(Uri uri) {
		String[] projection = { MediaColumns.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} else
			return null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode != RESULT_CANCELED) {
			if (requestCode == EzeidGlobal.PICK_IMAGE) {
				mImageCaptureUri = data.getData();
				selectedImagePath = getAbsolutePath(data.getData());
//				picture.setImageBitmap(EzeidUtil.decodeFile(selectedImagePath));
				imageByteArray = EzeidUtil.getBytesFromBitmap(EzeidUtil
						.decodeFile(selectedImagePath));
				imageBase64String = Base64.encodeToString(imageByteArray,
						Base64.DEFAULT);
				compressedImageBase64String = compressImageForIcon(EzeidUtil
						.decodeFile(selectedImagePath));
				
				doCrop();
				

			} else if (requestCode == EzeidGlobal.CAPTURE_IMAGE) {
				mImageCaptureUri = setImageUri();
				selectedImagePath = getImagePath();
//				picture.setImageBitmap(EzeidUtil.decodeFile(selectedImagePath));
				imageByteArray = EzeidUtil.getBytesFromBitmap(EzeidUtil
						.decodeFile(selectedImagePath));
				imageBase64String = Base64.encodeToString(imageByteArray,
						Base64.DEFAULT);
				compressedImageBase64String = compressImageForIcon(EzeidUtil
						.decodeFile(selectedImagePath));
				
				doCrop();
			}

			else if (requestCode == 300) {
				byte[] imageByte = data.getByteArrayExtra("croppedImage");
//				String mime = data.getStringExtra("mimeType");
				try {
					Bitmap bmap = BitmapFactory.decodeByteArray(imageByte, 0,
							imageByte.length);

					compressImageForIcon(bmap);

					Bitmap compressBitmap = BitmapFactory.decodeByteArray(
							compressedImageByteArray, 0,
							compressedImageByteArray.length);

					picture.setImageBitmap(bmap);
					if (!mgr.GetValueFromSharedPrefs("IDTypeID").equals("2")) {
						icon.setImageBitmap(compressBitmap);
						compressedImageBase64String = "data:" + EzeidUtil.getMimeType(selectedImagePath)
								+ ";base64," + compressImageForIcon(bmap);
					}

					imageByteArray = EzeidUtil.getBytesFromBitmap(bmap);
					imageBase64String = "data:"
							+ EzeidUtil.getMimeType(selectedImagePath)
							+ ";base64,"
							+ Base64.encodeToString(imageByteArray,
									Base64.DEFAULT);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			else if (requestCode == 400) {
				byte[] imageByte = data.getByteArrayExtra("croppedImage");
//				String mime = data.getStringExtra("mimeType");
				try {
					Bitmap bmap = BitmapFactory.decodeByteArray(imageByte, 0,
							imageByte.length);

					compressImageForIcon(bmap);

					Bitmap compressBitmap = BitmapFactory.decodeByteArray(
							compressedImageByteArray, 0,
							compressedImageByteArray.length);

					icon.setImageBitmap(compressBitmap);

					compressedImageBase64String = "data:" + EzeidUtil.getMimeType(selectedImagePath) + ";base64,"
							+ compressImageForIcon(compressBitmap);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}else if(requestCode == CROP_FROM_CAMERA){
				Bundle extras = data.getExtras();
		        if (extras != null) {	        	
		            Bitmap photo = extras.getParcelable("data");
		            	try{
		            	 imageByteArray = EzeidUtil.getBytesFromBitmap(photo);
							imageBase64String = "data:"
									+ EzeidUtil.getMimeType(selectedImagePath) + ";base64,"
									+ Base64.encodeToString(imageByteArray, Base64.DEFAULT);
							
				            picture.setImageBitmap(photo);
		            	}catch(NullPointerException e){
//		            		showUnsupportedFileDialog();
		            	}
		        }
			}  
		}
	}

	public void parseLocationSaveResponse(String obj) {
		try {
			if (obj.contains("[{\"TID\"")) {
				EzeidUtil.showToast(LocationAddressForm.this, "Save Successful");
				resumeValue = 1;
				finish();
			} else {
				EzeidUtil.showToast(LocationAddressForm.this, "Save Failed");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void SetMapPoint(String lat, String lon) {
		LatLng target = new LatLng(Double.parseDouble(lat),
				Double.parseDouble(lon));
		Builder builder = new Builder();
		builder.zoom(15);
		builder.target(target);
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(target, 15));
	}

	public void convertImagefromBase64(String image) {
		if (!image.isEmpty() && !image.equalsIgnoreCase("null")) {
			byte[] imageAsBytes = Base64.decode(image.getBytes(),
					Base64.DEFAULT);
			ImageView picture = (ImageView) this.findViewById(R.id.picture);
			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);
			picture.setImageBitmap(bitmap);
		} else {
			Drawable myDrawable = getResources().getDrawable(
					R.drawable.ic_launcher);
			picture.setImageDrawable(myDrawable);
		}
	}

	public void convertIconfromBase64(String icon) {
		if (!icon.isEmpty() && !icon.equalsIgnoreCase("null")) {
			byte[] imageAsBytes = Base64
					.decode(icon.getBytes(), Base64.DEFAULT);
			ImageView picture = (ImageView) this.findViewById(R.id.icon);
			Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0,
					imageAsBytes.length);
			picture.setImageBitmap(bitmap);
		} else {
			Drawable myDrawable = getResources().getDrawable(
					R.drawable.ic_launcher);
			picture.setImageDrawable(myDrawable);
		}
	}

	public boolean validateEmail(final String hex) {
		matcher = pattern.matcher(hex);
		return matcher.matches();
	}

	public boolean validateFields() {
		if (address1.getText().toString().isEmpty()) {
			address1.setError(getResources().getString(R.string.required));
//			address1TI.setError(getResources().getString(R.string.required));
			return false;
		} else if (address1.getText().toString().length() < 3) {
			address1.setError(getResources().getString(
					R.string.min_character_warning));
			
//			address1TI.setError(getResources().getString(
//					R.string.min_character_warning));
			return false;
		}

		address1.setError(null);
		if (country.getText().toString().isEmpty()) {
			country.setError(getResources().getString(R.string.required));
//			countryTI.setError(getResources().getString(R.string.required));
			return false;
		}

		if (countryId.getText().toString().isEmpty()) {
			country.setError("Select Country from list");
//			countryTI.setError("Select Country from list");
			return false;
		}

		country.setError(null);
		if (state.getText().toString().isEmpty()) {
			state.setError(getResources().getString(R.string.required));
//			stateTI.setError(getResources().getString(R.string.required));
			return false;
		}

		if (stateId.getText().toString().isEmpty()) {
			state.setError("Select State from list");
//			stateTI.setError("Select State from list");
			return false;
		}

		state.setError(null);
		if (city.getText().toString().isEmpty()) {
			city.setError(getResources().getString(R.string.required));
//			cityTI.setError(getResources().getString(R.string.required));
			return false;
		} else if (checkForNumberInString(city.getText().toString()) == false) {
			city.setError("Cannot contain numbers");
//			cityTI.setError("Cannot contain numbers");
			return false;
		}

		city.setError(null);
		if (postalCode.getText().toString().isEmpty()) {
			postalCode.setError(getResources().getString(R.string.required));
//			postalCodeTI.setError(getResources().getString(R.string.required));
			return false;
		}

		postalCode.setError(null);
		/*if (mobile.getText().toString().isEmpty()) {
			if(bundle.getInt("Type")!=3){
				mobile.setError(getResources().getString(R.string.required));
				return false;
			}
		} else if (mobile.getText().toString().length() < 5) {
			mobile.setError("Cannot be less than 5 digits");
			return false;
		}*/

		
		if(bundle.getInt("Type") == 1){
			if(mobile.getText().toString().isEmpty() && email.getText().toString().isEmpty()){
				EzeidUtil.showToast(LocationAddressForm.this, "Enter Email or Mobile or Both");
				return false;
			}else{
				return true;
			}
		}else{
			if (mobile.getText().toString().isEmpty()) {
				if(bundle.getInt("Type")==2){
					mobile.setError(getResources().getString(R.string.required));
//					mobileTI.setError(getResources().getString(R.string.required));
					return false;
				}
			} else if (mobile.getText().toString().length() < 5) {
				mobile.setError("Cannot be less than 5 digits");
//				mobileTI.setError("Cannot be less than 5 digits");
				return false;
			}
			
			mobile.setError(null);
			if (!email.getText().toString().isEmpty()) {
				if (!validateEmail(email.getText().toString())) {
					email.setError(getResources().getString(R.string.invaid_email));
//					emailTI.setError(getResources().getString(R.string.invaid_email));
					return false;
				}
			}else{
				if(bundle.getInt("Type")==2){
					email.setError(getResources().getString(R.string.required));
//					emailTI.setError(getResources().getString(R.string.required));
					return false;
				}
			}
		}
		
		if(!pinCode.getText().toString().isEmpty()){
			int pinNumber = Integer.parseInt(pinCode.getText().toString());
			if(pinNumber<100 || pinNumber >999){
				EzeidUtil.showToast(LocationAddressForm.this, "PIN should be between 100-999");
				return false;
			}else{
				return true;
			}
		}
		return true;
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (checkPlayServices()) {
			Log.i("", "Google Play Services exist!");
		} else {
			finish();
		}
		if (!checkGpsStatus()) {
			AlertForEnableGPS();
		} else {
			if (bundle.getInt("primary") != 2 && bundle.getInt("primary") != 3) {
				Location myLocation = map.getMyLocation();

				if (myLocation != null) {
					LatLng currentLatlng = new LatLng(myLocation.getLatitude(),
							myLocation.getLongitude());
//					Log.e("Alt", "**1**" + myLocation.getAltitude());
					altitude = myLocation.getAltitude();
					Builder builder = new Builder();
					builder.zoom(14);
					builder.target(currentLatlng);
					this.map.moveCamera(CameraUpdateFactory
							.newCameraPosition(builder.build()));
				} else {
					if(lat!=null && lon!=null){
						SetMapPoint(lat, lon);
					}else{
						try {
							double _lat = Double.parseDouble(mgr
									.GetValueFromSharedPrefs("CurrentLat"));
							double _long = Double.parseDouble(mgr
									.GetValueFromSharedPrefs("CurrentLong"));
							myLatLng = new LatLng(_lat, _long);
							Builder builder = new Builder();
							builder.zoom(14);
							builder.target(myLatLng);
							this.map.moveCamera(CameraUpdateFactory
									.newCameraPosition(builder.build()));
						} catch (Exception e) {
							e.printStackTrace();
							getMyLocation();
						}
					}
				}
			}
		}
	}

	private void getMyLocation() {
		/* Get current location */
		try {
			location = fusedLocationService.getLocation();
			gpsHandler.removeCallbacks(gpsRunnable);

			if (location != null) {
				Log.e("loc", "called 4");
				
				LatLng target = new LatLng(location.getLatitude(),
						location.getLongitude());

				mgr.SaveValueToSharedPrefs("CurrentLat",
						"" + location.getLatitude());
				mgr.SaveValueToSharedPrefs("CurrentLong",
						"" + location.getLongitude());

				Builder builder = new Builder();
				builder.zoom(15);
				builder.target(target);

				Log.e("Alt", "----" + location.getAltitude());
				
				altitude = location.getAltitude();

				this.map.animateCamera(CameraUpdateFactory
						.newCameraPosition(builder.build()));
			} else {
				gpsHandler.postDelayed(gpsRunnable, 1000);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	Handler gpsHandler = new Handler();

	Runnable gpsRunnable = new Runnable() {
		@Override
		public void run() {
			Log.e("loc", "called 5");
			getMyLocation();
		}
	};

	@Override
	public void onLocationResultAvailable(final Location location) {
		this.location = location;
		LocationAddressForm.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (location == null) {
					gpsHandler.post(gpsRunnable);
				} else {
					double lati = location.getLatitude();
					double longi = location.getLongitude();
					Log.e("Alt", "@@@@" + location.getAltitude());
					
					altitude = location.getAltitude();

					String lat_ = String.valueOf(lati);
					String lng_ = String.valueOf(longi);

					myLatLng = new LatLng(lati, longi);
					mgr.SaveValueToSharedPrefs("CurrentLat", lat_);
					mgr.SaveValueToSharedPrefs("CurrentLong", lng_);

					Builder builder = new Builder();
					builder.zoom(15);
					builder.target(myLatLng);
					
					Log.e("loc", "called 6");

					map.animateCamera(CameraUpdateFactory
							.newCameraPosition(builder.build()));
				}
			}
		});
	}

	private boolean checkGpsStatus() {
		/* Check GPS is On or off */
		boolean gpsStatus;
		locationManager = (LocationManager) this
				.getSystemService(LOCATION_SERVICE);
		/*
		 * locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
		 * MIN_TIME, MIN_DISTANCE, this);
		 */
		gpsStatus = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);
		return gpsStatus;
	}

	private boolean checkPlayServices() {
		/* Check Google play Services */
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (status != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
				location = fusedLocationService.getLocation();
			} else {
				finish();
			}
			return false;
		}
		return true;
	}

	private void AlertForEnableGPS() {
		AlertDialog.Builder alert = new AlertDialog.Builder(
				LocationAddressForm.this);
		alert.setTitle("EZEID GPS setting");
		alert.setMessage("Location Service is not enabled! Want to go to the settings menu?");
		alert.setPositiveButton("Settings",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent settingIntent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						LocationAddressForm.this.startActivityForResult(
								settingIntent, 100);
					}
				});
		alert.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alert.show();
	}

	private String compressImageForIcon(Bitmap bitmap) {
		Bitmap compressedBitmap = Bitmap.createScaledBitmap(bitmap, 48, 48,
				true);
		ByteArrayOutputStream blob = new ByteArrayOutputStream();
		compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, blob);
		compressedImageByteArray = EzeidUtil
				.getCompressedBytesFromBitmap(compressedBitmap);
		compressedImageBase64String = Base64.encodeToString(
				compressedImageByteArray, Base64.DEFAULT);
		return compressedImageBase64String;
	}

	private void initUiComponents() {
		fileName = new Date().getTime() + ".jpg";
		imageByteArray = new byte[0];
		compressedImageByteArray = new byte[0];
		TAG = getClass().getSimpleName();
//		eProgress = new EzeidProgress(LocationAddressForm.this);
		mToolbar = (Toolbar) findViewById(R.id.location_toolbar_actionbar);
		mToolbar.setTitle(Html.fromHtml("LOCATION"));
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		mToolbar.setNavigationIcon(getResources().getDrawable(
				R.drawable.ic_action_back));
		mToolbar.setNavigationOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LocationAddressForm.this.finish();
			}
		});
		
//		locationTitleTI = (TextInputLayout) findViewById(R.id.locationFieldTextInput);
//		address1TI = (TextInputLayout) findViewById(R.id.address1TextInput);
//		address2TI = (TextInputLayout) findViewById(R.id.address2TextInput);
//		countryTI = (TextInputLayout) findViewById(R.id.countryTextInput);
//		stateTI = (TextInputLayout) findViewById(R.id.stateTextInput);
//		cityTI = (TextInputLayout) findViewById(R.id.cityTextInput);
//		postalCodeTI = (TextInputLayout) findViewById(R.id.postalCodeTextInput);
//		mobileTI = (TextInputLayout) findViewById(R.id.mobileTextInput);
//		phoneTI = (TextInputLayout) findViewById(R.id.phoneTextInput);
//		emailTI = (TextInputLayout) findViewById(R.id.emailTextInput);
//		websiteTI = (TextInputLayout) findViewById(R.id.websiteTextInput);
//		pinTI; = (TextInputLayout) findViewById(R.id.pinTextInput);

		fusedLocationService = new FusedLocationService(this,
				LocationAddressForm.this);

		mobileISDCode = (Spinner) findViewById(R.id.isdCode);
		phoneISDCode = (Spinner) findViewById(R.id.phoneIsdCode);
		parkingStatus = (Spinner) findViewById(R.id.parkingStatus);

		pattern = Pattern.compile(EzeidGlobal.EMAIL_PATTERN);

		scrollView = (ScrollView) findViewById(R.id.scrollView);
		locationTitle = (EditText) findViewById(R.id.locationTitle);
		locationSearch = (AutoCompleteTextView) findViewById(R.id.locationField);
		clearLocationField = (ImageView) findViewById(R.id.clear);
		picture = (ImageView) findViewById(R.id.picture);

		address1 = (EditText) findViewById(R.id.address1);
		address2 = (EditText) findViewById(R.id.address2);
		postalCode = (EditText) findViewById(R.id.pincode);
		country = (AutoCompleteTextView) findViewById(R.id.country);
		state = (AutoCompleteTextView) findViewById(R.id.state);
		city = (AutoCompleteTextView) findViewById(R.id.city);
		mobile = (EditText) findViewById(R.id.mobileNumber);
		phone = (EditText) findViewById(R.id.phoneNumber);
		email = (EditText) findViewById(R.id.emailAddress);
		pinCode = (EditText) findViewById(R.id.pin);
		website = (EditText) findViewById(R.id.website);

		enablePin = (CheckBox) findViewById(R.id.enablePin);
		pinCode.setVisibility(View.INVISIBLE);

		cityId = (TextView) findViewById(R.id.cityId);
		stateId = (TextView) findViewById(R.id.stateId);
		countryId = (TextView) findViewById(R.id.countryId);

		mgr = new EzeidPreferenceHelper(getApplicationContext());
		bundle = getIntent().getExtras();
		searchButton = (Button) findViewById(R.id.search);

		pictureText = (TextView) findViewById(R.id.pictureText);
		iconText = (TextView) findViewById(R.id.iconText);
		icon = (ImageView) findViewById(R.id.icon);

		pictureIconLayout = (LinearLayout) findViewById(R.id.pictureIconLayout);

		EzeidUtil.setErrorMethod(address1, 44);
		EzeidUtil.setErrorMethod(address2, 44);
		EzeidUtil.setErrorMethod(locationTitle, 49);
		EzeidUtil.setErrorMethod(email, 49);
		EzeidUtil.setErrorMethod(website, 49);
		
		
//		EzeidUtil.setErrorMethod(address1TI, 44);
//		EzeidUtil.setErrorMethod(address2TI, 44);
//		EzeidUtil.setErrorMethod(locationTitleTI, 49);
//		EzeidUtil.setErrorMethod(emailTI, 49);
//		EzeidUtil.setErrorMethod(websiteTI, 49);
	}

	public class ParkingAdapter extends ArrayAdapter<String> {
		public ParkingAdapter(Context ctx, int txtViewResourceId,
				String[] objects) {
			super(ctx, txtViewResourceId, objects);
		}

		@Override
		public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
			return getCustomView(position, cnvtView, prnt);
		}

		@Override
		public View getView(int pos, View cnvtView, ViewGroup prnt) {
			return getCustomView(pos, cnvtView, prnt);
		}

		public View getCustomView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View mySpinner = inflater.inflate(R.layout.category_list_item, parent,
					false);
			TextView masterTag = (TextView) mySpinner
					.findViewById(R.id.categoryTitle);
			masterTag.setText(parking[position]);
			return mySpinner;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (EzeidUtil.isConnectedToInternet(LocationAddressForm.this)) {
//			eProgress.show();
			if (isSessionDialogShown == false) {
				if(!mgr.GetValueFromSharedPrefs("Token").isEmpty()){
					CheckSessionExpiredStatus();
				}
			}
		}else{

			EzeidUtil.showToast(LocationAddressForm.this, getResources()
					.getString(R.string.internet_warning));
		}

		if(lat!=null && lon!=null){
			SetMapPoint(lat, lon);
		}
		
	}

	private void CheckSessionExpiredStatus() {
//		eProgress.show();
		EzeidUtil.ShowEzeidDialog(LocationAddressForm.this, "Loading");
		String token = mgr.GetValueFromSharedPrefs("Token");
		if (!token.equalsIgnoreCase("") && sessionRetry < 5) {
			JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
					EZEIDUrlManager.getAPIUrl() + "ewtGetLoginCheck?Token=" + token,
					 new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {
//							eProgress.dismiss();
							EzeidUtil.DismissEzeidDialog();
//							SessionStatus(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
//							eProgress.dismiss();
							EzeidUtil.DismissEzeidDialog();
							EzeidUtil.showError(LocationAddressForm.this, error);
						}
					});
			EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
		} else {
//			eProgress.dismiss();
			EzeidUtil.DismissEzeidDialog();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Crouton.cancelAllCroutons();
	}

	private boolean checkForNumberInString(String checkStr) {
		Pattern pattern = Pattern.compile(numberRegex);
		Matcher m = pattern.matcher(checkStr);
		if (m.find()) {
			return false;
		}
		return true;
	}
	
	private void doCrop() {
			final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
	    	Intent intent = new Intent("com.android.camera.action.CROP");
	        intent.setType("image/*");
	        
	        List<ResolveInfo> list = getPackageManager().queryIntentActivities( intent, 0 );
	        int size = list.size();
	        if (size == 0) {	        
	        	Toast.makeText(this, "Cannot find image crop app", Toast.LENGTH_SHORT).show();
	            return;
	        } else {
	        	intent.setData(mImageCaptureUri);
	        	if(bundle.getInt("Type") == 2){
	        		intent.putExtra("outputX", 315);
		            intent.putExtra("outputY", 155);
		            intent.putExtra("aspectX", 63);
		            intent.putExtra("aspectY", 31);
	        	}else{
	        		intent.putExtra("outputX", 144);
		            intent.putExtra("outputY", 144);
		            intent.putExtra("aspectX", 1);
		            intent.putExtra("aspectY", 1);
	        	}
	            intent.putExtra("scale", true);
	            intent.putExtra("return-data", true);
	            
	        	if (size == 1) {
	        		Intent i 		= new Intent(intent);
		        	ResolveInfo res	= list.get(0);
		        	i.setComponent( new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
		        	startActivityForResult(i, CROP_FROM_CAMERA);
	        	} else {
			        for (ResolveInfo res : list) {
			        	final CropOption co = new CropOption();
			        	co.title 	= getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
			        	co.icon		= getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
			        	co.appIntent= new Intent(intent);
			        	co.appIntent.setComponent( new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
			            cropOptions.add(co);
			        }
		        
			        CropOptionAdapter adapter = new CropOptionAdapter(LocationAddressForm.this, cropOptions);
			        AlertDialog.Builder builder = new AlertDialog.Builder(this);
			        builder.setTitle("Choose Crop App");
			        builder.setAdapter( adapter, new DialogInterface.OnClickListener() {
			            public void onClick( DialogInterface dialog, int item ) {
			                startActivityForResult( cropOptions.get(item).appIntent, CROP_FROM_CAMERA);
			            }
			        });
		        
			        builder.setOnCancelListener( new DialogInterface.OnCancelListener() {
			            @Override
			            public void onCancel( DialogInterface dialog ) {
			                if (mImageCaptureUri != null ) {
			                    getContentResolver().delete(mImageCaptureUri, null, null );
			                    mImageCaptureUri = null;
			                }
			            }
			        } );
			        AlertDialog alert = builder.create();
			        alert.show();
	        	}
	        }
		}
	 
	private void selectImage() {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					LocationAddressForm.this);
			builder.setTitle("Add Photo!");
			builder.setItems(EzeidGlobal.items,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int item) {
							
							if (EzeidGlobal.items[item].equals("Take Photo")) {
								Intent intent = new Intent(
										MediaStore.ACTION_IMAGE_CAPTURE);
								intent.putExtra(MediaStore.EXTRA_OUTPUT,
										setImageUri());
								startActivityForResult(intent,
										EzeidGlobal.CAPTURE_IMAGE);
							} else if (EzeidGlobal.items[item]
									.equals("Choose from Library")) {
								Intent intent = new Intent(
										Intent.ACTION_PICK,
										MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
								intent.setType("image/*");
								startActivityForResult(intent,
										EzeidGlobal.PICK_IMAGE);
							} else if (EzeidGlobal.items[item].equals("Cancel")) {
								dialog.dismiss();
							}
						}
					});
			builder.show();
		}
	 
	private void saveData() {
			/* Update Primary Information */
			if (!mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
							params.put("Token",
									mgr.GetValueFromSharedPrefs("Token"));
							params.put("IDTypeID",""+accountType);
							params.put("FirstName", firstName);
							params.put("LastName", lastName);
							params.put("CompanyName", companyName);
							params.put("JobTitle", jobTitle);
							params.put("EZEID",
									mgr.GetValueFromSharedPrefs("EZEID"));
							params.put("Latitude", lat);
							params.put("Longitude", lon);
							params.put("Altitude", ""+altitude);
							params.put("AddressLine1", address1.getText().toString());
							params.put("AddressLine2", address2.getText().toString());
							params.put("CityTitle", city.getText().toString());
							params.put("StateID", stateId.getText().toString());
							params.put("CountryID", countryId.getText().toString());
							params.put("PostalCode", postalCode.getText().toString());
							params.put("PIN", pinCode.getText().toString());
							params.put("PhoneNumber", phone.getText().toString());
							params.put("MobileNumber", mobile.getText().toString());
							params.put("EMailID", email.getText().toString());
							params.put("Picture", imageBase64String);
							params.put("Website", website.getText().toString());
							params.put("PictureFileName", fileName);
							params.put("AboutCompany", aboutCompany);
							params.put("LanguageID", "1");
							params.put("NameTitleID", title);
							params.put("Icon", compressedImageBase64String);
							params.put("IconFileName", fileName);
							params.put("SelectionType", ""+bundle.getString("selectionType"));
							params.put("ISDMobileNumber", countries.get(
									mobileISDCode.getSelectedItemPosition())
									.get("ISDCode"));
							params.put("ISDPhoneNumber", countries.get(
									phoneISDCode.getSelectedItemPosition())
									.get("ISDCode"));
							params.put("DOB", bundle.getString("dateOfBirth"));
							params.put("Gender", "" + bundle.getInt("gender"));
							params.put("OpertaionType", "2");
							params.put("ParkingStatus", ""+parkingStatus.getSelectedItemPosition());
//							eProgress.show();
							EzeidUtil.ShowEzeidDialog(LocationAddressForm.this, "Loading");

							NetworkHandler.SavePrimaryInformation(TAG, locationsHandler,
									params);
							 Log.e("UPDATE PRIMARY INFO ",
									 ""+params);
				}else{
					params.put("IDTypeID", ""+accountType);
					params.put("EZEID", ezeid);
					params.put("Password", password);
					params.put("FirstName", firstName);
					params.put("LastName", lastName);
					params.put("CompanyName", companyName);
					params.put("JobTitle", jobTitle);
					params.put("Latitude", lat);
					params.put("Longitude", lon);
					params.put("Altitude", ""+altitude);
					params.put("AddressLine1", address1.getText().toString());
					params.put("AddressLine2", address2.getText().toString());
					params.put("CityTitle", city.getText().toString());
					params.put("PostalCode", postalCode.getText().toString());
					params.put("StateID", stateId.getText().toString());
					params.put("CountryID", countryId.getText().toString());
					params.put("PIN", pinCode.getText().toString());
					params.put("PhoneNumber", phone.getText().toString());
					params.put("MobileNumber", mobile.getText().toString());
					params.put("EMailID", email.getText().toString());
					params.put("Picture", imageBase64String);
					params.put("Website", website.getText().toString());
					params.put("AboutCompany", aboutCompany);
					params.put("SelectionType", ""+selectionType);
					params.put("PictureFileName", fileName);
					params.put("LanguageID", "1");
					params.put("NameTitleID", title);
					params.put("Icon", compressedImageBase64String);
					params.put("IconFileName", fileName);
					params.put("ISDMobileNumber", countries.get(
							phoneISDCode.getSelectedItemPosition())
							.get("ISDCode"));
					params.put("ISDPhoneNumber", countries.get(
							phoneISDCode.getSelectedItemPosition())
							.get("ISDCode"));
					params.put("DOB", bundle.getString("dateOfBirth"));
					params.put("Gender", "" + bundle.getInt("gender"));
					params.put("OpertaionType", "2");
					
					params.put("ParkingStatus", ""+parkingStatus.getSelectedItemPosition());
					params.put("RoleID", "0");
					params.put("Primary", "0");
					params.put("FunctionID", "0");
					params.put("Area", "0");
					params.put("CategoryID", "0");
					showDisclaimerDialog();
					
					Log.e("SAVE PRIMARY INFO ",
					 ""+params);
			}
		}
	
	private void showDisclaimerDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(LocationAddressForm.this);
		builder.setTitle("Disclaimer");
		builder.setMessage(Html.fromHtml("By clicking OK, you will accept the <a href='https://www.ezeone.com/#/terms'>Terms & Conditions</a>"));
		builder.setCancelable(false);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
//				Log.e("PARAMS", ""+params);
				EzeidUtil.ShowEzeidDialog(LocationAddressForm.this, "Loading");
				NetworkHandler.SavePrimaryInformation(TAG, locationsHandler, params);
			}
		});
		AlertDialog alert11 = builder.create();
		alert11.show();
		
		((TextView)alert11.findViewById(android.R.id.message)).setClickable(true);
		((TextView)alert11.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
	}
	
	private void parseSaveResponse(JSONObject obj) {
		try {
//			eProgress.dismiss();
			EzeidUtil.DismissEzeidDialog();
			if (obj.has("IsAuthenticate")) {
				if (obj.getBoolean("IsAuthenticate")) {
					EzeidUtil.showToast(LocationAddressForm.this, "Save Successful");
					mgr.SaveValueToSharedPrefs("Token", obj.getString("Token"));
					mgr.SaveValueToSharedPrefs("EZEID", ezeid);
					mgr.SaveValueToSharedPrefs("IDTypeID",""+bundle.getInt("Type"));

					if (bundle.getBoolean("signUp")) {
						showCompletionDialog();
					}else{
						Toast.makeText(getApplicationContext(), "Save Successful", Toast.LENGTH_SHORT).show();
						
						Intent homeIntent = new Intent(LocationAddressForm.this, LandingPage.class);
						homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TASK);
						startActivity(homeIntent);
						finish();
					}
				} else {
					EzeidUtil.showToast(LocationAddressForm.this, "Save Failed");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void showCompletionDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(LocationAddressForm.this);
		builder.setTitle("Congratulations");
		if (bundle.getInt("Type") == 1) {
			builder.setMessage(Html.fromHtml("Please Sign in to <a href='https://www.ezeone.com'>www.ezeone.com</a> portal to update other sections of information that helps us serve you better. <br/><br/> Other features available through <a href='https://www.ezeid.com'>www.ezeone.com</a> <br/> - Upload CV & share with employers of your choice <br/>- Upload commonly used documents to share with ease <br/>- Manage your Associates through Whitelist and also manage Blacklist <br/><br/> Use EZE ID and enjoy the power of ease and convenience. <br/><br/> EZEOne Team"));
		}else{
			builder.setMessage(Html.fromHtml("Please Sign in to <a href='https://www.ezeone.com'>www.ezeone.com</a> portal to complete Business Process configuration of your profile. <br/><br/> Please contact your nearest Area Partner or write to <a href=\"mailto:partner@ezeone.com\">partner@ezeone.com</a>  to get your Business Account Verified, that brings credibility and help you increase your business. <br/><br/> We wish you success in your business. <br/><br/> EZEOne Team"));
		}
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent homeIntent = new Intent(LocationAddressForm.this, LandingPage.class);
				homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(homeIntent);
				finish();
			}
		});
		AlertDialog alert11 = builder.create();
		alert11.show();
		
		((TextView)alert11.findViewById(android.R.id.message)).setClickable(true);
		((TextView)alert11.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
	}
}