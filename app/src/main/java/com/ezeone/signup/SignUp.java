package com.ezeone.signup;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.SplashScreen;
import com.ezeone.adapters.DropdownAdapter;
import com.ezeone.adapters.OtherLocationsAdapter;
import com.ezeone.backend.EzeidDbHelper;
import com.ezeone.helpers.Constant;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.helpers.ListViewUtils;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.pojo.OtherLocation;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.floatingaction.FloatingActionButton;
import com.ezeonelib.widget.cruton.Crouton;
import com.google.gson.Gson;

@SuppressLint({ "HandlerLeak", "NewApi" })
public class SignUp extends AppCompatActivity {

	private Spinner title, gender, type;
	private EditText jobTitle, ezeid, password, confirmPassword, firstName,
			lastName, companyName, aboutCompany, dateOfBirth;
	// private TextInputLayout
	// jobTitleTI,ezeidTI,passwordTI,confirmPasswordTI,firstNameTI,lastNameTI,companyNameTI,aboutCompanyTI,dateOfBirthTI;
	private Button checkButton, companyProfileButton;
	private ListView otherLocationListView;
	private FloatingActionButton addOtherLocation;
	private LinearLayout otherLocationLayout, dobLayout;
	private EzeidPreferenceHelper mgr;
	private ArrayList<OtherLocation> otherLocations;
	private ArrayList<HashMap<String, String>> titles;
	private Map<String, String> params;
	private Gson gson;
	private boolean isSessionDialogShown = false, idAvailable = false;;
	private String ezeidStr, passwordStr, confPasswordStr, firstNameStr,
			lastNameStr, companyNameStr, jobTitleStr, webStr, aboutCompStr,
			titleId, lat, lon, address1, address2, countryId, stateId, pinCode,
			mobileNumber, phoneNumber, email, cityName, countryName, stateName,
			img = "", phoneISD, mobileISD, parkingStatus, TAG = getClass()
					.getSimpleName();
	// city,imageString,image,icon = ""
	private int idType, deletePos, genderIndex, selectionType,
			sessionRetry = 0;
	// private EzeidProgress eProgress;
	private AlertDialog.Builder builder1;
	private Toolbar mToolbar;
	private ImageView available;
	private ProgressBar progressBar;
	private EzeidDbHelper helper;
	private Bundle bundle;
	private SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy",
			Locale.getDefault());
	private LinearLayout ezeoneIdPwdLayout, primaryInfoLayout;
	private boolean animated = false;

	Calendar c = Calendar.getInstance();
	int startYear = c.get(Calendar.YEAR);
	int startMonth = c.get(Calendar.MONTH);
	int startDay = c.get(Calendar.DAY_OF_MONTH);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		helper = new EzeidDbHelper(getApplicationContext());
		mgr = new EzeidPreferenceHelper(getApplicationContext());
		initUIComponents();

		Log.e("Token", mgr.GetValueFromSharedPrefs("Token"));
		if (EzeidUtil.isConnectedToInternet(SignUp.this)) {
			if (!mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
				ezeid.setEnabled(false);
				checkButton.setVisibility(View.GONE);
				LayoutParams lparams = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				ezeid.setLayoutParams(lparams);
				type.setEnabled(false);

				password.setVisibility(View.GONE);
				confirmPassword.setVisibility(View.GONE);
				primaryInfoLayout.setVisibility(View.VISIBLE);
				ezeoneIdPwdLayout.setVisibility(View.VISIBLE);
				NetworkHandler.GetUserDetails(TAG, handler, params,
						mgr.GetValueFromSharedPrefs("Token"));
			} else {
				// EzeidUtil.DismissEzeidDialog();
			}
		} else {
			EzeidUtil.showToast(SignUp.this,
					getResources().getString(R.string.internet_warning));
		}

		bundle = getIntent().getExtras();
		if (bundle != null) {
			if (bundle.getBoolean("edit")) {
				otherLocationLayout.setVisibility(View.VISIBLE);
			}
		}

		checkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (ezeid.getText().toString().length() != 0) {
					if (ezeid.getText().toString().length() > 2) {
						ezeidStr = ezeid.getText().toString();
						if (!ezeidStr.endsWith("AP")
								|| !ezeidStr.endsWith("ap")) {
							if (checkEzeIdHasDot(ezeid.getText().toString())) {
								if (EzeidUtil
										.isConnectedToInternet(SignUp.this)) {
									progressBar.setVisibility(View.VISIBLE);
									Map<String, String> params = new HashMap<String, String>();
									params.put("EZEID", ezeid.getText()
											.toString());
									NetworkHandler.CheckEzeId(TAG, handler,
											params);
								} else {
									progressBar.setVisibility(View.INVISIBLE);
									EzeidUtil.showToast(
											SignUp.this,
											getResources().getString(
													R.string.internet_warning));
								}
							} else {
								ezeid.setError(getResources().getString(
										R.string.special_character_warning));
								// ezeidTI.setError(getResources().getString(
								// R.string.special_character_warning));
							}
						} else {
							ezeid.setError("That id is not available");
							// ezeidTI.setError("That id is not available");
						}
					} else {
						ezeid.setError(getResources().getString(
								R.string.min_character_warning));
						// ezeidTI.setError(getResources().getString(
						// R.string.min_character_warning));
					}
				} else {
					ezeid.setError("Cannot be empty");
					ezeid.setBackground(getResources().getDrawable(
							R.drawable.ezeid_edit_text_selector_reg1));
					// f6b3b3
				}
			}
		});

		confirmPassword.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_NEXT) {
					if (validatePassword(password.getText().toString(),
							confirmPassword.getText().toString())) {
						// primaryInfoLayout.setVisibility(View.VISIBLE);
						// primaryInfoLayout.startAnimation(anim);
					} else {

					}
				}
				return false;
			}
		});

		addOtherLocation.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// eProgress.show();
				Intent addOtherLocation = new Intent(SignUp.this,
						LocationAddressForm.class);
				addOtherLocation.putExtra("primary", 1);
				addOtherLocation.putExtra("Type", idType);
				addOtherLocation.putExtra("TID", "0");
				startActivity(addOtherLocation);
			}
		});

		ezeid.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence charSeq, int arg1, int arg2,
					int arg3) {
				idAvailable = false;
				ezeid.setBackground(getResources().getDrawable(
						R.drawable.ezeid_edit_text_selector_reg1));
				available.setVisibility(View.INVISIBLE);

				if (charSeq.toString().length() > 14) {
					// ezeidTI.setError("Max limit 15 characters");
					ezeid.setError("Max limit 15 characters");
				}
				if (charSeq.toString().length() < 15) {
					ezeid.setError(null);
					// ezeidTI.setError(null);
				}

				if (charSeq.toString().isEmpty()) {
					idAvailable = false;
					ezeid.setBackground(getResources().getDrawable(
							R.drawable.ezeid_edit_text_selector_reg1));
					// ezeidTI.setBackground(getResources().getDrawable(
					// R.drawable.ezeid_edit_text_selector_reg1));
					available.setVisibility(View.INVISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});

		EzeidUtil.setErrorMethod(companyName, 149);
		EzeidUtil.setErrorMethod(firstName, 29);
		EzeidUtil.setErrorMethod(jobTitle, 99);
		EzeidUtil.setErrorMethod(lastName, 29);
		EzeidUtil.setErrorMethod(password, 34);
		EzeidUtil.setErrorMethod(confirmPassword, 34);
		EzeidUtil.setErrorMethod(aboutCompany, 99);

		// EzeidUtil.setErrorMethod(companyNameTI, 149);
		// EzeidUtil.setErrorMethod(firstNameTI, 29);
		// EzeidUtil.setErrorMethod(jobTitleTI, 99);
		// EzeidUtil.setErrorMethod(lastNameTI, 29);
		// EzeidUtil.setErrorMethod(passwordTI, 34);
		// EzeidUtil.setErrorMethod(confirmPasswordTI, 34);
		// EzeidUtil.setErrorMethod(aboutCompanyTI, 99);

		type.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				if (mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
					// if(animated==false){
					// ezeoneIdPwdLayout.startAnimation(anim);
					// ezeoneIdPwdLayout.setVisibility(View.VISIBLE);
					// animated = true;
					// }
				}

				switch (position) {
				case 0:
					idType = 2;
					dobLayout.setVisibility(View.GONE);
					selectionType = 1;
					companyProfileButton.setVisibility(View.VISIBLE);
					break;

				case 1:
					idType = 2;
					dobLayout.setVisibility(View.GONE);
					selectionType = 2;
					companyProfileButton.setVisibility(View.VISIBLE);
					break;

				case 2:
					idType = 1;
					dobLayout.setVisibility(View.VISIBLE);
					companyProfileButton.setVisibility(View.GONE);
					selectionType = 0;
					break;

				case 3:
					idType = 3;
					dobLayout.setVisibility(View.GONE);
					selectionType = 0;
					companyProfileButton.setVisibility(View.GONE);
					break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.save) {
			ezeidStr = ezeid.getText().toString();
			passwordStr = password.getText().toString();
			confPasswordStr = confirmPassword.getText().toString();
			firstNameStr = firstName.getText().toString();
			lastNameStr = lastName.getText().toString();
			companyNameStr = companyName.getText().toString();
			jobTitleStr = jobTitle.getText().toString();
			aboutCompStr = aboutCompany.getText().toString();

			if (!mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
				Intent addPrimaryLocation = new Intent(SignUp.this,
						LocationAddressForm.class);
				addPrimaryLocation.putExtra("primary", 0);
				addPrimaryLocation.putExtra("Type", idType);

				if (bundle != null) {
					addPrimaryLocation.putExtra("ezeid", ezeidStr);
					addPrimaryLocation.putExtra("password", passwordStr);
					addPrimaryLocation.putExtra("title", titleId);
					addPrimaryLocation.putExtra("firstName", firstNameStr);
					addPrimaryLocation.putExtra("lastname", lastNameStr);
					addPrimaryLocation.putExtra("companyName", companyNameStr);
					addPrimaryLocation.putExtra("jobTitle", jobTitleStr);
					addPrimaryLocation.putExtra("aboutCompany", aboutCompStr);
					addPrimaryLocation.putExtra("address1", address1);
					addPrimaryLocation.putExtra("address2", address2);
					addPrimaryLocation.putExtra("pincode", pinCode);
					addPrimaryLocation.putExtra("email", email);
					addPrimaryLocation.putExtra("phoneNumber", phoneNumber);
					addPrimaryLocation.putExtra("mobileNumber", mobileNumber);
					addPrimaryLocation.putExtra("website", webStr);
					addPrimaryLocation.putExtra("lat", lat);
					addPrimaryLocation.putExtra("lon", lon);
					addPrimaryLocation.putExtra("picture", img);
					addPrimaryLocation.putExtra("cityName", cityName);
					addPrimaryLocation.putExtra("countryName", countryName);
					addPrimaryLocation.putExtra("stateName", stateName);
					addPrimaryLocation.putExtra("countryId", countryId);
					addPrimaryLocation.putExtra("stateId", stateId);
					addPrimaryLocation.putExtra("parkingStatus", parkingStatus);
					addPrimaryLocation.putExtra("mobileISD", mobileISD);
					addPrimaryLocation.putExtra("phoneISD", phoneISD);
					addPrimaryLocation.putExtra("dateOfBirth", dateOfBirth
							.getText().toString());
					addPrimaryLocation.putExtra("gender",
							gender.getSelectedItemPosition());
					if (bundle.getBoolean("signUp")) {
						addPrimaryLocation.putExtra("signUp",
								bundle.getBoolean("signUp"));
					}
					// Log.e("LATLONG", lat+","+lon);
				}
				startActivity(addPrimaryLocation);

			} else {
				if (validator()) {
					Intent addPrimaryLocation = new Intent(SignUp.this,
							LocationAddressForm.class);
					addPrimaryLocation.putExtra("primary", 0);
					addPrimaryLocation.putExtra("Type", idType);
					addPrimaryLocation.putExtra("ezeid", ezeidStr);
					addPrimaryLocation.putExtra("password", passwordStr);
					addPrimaryLocation.putExtra("title", titleId);
					addPrimaryLocation.putExtra("firstName", firstNameStr);
					addPrimaryLocation.putExtra("lastname", lastNameStr);
					addPrimaryLocation.putExtra("companyName", companyNameStr);
					addPrimaryLocation.putExtra("jobTitle", jobTitleStr);
					addPrimaryLocation.putExtra("aboutCompany", aboutCompStr);
					addPrimaryLocation.putExtra("selectionType", selectionType);
					addPrimaryLocation.putExtra("gender",
							gender.getSelectedItemPosition());
					addPrimaryLocation.putExtra("dateOfBirth", dateOfBirth
							.getText().toString());
					addPrimaryLocation.putExtra("mobileISD", mobileISD);
					addPrimaryLocation.putExtra("phoneISD", phoneISD);
					if (bundle.getBoolean("signUp")) {
						addPrimaryLocation.putExtra("signUp",
								bundle.getBoolean("signUp"));
					}
					startActivity(addPrimaryLocation);
				}
			}
		}
		return super.onOptionsItemSelected(item);
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.arg1) {
			case Constant.MessageState.EZEID_AVAILABLE:
				ezeid.setError(null);
				parseAvailabilityResponse((JSONObject) msg.obj);
				break;

			case Constant.MessageState.EZEID_NOT_AVAILABLE:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				progressBar.setVisibility(View.INVISIBLE);
				break;

			case Constant.MessageState.LOCATION_DELETE_SUCCESS:
				parseLocationDeleteResponse((JSONObject) msg.obj);
				break;

			case Constant.MessageState.LOCATION_DELETE_FAILED:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(SignUp.this, (VolleyError) msg.obj);
				break;

			case Constant.MessageState.USER_DETAILS_AVAILABLE:
				parseUserDetails((JSONArray) msg.obj);
				NetworkHandler.GetSecondaryLocations(TAG, handler, SignUp.this);
				break;

			case Constant.MessageState.USER_DETAILS_UNAVAILABLE:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(SignUp.this, (VolleyError) msg.obj);
				break;

			case Constant.MessageState.SECONDARY_LOCATION_AVAILABLE:
				parseSecondaryLocation((JSONArray) msg.obj);
				break;

			case Constant.MessageState.SECONDARY_LOCATION_UNAVAILABLE:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(SignUp.this, (VolleyError) msg.obj);
				break;

			case Constant.MessageState.COMPANY_PROFILE_SAVED:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				parseCompanyProfileSave((JSONObject) msg.obj);
				break;

			case Constant.MessageState.COMPANY_PROFILE_NOT_SAVED:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(SignUp.this, (VolleyError) msg.obj);
				break;

			case Constant.MessageState.COMPANY_PROFILE_AVAILABLE:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				parseCompanyProfile((JSONObject) msg.obj);
				break;

			case Constant.MessageState.COMPANY_PROFILE_NOT_AVAILABLE:
				// eProgress.dismiss();
				EzeidUtil.DismissEzeidDialog();
				EzeidUtil.showError(SignUp.this, (VolleyError) msg.obj);
				break;
			}
		};
	};

	/* Check for availability of EZEID */
	private void parseAvailabilityResponse(JSONObject obj) {
		try {
			progressBar.setVisibility(View.INVISIBLE);
			if (obj.has("IsIdAvailable")) {
				if (obj.getBoolean("IsIdAvailable")) {
					ezeid.setBackgroundColor(Color.parseColor("#d7f2d7"));
					idAvailable = true;
					available.setVisibility(View.VISIBLE);
				} else {
					ezeid.setError("That Id is not available");
					// ezeidTI.setError("That Id is not available");
					idAvailable = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* Password Validation */
	private boolean validatePassword(String pwd, String cpwd) {
		if (!pwd.isEmpty()) {
			if (!pwd.contains(" ")) {
				if (pwd.length() > 2 && pwd.length() < 35) {
					if (!cpwd.isEmpty()) {
						if (!cpwd.contains(" ")) {
							if (cpwd.length() > 2 && cpwd.length() < 35) {
								if (pwd.equals(cpwd)) {
									return true;
								} else {
									confirmPassword
											.setError("Passwords do not match");
									// confirmPasswordTI.setError("Passwords do not match");
									return false;
								}
							} else {
								confirmPassword
										.setError(getResources().getString(
												R.string.min_character_warning));

								// confirmPasswordTI.setError(getResources().getString(
								// R.string.min_character_warning));
							}
						} else {
							confirmPassword
									.setError("Cannot contain whitespaces");
							// confirmPasswordTI.setError("Cannot contain whitespaces");
						}
					} else {
						confirmPassword.setError(getResources().getString(
								R.string.required));
						// confirmPasswordTI.setError(getResources().getString(
						// R.string.required));
					}
				} else {
					password.setError(getResources().getString(
							R.string.min_character_warning));
					// passwordTI.setError(getResources().getString(
					// R.string.min_character_warning));
				}
			} else {
				password.setError("Cannot contain whitespaces");
				// passwordTI.setError("Cannot contain whitespaces");
			}
		} else {
			password.setError(getResources().getString(R.string.required));
			// passwordTI.setError(getResources().getString(R.string.required));
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	public void parseUserDetails(JSONArray arr) {
		try {
			if (arr.length() > 0) {
				for (int i = 0; i < arr.length(); i++) {
					JSONObject obj = arr.getJSONObject(i);
					if (obj.has("LocationID")) {
						idAvailable = true;
						firstName.setText(obj.getString("FirstName"));
						lastName.setText(obj.getString("LastName"));
						companyName.setText(obj.getString("CompanyName"));
						jobTitle.setText(obj.getString("JobTitle"));
						if (obj.isNull("AboutCompany")) {
							aboutCompany.setText("");
						} else {
							aboutCompany.setText(obj.getString("AboutCompany"));
						}

						mgr.SaveValueToSharedPrefs("IDTypeID",
								obj.getString("IDTypeID"));

						if (!obj.isNull("EZEIDVerifiedID")) {
							mgr.SaveValueToSharedPrefs("EZEIDVerifiedID", ""
									+ obj.getInt("EZEIDVerifiedID"));
						}

						ezeid.setText(obj.getString("EZEID"));

						int titleIndex = (obj.getInt("NameTitleID") - 1);
						title.setSelection(titleIndex);

						address1 = obj.getString("AddressLine1");
						address2 = obj.getString("AddressLine2");
						if (!obj.isNull("CityID")) {
							// city = obj.getString("CityID");
						} else {
							// city = "0";
						}

						if (!obj.isNull("StateID")) {
							stateId = obj.getString("StateID");
						} else {
							stateId = "0";
						}

						if (!obj.isNull("CountryID")) {
							countryId = obj.getString("CountryID");
						} else {
							countryId = "0";
						}
						pinCode = obj.getString("PostalCode");
						email = obj.getString("EMailID");
						phoneNumber = obj.getString("PhoneNumber");
						mobileNumber = obj.getString("MobileNumber");
						webStr = obj.getString("Website");
						lat = obj.getString("Latitude");
						lon = obj.getString("Longitude");
						parkingStatus = obj.getString("ParkingStatus");

						mobileISD = obj.getString("ISDMobileNumber");
						phoneISD = obj.getString("ISDPhoneNumber");

						gender.setSelection(obj.getInt("Gender"));

						if (!obj.getString("DOB").isEmpty()) {
							String dob = formatter.format(new Date(obj
									.getString("DOB")));
							dateOfBirth.setText(dob);
						}
						// icon = "";
						if (obj.has("Icon")) {
							// icon = obj.getString("Icon");
						}
						cityName = obj.getString("CityTitle");
						countryName = obj.getString("CountryTitle");
						stateName = obj.getString("StateTitle");

						if (obj.isNull("Picture")) {
							// image = "";
						} else {
							// image = obj.getString("Picture");
							img = obj.getString("Picture");
							// imageString = image;
						}

						if (obj.getString("IDTypeID").equals("1")) {
							type.setSelection(2);

						} else if (obj.getString("IDTypeID").equals("2")) {
							if (obj.getString("SelectionType").equals("1")) {
								type.setSelection(0);
							} else {
								type.setSelection(1);
							}
						} else if (obj.getString("IDTypeID").equals("3")) {
							type.setSelection(3);
						}

						if (!obj.isNull("EZEIDVerifiedID")) {
							if (obj.getString("EZEIDVerifiedID") == "2"
									&& obj.getString("IDTypeID") == "2") {
								mgr.SaveValueToSharedPrefs("isVerified",
										obj.getString("EZEIDVerifiedID"));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void parseLocationDeleteResponse(JSONObject obj) {
		try {
			if (obj.has("IsDeleted")) {
				if (obj.getBoolean("IsDeleted")) {
					otherLocations.remove(deletePos);
					OtherLocationsAdapter adapter = new OtherLocationsAdapter(
							getApplicationContext(),
							R.layout.location_list_item, otherLocations);
					otherLocationListView.setAdapter(adapter);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (EzeidUtil.isConnectedToInternet(SignUp.this)) {
			if (isSessionDialogShown == false) {
				if (!mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
					CheckSessionExpiredStatus();
				}
			}
		} else {
			EzeidUtil.showToast(SignUp.this,
					getResources().getString(R.string.internet_warning));
		}

		if (LocationAddressForm.resumeValue == 1) {
			// eProgress.show();
			EzeidUtil.DismissEzeidDialog();
			NetworkHandler.GetUserDetails(TAG, handler, params,
					mgr.GetValueFromSharedPrefs("Token"));
			LocationAddressForm.resumeValue = 0;
		} else {
		}
	}

	private boolean checkEzeIdHasDot(String ezeid) {
		String dot = "[\\\\!\"#$%&()*+,./:;<=>?\\[\\]^{|}~`']+";
		Pattern pattern = Pattern.compile(dot);
		Matcher m = pattern.matcher(ezeid);
		if (ezeid.contains(" ")) {
			return false;
		}
		if (m.find()) {
			return false;
		}
		return true;
	}

	public void parseSecondaryLocation(JSONArray arr) {
		try {
			if (otherLocations.size() > 0) {
				otherLocations.clear();
			}

			if (arr.length() > 0) {
				for (int i = 0; i < arr.length(); i++) {

					OtherLocation location = gson.fromJson(arr.get(i)
							.toString(), OtherLocation.class);
					otherLocations.add(location);
				}

				OtherLocationsAdapter adapter = new OtherLocationsAdapter(
						getApplicationContext(), R.layout.location_list_item,
						otherLocations) {

					@Override
					public View getView(final int position, View convertView,
							ViewGroup parent) {
						View view = super
								.getView(position, convertView, parent);
						final OtherLocation location = getItem(position);
						ImageView deleteImage = (ImageView) view
								.findViewById(R.id.deleteLocation);
						deleteImage.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View arg0) {
								builder1.setMessage("Are you sure you want to delete?");
								builder1.setCancelable(true);
								builder1.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												Map<String, String> params = new HashMap<String, String>();
												params.put("TID",
														location.getTID());
												params.put(
														"Token",
														mgr.GetValueFromSharedPrefs("Token"));
												NetworkHandler.DeleteLocation(
														TAG, handler, params);
												deletePos = position;
											}
										});
								builder1.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												dialog.cancel();
											}
										});
								AlertDialog alert11 = builder1.create();
								alert11.show();
							}
						});
						return view;
					}
				};
				otherLocationListView.setAdapter(adapter);

				ListViewUtils
						.setListViewHeightBasedOnChildren(otherLocationListView);

				otherLocationListView
						.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								Intent intent = new Intent(
										getApplicationContext(),
										LocationAddressForm.class);
								intent.putExtra("primary", 3);
								intent.putExtra("TID", otherLocations.get(arg2)
										.getTID());
								intent.putExtra("LocTitle",
										otherLocations.get(arg2).getLocTitle());
								intent.putExtra("Latitude",
										otherLocations.get(arg2).getLatitude());
								intent.putExtra("Longitude", otherLocations
										.get(arg2).getLongitude());
								intent.putExtra("AddressLine1", otherLocations
										.get(arg2).getAddressLine1());
								intent.putExtra("AddressLine2", otherLocations
										.get(arg2).getAddressLine2());
								intent.putExtra("CountryID", otherLocations
										.get(arg2).getCountryID());
								intent.putExtra("StateID",
										otherLocations.get(arg2).getStateID());
								intent.putExtra("PostalCode", otherLocations
										.get(arg2).getPostalCode());
								intent.putExtra("PIN", otherLocations.get(arg2)
										.getPIN());
								intent.putExtra("EMailID",
										otherLocations.get(arg2).getEMailID());
								intent.putExtra("PhoneNumber", otherLocations
										.get(arg2).getPhoneNumber());
								intent.putExtra("MobileNumber", otherLocations
										.get(arg2).getMobileNumber());
								intent.putExtra("WebSite",
										otherLocations.get(arg2).getWebsite());
								intent.putExtra("picture",
										otherLocations.get(arg2).getPicture());
								intent.putExtra("cityName",
										otherLocations.get(arg2).getCityTitle());
								intent.putExtra("icon", otherLocations
										.get(arg2).getIcon());

								intent.putExtra("salesEnquiryEmail",
										otherLocations.get(arg2)
												.getSalesEnquiryMailID());
								intent.putExtra("homeDeliveryEmail",
										otherLocations.get(arg2)
												.getHomeDeliveryMailID());
								intent.putExtra("reservationEmail",
										otherLocations.get(arg2)
												.getReservationMailID());
								intent.putExtra("supportEmail", otherLocations
										.get(arg2).getSupportMailID());
								intent.putExtra("cvEmail",
										otherLocations.get(arg2).getCVMailID());

								intent.putExtra("workingHours", otherLocations
										.get(arg2).getWorkingHours());
								intent.putExtra("openCloseStatus",
										otherLocations.get(arg2)
												.getOpenStatus());
								intent.putExtra("parkingStatus", otherLocations
										.get(arg2).getParkingStatus());

								intent.putExtra("mobileISD", otherLocations
										.get(arg2).getISDMobileNumber());

								intent.putExtra("phoneISD",
										otherLocations.get(arg2)
												.getISDPhoneNumber());

								startActivity(intent);
							}
						});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		Crouton.clearCroutonsForActivity(this);
		Crouton.cancelAllCroutons();
		isSessionDialogShown = false;
		super.onDestroy();
	}

	@SuppressWarnings("deprecation")
	private void initUIComponents() {
		mToolbar = (Toolbar) findViewById(R.id.registration_toolbar_actionbar);
		if (!mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
			mToolbar.setTitle(Html.fromHtml("PROFILE"));
		} else {
			mToolbar.setTitle(Html.fromHtml("SIGN UP"));
		}
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		mToolbar.setNavigationIcon(getResources().getDrawable(
				R.drawable.ic_action_back));
		mToolbar.setNavigationOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SignUp.this.finish();
			}
		});
		// eProgress = new EzeidProgress(SignUp.this);
		gson = new Gson();

		// ezeidTI = (TextInputLayout)findViewById(R.id.ezeidTextinput);
		// passwordTI = (TextInputLayout)findViewById(R.id.passwordTextInput);
		// confirmPasswordTI =
		// (TextInputLayout)findViewById(R.id.confirmPasswordTextInput);
		// firstNameTI = (TextInputLayout)findViewById(R.id.firstNameTextInput);
		// lastNameTI = (TextInputLayout)findViewById(R.id.lastNameTextInput);
		// companyNameTI =
		// (TextInputLayout)findViewById(R.id.companyNameTextInput);
		// aboutCompanyTI =
		// (TextInputLayout)findViewById(R.id.aboutCompanyTextInput);
		// dateOfBirthTI = (TextInputLayout)findViewById(R.id.dobTextInput);
		// jobTitleTI = (TextInputLayout)findViewById(R.id.jobTitleTextInput);

		ezeoneIdPwdLayout = (LinearLayout) findViewById(R.id.ezeoneidPwdLayout);
		primaryInfoLayout = (LinearLayout) findViewById(R.id.ezeonePrimaryInfoLayout);

		type = (Spinner) findViewById(R.id.typeSpinner);
		available = (ImageView) findViewById(R.id.available);
		companyProfileButton = (Button) findViewById(R.id.companyProfileButton);
		title = (Spinner) findViewById(R.id.title);
		jobTitle = (EditText) findViewById(R.id.jobTitle);
		ezeid = (EditText) findViewById(R.id.ezidTextField);
		password = (EditText) findViewById(R.id.password);
		confirmPassword = (EditText) findViewById(R.id.confirmPassword);
		firstName = (EditText) findViewById(R.id.firstName);
		lastName = (EditText) findViewById(R.id.lastName);
		companyName = (EditText) findViewById(R.id.companyName);
		aboutCompany = (EditText) findViewById(R.id.aboutCompany);
		checkButton = (Button) findViewById(R.id.checkAvailability);
		addOtherLocation = (FloatingActionButton) findViewById(R.id.addOtherLocation);
		addOtherLocation.setColorNormal(Color.parseColor("#27ae60"));
		addOtherLocation.setColorPressed(Color.parseColor("#2ecc71"));
		addOtherLocation.setIcon(R.drawable.ic_action_increase);
		otherLocationListView = (ListView) findViewById(R.id.otherLocationListView);
		otherLocationLayout = (LinearLayout) findViewById(R.id.otherlocations);
		otherLocations = new ArrayList<OtherLocation>();
		titles = new ArrayList<HashMap<String, String>>();
		titles = helper.getAllTitles();
		params = new HashMap<String, String>();
		builder1 = new AlertDialog.Builder(SignUp.this);
		progressBar = (ProgressBar) findViewById(R.id.usernameProgress);
		gender = (Spinner) findViewById(R.id.gender);
		dateOfBirth = (EditText) findViewById(R.id.dob);
		dobLayout = (LinearLayout) findViewById(R.id.dobLayout);
		dobLayout.setVisibility(View.GONE);
		gender.setAdapter(new DropdownAdapter(getApplicationContext(),
				R.layout.category_list_item, getResources().getStringArray(
						R.array.gender)));

		companyProfileButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!mgr.GetValueFromSharedPrefs("Token").isEmpty()) {
					// eProgress.show();
					EzeidUtil.ShowEzeidDialog(SignUp.this, "Loading");
					NetworkHandler.GetCompanyProfile(TAG,
							mgr.GetValueFromSharedPrefs("Token"), handler,
							params);
				} else {
					showCompanyProfileDialog("");
				}
			}
		});

		ezeid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(15),
				new InputFilter.AllCaps() });

		type.setAdapter(new DropdownAdapter(getApplicationContext(),
				R.layout.category_list_item, getResources().getStringArray(
						R.array.type)));

		dateOfBirth.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				final Calendar mcurrentDate = Calendar.getInstance();
				int mYear = mcurrentDate.get(Calendar.YEAR);
				int mMonth = mcurrentDate.get(Calendar.MONTH);
				int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

				DatePickerDialog mDatePicker = new DatePickerDialog(
						SignUp.this, new OnDateSetListener() {
							public void onDateSet(DatePicker datepicker,
									int selectedyear, int selectedmonth,
									int selectedday) {

								mcurrentDate.set(Calendar.YEAR, selectedyear);
								mcurrentDate.set(Calendar.MONTH, selectedmonth);
								mcurrentDate.set(Calendar.DAY_OF_MONTH,
										selectedday);
								dateOfBirth.setText(formatter
										.format(mcurrentDate.getTime()));
							}
						}, mYear, mMonth, mDay);
				mDatePicker.setTitle("Select Date of birth");
				mDatePicker.getDatePicker().setMaxDate(new Date().getTime());
				mDatePicker.show();
			}
		});

		final SimpleAdapter adapter = new SimpleAdapter(
				getApplicationContext(), titles, R.layout.category_list_item,
				new String[] { "Title", "TitleID" }, new int[] {
						R.id.categoryTitle, R.id.categoryId });
		title.setAdapter(adapter);

		title.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				titleId = titles.get(arg2).get("TitleID");
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	@Override
	protected void onPause() {
		super.onPause();
		// eProgress.dismiss();
		// EzeidUtil.DismissEzeidDialog();
	}

	/* Validation */
	private boolean validator() {
		if (type.getSelectedItemPosition() == -1) {
			EzeidUtil.showToast(SignUp.this, "Select an account type");
			return false;
		}

		if (ezeidStr.isEmpty()) {
			ezeid.setError(getResources().getString(R.string.required));
			// ezeid.setError(getResources().getString(R.string.required));
			return false;
		} else if (ezeidStr.length() < 3) {
			ezeid.setError(getResources().getString(
					R.string.min_character_warning));
			// ezeidTI.setError(getResources().getString(
			// R.string.min_character_warning));

			return false;
		}

		ezeid.setError(null);
		if (!idAvailable) {
			ezeid.setError("Check the availability of the id");
			// ezeidTI.setError("Check the availability of the id");
			return false;
		}

		ezeid.setError(null);
		if (!validatePassword(passwordStr, confPasswordStr)) {
			return false;
		} else {
			/*
			 * if(mgr.GetValueFromSharedPrefs("Token").isEmpty()){
			 * primaryInfoLayout.setVisibility(View.VISIBLE);
			 * primaryInfoLayout.startAnimation(anim); firstName.requestFocus();
			 * }
			 */
		}

		if (firstNameStr.isEmpty()) {
			if (idType != 3) {
				firstName.setError(getResources().getString(R.string.required));
				// firstNameTI.setError(getResources().getString(R.string.required));
				return false;
			}
		} else if (firstNameStr.length() < 3) {
			firstName.setError(getResources().getString(
					R.string.min_character_warning));
			// firstNameTI.setError(getResources().getString(R.string.min_character_warning));
		}

		firstName.setError(null);
		if (genderIndex == -1) {
			EzeidUtil.showToast(SignUp.this,
					getResources().getString(R.string.select_gender));
			return false;
		}

		if (idType == 2) {
			if (companyName.getText().toString().isEmpty()) {
				companyName.setError(getResources()
						.getString(R.string.required));
				// companyNameTI.setError(getResources()
				// .getString(R.string.required));
				return false;
			}
		}

		return true;
	}

	private void SessionDialog() {
		isSessionDialogShown = true;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.sessionExpired)
				.setMessage("Your session has expired. Please login again!")
				.setPositiveButton(R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								mgr.SaveValueToSharedPrefs("Token", "");
								Intent intent = new Intent(SignUp.this,
										SplashScreen.class);
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
										| Intent.FLAG_ACTIVITY_CLEAR_TASK);
								startActivity(intent);
								finish();
							}
						});
	}

	private void CheckSessionExpiredStatus() {
		// eProgress.show();
		EzeidUtil.ShowEzeidDialog(SignUp.this, "Loading");
		String token = mgr.GetValueFromSharedPrefs("Token");
		if (!token.equalsIgnoreCase("") && sessionRetry < 5) {
			JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
					EZEIDUrlManager.getAPIUrl() + "ewtGetLoginCheck?Token="
							+ token, new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {
							// eProgress.dismiss();
							EzeidUtil.DismissEzeidDialog();
							SessionStatus(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							// eProgress.dismiss();
							EzeidUtil.DismissEzeidDialog();
							EzeidUtil.showError(getApplicationContext(), error);
						}
					});
			EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
		} else {
			// eProgress.dismiss();
			EzeidUtil.DismissEzeidDialog();
		}
	}

	private void SessionStatus(JSONObject response) {
		if (!response.isNull("IsAvailable")) {
			try {
				String status = response.getString("IsAvailable");
				if (status.equalsIgnoreCase("true")) {
				} else {
					SessionDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void showCompanyProfileDialog(String tagline) {
		final Dialog dialog = new Dialog(SignUp.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.company_profile_dialog_layout);
		dialog.show();

		final EditText companyProfile = (EditText) dialog
				.findViewById(R.id.companyProfile);
		Button saveProfile = (Button) dialog
				.findViewById(R.id.saveCompanyProfile);
		Button cancelProfile = (Button) dialog
				.findViewById(R.id.cancelCompanyProfile);
		companyProfile.setText(tagline);

		saveProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!companyProfile.getText().toString().isEmpty()) {
					// eProgress.show();
					EzeidUtil.ShowEzeidDialog(SignUp.this, "Loading");
					Map<String, String> profileParams = new HashMap<String, String>();
					profileParams.put("AboutCompany", companyProfile.getText()
							.toString());
					profileParams.put("Token",
							mgr.GetValueFromSharedPrefs("Token"));
					NetworkHandler.SaveCompanyProfile(TAG, profileParams,
							handler);
					dialog.dismiss();
				} else {
					Toast.makeText(getApplicationContext(),
							"Enter company profile", Toast.LENGTH_LONG).show();
				}
			}
		});

		cancelProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
	}

	private void parseCompanyProfile(JSONObject obj) {
		try {
			if (obj.has("Result")) {
				JSONArray result = obj.getJSONArray("Result");
				if (result.length() > 0) {
					for (int i = 0; i < result.length(); i++) {
						String tagline = result.getJSONObject(i).getString(
								"TagLine");
						if (tagline.equalsIgnoreCase("null")) {
							tagline = "";
							showCompanyProfileDialog(tagline);
						} else {
							showCompanyProfileDialog(tagline);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void parseCompanyProfileSave(JSONObject obj) {
		try {
			if (obj.has("IsSuccessfull")) {
				if (obj.getBoolean("IsSuccessfull")) {
					EzeidUtil.showToast(SignUp.this, "Company profile updated");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
