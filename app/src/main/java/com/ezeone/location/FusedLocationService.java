package com.ezeone.location;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.view.View;

import com.ezeone.R;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.dialog.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FusedLocationService implements LocationListener,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {// 5 * 60 * 1000
	// private static final long INTERVAL = 1000 * 30;
	// private static final long FASTEST_INTERVAL = 1000 * 5;
	private static final long INTERVAL = 5 * 60 * 1000;
	private static final long FASTEST_INTERVAL = 2 * 60 * 1000;
	private static final long ONE_MIN = 1000 * 60;
	private static final long REFRESH_TIME = ONE_MIN * 5;
	private static final float MINIMUM_ACCURACY = 50.0f;
	private Activity locationActivity;
	private LocationRequest locationRequest;
	private GoogleApiClient googleApiClient;
	private Location location;
	private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;
	private MaterialDialog locDialog;
	private EzeidCountown timer;
	private final long startTime = 5000;
	private final long interval = 1000;
	private Context context;

	public FusedLocationService(Activity locationActivity, Context context) {
		locationRequest = LocationRequest.create();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setInterval(INTERVAL);
		locationRequest.setFastestInterval(FASTEST_INTERVAL);
		this.locationActivity = locationActivity;
		this.context = context;
		timer = new EzeidCountown(startTime, interval);
		timer.start();
		googleApiClient = new GoogleApiClient.Builder(locationActivity)
				.addApi(LocationServices.API).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).build();

		if (googleApiClient != null) {
			googleApiClient.connect();
		}
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Location currentLocation = fusedLocationProviderApi
				.getLastLocation(googleApiClient);
		if (currentLocation != null && currentLocation.getTime() > REFRESH_TIME) {
			location = currentLocation;
		} else {
			fusedLocationProviderApi.requestLocationUpdates(googleApiClient,
					locationRequest, this);
			// Schedule a Thread to unregister location listeners
			Executors.newScheduledThreadPool(1).schedule(new Runnable() {
				@Override
				public void run() {
					fusedLocationProviderApi.removeLocationUpdates(
							googleApiClient, FusedLocationService.this);
				}
			}, ONE_MIN, TimeUnit.MILLISECONDS);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// if the existing location is empty or
		// the current location accuracy is greater than existing accuracy
		// then store the current location
		if (null == this.location
				|| location.getAccuracy() < this.location.getAccuracy()) {
			this.location = location;
			// if the accuracy is not better, remove all location updates for
			// this listener
			if (this.location.getAccuracy() < MINIMUM_ACCURACY) {
				fusedLocationProviderApi.removeLocationUpdates(googleApiClient,
						this);
				// Home.mapProgress.setVisibility(View.INVISIBLE);
				// Home.locationFound.setVisibility(View.VISIBLE);
			}
		}
	}

	public Location getLocation() {
		return this.location;
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {

	}

	private class EzeidCountown extends CountDownTimer {

		public EzeidCountown(long startTime, long countDownInterval) {
			super(startTime, countDownInterval);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// mapProgress.setVisibility(View.VISIBLE);
		}

		@Override
		public void onFinish() {

			if (location == null) {
				// Home.locationFound.setVisibility(View.INVISIBLE);
				SearchLocation();
			}/*
			 * else { //Home.locationFound.setVisibility(View.VISIBLE); }
			 */
		}
	}

	private void SearchLocation() {

		locDialog = new MaterialDialog(context);
		if (locDialog != null) {
			locDialog.setBackgroundResource(R.drawable.abc_cab_background_internal_bg);
			final String cancel = context.getResources().getString(
					R.string.cancel);
			locDialog
					.setTitle("EZEID")
					.setMessage(
							"Unable to get your current location. Please make sure getting the Google location servics! ")
					.setPositiveButton("OK", new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							locDialog.dismiss();
							locationActivity.startActivityForResult(new Intent(
									Settings.ACTION_LOCATION_SOURCE_SETTINGS),
									EzeidUtil.LOCATION_CODE);
						}
					}).setNegativeButton(cancel, new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							locDialog.dismiss();
						}
					}).setCanceledOnTouchOutside(false).show();
		}
		locDialog.show();
	}
}