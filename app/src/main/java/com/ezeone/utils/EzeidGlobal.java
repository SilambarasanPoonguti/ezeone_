package com.ezeone.utils;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

public class EzeidGlobal extends Application {

	public static final String TAG = EzeidGlobal.class.getSimpleName();
	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;
	private static EzeidGlobal mInstance;
	// public static ArrayList<HashMap<String, String>> titleList;

//	public static String EzeidUrl = "http://182.73.205.244:3001/";

//	 public static String EzeidUrl = "http://10.0.100.223:3001/";
//	 public static String EzeidUrl = "http://104.199.128.226:3001/";
//	 public static String EzeidUrl = "https://www.ezeid.com/";
	public static String EzeidUrl = "https://www.ezeone.com/";
	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		TypefaceUtil.overrideFont(getApplicationContext(), "SERIF",
				"fonts/Roboto-Light.ttf");
	}

	public static synchronized EzeidGlobal getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(this.mRequestQueue,
					new LruBitmapCache());
		}
		return this.mImageLoader;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		// set the default tag if tag is empty
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		VolleyLog.d("Adding request to queue: %s", req.getUrl());
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	/**
	 * Cancels all pending requests by the specified TAG, it is important to
	 * specify a TAG so that the pending/ongoing requests can be cancelled.
	 * 
	 * @param tag
	 */
	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}

	public static JSONArray userDetailsObj = null;

	public static final long MIN_TIME = 400;
	public static final float MIN_DISTANCE = 1000;

	public static final int PICK_IMAGE = 1;
	public static final int CAPTURE_IMAGE = 2;
	public static final int EZEID_SEARCH_LOC = 12;
	public static final int EZEID_DELAY_IN_MILLIS = 1000;
	public static final int CAMERA_CHANGE_CALLBACK = 420;
	public static final int CAMERA_DELAY = 1000;
	public static final int ACTIVITY_RECORD_SOUND = 4;
	public static int LAST_SELECTED_POS = 0;
	public static String EZEID_TYPE = "";
	public static String SALES_TYPE = "";

	public static final CharSequence[] items = { "Take Photo",
			"Choose from Library", "Cancel" };

	public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete/";
	public static final String OUT_JSON = "json?";

	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

}