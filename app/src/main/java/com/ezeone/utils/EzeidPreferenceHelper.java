package com.ezeone.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class EzeidPreferenceHelper {

	Context context;
	SharedPreferences sharedPreferences;

	public EzeidPreferenceHelper(Context context) {
		this.context = context;
		sharedPreferences = context.getSharedPreferences("Ezeid",
				Context.MODE_PRIVATE);
	}

	public SharedPreferences getGcmPreferences() {

		return sharedPreferences;
	}

	public String GetValueFromSharedPrefs(String KeyName) {
		return sharedPreferences.getString(KeyName, "");
	}

	

	public boolean SaveValueToSharedPrefs(String KeyName, String value) {
		Editor editor = sharedPreferences.edit();
		editor.putString(KeyName, value);
		editor.commit();
		return true;
	}

	public int GetIntFromSharedPrefs(String KeyName) {
		return sharedPreferences.getInt(KeyName, 1);
	}

	public boolean GetBoolFromSharedPrefs(String KeyName) {
		return sharedPreferences.getBoolean(KeyName, false);
	}

	public void savePreferenceIndex(String keyName, int index) {
		Editor editor = sharedPreferences.edit();
		editor.putInt(keyName, index);
		editor.commit();
	}

	public int getPreferenceIndex(String keyName) {
		return sharedPreferences.getInt(keyName, 0);
	}
	
	public void SaveBooleanValueToSharedPrefs(String KeyName,boolean value){
		Editor editor = sharedPreferences.edit();
		editor.putBoolean(KeyName, value);
		editor.commit();
	}
}
