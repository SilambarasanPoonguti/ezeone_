package com.ezeone.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ezeone.R;
import com.ezeone.SplashScreen;
import com.ezeonelib.widget.cruton.Crouton;
import com.ezeonelib.widget.cruton.Style;

public class EzeidUtil {

	public static File Copy_sourceLocation;
	public static File Paste_Target_Location;
	public static File MY_IMG_DIR, Default_DIR;
	public static Uri uri;
	public static Intent pictureActionIntent = null;
	public static final int CAMERA_PICTURE = 1;
	public static final int GALLERY_PICTURE = 2;
	public static final int LOCATION_CODE = 3;
	private static final int ANIMATION_DURATION = 300;

	private static AnimatorSet mExpandAnimation = new AnimatorSet()
			.setDuration(ANIMATION_DURATION);
	private static AnimatorSet mCollapseAnimation = new AnimatorSet()
			.setDuration(ANIMATION_DURATION);
	private static final float COLLAPSED_PLUS_ROTATION = 0f;
	private static final float EXPANDED_PLUS_ROTATION = 90f + 45f;

	private static int color;
	private static Paint paint;
	private static Rect rect;
	private static RectF rectF;
	private static Bitmap imageOut;
	private static Canvas canvas;
	private static float roundPx;

	private static boolean mExpanded;
	private static EzeidLoadingProgress ezeidLoadingProgress;
	private static Handler handlerDialog;
	private static Runnable runnableDialog;

	public static void expand(final View v) {
		v.measure(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		final int targetHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT
						: (int) (targetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (targetHeight / v.getContext().getResources()
				.getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime,
					Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight
							- (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (initialHeight / v.getContext().getResources()
				.getDisplayMetrics().density));
		v.startAnimation(a);
	}

	public static String Get_Random_File_Name() {
		final Calendar c = Calendar.getInstance();
		int myYear = c.get(Calendar.YEAR);
		int myMonth = c.get(Calendar.MONTH);
		int myDay = c.get(Calendar.DAY_OF_MONTH);
		String Random_Image_Text = "" + myDay + myMonth + myYear + "_"
				+ Math.random();
		return Random_Image_Text;
	}

	// Copy your image into specific folder
	public static File copyFile(File current_location, File destination_location) {
		Copy_sourceLocation = new File("" + current_location);
		Paste_Target_Location = new File("" + destination_location + "/"
				+ EzeidUtil.Get_Random_File_Name() + ".jpg");

		Log.v("Purchase-File", "sourceLocation: " + Copy_sourceLocation);
		Log.v("Purchase-File", "targetLocation: " + Paste_Target_Location);
		try {
			// 1 = move the file, 2 = copy the file
			int actionChoice = 2;
			// moving the file to another directory
			if (actionChoice == 1) {
				if (Copy_sourceLocation.renameTo(Paste_Target_Location)) {
					Log.i("Purchase-File", "Move file successful.");
				} else {
					Log.i("Purchase-File", "Move file failed.");
				}
			}

			// we will copy the file
			else {
				// make sure the target file exists
				if (Copy_sourceLocation.exists()) {

					InputStream in = new FileInputStream(Copy_sourceLocation);
					OutputStream out = new FileOutputStream(
							Paste_Target_Location);

					// Copy the bits from instream to outstream
					byte[] buf = new byte[1024];
					int len;

					while ((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
					in.close();
					out.close();

					Log.i("copyFile", "Copy file successful.");

				} else {
					Log.i("copyFile", "Copy file failed. Source file missing.");
				}
			}

		} catch (NullPointerException e) {
			Log.i("copyFile", "" + e);

		} catch (Exception e) {
			Log.i("copyFile", "" + e);
		}
		return Paste_Target_Location;
	}

	// decode your image into bitmap format
	public static Bitmap decodeFile(File f) {
		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 70;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale++;
			}

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Create New Dir (folder) if not exist
	public static File Create_MY_IMAGES_DIR() {
		try {
			// Get SD Card path & your folder name
			MY_IMG_DIR = new File(Environment.getExternalStorageDirectory(),
					"/My_Image/");

			// check if exist
			if (!MY_IMG_DIR.exists()) {
				// Create New folder
				MY_IMG_DIR.mkdirs();
				Log.i("path", ">>.." + MY_IMG_DIR);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return MY_IMG_DIR;
	}

	public static boolean isConnectedToInternet(final Context scontext) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) scontext
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null)
					for (int i = 0; i < info.length; i++)
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}

			}
			return false;
		} catch (Exception e) {

			return false;
		}
	}

	public static Bitmap ByteTOBitmap(String blobs) {
		Bitmap bitmap = null;
		// bitmap.recycle();
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();// Create
																		// object
																		// of
																		// bitmapfactory's
																		// option
																		// method
																		// for
																		// further
																		// option
																		// use
			options.inPurgeable = true;
			byte[] decodedString = Base64.decode(blobs, Base64.DEFAULT);
			bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
					decodedString.length, options);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void showToast(Context context, String message) {

		Style style = new Style.Builder()
				.setBackgroundColorValue(Color.parseColor("#4cb5ab"))
				.setGravity(Gravity.CENTER_HORIZONTAL)
				.setTextColorValue(Color.parseColor("#FFFFFF")).build();

		Crouton.showText((Activity) context, message, style);
	}

	public class BitmapScaler {
		// Scale and maintain aspect ratio given a desired width
		// BitmapScaler.scaleToFitWidth(bitmap, 100);
		public Bitmap scaleToFitWidth(Bitmap b, int width) {
			float factor = width / (float) b.getWidth();
			return Bitmap.createScaledBitmap(b, width,
					(int) (b.getHeight() * factor), true);
		}

		// Scale and maintain aspect ratio given a desired height
		// BitmapScaler.scaleToFitHeight(bitmap, 100);
		public Bitmap scaleToFitHeight(Bitmap b, int height) {
			float factor = height / (float) b.getHeight();
			return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor),
					height, true);
		}

	}

	public static void scaleImage(ImageView view, int boundBoxInDp) {
		// Get the ImageView and its bitmap
		Drawable drawing = view.getDrawable();
		Bitmap bitmap = ((BitmapDrawable) drawing).getBitmap();

		// Get current dimensions
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();

		// Determine how much to scale: the dimension requiring less scaling is
		// closer to the its side. This way the image always stays inside your
		// bounding box AND either x/y axis touches it.
		float xScale = ((float) boundBoxInDp) / width;
		float yScale = ((float) boundBoxInDp) / height;
		float scale = (xScale <= yScale) ? xScale : yScale;

		// Create a matrix for the scaling and add the scaling data
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale);

		// Create a new bitmap and convert it to a format understood by the
		// ImageView
		Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height,
				matrix, true);
		BitmapDrawable result = new BitmapDrawable(scaledBitmap);
		width = scaledBitmap.getWidth();
		height = scaledBitmap.getHeight();

		// Apply the scaled bitmap
		view.setImageDrawable(result);

		// Now change ImageView's dimensions to match the scaled image
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
				.getLayoutParams();
		params.width = width;
		params.height = height;
		view.setLayoutParams(params);
	}

	// private int dpToPx(int dp)
	// {
	// float density =
	// getApplicationContext().getResources().getDisplayMetrics().density;
	// return Math.round((float)dp * density);
	// }

	public static Bitmap createBitmapFromLayout(Context context,
			String iconStr, String typeStatus, String roleType) {
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// Inflate the layout into a view and configure it the way you like
		RelativeLayout view = new RelativeLayout(context);
		mInflater.inflate(R.layout.ezeid_business_drawable_layout, view, true);
		ImageView icon = (ImageView) view.findViewById(R.id.roleTypeMarker);
		ImageView statusImg = (ImageView) view.findViewById(R.id.statusImg);
		if (!iconStr.equalsIgnoreCase("")) {
			// iconStr = iconStr.substring(22, iconStr.length());
			Bitmap iconBitmap = ByteTOBitmap(iconStr);
			if (iconBitmap != null) {
				icon.setImageBitmap(iconBitmap);
			} else {
				icon.setImageResource(R.drawable.ic_launcher);
			}
		} else {
			icon.setImageResource(R.drawable.ic_launcher);
		}

		if (roleType.equalsIgnoreCase("2") || roleType.equalsIgnoreCase("3")) {
			int res = 0;
			statusImg.setVisibility(View.VISIBLE);
			if (typeStatus.equalsIgnoreCase("1")) {
				res = R.drawable.ic_action_open_business;
				statusImg.setImageResource(res);
			} else if (typeStatus.equalsIgnoreCase("2")) {
				res = R.drawable.ic_action_close_business;
				statusImg.setImageResource(res);
			} else {
				statusImg.setVisibility(View.GONE);
			}

		} else {
			statusImg.setVisibility(View.GONE);
		}

		// Provide it with a layout params. It should necessarily be wrapping
		// the
		// content as we not really going to have a parent for it.
		view.setLayoutParams(new LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));

		// Pre-measure the view so that height and width don't remain null.
		view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

		// Assign a size and position to the view and all of its descendants
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

		// Create the bitmap
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
				view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		// Create a canvas with the specified bitmap to draw into
		Canvas c = new Canvas(bitmap);

		// Render this view (and all of its children) to the given Canvas
		view.draw(c);
		return bitmap;
	}

	public static Bitmap DynamicMarker(Context context, String name,
			String typeStatus, String roleType) {
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// Inflate the layout into a view and configure it the way you like
		final RelativeLayout view = new RelativeLayout(context);
		mInflater.inflate(R.layout.ezeid_dynamic_marker, view, true);
		TextView markerTitle = (TextView) view.findViewById(R.id.markerTitle);
		ImageView markerImage = (ImageView) view.findViewById(R.id.markerImage);
		ImageView markerStatus = (ImageView) view
				.findViewById(R.id.markerStatus);
		if (name.equalsIgnoreCase("")) {
			markerTitle.setVisibility(View.GONE);
		} else {
			markerTitle.setVisibility(View.VISIBLE);
			markerTitle.setText(name);
		}

		if (roleType.equalsIgnoreCase("1")) {
			markerImage.setImageResource(R.drawable.ic_marker_individual);
		} else if (roleType.equalsIgnoreCase("2")) {
			markerImage.setImageResource(R.drawable.ic_marker_business);
		} else {// selected_marker
			markerImage.setImageResource(R.drawable.ic_marker_public_places);
		}

		if (roleType.equalsIgnoreCase("2") || roleType.equalsIgnoreCase("3")) {
			int res = 0;
			markerStatus.setVisibility(View.VISIBLE);
			if (typeStatus.equalsIgnoreCase("1")) {
				res = R.drawable.ic_action_open_business;
				markerStatus.setImageResource(res);
			} else if (typeStatus.equalsIgnoreCase("2")) {
				res = R.drawable.ic_action_close_business;
				markerStatus.setImageResource(res);
			} else {
				markerStatus.setVisibility(View.GONE);
			}

		} else {
			markerStatus.setVisibility(View.GONE);
		}

		// Provide it with a layout params. It should necessarily be wrapping
		// the
		// content as we not really going to have a parent for it.
		view.setLayoutParams(new LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT));

		// Pre-measure the view so that height and width don't remain null.
		view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

		// Assign a size and position to the view and all of its descendants
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

		// Create the bitmap
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
				view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		// Create a canvas with the specified bitmap to draw into
		Canvas c = new Canvas(bitmap);

		// Render this view (and all of its children) to the given Canvas
		view.draw(c);
		return bitmap;
	}

	private static class RotatingDrawable extends LayerDrawable {
		public RotatingDrawable(Drawable drawable) {
			super(new Drawable[] { drawable });
		}

		private float mRotation;

		public float getRotation() {
			return mRotation;
		}

		public void setRotation(float rotation) {
			mRotation = rotation;
			invalidateSelf();
		}

		@Override
		public void draw(Canvas canvas) {
			canvas.save();
			canvas.rotate(mRotation, getBounds().centerX(), getBounds()
					.centerY());
			super.draw(canvas);
			canvas.restore();
		}
	}

	public static Drawable getIconDrawable(Drawable drawable) {

		final RotatingDrawable rotatingDrawable = new RotatingDrawable(drawable);

		final OvershootInterpolator interpolator = new OvershootInterpolator();

		final ObjectAnimator collapseAnimator = ObjectAnimator.ofFloat(
				rotatingDrawable, "rotation", EXPANDED_PLUS_ROTATION,
				COLLAPSED_PLUS_ROTATION);
		final ObjectAnimator expandAnimator = ObjectAnimator.ofFloat(
				rotatingDrawable, "rotation", COLLAPSED_PLUS_ROTATION,
				EXPANDED_PLUS_ROTATION);
		rotatingDrawable.setRotation(45f);
		collapseAnimator.setInterpolator(interpolator);
		expandAnimator.setInterpolator(interpolator);

		mExpandAnimation.play(expandAnimator);
		mCollapseAnimation.play(collapseAnimator);

		return rotatingDrawable;
	}

	public static void toggle() {
		if (mExpanded) {
			collapse();
		} else {
			expand();
		}
	}

	public static void collapse() {
		if (mExpanded) {
			mExpanded = false;
			mCollapseAnimation.start();
			mExpandAnimation.cancel();
		}
	}

	public static void expand() {

		// mCollapseAnimation.cancel();
		mExpandAnimation.start();
	}

	public static byte[] getBytesFromBitmap(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 100, stream);
		return stream.toByteArray();
	}

	public static byte[] getCompressedBytesFromBitmap(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 60, stream);
		return stream.toByteArray();
	}

	public static Bitmap decodeFile(String path) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, o);
			// The new size we want to scale to
			final int REQUIRED_SIZE = 70;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeFile(path, o2);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getMimeType(String url) {
		String type = null;
		String extension = MimeTypeMap.getFileExtensionFromUrl(url);
		if (extension != null) {
			MimeTypeMap mime = MimeTypeMap.getSingleton();
			type = mime.getMimeTypeFromExtension(extension);
		}
		return type;
	}

	public static Bitmap GetRoundedRectBitmap(Bitmap bitmap, int pixels) {
		imageOut = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
				Bitmap.Config.ARGB_8888);
		canvas = new Canvas(imageOut);

		color = 0xff424242;
		paint = new Paint();
		rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		rectF = new RectF(rect);
		roundPx = pixels;

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		/*
		 * paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
		 * canvas.drawBitmap(bitmap, rect, rect, paint);
		 */

		return imageOut;
	}

	public static String GetFileType(String picture) {
		String base64 = "";
		try {
			StringTokenizer tokenizer = new StringTokenizer(picture, "/");
			String srctype1 = tokenizer.nextToken();
			String srctype2 = tokenizer.nextToken();
			StringTokenizer tokenizer2 = new StringTokenizer(srctype2, ";");
			String type1 = tokenizer2.nextToken();
			if (type1.length() == 3) {
				base64 = picture.substring(22, picture.length());
			} else if (type1.length() == 4) {
				base64 = picture.substring(23, picture.length());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return base64;
	}

	// Load a bitmap from a resource with a target size
	public static Bitmap decodeSampledBitmapFromResource(Resources res,
			int resId, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	// Load a bitmap from a resource with a target size
	public static Bitmap decodeDynamicBitmapFromResource(String blobs,
			int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		byte[] decodedString = Base64.decode(blobs, Base64.DEFAULT);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);
		Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
				decodedString.length, options);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return bitmap;
	}

	// Given the bitmap size and View size calculate a subsampling size (powers
	// of 2)
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		int inSampleSize = 1; // Default subsampling size
		// See if image raw height and width is bigger than that of required
		// view
		if (options.outHeight > reqHeight || options.outWidth > reqWidth) {
			// bigger
			final int halfHeight = options.outHeight / 2;
			final int halfWidth = options.outWidth / 2;
			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}
		return inSampleSize;
	}

	public static Bitmap convertBitmap(String blobs) {

		Bitmap bitmap = null;
		BitmapFactory.Options bfOptions = new BitmapFactory.Options();
		bfOptions.inDither = false; // Disable Dithering mode
		bfOptions.inPurgeable = true; // Tell to gc that whether it needs free
										// memory, the Bitmap can be cleared
		bfOptions.inInputShareable = true; // Which kind of reference will be
											// used to recover the Bitmap data
											// after being clear, when it will
											// be used in the future
		bfOptions.inTempStorage = new byte[32 * 1024];
		byte[] decodedString = Base64.decode(blobs, Base64.DEFAULT);
		bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
				decodedString.length, bfOptions);

		return bitmap;
	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(),
				MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
						LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}

	public static void SetListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null) {
			// pre-condition
			return;
		}

		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight
				+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
	}

	public static JSONObject getJSONfromURL(String url) {

		// initialize
		InputStream is = null;
		String result = "";
		JSONObject jObject = null;

		// http post
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObject = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jObject;
	}

	private static void blinkMarker(ImageView imageView) {
		Animation animation = new AlphaAnimation(1, 0);
		animation.setDuration(1400);
		animation.setInterpolator(new LinearInterpolator());
		animation.setRepeatCount(Animation.INFINITE);
		animation.setRepeatMode(Animation.REVERSE);
		imageView.startAnimation(animation);
	}

	public static void ChangeEditEffect(final LinearLayout layout,
			Context context) {
		Integer colorFrom = context.getResources().getColor(R.color.floatBtnBg);
		Integer colorTo = context.getResources().getColor(R.color.white);
		ValueAnimator valueAnimator = ValueAnimator.ofObject(
				new ArgbEvaluator(), colorFrom, colorTo);
		valueAnimator.setDuration(500);
		valueAnimator.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator animator) {
				// TODO Auto-generated method stub
				layout.setBackgroundColor((Integer) animator.getAnimatedValue());
			}
		});
		valueAnimator.start();

	}

	public static String milliSecondsToTimer(long milliseconds) {
		String finalTimerString = "";
		String secondsString = "";

		// Convert total duration into time
		int hours = (int) (milliseconds / (1000 * 60 * 60));
		int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
		int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
		// Add hours if there
		if (hours > 0) {
			finalTimerString = hours + ":";
		}

		// Prepending 0 to seconds if it is one digit
		if (seconds < 10) {
			secondsString = "0" + seconds;
		} else {
			secondsString = "" + seconds;
		}

		finalTimerString = finalTimerString + minutes + ":" + secondsString;

		// return timer string
		return finalTimerString;
	}

	public static int getProgressPercentage(long currentDuration,
			long totalDuration) {
		Double percentage = (double) 0;

		long currentSeconds = (int) (currentDuration / 1000);
		long totalSeconds = (int) (totalDuration / 1000);

		// calculating percentage
		percentage = (((double) currentSeconds) / totalSeconds) * 100;

		// return percentage
		return percentage.intValue();
	}

	public static int progressToTimer(int progress, int totalDuration) {
		int currentDuration = 0;
		totalDuration = (int) (totalDuration / 1000);
		currentDuration = (int) ((((double) progress) / 100) * totalDuration);

		// return current duration in milliseconds
		return currentDuration * 1000;
	}

	public static void setErrorMethod(final EditText et, final int maxLength) {
		et.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() > maxLength) {
					et.setError("Max length is " + (maxLength + 1));
				}
				if (s.length() < maxLength) {
					et.setError(null);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.toString().length() > maxLength) {
					et.setError("Max length is " + (maxLength + 1));
				}
			}
		});
	}

	public static void ShowEzeidDialog(Context ctx, String content) {
		ezeidLoadingProgress = new EzeidLoadingProgress(ctx, content);
		handlerDialog = new Handler();
		runnableDialog = new Runnable() {

			@Override
			public void run() {
				if (ezeidLoadingProgress != null) {
					if (ezeidLoadingProgress.isShowing()) {
						ezeidLoadingProgress.dismiss();
					}
				}
			}
		};
		ezeidLoadingProgress.show();
	}

	public static void DismissEzeidDialog() {
		handlerDialog.removeCallbacks(runnableDialog);
		if (ezeidLoadingProgress.isShowing()) {
			ezeidLoadingProgress.dismiss();
		}
	}

	public static void showError(Context context, VolleyError err) {
		try {
			if (err instanceof TimeoutError) {
				showToast(context, "Connection Timed out. Please try again");
			} else if (err instanceof ServerError) {
				showToast(context,
						"Server did not respond properly. Please try again");
			} else if (err instanceof AuthFailureError) {
				showToast(context, "Something went wrong. Please try again");
				showSessionExpiredDialog(context);
			} else if (err instanceof NoConnectionError) {
				if (err.getMessage().contains(
						"No authentication challenges found")) {
					showSessionExpiredDialog(context);
				} else {
					showToast(context, "Please check your Internet connection.");
				}
			} else if (err instanceof NetworkError) {
				if (err.getMessage().contains(
						"No authentication challenges found")) {
					showSessionExpiredDialog(context);
				} else {
					showToast(context, "Please check your Internet connection.");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void showSessionExpiredDialog(final Context context) {
		final EzeidPreferenceHelper mgr = new EzeidPreferenceHelper(context);
		AlertDialog.Builder builder1;
		builder1 = new AlertDialog.Builder(context);
		builder1.setTitle("Session Expired!");
		builder1.setMessage("Your Session has expired. Please login again");
		builder1.setCancelable(false);
		builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
				mgr.SaveValueToSharedPrefs("Token", "");
				Intent homeIntent = new Intent(context, SplashScreen.class);
				homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				context.startActivity(homeIntent);
				((Activity) context).finish();
			}
		});
		AlertDialog alert11 = builder1.create();
		alert11.show();
	}

}