package com.ezeone.listeners;

import android.location.Address;

public interface AddressResultListener {
	public void onAddressAvailable(Address address);
}
