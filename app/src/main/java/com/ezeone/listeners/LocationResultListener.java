package com.ezeone.listeners;

import android.location.Location;

public interface LocationResultListener {

	public void onLocationResultAvailable(Location location);
}
