package com.ezeone.listeners;

public interface OnGestureListener {
	void onSwipe(int direction);

	void onDoubleTap();
}
