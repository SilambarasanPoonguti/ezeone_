package com.ezeone.listeners;

import org.json.JSONArray;

public interface MapSearchResultsListener {
	public void onSearchResults(JSONArray array);
}
