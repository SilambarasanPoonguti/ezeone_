package com.ezeone.helpers;

import android.content.Context;
import android.os.AsyncTask;

import com.ezeone.listeners.MapSearchResultsListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class MapSearchResults extends AsyncTask<String, String, JSONArray> {

	private String API_KEY;
	Context ctx;
//	double latitude, longitude;
	String keyword,type;
	MapSearchResultsListener listener;

	public MapSearchResults(String apikey, Context context,  MapSearchResultsListener listener, String keyword) {
		this.API_KEY = apikey;
		this.ctx = context;
//		this.latitude = latitude;
//		this.longitude = longitude;
		this.listener = listener;
		this.keyword = keyword;
	}

	// https://maps.googleapis.com/maps/api/place/search/json?location=28.632808,77.218276&radius=500&types=atm&sensor=false&key=apikey
	//https://maps.googleapis.com/maps/api/place/textsearch/json?&location=12.935281456079087,77.57393784821033&radius=1000&sensor=true&query=kodaikanal&key=AIzaSyCaTEJkxndoKLL5grE6NE-_bhkBdYIy7Dw
	private String makeUrl(String place) {
		place = place.replaceAll(" ", "%20");
		StringBuilder urlString = new StringBuilder(
				"https://maps.googleapis.com/maps/api/place/textsearch/json?");
		// https://maps.googleapis.com/maps/api/place/textsearch/json?location=12.931752336668358,77.57201168686152&radius=500&sensor=true&query=cafe&key=AIzaSyCaTEJkxndoKLL5grE6NE-_bhkBdYIy7Dw
//		if (place.equals("")) {
//			urlString.append("&location=");
//			urlString.append(Double.toString(latitude));
//			urlString.append(",");
//			urlString.append(Double.toString(longitude));
//			urlString.append("&radius=1000");
			// urlString.append("&types="+place);
			urlString.append("&sensor=true&key=" + API_KEY);
//		} else {
//			urlString.append("&location=");
//			urlString.append(Double.toString(latitude));
//			urlString.append(",");
//			urlString.append(Double.toString(longitude));
//			urlString.append("&radius=1000");
			urlString.append("&sensor=true");
			urlString.append("&query=" + place);
			urlString.append("&key=" + API_KEY);

//		}

		return urlString.toString();

	}

	protected String getJSON(String url) {
		return getUrlContents(url);
	}

	private String getUrlContents(String theUrl) {

		StringBuilder content = new StringBuilder();

		try {
			URL url = new URL(theUrl);
			URLConnection urlConnection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(urlConnection.getInputStream()), 8);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				content.append(line + "\n");
			}
			bufferedReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content.toString();

	}

	public JSONObject getLocationFormGoogle(String urlString) {

		// HttpGet httpGet = new HttpGet(
		// "http://maps.google.com/maps/api/geocode/json?address="
		// + placesName + "&ka&sensor=false");
		// URL url = new URL(urlString);
		// URLConnection urlConnection = url.openConnection();
		//urlString = urlString.replaceAll(" ", "%20");
		HttpGet httpGet = new HttpGet(urlString);
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject;
	}

	@Override
	protected JSONArray doInBackground(String... params) {

		JSONArray array = null;
		try {

			String urlString = makeUrl(keyword);
			// String json = getJSON(urlString);
			// System.out.println(json);
			// JSONObject object = new JSONObject(json);
			JSONObject object = getLocationFormGoogle(urlString);
			array = object.getJSONArray("results");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array;
	}

	@Override
	protected void onPostExecute(JSONArray result) {
		super.onPostExecute(result);
		if (result != null) {
			if (listener != null) {
				listener.onSearchResults(result);
			}
		}
	}

}
