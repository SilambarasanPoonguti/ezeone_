package com.ezeone.helpers;

public final class EZEIDUrlManager {
	
	private final static boolean isDebug = false;
//	private final static String uatAPI = "http://182.73.205.244:3001/";
	private final static String uatAPI = "http://104.199.128.226:3001/";
	private final static String prodAPI = "https://www.ezeone.com/";
	
	public static String getAPIUrl()
	{
		return isDebug?uatAPI:prodAPI;
	}

}
