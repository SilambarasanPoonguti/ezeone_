package com.ezeone.helpers;

public class Constant {
	public static final class MessageState {
		public static final int LOCATION_AVAILABLE = 1;
		public static final int LOCATION_UNAVAILABLE = 2;
		public static final int EZEID_AVAILABLE = 3;
		public static final int EZEID_NOT_AVAILABLE = 4;
		public static final int CATEGORIES_AVAILABLE = 5;
		public static final int CATEGORIES_UNAVAILABLE = 6;
		public static final int CITIES_AVAILABLE = 7;
		public static final int CITIES_UNAVAILABLE = 8;
		public static final int COUNTRIES_AVAILABLE = 9;
		public static final int COUNTRIES_UNAVAILABLE = 10;
		public static final int ROLES_AVAILABLE = 11;
		public static final int ROLES_UNAVAILABLE = 12;
		public static final int QUICK_REGISTRATION_SUCCESS = 13;
		public static final int QUICK_REGISTRATION_FAILED = 14;
		public static final int STATES_AVAILABLE = 15;
		public static final int STATES_UNAVAILABLE = 16;
		public static final int LANGUAGES_AVAILABLE = 17;
		public static final int LANGUAGES_UNAVAILABLE = 18;
		public static final int TITLES_AVAILABLE = 19;
		public static final int TITLES_UNAVAILABLE = 20;
		public static final int FUNCTIONS_AVAILABLE = 21;
		public static final int FUNCTIONS_UNAVAILABLE = 22;
		public static final int PRIMARY_DATA_SAVE_SUCCESS = 23;
		public static final int PRIMARY_DATA_SAVE_FAIL = 24;
		public static final int LOCATION_SAVE_SUCCESS = 25;
		public static final int LOCATION_SAVE_FAILED = 26;
		public static final int LOCATION_DELETE_SUCCESS = 27;
		public static final int LOCATION_DELETE_FAILED = 28;
		public static final int USER_DETAILS_AVAILABLE = 29;
		public static final int USER_DETAILS_UNAVAILABLE = 30;
		public static final int DOCUMENT_SAVED = 31;
		public static final int DOCUMENT_SAVE_FAILED = 32;
		public static final int DOWNLOAD_SUCCESS = 33;
		public static final int DOWNLOAD_FAILED = 34;
		public static final int SECONDARY_LOCATION_AVAILABLE = 35;
		public static final int SECONDARY_LOCATION_UNAVAILABLE = 36;
		public static final int PASSWORD_SENT = 37;
		public static final int PASSWORD_NOT_SENT = 38;
		public static final int PROFILE_PICTURE_UPDATED = 39;
		public static final int PROFILE_PICTURE_UPDATE_FAILED = 40;
		public static final int PASSWORD_CHANGED = 41;
		public static final int PASSWORD_CHANGE_FAILED = 42;
		public static final int BUSINESS_LISTING_UPDATED = 43;
		public static final int BUSINESS_LISTING_UPDATE_FAILED = 44;
		public static final int BUSINESS_LISTING_GET_SUCCESS = 45;
		public static final int BUSINESS_LISTING_GET_FAILED = 46;
		public static final int DOCUMENT_UPLOAD_SUCCESS = 47;
		public static final int DOCUMENT_UPLOAD_FAILED = 48;
		public static final int DOCUMENT_AVAILABLE = 49;
		public static final int DOCUMENT_UNAVAILABLE = 50;
		public static final int DOCUMENT_PIN_UPDATED = 51;
		public static final int DOCUMENT_PIN_UPDATE_FAILED = 52;
		public static final int DOCUMENT_INFO_AVAILABLE = 53;
		public static final int DOCUMENT_INFO_UNAVAILABLE = 54;
		public static final int PIN_AVAILABLE = 55;
		public static final int PIN_UNAVAILABLE = 56;
		public static final int CV_INFO_UPDATED = 57;
		public static final int CV_INFO_UPDATE_FAILED = 58;
		public static final int CV_INFO_AVAILABLE = 59;
		public static final int CV_INFO_UNAVAILABLE = 60;
		public static final int FUNCTION_ROLE_MAPPED_AVAILABLE = 61;
		public static final int FUNCTION_ROLE_MAPPED_UNAVAILABLE = 62;
		public static final int SESSION_EXPIRED = 64;
		public static final int SESSION_EXPIRED_ERROR = 65;

		public static final int REQUEST_USERDETAIL_SUCCESS = 66;
		public static final int REQUEST_USERDETAIL_FAIL = 67;
		public static final int REQUEST_SESSION_SUCCESS = 68;
		public static final int REQUEST_SESSION_FAIL = 69;
		public static final int REQUEST_LOGIN_SUCCESS = 70;
		public static final int REQUEST_LOGIN_FAIL = 71;
		public static final int REQUEST_SEARCH_INFO_SUCCESS = 72;
		public static final int REQUEST_SEARCH_INFO_FAIL = 73;

		public static final int COMPANY_PROFILE_SAVED = 92;
		public static final int COMPANY_PROFILE_NOT_SAVED = 93;

		public static final int COMPANY_PROFILE_AVAILABLE = 94;
		public static final int COMPANY_PROFILE_NOT_AVAILABLE = 95;
		
		public static final int PRIMARY_DETAILS_AVAILABLE = 96;
		public static final int PRIMARY_DETAILS_NOTAVAILABLE = 97;
		
		public static final int RESOURCES_AVAILABLE = 98;
		public static final int RESOURCES_UNAVAILABLE = 99;
		
		public static final int SERVICES_AVAILABLE = 100;
		public static final int SERVICES_UNAVAILABLE = 101;
		
		public static final int SERVICE_RESOURCE_AVAILABLE = 102;
		public static final int SERVICE_RESOURCE_UNAVAILABLE = 103;

		public static final int RESERVATION_SAVED = 104;
		public static final int RESERVATION_SAVE_FAILED = 105;
		
		public static final int RESERVATION_DATA_AVAILABLE = 106;
		public static final int RESERVATION_DATA_NOT_AVAILABLE = 107;
		
		public static final int RESERVATION_DETAILS_AVAILABLE = 108;
		public static final int RESERVATION_DETAILS_NOT_AVAILABLE = 109;
		
		public static final int RESERVATION_DETAILS_UPDATED = 110;
		public static final int RESERVATION_DETAILS_UPDATE_FAILED = 111;
		
		public static final int WORKING_HOURS_AVAILABLE = 112;
		public static final int WORKING_HOURS_UNAVAILABLE = 113;
	}
}
