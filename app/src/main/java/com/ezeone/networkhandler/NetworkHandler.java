package com.ezeone.networkhandler;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.ezeone.helpers.Constant;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidPreferenceHelper;

public class NetworkHandler {

    private static EzeidPreferenceHelper mgr;

    public static void PlacesAutocomplete(String tag, Handler handler,
                                          final Map<String, String> params, String url) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.LOCATION_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.LOCATION_UNAVAILABLE));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void CheckEzeId(String tag, Handler handler,
                                  final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewGetEZEID?EZEID="
                + params.get("EZEID");

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.EZEID_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.EZEID_NOT_AVAILABLE));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void GetCategories(String tag, Handler handler,
                                     final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetCategory?LangID=1";

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.CATEGORIES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.CATEGORIES_UNAVAILABLE));

        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void GetCountries(String tag, Handler handler) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetCountry?LangID=1";

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.COUNTRIES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.COUNTRIES_UNAVAILABLE));

        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void GetStates(String tag, Handler handler,
                                 final Map<String, String> params, Context ctx) {

        mgr = new EzeidPreferenceHelper(ctx);

        String url = EZEIDUrlManager.getAPIUrl()
                + "ewmGetState?LangID=1&CountryID="
                + mgr.GetValueFromSharedPrefs("countryId");

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.STATES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.STATES_UNAVAILABLE));

        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void GetCities(String tag, Handler handler,
                                 final Map<String, String> params, Context ctx) {

        mgr = new EzeidPreferenceHelper(ctx);

        String url = EZEIDUrlManager.getAPIUrl()
                + "ewmGetCity?LangID=1&StateID="
                + mgr.GetValueFromSharedPrefs("stateId");
        ;

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.CITIES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.CITIES_UNAVAILABLE));

        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void GetRoleType(String tag, Handler handler,
                                   final Map<String, String> params, String functionId) {

        String url = EZEIDUrlManager.getAPIUrl()
                + "ewmGetRoles?LangID=1&FunctionID=" + functionId;

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.ROLES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.ROLES_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void QuickRegister(String tag, Handler handler,
                                     final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewSaveQucikEZEData";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.QUICK_REGISTRATION_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.QUICK_REGISTRATION_FAILED));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void GetLanguages(String tag, Handler handler,
                                    final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetLanguage?LangID=1";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.LANGUAGES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.LANGUAGES_UNAVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetTitles(String tag, Handler handler) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetMTitle?LangID=1";

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.TITLES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.TITLES_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void SavePrimaryInformation(String tag, Handler handler,
                                              final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewSavePrimaryEZEData";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.PRIMARY_DATA_SAVE_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.PRIMARY_DATA_SAVE_FAIL));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(7 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void GetFunctions(String tag, Handler handler) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetFunctions?LangID=1";

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.FUNCTIONS_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.FUNCTIONS_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void SaveOtherLocations(String tag, Handler handler,
                                          final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewmAddLocation";

        StringRequest jsObjRequest = new StringRequest(Request.Method.POST,
                url, ResponseListener.<String>createGenericReqSuccessListener(
                handler, Constant.MessageState.LOCATION_SAVE_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.LOCATION_SAVE_FAILED)) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void DeleteLocation(String tag, Handler handler,
                                      final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewDeleteLocation";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.LOCATION_DELETE_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.LOCATION_DELETE_FAILED));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void GetUserDetails(String tag, Handler handler,
                                      final Map<String, String> params, String token) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewtGetUserDetails?Token="
                + token;

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.USER_DETAILS_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.USER_DETAILS_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);

    }

    public static void SaveDocument(String tag, Handler handler,
                                    final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewtSaveDoc";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.DOCUMENT_SAVED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.DOCUMENT_SAVE_FAILED));

        EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void DownloadDocument(String tag, Handler handler, String url) {

        StringRequest req = new StringRequest(url,
                ResponseListener.<String>createGenericReqSuccessListener(
                        handler, Constant.MessageState.DOWNLOAD_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.DOWNLOAD_FAILED));

        req.setShouldCache(false);
        req.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(req);

    }

    public static void GetSecondaryLocations(String tag, Handler handler,
                                             Context ctx) {

        EzeidPreferenceHelper mgr = new EzeidPreferenceHelper(ctx);

        String url = EZEIDUrlManager.getAPIUrl() + "ewtGetSecondaryLoc?Token="
                + mgr.GetValueFromSharedPrefs("Token");

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.SECONDARY_LOCATION_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.SECONDARY_LOCATION_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

    public static void ForgotPassword(String tag, Handler handler,
                                      final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewtForgetPassword";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.PASSWORD_SENT),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.PASSWORD_NOT_SENT));

        EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void UpdateProfilePicture(String tag, Handler handler,
                                            final Map<String, String> params) {
        String url = EZEIDUrlManager.getAPIUrl() + "ewtUpdateProfilePicture";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.PROFILE_PICTURE_UPDATED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.PROFILE_PICTURE_UPDATE_FAILED));

        EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void ChangePassword(String tag, Handler handler,
                                      final Map<String, String> params) {

        String url = EZEIDUrlManager.getAPIUrl() + "ewtChangePassword";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.PASSWORD_CHANGED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.PASSWORD_CHANGE_FAILED));

        EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void UpdateBusinessListing(String tag, Handler handler,
                                             final Map<String, String> params) {
        String url = EZEIDUrlManager.getAPIUrl() + "ewtUpdateBussinessListing";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                new JSONObject(params),
                ResponseListener
                        .<JSONObject>createGenericReqSuccessListener(handler,
                                Constant.MessageState.BUSINESS_LISTING_UPDATED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.BUSINESS_LISTING_UPDATE_FAILED));

        EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetBusinessListing(String tag, Handler handler,
                                          String url) {

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.BUSINESS_LISTING_GET_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.BUSINESS_LISTING_GET_FAILED));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

    // public static void UploadDoc(String tag, Handler handler,
    // final Map<String, String> params,File file,String stringPart) {
    //
    // String url = EZEIDUrlManager.getAPIUrl() + "ewTUploadDoc";
    //
    // MultipartRequest mpr = new MultipartRequest(url,
    // ResponseListener.createErrorListener(handler,
    // Constant.MessageState.DOCUMENT_UPLOAD_FAILED), ResponseListener.<String>
    // createGenericReqSuccessListener(
    // handler, Constant.MessageState.DOCUMENT_UPLOAD_SUCCESS), file,
    // stringPart, params);
    //
    // EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
    // mpr.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
    // EzeidGlobal.getInstance().addToRequestQueue(mpr);
    //
    //
    //
    // }

    public static void GetDocument(String tag, Handler handler,
                                   final Map<String, String> params, int type, Context ctx) {

        mgr = new EzeidPreferenceHelper(ctx);

        String url = EZEIDUrlManager.getAPIUrl() + "ewTgetDocument?TokenNo="
                + mgr.GetValueFromSharedPrefs("Token") + "&Type=" + type;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.DOCUMENT_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.DOCUMENT_UNAVAILABLE));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void UpdatePin(String tag, Handler handler,
                                 final Map<String, String> params) {
        String url = EZEIDUrlManager.getAPIUrl() + "ewtUpdateDocPin";

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.DOCUMENT_PIN_UPDATED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.DOCUMENT_PIN_UPDATE_FAILED));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

	/*
     * public static void GetDocumentInformation(String tag, Handler handler,
	 * final Map<String, String> params, int type, Context ctx) {
	 * 
	 * mgr = new EzeidPreferenceHelper(ctx);
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl() + "ewtGetDoc?TokenNo=" +
	 * mgr.GetValueFromSharedPrefs("Token") + "&Type=" + type;
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest( url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.DOCUMENT_INFO_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.DOCUMENT_INFO_UNAVAILABLE));
	 */

    // public static void UploadDoc(String tag, Handler handler,
    // final Map<String, String> params,File file,String stringPart) {
    //
    // String url = EZEIDUrlManager.getAPIUrl() + "ewTUploadDoc";
    //
    // MultipartRequest mpr = new MultipartRequest(url,
    // ResponseListener.createErrorListener(handler,
    // Constant.MessageState.DOCUMENT_UPLOAD_FAILED), ResponseListener.<String>
    // createGenericReqSuccessListener(
    // handler, Constant.MessageState.DOCUMENT_UPLOAD_SUCCESS), file,
    // stringPart, params);
    //
    // EzeidGlobal.getInstance().getRequestQueue().getCache().remove(tag);
    // mpr.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
    // EzeidGlobal.getInstance().addToRequestQueue(mpr);
    //
    //
    //
    // }

	/*
	 * public static void GetDocument(String tag, Handler handler, final
	 * Map<String, String> params, int type, Context ctx) {
	 * 
	 * mgr = new EzeidPreferenceHelper(ctx);
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl() + "ewTgetDocument?TokenNo=" +
	 * mgr.GetValueFromSharedPrefs("Token") + "&Type=" + type;
	 * 
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.GET, url, new JSONObject(params),
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.DOCUMENT_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.DOCUMENT_UNAVAILABLE));
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void UpdatePin(String tag, Handler handler, final
	 * Map<String, String> params) { String url = EZEIDUrlManager.getAPIUrl() +
	 * "ewtUpdateDocPin";
	 * 
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.POST, url, new JSONObject(params),
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.DOCUMENT_PIN_UPDATED),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.DOCUMENT_PIN_UPDATE_FAILED));
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void GetDocumentInformation(String tag, Handler handler,
	 * final Map<String, String> params, int type, Context ctx) {
	 * 
	 * mgr = new EzeidPreferenceHelper(ctx);
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl() + "ewtGetDoc?TokenNo=" +
	 * mgr.GetValueFromSharedPrefs("Token") + "&Type=" + type;
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest( url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.DOCUMENT_INFO_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.DOCUMENT_INFO_UNAVAILABLE));
	 * 
	 * jsArrRequest.setShouldCache(false); jsArrRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest); }
	 * 
	 * public static void GetPinInformation(String tag, Handler handler, final
	 * Map<String, String> params, Context ctx) {
	 * 
	 * mgr = new EzeidPreferenceHelper(ctx);
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl() + "ewtGetDocPin?TokenNo=" +
	 * mgr.GetValueFromSharedPrefs("Token");
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.PIN_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.PIN_UNAVAILABLE));
	 * 
	 * jsArrRequest.setShouldCache(false); jsArrRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest); }
	 * 
	 * public static void SaveCVInformation(String tag, Handler handler, final
	 * Map<String, String> params) { String url = EZEIDUrlManager.getAPIUrl() +
	 * "ewtSaveCVInfo";
	 * 
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.POST, url, new JSONObject(params),
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.CV_INFO_UPDATED),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.CV_INFO_UPDATE_FAILED));
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void GetCVInformation(String tag, Handler handler, final
	 * Map<String, String> params, Context ctx) {
	 * 
	 * mgr = new EzeidPreferenceHelper(ctx);
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl() + "ewtGetCVInfo?TokenNo=" +
	 * mgr.GetValueFromSharedPrefs("Token");
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.CV_INFO_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.CV_INFO_UNAVAILABLE));
	 * 
	 * jsArrRequest.setShouldCache(false); jsArrRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest); }
	 */

    public static void SessionExpire(String tag, Handler handler,
                                     final Map<String, String> params, Context ctx) {
        mgr = new EzeidPreferenceHelper(ctx);

        String url = EZEIDUrlManager.getAPIUrl() + "ewtGetLoginCheck?Token="
                + mgr.GetValueFromSharedPrefs("Token");

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, url, new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.SESSION_EXPIRED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.SESSION_EXPIRED_ERROR));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetCVInformation(String tag, Handler handler,
                                        final Map<String, String> params, Context ctx) {

        mgr = new EzeidPreferenceHelper(ctx);

        String url = EZEIDUrlManager.getAPIUrl() + "ewtGetCVInfo?TokenNo="
                + mgr.GetValueFromSharedPrefs("Token");

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.CV_INFO_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.CV_INFO_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

    public static void GetAllRoles(String tag, Handler handler) {
        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetRoleType?LangID=1";

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler, Constant.MessageState.ROLES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.ROLES_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

    public static void GetFunctionRoleMapped(String tag, Handler handler) {
        String url = EZEIDUrlManager.getAPIUrl() + "ewmGetFunctionRoleMapping";

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.FUNCTION_ROLE_MAPPED_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.FUNCTION_ROLE_MAPPED_UNAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

    public static void GetLoginStatus(String tag, Handler handler,
                                      final Map<String, String> params) {

        // Log.e("LOGIN", EzeidGlobal.EzeidUrl + "ewLogin");

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, EzeidGlobal.EzeidUrl + "ewLogin",
                new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.REQUEST_LOGIN_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.REQUEST_LOGIN_FAIL));

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);

    }

    public static void RequestUserDetail(String tag, Handler handler, String url) {

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.REQUEST_USERDETAIL_SUCCESS),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.REQUEST_USERDETAIL_FAIL));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

	/* CHAT METHODS */
	/*
	 * public static void CreateChatGroup(String tag, Handler handler, final
	 * Map<String, String> params){ JsonObjectRequest jsObjRequest = new
	 * JsonObjectRequest( Request.Method.POST, EZEIDUrlManager.getAPIUrl() +
	 * "ewtCreateGroup", new JSONObject(params), ResponseListener.<JSONObject>
	 * createGenericReqSuccessListener( handler,
	 * Constant.MessageState.CHAT_GROUP_CREATED),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.CHAT_GROUP_CREATION_FAILED));
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void AddMembersToChatGroup(String tag, Handler handler,
	 * final Map<String, String> params){ JsonObjectRequest jsObjRequest = new
	 * JsonObjectRequest( Request.Method.POST, EZEIDUrlManager.getAPIUrl() +
	 * "ewtSaveGroupMembers", new JSONObject(params),
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.CHAT_GROUP_ADD_MEMBERS_SUCCESS),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.CHAT_GROUP_ADD_MEMBERS_FAILED));
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void DeleteMembersFromChatGroup(String tag, Handler
	 * handler, final Map<String, String> params,String token,String memberID){
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.DELETE, EZEIDUrlManager.getAPIUrl() +
	 * "ewtDeleteGroupMembers?Token="+token+"&MemberID="+memberID, new
	 * JSONObject(params), ResponseListener.<JSONObject>
	 * createGenericReqSuccessListener( handler,
	 * Constant.MessageState.CHAT_GROUP_DELETE_MEMBERS_SUCCESS),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.CHAT_GROUP_DELETE_MEMBERS_FAILED));
	 * 
	 * // Log.e("Delete member", EzeidGlobal.EzeidUrl +
	 * "ewtDeleteGroupMembers?Token="+token+"&MemberID="+memberID);
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * 
	 * public static void SendChatMessage(String tag, Handler handler, final
	 * Map<String, String> params){ JsonObjectRequest jsObjRequest = new
	 * JsonObjectRequest( Request.Method.POST, EZEIDUrlManager.getAPIUrl() +
	 * "ewtSaveChatMessage", new JSONObject(params),
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.MESSAGE_SENT),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.MESSAGE_NOT_SENT));
	 * 
	 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void GetGroupList(String tag, Handler handler,String
	 * token){
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl()+"ewtGetGroupList?Token="+token;
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.CHAT_GROUPS_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.CHAT_GROUPS_NOT_AVAILABLE));
	 * 
	 * jsArrRequest.setShouldCache(false); jsArrRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest); }
	 */

    public static void SearchEZEID(String tag, Handler handler,
                                   final Map<String, String> params) {
		/*
		 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
		 * Request.Method.POST, EZEIDUrlManager.getAPIUrl() +
		 * "ewSearchByKeywords", new JSONObject(params),
		 * ResponseListener.<JSONObject> createGenericReqSuccessListener(
		 * handler, Constant.MessageState.EZEID_FOUND),
		 * ResponseListener.createErrorListener(handler,
		 * Constant.MessageState.EZEID_NOT_FOUND));
		 * 
		 * jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5,
		 * 1.0f)); EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
		 */
    }

	/*
	 * public static void GetSalesItem(String tag,Handler handler,String token){
	 * 
	 * String url =
	 * EZEIDUrlManager.getAPIUrl()+"ewtGetItemList?Token="+token+"&FunctionType=0"
	 * ;
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.SALES_ITEM_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.SALES_ITEM_NOT_AVAILABLE));
	 * 
	 * jsArrRequest.setShouldCache(false); jsArrRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest); }
	 */

    public static void SaveCompanyProfile(String tag,
                                          final Map<String, String> params, Handler handler) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, EZEIDUrlManager.getAPIUrl()
                + "ewtCompanyProfile", new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.COMPANY_PROFILE_SAVED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.COMPANY_PROFILE_NOT_SAVED));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetCompanyProfile(String tag, String token,
                                         Handler handler, final Map<String, String> params) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, EZEIDUrlManager.getAPIUrl()
                + "ewtCompanyProfile?Token=" + token, new JSONObject(
                params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.COMPANY_PROFILE_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.COMPANY_PROFILE_NOT_AVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

	/*
	 * public static void GetPrimaryDetails(String tag, String token, String
	 * ezeid, Handler handler) {
	 * 
	 * String url = EZEIDUrlManager.getAPIUrl() +
	 * "ewtEZEIDPrimaryDetails?Token=" + token + "&EZEID=" + ezeid;
	 * 
	 * JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
	 * ResponseListener.<JSONArray> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.PRIMARY_DETAILS_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.PRIMARY_DETAILS_NOTAVAILABLE));
	 * 
	 * jsArrRequest.setShouldCache(false); jsArrRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest); }
	 * 
	 * public static void GetResources(String tag, String ezeid, Handler
	 * handler) {
	 * 
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.GET, EZEIDUrlManager.getAPIUrl() +
	 * "reservation_resource?ezeid=" + ezeid, null,
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.RESOURCES_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.RESOURCES_UNAVAILABLE));
	 * 
	 * jsObjRequest.setShouldCache(false); jsObjRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void GetMappedServices(String tag, String ezeid, String
	 * resourceid, Handler handler) {
	 * 
	 * JsonObjectRequest jsObjRequest = new JsonObjectRequest(
	 * Request.Method.GET, EZEIDUrlManager.getAPIUrl() +
	 * "reservation_maped_services?ezeid=" + ezeid + "&resourceid=" +
	 * resourceid, null, ResponseListener.<JSONObject>
	 * createGenericReqSuccessListener( handler,
	 * Constant.MessageState.SERVICES_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.SERVICES_UNAVAILABLE));
	 * 
	 * jsObjRequest.setShouldCache(false); jsObjRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void GetServiceForResource(String tag, String ezeid,
	 * Handler handler) { JsonObjectRequest jsObjRequest = new
	 * JsonObjectRequest( Request.Method.GET, EZEIDUrlManager.getAPIUrl() +
	 * "reservation_resource_service_map?ezeid=" + ezeid, null,
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.SERVICE_RESOURCE_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.SERVICE_RESOURCE_UNAVAILABLE));
	 * 
	 * jsObjRequest.setShouldCache(false); jsObjRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void SaveReservation(String tag, Handler handler, final
	 * Map<String, String> params) { JsonObjectRequest jsObjRequest = new
	 * JsonObjectRequest( Request.Method.POST, EZEIDUrlManager.getAPIUrl() +
	 * "reservation_transaction", new JSONObject(params),
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.RESERVATION_SAVED),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.RESERVATION_SAVE_FAILED));
	 * 
	 * jsObjRequest.setShouldCache(false); jsObjRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 * 
	 * public static void GetReservation(String tag, String ezeid, String
	 * resourceId, String date, Handler handler) { JsonObjectRequest
	 * jsObjRequest = new JsonObjectRequest( Request.Method.GET,
	 * EZEIDUrlManager.getAPIUrl() + "reservation_transaction?toEzeid=" + ezeid
	 * + "&resourceid=" + resourceId + "&date=" + date, null,
	 * ResponseListener.<JSONObject> createGenericReqSuccessListener( handler,
	 * Constant.MessageState.RESERVATION_DATA_AVAILABLE),
	 * ResponseListener.createErrorListener(handler,
	 * Constant.MessageState.RESERVATION_DATA_NOT_AVAILABLE));
	 * 
	 * jsObjRequest.setShouldCache(false); jsObjRequest.setRetryPolicy(new
	 * DefaultRetryPolicy(3 * 1000, 5, 1.0f));
	 * EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest); }
	 */

    public static void GetPrimaryDetails(String tag, String token,
                                         String ezeid, Handler handler) {

        String url = EZEIDUrlManager.getAPIUrl()
                + "ewtEZEIDPrimaryDetails?Token=" + token + "&EZEID=" + ezeid;

        JsonArrayRequest jsArrRequest = new JsonArrayRequest(url,
                ResponseListener.<JSONArray>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.PRIMARY_DETAILS_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.PRIMARY_DETAILS_NOTAVAILABLE));

        jsArrRequest.setShouldCache(false);
        jsArrRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsArrRequest);
    }

    public static void GetResources(String tag, String ezeid, Handler handler) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, EZEIDUrlManager.getAPIUrl()
                + "reservation_resource?ezeid=" + ezeid,
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.RESOURCES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.RESOURCES_UNAVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetMappedServices(String tag, String ezeid,
                                         String resourceid, Handler handler) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, EZEIDUrlManager.getAPIUrl()
                + "reservation_maped_services?ezeid=" + ezeid
                + "&resourceid=" + resourceid,
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.SERVICES_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.SERVICES_UNAVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetServiceForResource(String tag, String ezeid,
                                             Handler handler) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, EZEIDUrlManager.getAPIUrl()
                + "reservation_resource_service_map?ezeid=" + ezeid,

                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.SERVICE_RESOURCE_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.SERVICE_RESOURCE_UNAVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void SaveReservation(String tag, Handler handler,
                                       final Map<String, String> params) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.POST, EZEIDUrlManager.getAPIUrl()
                + "reservation_transaction", new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.RESERVATION_SAVED),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.RESERVATION_SAVE_FAILED));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetReservation(String tag, String ezeid,
                                      String resourceId, String date, Handler handler) {

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET, EZEIDUrlManager.getAPIUrl()
                + "reservation_transaction?toEzeid=" + ezeid
                + "&resourceid=" + resourceId + "&date=" + date,
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.RESERVATION_DATA_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.RESERVATION_DATA_NOT_AVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetReservationDetails(String tag, String tid,
                                             Handler handler) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET,
                EZEIDUrlManager.getAPIUrl() + "reservation_trans_details?TID="
                        + tid,

                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.RESERVATION_DETAILS_AVAILABLE),
                ResponseListener
                        .createErrorListener(
                                handler,
                                Constant.MessageState.RESERVATION_DETAILS_NOT_AVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void UpdateReservatioStatus(String tag, Handler handler,
                                              Map<String, String> params) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.PUT,
                EZEIDUrlManager.getAPIUrl() + "reservation_transaction",
                new JSONObject(params),
                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler,
                        Constant.MessageState.RESERVATION_DETAILS_UPDATED),
                ResponseListener
                        .createErrorListener(
                                handler,
                                Constant.MessageState.RESERVATION_DETAILS_UPDATE_FAILED));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

    public static void GetWorkingHours(String tag, Handler handler,
                                       String token, String locID) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(
                Request.Method.GET,
                EZEIDUrlManager.getAPIUrl()
                        + "ewtGetWorkingHrsHolidayList?Token=" + token
                        + "&LocID=" + locID,

                ResponseListener.<JSONObject>createGenericReqSuccessListener(
                        handler, Constant.MessageState.WORKING_HOURS_AVAILABLE),
                ResponseListener.createErrorListener(handler,
                        Constant.MessageState.WORKING_HOURS_UNAVAILABLE));

        jsObjRequest.setShouldCache(false);
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
        EzeidGlobal.getInstance().addToRequestQueue(jsObjRequest);
    }

}
