package com.ezeone;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.backend.EzeidDbHelper;
import com.ezeone.helpers.Constant;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.networkhandler.VolleyErrorHelper;
import com.ezeone.signup.SignUp;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.dialog.MaterialDialog;
import com.ezeonelib.widget.cruton.Crouton;

public class SplashScreen extends AppCompatActivity implements OnClickListener,
        OnEditorActionListener {

    private Button signUp, login;
    private MaterialDialog mMaterialDialog, sessionDialog;
    private TextView forgotPwd, termsOfUse, aboutUs, appDesBar;
    private EditText username, password;
    private String firstName;
    private int sessionRetry = 0;
    private boolean isSessionDialogShown = false;
    public static String token, masterID, isVerified, userModuleRights,
            userType;
    private EzeidPreferenceHelper ezeidPreferenceHelper;
    private String TAG = SplashScreen.class.getSimpleName();
    private static final int SPLASH_SHOW_TIME = 4000;
    private int get_configuration_count = 0;
    private EzeidDbHelper helper;
    private EzeidLoadingProgress ezeidLoadingProgress;
    private Handler handlerDialog;
    private Runnable runnableDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        helper = new EzeidDbHelper(getApplicationContext());
        InitComponentsToUi();

        if (EzeidUtil.isConnectedToInternet(getApplicationContext())) {
            if (ezeidPreferenceHelper.GetBoolFromSharedPrefs("tablesLoaded") == false) {
                NetworkHandler.GetTitles(TAG, handler);
            }
        } else {
            EzeidUtil.showToast(SplashScreen.this,
                    getResources().getString(R.string.internet_warning));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isSessionDialogShown == false) {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String logout = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("LOGOUT");

            if (!token.isEmpty()) {
                if (logout.equalsIgnoreCase("false")) {
                    if (EzeidUtil.isConnectedToInternet(SplashScreen.this)) {
                        CheckSessionExpiredStatus(token);
                    } else {
                        EzeidUtil.showToast(SplashScreen.this, getResources().getString(R.string.internet_warning));
                    }
                }
            } else {
                username.setVisibility(View.VISIBLE);
                username.setFocusable(true);
                password.setVisibility(View.VISIBLE);
                forgotPwd.setVisibility(View.VISIBLE);
                signUp.setVisibility(View.VISIBLE);
                login.setVisibility(View.VISIBLE);
                appDesBar.setVisibility(View.VISIBLE);
            }

        }
    }

    private void InitComponentsToUi() {

        ezeidPreferenceHelper = new EzeidPreferenceHelper(SplashScreen.this);
        signUp = (Button) findViewById(R.id.signUpbtn);
        login = (Button) findViewById(R.id.loginBtn);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        forgotPwd = (TextView) findViewById(R.id.forgotPwd);
        termsOfUse = (TextView) findViewById(R.id.termsOfUse);
        aboutUs = (TextView) findViewById(R.id.aboutUs);
        appDesBar = (TextView) findViewById(R.id.textView4);
        signUp.setOnClickListener(this);
        login.setOnClickListener(this);
        forgotPwd.setOnClickListener(this);
        password.setOnEditorActionListener(this);
        termsOfUse.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void CheckSessionExpiredStatus(String token) {
        ShowEzeidDialog("Please wait!");
        if (!token.equalsIgnoreCase(null) && sessionRetry < 3) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                    EzeidGlobal.EzeidUrl + "ewtGetLoginCheck?Token=" + token,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            DismissEzeidDialog();
                            SessionStatus(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    DismissEzeidDialog();
                    sessionRetry += 1;
                    if (error instanceof TimeoutError) {
                        SessionTryAgain();
                    } else if (error instanceof ServerError
                            || error instanceof AuthFailureError) {
                        EzeidUtil.showToast(SplashScreen.this,
                                "Network is unreachable!");
                    } else if (error instanceof NetworkError
                            || error instanceof NoConnectionError) {
                        SessionTryAgain();
                    } else {
                        SessionTryAgain();
                    }
                }
            });
            req.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(req, TAG);

        } else {
            DismissEzeidDialog();
        }
    }

    private void SessionTryAgain() {
        String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
        if (!token.equalsIgnoreCase("null") || !token.equalsIgnoreCase("")) {
            if (EzeidUtil.isConnectedToInternet(SplashScreen.this)) {
                CheckSessionExpiredStatus(token);
            } else {
                EzeidUtil.showToast(SplashScreen.this, getResources().getString(R.string.internet_warning));
            }
        }
    }

    private void SessionStatus(JSONObject response) {

        if (!response.isNull("IsAvailable")) {
            try {
                String status = response.getString("IsAvailable");
                if (status.equalsIgnoreCase("true")) {

                    ActivityStatus();
                } else {
                    SessionDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void ActivityStatus() {
        try {

            ShowEzeidDialog("Loading...");

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    if (EzeidUtil.isConnectedToInternet(SplashScreen.this)) {
                        RequestUserDetail();
                        DismissEzeidDialog();
                        if (ezeidPreferenceHelper.GetValueFromSharedPrefs(
                                "GET_CONFIGURE_STATUS")
                                .equalsIgnoreCase("true")) {
                            startActivity(new Intent(SplashScreen.this,
                                    LandingPage.class));
                            SplashScreen.this.finish();
                        } else {
                            get_configuration_count++;
                            if (get_configuration_count < 3) {
                                RequestTabTitles();
                            } else {
                                EzeidUtil.showToast(SplashScreen.this,
                                        "Failed to load configurations.");
                            }
                        }
                    } else {
                        DismissEzeidDialog();
                        EzeidUtil.showToast(SplashScreen.this,
                                "Please check your internet connection");
                    }

                }
            }, SPLASH_SHOW_TIME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SessionDialog() {
        isSessionDialogShown = true;
        sessionDialog = new MaterialDialog(this);
        sessionDialog
                .setBackgroundResource(R.drawable.abc_cab_background_internal_bg);

        sessionDialog.setTitle(R.string.sessionExpired)
                .setMessage("Your session has expired. Please login again!")
                .setPositiveButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sessionDialog.dismiss();
                        username.setVisibility(View.VISIBLE);
                        password.setVisibility(View.VISIBLE);
                        forgotPwd.setVisibility(View.VISIBLE);
                        signUp.setVisibility(View.VISIBLE);
                        login.setVisibility(View.VISIBLE);
                        appDesBar.setVisibility(View.VISIBLE);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs("Token",
                                "");
                    }
                });
        sessionDialog.setCanceledOnTouchOutside(true).show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.signUpbtn:
                Intent signUpIntent = new Intent(SplashScreen.this, SignUp.class);
                signUpIntent.putExtra("signUp", true);
                startActivity(signUpIntent);
                // ListDialog() ;
                break;

            case R.id.forgotPwd:
                OnForgotPwd();
                break;

            case R.id.loginBtn:
                SetValidation();
                break;

            case R.id.termsOfUse:
                OnTermsOfUse();
                break;

            case R.id.aboutUs:
                OnAboutUs();
                break;

        }
    }

    public void OnTermsOfUse() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.terms_url)));
        startActivity(intent);
    }

    public void OnAboutUs() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.about_url)));
        startActivity(intent);
    }

    private void OnForgotPwd() {
        mMaterialDialog = new MaterialDialog(this);
        if (mMaterialDialog != null) {
            LinearLayout root = new LinearLayout(SplashScreen.this);
            View view = LayoutInflater.from(this).inflate(
                    R.layout.forgot_pwd_dialog, root);
            // EditText mobile = (EditText) view.findViewById(R.id.mobileFpwd);
            final EditText email = (EditText) view
                    .findViewById(R.id.emailIdFpwd);
            Button resetFpwd = (Button) view.findViewById(R.id.fPwdBtn);
            resetFpwd.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (!email.getText().toString().isEmpty()) {
                        ResetPwdRequestSuccess(email.getText().toString());
                        mMaterialDialog.dismiss();
                    } else {
                        email.setError("Required");
                    }

                }
            });

            mMaterialDialog.setView(view).show();
        }
    }

    private void ResetPwdRequestSuccess(String ezeid) {
        /*
         * EzeidUtil.showUnstablePopup(SplashScreen.this, customPopup,
		 * forgotPwdLink, getResources().getString(R.string.pwdResetMsg));
		 */
        ShowEzeidDialog("Reseting password!");
        Map<String, String> params = new HashMap<String, String>();
        params.put("EZEID", ezeid);

        NetworkHandler.ForgotPassword(TAG, handler, params);

    }

    private void GetLoginStatus(String username, String password) {

        Map<String, String> params = new HashMap<String, String>();

        if (!username.equalsIgnoreCase("") && !password.equalsIgnoreCase("")) {
            // if (username.contains(".")) {
            // ezeidPreferenceHelper
            // .SaveValueToSharedPrefs("USER", "sub_user");
            // } else {
            // ezeidPreferenceHelper.SaveValueToSharedPrefs("USER", "");
            // }
            params.put("UserName", username);
            params.put("Password", password);
            // eProgress.show();
            ShowEzeidDialog("Authenticating...");
            NetworkHandler.GetLoginStatus(TAG, handler, params);
        }
    }

    private void GetSignInStatus(JSONObject response) {
        try {
            firstName = response.getString("FirstName");
            token = response.getString("Token");
            masterID = response.getString("MasterID");
            isVerified = response.getString("Verified");
            userModuleRights = response.getString("UserModuleRights");
            userType = response.getString("Type");

            if (response.getBoolean("IsAuthenticate") && !(token.isEmpty())) {

                if (masterID.equalsIgnoreCase("0")) {
                    ezeidPreferenceHelper.SaveValueToSharedPrefs("USER",
                            "MAIN_USER");
                } else {
                    ezeidPreferenceHelper.SaveValueToSharedPrefs("USER",
                            "SUB_USER");
                }
                ezeidPreferenceHelper.SaveValueToSharedPrefs("LOGOUT", "false");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("Type", userType);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("USER_STATUS",
                        isVerified);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("ModuleRights",
                        userModuleRights);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("Token", token);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("FirstName",
                        firstName);
                ezeidPreferenceHelper.SaveValueToSharedPrefs("MasterIDLog",
                        masterID);

                RequestUserDetail();

                if (!isVerified.equalsIgnoreCase("")) {
                    if (isVerified.equalsIgnoreCase("2")) {
                        RequestTabTitles();
                    } else {
                        startActivity(new Intent(SplashScreen.this,
                                LandingPage.class));
                        SplashScreen.this.finish();
                    }
                }

            } else {
                EzeidUtil.showToast(SplashScreen.this,
                        "Please enter valid Username and password");
                ezeidPreferenceHelper.SaveValueToSharedPrefs("LOGOUT", "true");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                || (actionId == EditorInfo.IME_ACTION_DONE)) {

            SetValidation();
        }
        return false;
    }

    private void SetValidation() {
        Drawable icon = getResources().getDrawable(R.drawable.ic_launcher);
        icon.setBounds(new Rect(0, 0, icon.getIntrinsicWidth(), icon
                .getIntrinsicHeight()));

        if (EzeidUtil.isConnectedToInternet(SplashScreen.this)) {

            String usernameStr = username.getText().toString();
            String passwordStr = password.getText().toString();
            if (TextUtils.isEmpty(usernameStr)) {
                EzeidUtil
                        .showToast(SplashScreen.this, "Username is mandatory!");
            } else if (TextUtils.isEmpty(passwordStr)) {
                EzeidUtil
                        .showToast(SplashScreen.this, "Password is mandatory!");
            } else {
                GetLoginStatus(usernameStr, passwordStr);
            }
        } else {
            EzeidUtil.showToast(SplashScreen.this, "You're in offline!");
        }
    }

    private void RequestUserDetail() {

        String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
        NetworkHandler.RequestUserDetail(TAG, handler, EzeidGlobal.EzeidUrl
                + "ewtGetUserDetails?Token=" + token);

    }

    Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.arg1) {
                case Constant.MessageState.PASSWORD_SENT:
                    DismissEzeidDialog();
                    EzeidUtil.showToast(SplashScreen.this,
                            "Password reset successfully!");
                    parseForgotPasswordResponse((JSONObject) msg.obj);
                    break;

                case Constant.MessageState.PASSWORD_NOT_SENT:
                    DismissEzeidDialog();
                    EzeidUtil
                            .showToast(SplashScreen.this, "Password reset failed!");
                    VolleyErrorHelper.getMessage(msg.obj, SplashScreen.this, false);
                    break;

                case Constant.MessageState.REQUEST_LOGIN_SUCCESS:
                    // eProgress.dismiss();
                    DismissEzeidDialog();
                    GetSignInStatus((JSONObject) msg.obj);
                    break;

                case Constant.MessageState.REQUEST_LOGIN_FAIL:
                    // eProgress.dismiss();
                    DismissEzeidDialog();
                    VolleyErrorHelper.getMessage(msg.obj, SplashScreen.this, false);
                    break;

                case Constant.MessageState.REQUEST_USERDETAIL_SUCCESS:
                    // eProgress.dismiss();
                    // DismissEzeidDialog();
                    EzeidGlobal.userDetailsObj = ((JSONArray) msg.obj);
                    break;

                case Constant.MessageState.REQUEST_USERDETAIL_FAIL:
                    // eProgress.dismiss();
                    // DismissEzeidDialog();
                    VolleyErrorHelper.getMessage(msg.obj, SplashScreen.this, false);
                    break;

                case Constant.MessageState.TITLES_AVAILABLE:
                    parseTitles((JSONArray) msg.obj);
                    break;

                case Constant.MessageState.TITLES_UNAVAILABLE:
                    ezeidPreferenceHelper.SaveBooleanValueToSharedPrefs("tablesLoaded", false);
                    EzeidUtil.showError(SplashScreen.this, (VolleyError) msg.obj);
                    break;

                case Constant.MessageState.COUNTRIES_AVAILABLE:
                    parseCountries((JSONArray) msg.obj);
                    break;

                case Constant.MessageState.COUNTRIES_UNAVAILABLE:
                    ezeidPreferenceHelper.SaveBooleanValueToSharedPrefs("tablesLoaded", false);
                    EzeidUtil.showError(SplashScreen.this, (VolleyError) msg.obj);
                    break;

                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Crouton.cancelAllCroutons();
    }

    private void parseForgotPasswordResponse(JSONObject obj) {
        try {
            if (obj.has("IsChanged")) {
                if (obj.getBoolean("IsChanged")) {
                    mMaterialDialog.dismiss();
                    EzeidUtil.showToast(SplashScreen.this,
                            "Password sent to your email id");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finish() {
        EzeidGlobal.getInstance().getRequestQueue().cancelAll(TAG);
        super.finish();
    }

    private void RequestTabTitles() {
        try {
            ShowEzeidDialog("Loading configurations!");
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String url = EZEIDUrlManager.getAPIUrl() + "ewtConfig?Token="
                    + token;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {

                            DismissEzeidDialog();
                            BindTitles(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    DismissEzeidDialog();
                    BindTitles(null);
                }
            });

            int socketTimeout = 5000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonArrayRequest.setRetryPolicy(policy);
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindTitles(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String salesTitle = jsonObject.getString("SalesTitle");
                        String reservationTitle = jsonObject
                                .getString("ReservationTitle");
                        String homeDeliveryTitle = jsonObject
                                .getString("HomeDeliveryTitle");
                        String serviceTitle = jsonObject
                                .getString("ServiceTitle");
                        String resumeTitle = jsonObject
                                .getString("ResumeTitle");
                        String salesFormMsg = jsonObject
                                .getString("SalesFormMsg");
                        String reservationFormMsg = jsonObject
                                .getString("ReservationFormMsg");
                        String homeDeliveryFormMsg = jsonObject
                                .getString("HomeDeliveryFormMsg");
                        String serviceFormMsg = jsonObject
                                .getString("ServiceFormMsg");
                        String resumeFormMsg = jsonObject
                                .getString("ResumeFormMsg");
                        // String businessCategoryID = jsonObject
                        // .getString("BusinessCategoryID");
                        // String businessKeywords = jsonObject
                        // .getString("BusinessKeywords");
                        String salesItemListType = jsonObject
                                .getString("SalesItemListType");
                        String homeDeliveryItemListType = jsonObject
                                .getString("HomeDeliveryItemListType");
                        // String resumeKeyword = jsonObject
                        // .getString("ResumeKeyword");
                        // String reservationDisplayFormat = jsonObject
                        // .getString("ReservationDisplayFormat");
                        // String dataRefreshInterval = jsonObject
                        // .getString("DataRefreshInterval");
                        // String brochureFileName = jsonObject
                        // .getString("BrochureFileName");
                        // String userModuleRights = jsonObject
                        // .getString("UserModuleRights");

                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "SalesTitle", salesTitle);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "ReservationTitle", reservationTitle);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "HomeDeliveryTitle", homeDeliveryTitle);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "ServiceTitle", serviceTitle);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "ResumeTitle", resumeTitle);

                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "SalesFormMsg", salesFormMsg);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "ReservationFormMsg", reservationFormMsg);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "HomeDeliveryFormMsg", homeDeliveryFormMsg);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "ServiceFormMsg", serviceFormMsg);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "ResumeFormMsg", resumeFormMsg);

                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "SalesItemListType", salesItemListType);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "HomeDeliveryItemListType",
                                homeDeliveryItemListType);
                        ezeidPreferenceHelper.SaveValueToSharedPrefs(
                                "GET_CONFIGURE_STATUS", "true");

                    }
                }

            } else {
                ezeidPreferenceHelper.SaveValueToSharedPrefs(
                        "GET_CONFIGURE_STATUS", "false");
            }

            if (ezeidPreferenceHelper.GetValueFromSharedPrefs(
                    "GET_CONFIGURE_STATUS").equalsIgnoreCase("true")) {
                startActivity(new Intent(SplashScreen.this,
                        LandingPage.class));
                SplashScreen.this.finish();
            } else {
                get_configuration_count++;
                if (get_configuration_count < 3) {
//					Log.i(TAG, "GET_CONFIGURE_STATUS_count: false "
//							+ get_configuration_count);
                    RequestTabTitles();
                } else {
                    EzeidUtil.showToast(SplashScreen.this,
                            "Failed to load configurations.");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowEzeidDialog(String content) {
        ezeidLoadingProgress = new EzeidLoadingProgress(SplashScreen.this,
                content);
        handlerDialog = new Handler();
        runnableDialog = new Runnable() {

            @Override
            public void run() {
                if (ezeidLoadingProgress != null) {
                    if (ezeidLoadingProgress.isShowing()) {
                        ezeidLoadingProgress.dismiss();
                    }
                }
            }
        };
        ezeidLoadingProgress.show();
    }

    private void DismissEzeidDialog() {
        handlerDialog.removeCallbacks(runnableDialog);
        if (ezeidLoadingProgress.isShowing()) {
            ezeidLoadingProgress.dismiss();
        }
    }

    private void parseTitles(JSONArray array) {
        try {
            if (array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject titleJsonObj = array.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("TitleID", titleJsonObj.getString("TitleID"));
                    map.put("Title", titleJsonObj.getString("Title"));
                    helper.insertTitles(map);
                }
                NetworkHandler.GetCountries(TAG, handler);
            }
        } catch (Exception e) {
            ezeidPreferenceHelper.SaveBooleanValueToSharedPrefs("tablesLoaded", false);
            e.printStackTrace();
        }
    }

    private void parseCountries(JSONArray array) {
        try {
            if (array.length() > 0) {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject countryObj = array.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("CountryName", countryObj.getString("CountryName"));
                    map.put("CountryID", countryObj.getString("CountryID"));
                    map.put("ISDCode", countryObj.getString("ISDCode"));
                    helper.insertCountries(map);
                }
                ezeidPreferenceHelper.SaveBooleanValueToSharedPrefs("tablesLoaded", true);
            }
        } catch (Exception e) {
            ezeidPreferenceHelper.SaveBooleanValueToSharedPrefs("tablesLoaded", false);
            e.printStackTrace();
        }
    }
}
