package com.ezeone.backend;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class EzeidDbHelper extends SQLiteOpenHelper{
	
	public EzeidDbHelper(Context applicationcontext) {
		super(applicationcontext, "ezeidDb.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS titles ( rowId INTEGER PRIMARY KEY,TitleID TEXT, Title TEXT)");
		Log.e("Table Titles", "Created");
		
		db.execSQL("CREATE TABLE IF NOT EXISTS countries ( rowId INTEGER PRIMARY KEY,CountryID TEXT, CountryName TEXT,ISDCode TEXT)");
		Log.e("Table Countries", "Created");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS titles");
		db.execSQL("DROP TABLE IF EXISTS countries");
	}
	
	
	public void insertTitles(HashMap<String, String> queryValues){
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("TitleID", queryValues.get("TitleID"));
		values.put("Title", queryValues.get("Title"));
		database.insert("titles", null, values);
		Log.e("INSERTING", ""+queryValues.get("Title"));
		database.close();
	}
	
	public void insertCountries(HashMap<String, String> queryValues){
		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("CountryID", queryValues.get("CountryID"));
		values.put("CountryName", queryValues.get("CountryName"));
		values.put("ISDCode",queryValues.get("ISDCode"));
		database.insert("countries", null, values);
		Log.e("INSERTING", ""+queryValues.get("CountryName"));
		database.close();
	}
	
	public ArrayList<HashMap<String, String>> getAllCountries() {
		ArrayList<HashMap<String, String>> functions;
		functions = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT  * FROM countries";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("rowId", cursor.getString(0));
				map.put("CountryID", cursor.getString(1));
				map.put("CountryName", cursor.getString(2));
				map.put("ISDCode", cursor.getString(3));
				functions.add(map);
			} while (cursor.moveToNext());
		}
		cursor.close();
		
		return functions;
	}
	
	public ArrayList<HashMap<String, String>> getAllTitles() {
		ArrayList<HashMap<String, String>> functions;
		functions = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT  * FROM titles";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("rowId", cursor.getString(0));
				map.put("TitleID", cursor.getString(1));
				map.put("Title", cursor.getString(2));
				functions.add(map);
			} while (cursor.moveToNext());
		}
		cursor.close();
		
		return functions;
	}
	
	public ArrayList<HashMap<String, String>> getCountry(int id) {
		ArrayList<HashMap<String, String>> functions;
		functions = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT  * FROM countries where CountryID='" + id
				+ "'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("rowId", cursor.getString(0));
				map.put("CountryID", cursor.getString(1));
				map.put("CountryName", cursor.getString(2));
				map.put("ISDCode", cursor.getString(3));
				functions.add(map);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return functions;
	}
	
	public ArrayList<HashMap<String, String>> getTitle(int id) {
		ArrayList<HashMap<String, String>> functions;
		functions = new ArrayList<HashMap<String, String>>();
		String selectQuery = "SELECT  * FROM titles where TitleID='" + id
				+ "'";
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.rawQuery(selectQuery, null);
		if (cursor.moveToFirst()) {
			do {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("rowId", cursor.getString(0));
				map.put("TitleID", cursor.getString(1));
				map.put("Title", cursor.getString(2));
				functions.add(map);
			} while (cursor.moveToNext());
		}
		cursor.close();
		return functions;
	}
}

