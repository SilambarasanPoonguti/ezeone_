package com.ezeone.manager.humanresource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.SplashScreen;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.date.time.RadialPickerLayout;
import com.ezeonelib.date.time.TimePickerDialog;
import com.ezeonelib.dialog.MaterialDialog;
import com.ezeonelib.floatingaction.ButtonFloatSmall;

public class HREnquiry extends AppCompatActivity implements OnClickListener,
		OnItemSelectedListener, TimePickerDialog.OnTimeSetListener,
		DatePickerDialog.OnDateSetListener {

	private static final String TAG = HREnquiry.class.getSimpleName();
	private Toolbar toolbar;
	private RelativeLayout contact_info;
	private EditText item_update_notes;
	private Button submit_order, cancel_order;
	private EzeidPreferenceHelper ezeidPreferenceHelper;
	private int sessionRetry = 0;
	private EzeidLoadingProgress ezeidLoadingProgress;
	private Handler handlerDialog;
	private Runnable runnableDialog;
	private boolean isEdited = false;
	private EditText salesRequester, addressBox, nextActionDateBox, notesBox;
	private ButtonFloatSmall editActionDate, checkEzeidAvailability;
	private Button requseterOk, requseterCancel;
	private Spinner statusFilter, actionFilter, folderFilter, locationFilter;
	private SimpleAdapter statusAdapter, actionAdapter, folderAdapter,
			locationAdapter;
	private LinearLayout folderLayout, checkEzeidStatusLayout;
	private TextView contact_info_text, checkEzeidStatusText,
			checkEzeidStatusLine;
	private ArrayList<HashMap<String, String>> status_array, action_array,
			folder_array;
	private int status_count = 0, action_count = 0, folder_count = 0;
	private TextView switchLoc;
	private RadioButton custEzeid, custOther;
	private RadioGroup userType;
	private ProgressBar contactInfoProgress;
	private String firstName = "", lastName = "", mobileNumber = "",
			latitude = "", longitude = "", dateStr = "", timeStr = "",
			prefDateTimeStr = "", country = "", state = "", city = "",
			area = "", rlatitude = "0", rlongitude = "0", folderRuleID = "",
			statusID = "", nextActionID = "", durations, messageID = "",
			ezistingNotes = "", taskDateTimeStr = "", nextActionDateStr = "",
			messageText = "", contactInfo = "", update_statusID = "",
			update_nextActionID = "", update_folderRuleID = "",
			update_locID = "", update_messageText = "",
			update_taskDateTimeStr = "", update_nextActionDateStr = "",
			update_contactInfo = "", update_ezistingNotes = "", ezeid = "",
			req_ezeid = "", deliveryAddress = "", edit_statusID = "",
			edit_nextActionID = "", edit_floderID = "";

	public static final String DATEPICKER_TAG = "datepicker";
	public static final String TIMEPICKER_TAG = "timepicker";
	private Calendar calendar;
	private DatePickerDialog datePickerDialog;
	private TimePickerDialog timePickerDialog;
	private boolean isRequesterAdded = false, isItemsEditAgain = false;
	private Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_sales_message_only);
		toolbar = (Toolbar) findViewById(R.id.toolbar_addItem);
		toolbar.setTitle(Html.fromHtml("SEND CV"));
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		toolbar.setNavigationIcon(getResources().getDrawable(
				R.drawable.ic_action_back));
		toolbar.setNavigationOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HREnquiry.this.finish();
			}
		});

		InitComponents();
		ActivityStatus();

		if (savedInstanceState != null) {
			DatePickerDialog dpd = (DatePickerDialog) getSupportFragmentManager()
					.findFragmentByTag(DATEPICKER_TAG);
			if (dpd != null) {
				dpd.setOnDateSetListener(this);
			}

			TimePickerDialog tpd = (TimePickerDialog) getSupportFragmentManager()
					.findFragmentByTag(TIMEPICKER_TAG);
			if (tpd != null) {
				tpd.setOnTimeSetListener(this);
			}
		}
		bundle = getIntent().getExtras();

	}

	
	@Override
	protected void onResume() {
		super.onResume();
		if (EzeidUtil.isConnectedToInternet(getApplicationContext()) == false) {

			EzeidUtil.showToast(HREnquiry.this, getResources()
					.getString(R.string.internet_warning));
		}
	}
	
	

	private void BindBundle() {

		if (bundle != null) {

			item_update_notes.setText(bundle.getString("Message"));
			messageID = bundle.getString("TID");

			if (messageID.equalsIgnoreCase("")) {
				contact_info_text.setText(Html.fromHtml(getResources()
						.getString(R.string.customer_info_warn)));
				isItemsEditAgain = false;
				isRequesterAdded = false;
			} else {
				contact_info_text.setText(Html.fromHtml(getResources()
						.getString(R.string.customer_info_msg)));
				isItemsEditAgain = true;
				isRequesterAdded = true;
			}

		} else {
			contact_info_text.setText(Html.fromHtml(getResources().getString(
					R.string.customer_info_warn)));
		}
	}

	private void InitComponents() {

		ezeidPreferenceHelper = new EzeidPreferenceHelper(HREnquiry.this);

		item_update_notes = (EditText) findViewById(R.id.item_update_notes);
		submit_order = (Button) findViewById(R.id.submit_order);
		cancel_order = (Button) findViewById(R.id.cancel_order);

		submit_order.setText("SUBMIT");
		cancel_order.setText("CANCEL");

		submit_order.setOnClickListener(this);
		cancel_order.setOnClickListener(this);
		contact_info_text = (TextView) findViewById(R.id.contact_info_text);
		contact_info = (RelativeLayout) findViewById(R.id.contact_info);
		contact_info.setOnClickListener(this);
		item_update_notes.setHint("Notes");
		item_update_notes.setHintTextColor(getResources().getColor(
				R.color.met_green));
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.submit_order:
			if (ezeidPreferenceHelper.GetValueFromSharedPrefs(
					"ITEM_TO_ADD_USER").equalsIgnoreCase("business_mgr")) {
				SaveItems();
			} else {
				SaveExternalOrder();
			}
			break;

		case R.id.cancel_order:
			finish();
			break;

		case R.id.nextActionEdit:
			SetCalendar();
			GetDatePicker();
			break;

		case R.id.contact_info:
			GetRequester();
			break;
		}
	}

	private void GetRequester() {
		// isItemsEditAgain = false;
		if (ezeidPreferenceHelper.GetValueFromSharedPrefs("ITEM_TO_ADD_USER")
				.equalsIgnoreCase("business_mgr")) {
			// if (EzeidGlobal.EZEID_TYPE.equalsIgnoreCase("1")) {
			// GetRequesterDetails();
			// } else {
			ShowEzeidDialog("Please wait!");
			BindActionStatusFilters();
			BindNextActionFilters();
			BindFolderFilters();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					DismissEzeidDialog();
					GetRequesterDetails();
					if (isItemsEditAgain == true
							&& !messageID.equalsIgnoreCase("")) {
						if (null != salesRequester) {
							if (bundle.getString("EZEID").isEmpty()
									|| bundle.getString("EZEID") == null
									|| bundle.getString("EZEID")
											.equalsIgnoreCase("null")) {
								salesRequester.setText("");
							} else {
								salesRequester.setText(bundle
										.getString("EZEID"));
								GetPrimaryDetails(bundle.getString("EZEID"));
							}
						}
					}
				}
			}, 3000);
			// }
		} else {
			// GetPrimaryDetails(ezeidPreferenceHelper
			// .GetValueFromSharedPrefs("EZEID"));
			GetConsumerDetails();
		}
	}

	private void GetConsumerDetails() {

		LayoutInflater inflater = getLayoutInflater();
		View dialoglayout = inflater.inflate(
				R.layout.ezeid_add_requester_info_layout_external,
				(ViewGroup) findViewById(R.id.requester_external));
		final Dialog dialog = new Dialog(HREnquiry.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(dialoglayout);
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);

		locationFilter = (Spinner) dialog.findViewById(R.id.locFilter);
		LinearLayout reqphoneLayout = (LinearLayout) dialog
				.findViewById(R.id.reqphoneLayout);
		TextView ezeids = (TextView) dialog.findViewById(R.id.contact_ezeid);
		TextView reqName = (TextView) dialog.findViewById(R.id.reqName);
		TextView reqMobile = (TextView) dialog.findViewById(R.id.reqMobile);
		TextView reqphone = (TextView) dialog.findViewById(R.id.reqphone);
		addressBox = (EditText) dialog.findViewById(R.id.addressBox);
		notesBox = (EditText) dialog.findViewById(R.id.notesBox);
		requseterOk = (Button) dialog.findViewById(R.id.requseterOk);
		requseterCancel = (Button) dialog.findViewById(R.id.requseterCancel);
		ezeids.setText(ezeidPreferenceHelper.GetValueFromSharedPrefs("EZEID"));
		reqName.setText(ezeidPreferenceHelper
				.GetValueFromSharedPrefs("USER_NAME"));
		reqMobile.setText(ezeidPreferenceHelper
				.GetValueFromSharedPrefs("USER_MOBILE"));
		if (!ezeidPreferenceHelper.GetValueFromSharedPrefs("USER_PHONE")
				.equalsIgnoreCase("")) {
			reqphone.setText(ezeidPreferenceHelper
					.GetValueFromSharedPrefs("USER_PHONE"));
		} else {
			reqphoneLayout.setVisibility(View.GONE);
		}
		notesBox.setVisibility(View.GONE);
		if (locationAdapter != null) {
			locationFilter.setAdapter(locationAdapter);
		}
		locationFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				rlatitude = ((TextView) view.findViewById(R.id.lati)).getText()
						.toString();
				rlongitude = ((TextView) view.findViewById(R.id.longi))
						.getText().toString();

				String masterTag = ((TextView) view
						.findViewById(R.id.masterTag)).getText().toString();
				if (masterTag.equalsIgnoreCase("< Other >")) {
					addressBox.setVisibility(View.VISIBLE);
				} else {
					addressBox.setVisibility(View.GONE);
					if (!rlatitude.equalsIgnoreCase("")
							&& !rlongitude.equalsIgnoreCase("")) {
						GetAddress(rlatitude, rlongitude);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				isEdited = false;
			}
		});
		requseterCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});
		requseterOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialog.dismiss();
				isRequesterAdded = true;
				contact_info_text.setText(Html.fromHtml(getResources()
						.getString(R.string.customer_info_msg)));

			}
		});
		dialog.show();
	}

	private void GetRequesterDetails() {
		LayoutInflater inflater = getLayoutInflater();
		View dialoglayout = inflater.inflate(
				R.layout.ezeid_add_requester_info_layout,
				(ViewGroup) findViewById(R.id.requesterRoot));
		final Dialog dialog = new Dialog(HREnquiry.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(dialoglayout);
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);

		salesRequester = (EditText) dialog.findViewById(R.id.salesRequesters);
		addressBox = (EditText) dialog.findViewById(R.id.addressBox);

		notesBox = (EditText) dialog.findViewById(R.id.notesBox);
		notesBox.setVisibility(View.GONE);
		nextActionDateBox = (EditText) dialog
				.findViewById(R.id.nextActionDateBox);
		nextActionDateBox.setEnabled(false);
		nextActionDateBox.setTextColor(Color.rgb(0, 122, 165));
		editActionDate = (ButtonFloatSmall) dialog
				.findViewById(R.id.nextActionEdit);
		checkEzeidAvailability = (ButtonFloatSmall) dialog
				.findViewById(R.id.checkEzeidAvailability);
		requseterOk = (Button) dialog.findViewById(R.id.requseterOk);
		requseterCancel = (Button) dialog.findViewById(R.id.requseterCancel);
		statusFilter = (Spinner) dialog.findViewById(R.id.statusFilter);
		actionFilter = (Spinner) dialog.findViewById(R.id.actionFilter);
		folderFilter = (Spinner) dialog.findViewById(R.id.folderFilter);
		locationFilter = (Spinner) dialog.findViewById(R.id.locFilter);
		checkEzeidStatusText = (TextView) dialog
				.findViewById(R.id.checkEzeidStatus);
		checkEzeidStatusLine = (TextView) dialog
				.findViewById(R.id.checkEzeidStatusBar);
		// notesLayout = (LinearLayout) dialog.findViewById(R.id.notesLayout);

		folderLayout = (LinearLayout) dialog.findViewById(R.id.folderLayout);
		// reqStatuslayout = (LinearLayout)
		// dialog.findViewById(R.id.statusLayout);
		// reqActionLayout = (LinearLayout)
		// dialog.findViewById(R.id.actionLayout);
		// reqActionDateLayout = (LinearLayout) dialog
		// .findViewById(R.id.actionDateLayout);
		// reqAvailabilityLayout = (LinearLayout) dialog
		// .findViewById(R.id.salesRequesterLayout);
		// userTypeLayout = (LinearLayout) dialog
		// .findViewById(R.id.userTypeLayout);
		//
		// reqBar1 = (TextView) dialog.findViewById(R.id.bar1);
		// reqBar2 = (TextView) dialog.findViewById(R.id.bar2);

		checkEzeidStatusLayout = (LinearLayout) dialog
				.findViewById(R.id.checkEzeidStatusLayout);
		switchLoc = (TextView) dialog.findViewById(R.id.switchCurloc);

		userType = (RadioGroup) dialog.findViewById(R.id.userType);
		custEzeid = (RadioButton) dialog.findViewById(R.id.custEzeid);
		custOther = (RadioButton) dialog.findViewById(R.id.custOther);
		editActionDate.setOnClickListener(this);
		custEzeid.setChecked(true);

		if (bundle != null) {
			addressBox.setText(bundle.getString("DeliveryAddress"));
			notesBox.setText(bundle.getString("Notes_lbl"));
			nextActionDateBox.setText(bundle.getString("Next_action_date"));
			// statusFilter.setSelection(Integer.parseInt(bundle
			// .getString("StatusID")));
			// actionFilter.setSelection(Integer.parseInt(bundle
			// .getString("ActionID")));
			// folderFilter.setSelection(Integer.parseInt(bundle
			// .getString("FolderID")));

			addressBox.setText(bundle.getString("DeliveryAddress"));
			if (bundle.getString("EZEID").isEmpty()
					|| bundle.getString("EZEID") == null) {
				salesRequester.setText("");
			} else {
				salesRequester.setText(bundle.getString("EZEID"));
				GetPrimaryDetails(bundle.getString("EZEID"));
			}
			requseterOk.setText("OK");

		}

		if (custEzeid.isChecked() == true) {
			checkEzeidAvailability.setVisibility(View.VISIBLE);
		} else {
			checkEzeidAvailability.setVisibility(View.GONE);
		}
		userType.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (custEzeid.isChecked() == true) {
					salesRequester.setHint("Contact Informations");
					addressBox.setVisibility(View.GONE);
					checkEzeidStatusLine.setVisibility(View.VISIBLE);
					checkEzeidAvailability.setVisibility(View.VISIBLE);
					checkEzeidStatusLayout.setVisibility(View.VISIBLE);
				} else if (custOther.isChecked()) {
					salesRequester.setHint("Contact name and phone no");
					addressBox.setVisibility(View.VISIBLE);
					checkEzeidStatusLine.setVisibility(View.GONE);
					checkEzeidAvailability.setVisibility(View.GONE);
					checkEzeidStatusLayout.setVisibility(View.GONE);
				}
			}
		});

		contactInfoProgress = (ProgressBar) dialog
				.findViewById(R.id.contactInfoProgress);
		contactInfoProgress.getIndeterminateDrawable().setColorFilter(
				Color.rgb(50, 175, 230),
				android.graphics.PorterDuff.Mode.MULTIPLY);
		// BindUserFilter();

		statusFilter.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				edit_statusID = ((TextView) view.findViewById(R.id.masterId))
						.getText().toString();
				if (status_count == 0) {
					isEdited = false;
					status_count++;
				} else {
					isEdited = true;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				isEdited = false;
			}
		});
		actionFilter.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				edit_nextActionID = ((TextView) view
						.findViewById(R.id.masterId)).getText().toString();
				if (action_count == 0) {
					isEdited = false;
					action_count++;
				} else {
					isEdited = true;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				isEdited = false;
			}
		});
		folderFilter.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				edit_floderID = ((TextView) view.findViewById(R.id.folderTID))
						.getText().toString();
				if (folder_count == 0) {
					isEdited = false;
					folder_count++;
				} else {
					isEdited = true;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				isEdited = false;
			}
		});

		locationFilter.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				rlatitude = ((TextView) view.findViewById(R.id.lati)).getText()
						.toString();
				rlongitude = ((TextView) view.findViewById(R.id.longi))
						.getText().toString();
				// showToast("locationFilter: location=" + rlatitude + " "
				// + rlongitude);
				String masterTag = ((TextView) view
						.findViewById(R.id.masterTag)).getText().toString();
				if (masterTag.equalsIgnoreCase("< Other >")) {
					addressBox.setVisibility(View.VISIBLE);
				} else {
					addressBox.setVisibility(View.GONE);
					if (!rlatitude.equalsIgnoreCase("")
							&& !rlongitude.equalsIgnoreCase("")) {
						GetAddress(rlatitude, rlongitude);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		editActionDate.setOnClickListener(this);

		requseterCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				// menu_deliver_persion.getItem(0).setIcon(
				// R.drawable.ic_action_requester_normal);
				// contact_info_text.setText(Html.fromHtml(getResources()
				// .getString(R.string.customer_info_warn)));
				// isRequesterAdded = false;
				if (bundle == null) {
					isRequesterAdded = false;
				}
			}
		});
		requseterOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// validation here.
				if (salesRequester.getText().toString().equalsIgnoreCase("")) {
					checkEzeidStatusText
							.setText("Requester EZEID is mandatory!");
				} else if (nextActionDateBox.getText().toString()
						.equalsIgnoreCase("")) {
					showToast("Next action date is mandatory!");
				} else {
					isRequesterAdded = true;
					// menu_deliver_persion.getItem(0).setIcon(
					// R.drawable.ic_action_requester_selected);
					contact_info_text.setText(Html.fromHtml(getResources()
							.getString(R.string.customer_info_msg)));
					dialog.dismiss();
				}
			}
		});

		if (statusAdapter != null) {
			statusFilter.setAdapter(statusAdapter);
			if (bundle != null) {
				SetDefaultStatus();
			}
		} else {
			BindActionStatusFilters();
		}
		if (actionAdapter != null) {
			actionFilter.setAdapter(actionAdapter);
			if (bundle != null) {
				SetDefaultActions();
			}
		} else {
			BindNextActionFilters();
		}
		if (folderAdapter != null) {
			folderFilter.setAdapter(folderAdapter);
			if (bundle != null) {
				SetDefaultFolder();
			}
		} else {
			BindFolderFilters();
		}
		if (locationAdapter != null) {
			locationFilter.setAdapter(locationAdapter);
		}

		switchLoc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// is switchLoc checked?
				if (((CheckBox) v).isChecked()) {
					EzeidUtil.ChangeEditEffect(folderLayout, HREnquiry.this);
					folderLayout.setBackgroundColor(Color.rgb(216, 216, 216));
					folderFilter.setEnabled(false);
				} else {
					folderLayout.setBackgroundColor(Color.rgb(255, 255, 255));
					folderFilter.setEnabled(true);
				}
			}
		});
		checkEzeidAvailability.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!salesRequester.getText().toString().equalsIgnoreCase("")) {
					GetPrimaryDetails(salesRequester.getText().toString());
				}
			}
		});

		dialog.show();
	}

	void showToast(CharSequence msg) {
		Toast.makeText(HREnquiry.this, msg, Toast.LENGTH_SHORT).show();
	}

	private void SetDefaultStatus() {
		// statusID
		if (status_array != null) {
			if (status_array.size() > 0) {
				int count = 0;
				for (HashMap<String, String> hashMap : status_array) {
					String status = hashMap.get("TID");
					if (bundle.getString("StatusID").equalsIgnoreCase(status)) {
						statusFilter.setSelection(count);
					} else {
						count++;
					}
				}
			}
		}
	}

	private void SetDefaultActions() {
		if (action_array != null) {
			if (action_array.size() > 0) {
				int count = 0;
				for (HashMap<String, String> hashMap : action_array) {
					String status = hashMap.get("TID");
					if (bundle.getString("ActionID").equalsIgnoreCase(status)) {
						actionFilter.setSelection(count);
					} else {
						count++;
					}
				}
			}
		}
	}

	private void SetDefaultFolder() {
		// statusID
		if (folder_array != null) {
			if (folder_array.size() > 0) {
				int count = 0;
				for (HashMap<String, String> hashMap : folder_array) {
					String status = hashMap.get("TID");
					if (bundle.getString("FolderID").equalsIgnoreCase(status)) {
						folderFilter.setSelection(count);
					} else {
						count++;
					}
				}
			}
		}
	}

	private void ShowEzeidDialog(String content) {
		ezeidLoadingProgress = new EzeidLoadingProgress(HREnquiry.this, content);
		handlerDialog = new Handler();
		runnableDialog = new Runnable() {

			@Override
			public void run() {
				if (ezeidLoadingProgress != null) {
					if (ezeidLoadingProgress.isShowing()) {
						ezeidLoadingProgress.dismiss();
					}
				}
			}
		};
		ezeidLoadingProgress.show();
	}

	private void DismissEzeidDialog() {
		handlerDialog.removeCallbacks(runnableDialog);
		if (ezeidLoadingProgress.isShowing()) {
			ezeidLoadingProgress.dismiss();
		}
	}

	protected void GetAddress(String lati, String longi) {
		try {
			JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
					Request.Method.GET,
					"http://maps.googleapis.com/maps/api/geocode/json?latlng="
							+ lati + "," + longi + "&sensor=true",
					new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {
							BindAddress(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							VolleyLog.e(TAG, "error:" + error);
							BindAddress(null);
						}
					});
			jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindAddress(JSONObject jsonObj) {
		String address1 = "";
		String address2 = "";
		String PIN = "";
		String full_address = "";
		try {
			if (null != jsonObj) {
				String Status = jsonObj.getString("status");
				if (Status.equalsIgnoreCase("OK")) {
					JSONArray Results = jsonObj.getJSONArray("results");
					JSONObject zero = Results.getJSONObject(0);
					JSONArray address_components = zero
							.getJSONArray("address_components");

					JSONObject geoObj = zero.getJSONObject("geometry");
					JSONObject locObj = geoObj.getJSONObject("location");
					rlatitude = locObj.getString("lat");
					rlongitude = locObj.getString("lng");

					for (int i = 0; i < address_components.length(); i++) {
						JSONObject zero2 = address_components.getJSONObject(i);
						String long_name = zero2.getString("long_name");
						JSONArray mtypes = zero2.getJSONArray("types");
						String Type = mtypes.getString(0);

						if (TextUtils.isEmpty(long_name) == false
								|| !long_name.equals(null)
								|| long_name.length() > 0 || long_name != "") {
							if (Type.equalsIgnoreCase("street_number")) {
								address1 = long_name + " ";
							} else if (Type.equalsIgnoreCase("route")) {
								address1 = address1 + long_name;
							} else if (Type.equalsIgnoreCase("sublocality")) {
								address2 = long_name;
							} else if (Type.equalsIgnoreCase("locality")) {
								// Address2 = Address2 + long_name + ", ";
								city = long_name;
							} else if (Type
									.equalsIgnoreCase("administrative_area_level_2")) {
								country = long_name;
							} else if (Type
									.equalsIgnoreCase("administrative_area_level_1")) {
								state = long_name;
							} else if (Type.equalsIgnoreCase("country")) {
								country = long_name;
							} else if (Type.equalsIgnoreCase("postal_code")) {
								PIN = long_name;
							}
						}
						full_address = address1 + "," + address2 + "," + city
								+ "," + state + "," + country + "," + PIN;
						// Log.i(TAG, "full_address: " + full_address);
					}
				}
			} else {
				// Log.i(TAG, "null response");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void GetPrimaryDetails(String ezeid) {
		try {
			if (!ezeid.equalsIgnoreCase("")) {
				String token = ezeidPreferenceHelper
						.GetValueFromSharedPrefs("Token");
				String url = EZEIDUrlManager.getAPIUrl()
						+ "ewtEZEIDPrimaryDetails?Token=" + token + "&EZEID="
						+ ezeid;
				contactInfoProgress.setVisibility(View.VISIBLE);
				JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
						new Response.Listener<JSONArray>() {
							@Override
							public void onResponse(JSONArray response) {
								PrimaryDetail(response);
							}
						}, new Response.ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError error) {
								PrimaryDetail(null);
							}
						});
				jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
						DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
						DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
				EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void PrimaryDetail(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len > 0) {
					for (int i = 0; i < len; i++) {

						JSONObject jsonObject = response.getJSONObject(i);
						// String tID = jsonObject.getString("TID");
						ezeid = jsonObject.getString("EZEID");
						firstName = jsonObject.getString("FirstName");
						lastName = jsonObject.getString("LastName");
						mobileNumber = jsonObject.getString("MobileNumber");
						latitude = jsonObject.getString("Latitude");
						longitude = jsonObject.getString("Longitude");
						salesRequester.setText("" + firstName + " " + lastName
								+ " - " + mobileNumber);
					}
					if (!latitude.equalsIgnoreCase("")
							&& !longitude.equalsIgnoreCase("")) {
						GetAddress(latitude, longitude);
					}
				}
				checkEzeidStatusText.setVisibility(View.GONE);
			} else {
				checkEzeidStatusText.setText("Invalid EZEID!");
			}
			contactInfoProgress.setVisibility(View.GONE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void CheckSessionExpiredStatus() {
		String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
		if (!token.equalsIgnoreCase("") && sessionRetry < 5) {
			JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
					EzeidGlobal.EzeidUrl + "ewtGetLoginCheck?Token=" + token,
					 new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {
							SessionStatus(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							sessionRetry += 1;
							if (error instanceof TimeoutError) {
								SessionTryAgain();
							} else if (error instanceof ServerError
									|| error instanceof AuthFailureError) {
								EzeidUtil.showToast(HREnquiry.this,
										"Network is unreachable!");
							} else if (error instanceof NetworkError
									|| error instanceof NoConnectionError) {
								SessionTryAgain();
							} else {
								SessionTryAgain();
							}
						}
					});
			req.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
		}
	}

	private void SessionTryAgain() {
		CheckSessionExpiredStatus();
	}

	private void SessionStatus(JSONObject response) {
		if (!response.isNull("IsAvailable")) {
			try {
				String status = response.getString("IsAvailable");
				if (status.equalsIgnoreCase("true")) {
					ActivityStatus();
				} else {
					SessionDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void SessionDialog() {
		final MaterialDialog materialDialog = new MaterialDialog(this);
		materialDialog
				.setBackgroundResource(R.drawable.abc_cab_background_internal_bg);
		materialDialog.setTitle(R.string.sessionExpired)
				.setMessage("Your session has expired. Please login again!")
				.setPositiveButton(R.string.ok, new OnClickListener() {
					@Override
					public void onClick(View v) {
						materialDialog.dismiss();
						ezeidPreferenceHelper.SaveValueToSharedPrefs("Token",
								"");
						startActivity(new Intent(HREnquiry.this,
								SplashScreen.class));
						HREnquiry.this.finish();
					}
				});
		materialDialog.setCanceledOnTouchOutside(true).show();
	}

	private void ActivityStatus() {
		String masterID = ezeidPreferenceHelper
				.GetValueFromSharedPrefs("MasterIDLog");
		if (masterID.equalsIgnoreCase("0")) {
			BindLocationsFilters();
		} else {
			locationAdapter = null;
		}
		bundle = getIntent().getExtras();
		BindBundle();
		GetEditedItems();

	}

	private void GetEditedItems() {
		Intent intent = getIntent();
		if (null != intent) {
			if (intent.getStringExtra("TID") != null) {
				messageID = intent.getStringExtra("TID");
				String edit = intent.getStringExtra("ITEM_EDIT");
				update_ezistingNotes = intent.getStringExtra("Notes_lbl");
				update_statusID = intent.getStringExtra("StatusID");
				update_nextActionID = intent.getStringExtra("ActionID");
				update_folderRuleID = intent.getStringExtra("FolderID");
				update_locID = intent.getStringExtra("LocID");
				update_messageText = intent.getStringExtra("Message");
				update_taskDateTimeStr = intent.getStringExtra("TaskDateTime");
				update_nextActionDateStr = intent
						.getStringExtra("Next_action_date");
				update_contactInfo = intent.getStringExtra("ContactInfo");
				deliveryAddress = intent.getStringExtra("DeliveryAddress");
				ezeid = intent.getStringExtra("EZEID");
				req_ezeid = intent.getStringExtra("RequesterEZEID");
				if (edit.equalsIgnoreCase("") || edit.equalsIgnoreCase(null)) {
					isItemsEditAgain = false;
					isRequesterAdded = false;
					contact_info_text.setText(Html.fromHtml(getResources()
							.getString(R.string.customer_info_warn)));
				} else {
					isItemsEditAgain = true;
					isRequesterAdded = true;
					contact_info_text.setText(Html.fromHtml(getResources()
							.getString(R.string.customer_info_msg)));
				}
			} else {
				String get_local_user_type = ezeidPreferenceHelper
						.GetValueFromSharedPrefs("ITEM_TO_ADD_USER");
				if (get_local_user_type.equalsIgnoreCase("business_mgr")) {
					contact_info_text.setText(Html.fromHtml(getResources()
							.getString(R.string.customer_info_warn)));
				} else {
					contact_info_text.setText(Html.fromHtml(getResources()
							.getString(R.string.customer_info_msg)));
				}
			}

		}
	}

	private void BindActionStatusFilters() {
		try {
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String masterID = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("MasterID");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetStatusType?Token=" + token + "&MasterID="
					+ masterID + "&FunctionType=4";
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {
						@Override
						public void onResponse(JSONArray response) {
							BindStatus(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							BindStatus(null);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void BindLocationsFilters() {
		try {
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetLocationList?Token=" + token;
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {
						@Override
						public void onResponse(JSONArray response) {
							BindLocations(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							BindLocations(null);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void BindNextActionFilters() {
		try {
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String masterID = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("MasterID");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetActionType?Token=" + token + "&MasterID="
					+ masterID + "&FunctionType=4";

			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {
						@Override
						public void onResponse(JSONArray response) {
							BindActions(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							BindActions(null);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindActions(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					action_array = new ArrayList<HashMap<String, String>>();
					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String masterID = jsonObject.getString("MasterID");
						String actionTitle = jsonObject
								.getString("ActionTitle");

						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("MasterID", masterID);
						map.put("ActionTitle", actionTitle);
						action_array.add(map);

						actionAdapter = new SimpleAdapter(
								HREnquiry.this,
								action_array,
								R.layout.ezeid_tagid_row,
								new String[] { "TID", "MasterID", "ActionTitle" },
								new int[] { R.id.masterId,
										R.id.masterPercentage, R.id.masterTag });
						// actionFilter.setAdapter(actionAdapter);
					}
				}
			} else {
				action_array = new ArrayList<HashMap<String, String>>();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("TID", "0");
				map.put("MasterID", "0");
				map.put("ActionTitle", "No next actions added");
				action_array.add(map);

				actionAdapter = new SimpleAdapter(HREnquiry.this, action_array,
						R.layout.ezeid_tagid_row, new String[] { "TID",
								"MasterID", "ActionTitle" }, new int[] {
								R.id.masterId, R.id.masterPercentage,
								R.id.masterTag });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindLocations(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String masterID = jsonObject.getString("MasterID");
						String locTitle = jsonObject.getString("LocTitle");
						String latitude = jsonObject.getString("Latitude");
						String longitude = jsonObject.getString("Longitude");

						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("MasterID", masterID);
						map.put("LocTitle", locTitle);
						map.put("Latitude", latitude);
						map.put("Longitude", longitude);
						arrayList.add(map);
					}

					HashMap<String, String> map1 = new HashMap<String, String>();
					map1.put("TID", "0");
					map1.put("MasterID", "0");
					map1.put("LocTitle", "Current Location");
					map1.put("Latitude", ezeidPreferenceHelper
							.GetValueFromSharedPrefs("Latitude"));
					map1.put("Longitude", ezeidPreferenceHelper
							.GetValueFromSharedPrefs("Longitude"));
					arrayList.add(map1);

					HashMap<String, String> map = new HashMap<String, String>();
					map.put("TID", "0");
					map.put("MasterID", "0");
					map.put("LocTitle", "< Other >");
					map.put("Latitude", "");
					map.put("Longitude", "");
					arrayList.add(map);

					locationAdapter = new SimpleAdapter(HREnquiry.this,
							arrayList, R.layout.ezeid_tagid_row, new String[] {
									"TID", "MasterID", "LocTitle", "Latitude",
									"Longitude" }, new int[] { R.id.masterId,
									R.id.masterPercentage, R.id.masterTag,
									R.id.lati, R.id.longi });
				}
			} else {
				ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("TID", "0");
				map.put("MasterID", "0");
				map.put("LocTitle", "No locations added");
				map.put("Latitude", "");
				map.put("Longitude", "");
				arrayList.add(map);

				locationAdapter = new SimpleAdapter(HREnquiry.this, arrayList,
						R.layout.ezeid_tagid_row,
						new String[] { "TID", "MasterID", "LocTitle",
								"Latitude", "Longitude" }, new int[] {
								R.id.masterId, R.id.masterPercentage,
								R.id.masterTag, R.id.lati, R.id.longi });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindStatus(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					status_array = new ArrayList<HashMap<String, String>>();
					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String statusTitle = jsonObject
								.getString("StatusTitle");
						String progressPercent = jsonObject
								.getString("ProgressPercent");
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("StatusTitle", statusTitle);
						map.put("ProgressPercent", progressPercent);
						status_array.add(map);

						statusAdapter = new SimpleAdapter(HREnquiry.this,
								status_array, R.layout.ezeid_tagid_row,
								new String[] { "TID", "StatusTitle",
										"ProgressPercent" }, new int[] {
										R.id.masterId, R.id.masterTag,
										R.id.masterPercentage });
					}
				}
			} else {
				status_array = new ArrayList<HashMap<String, String>>();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("TID", "0");
				map.put("StatusTitle", "No stages added");
				map.put("ProgressPercent", "");
				status_array.add(map);

				statusAdapter = new SimpleAdapter(HREnquiry.this, status_array,
						R.layout.ezeid_tagid_row, new String[] { "TID",
								"StatusTitle", "ProgressPercent" }, new int[] {
								R.id.masterId, R.id.masterTag,
								R.id.masterPercentage });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void BindFolderFilters() {
		try {
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String masterID = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("MasterID");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetFolderList?Token=" + token + "&MasterID="
					+ masterID + "&FunctionType=4";
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {
						@Override
						public void onResponse(JSONArray response) {
							BindFolders(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							BindFolders(null);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindFolders(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					folder_array = new ArrayList<HashMap<String, String>>();
					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String masterID = jsonObject.getString("MasterID");
						String folderTitle = jsonObject
								.getString("FolderTitle");
						String ruleFunction = jsonObject
								.getString("RuleFunction");
						String ruleType = jsonObject.getString("RuleType");
						String countryIDs = jsonObject.getString("CountryIDs");
						String matchAdminLevel = jsonObject
								.getString("MatchAdminLevel");
						String mappedNames = jsonObject
								.getString("MappedNames");
						String latitude = jsonObject.getString("Latitude");
						String longitude = jsonObject.getString("Longitude");

						String proximity = jsonObject.getString("Proximity");
						String defaultFolder = jsonObject
								.getString("DefaultFolder");
						String folderStatus = jsonObject
								.getString("FolderStatus");
						String seqNoFrefix = jsonObject
								.getString("SeqNoFrefix");
						String runningSeqNo = jsonObject
								.getString("RunningSeqNo");
						String createdDate = jsonObject
								.getString("CreatedDate");
						String lUDate = jsonObject.getString("LUDate");

						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("MasterID", masterID);
						map.put("FolderTitle", folderTitle);
						map.put("RuleFunction", ruleFunction);
						map.put("RuleType", ruleType);
						map.put("CountryIDs", countryIDs);
						map.put("MatchAdminLevel", matchAdminLevel);
						map.put("MappedNames", mappedNames);
						map.put("Latitude", latitude);
						map.put("Longitude", longitude);
						map.put("Proximity", proximity);
						map.put("DefaultFolder", defaultFolder);
						map.put("FolderStatus", folderStatus);
						map.put("SeqNoFrefix", seqNoFrefix);
						map.put("RunningSeqNo", runningSeqNo);
						map.put("CreatedDate", createdDate);
						map.put("LUDate", lUDate);
						folder_array.add(map);

						folderAdapter = new SimpleAdapter(HREnquiry.this,
								folder_array, R.layout.ezeid_folder_row,
								new String[] { "TID", "MasterID",
										"FolderTitle", "RuleFunction",
										"RuleType", "CountryIDs",
										"MatchAdminLevel", "MappedNames",
										"Latitude", "Longitude", "Proximity",
										"DefaultFolder", "FolderStatus",
										"SeqNoFrefix", "RunningSeqNo",
										"CreatedDate", "LUDate" }, new int[] {
										R.id.folderTID, R.id.masterID,
										R.id.folderTitle, R.id.ruleFunction,
										R.id.ruleType, R.id.countryIDs,
										R.id.matchAdminLevel, R.id.mappedNames,
										R.id.latitude, R.id.longitude,
										R.id.proximity, R.id.defaultFolder,
										R.id.folderStatus, R.id.seqNoFrefix,
										R.id.runningSeqNo, R.id.createdDate,
										R.id.lUDate });
						// folderFilter.setAdapter(folderAdapter);
					}
				}
			} else {
				folder_array = new ArrayList<HashMap<String, String>>();
				HashMap<String, String> map_ = new HashMap<String, String>();
				map_.put("TID", "");
				map_.put("MasterID", "");
				map_.put("FolderTitle", "No folder rule added");
				map_.put("RuleFunction", "");
				map_.put("RuleType", "");
				map_.put("CountryIDs", "");
				map_.put("MatchAdminLevel", "");
				map_.put("MappedNames", "");
				map_.put("Latitude", "");
				map_.put("Longitude", "");
				map_.put("Proximity", "");
				map_.put("DefaultFolder", "");
				map_.put("FolderStatus", "");
				map_.put("SeqNoFrefix", "");
				map_.put("RunningSeqNo", "");
				map_.put("CreatedDate", "");
				map_.put("LUDate", "");
				folder_array.add(map_);
				folderAdapter = new SimpleAdapter(HREnquiry.this, folder_array,
						R.layout.ezeid_folder_row, new String[] { "TID",
								"MasterID", "FolderTitle", "RuleFunction",
								"RuleType", "CountryIDs", "MatchAdminLevel",
								"MappedNames", "Latitude", "Longitude",
								"Proximity", "DefaultFolder", "FolderStatus",
								"SeqNoFrefix", "RunningSeqNo", "CreatedDate",
								"LUDate" }, new int[] { R.id.folderTID,
								R.id.masterID, R.id.folderTitle,
								R.id.ruleFunction, R.id.ruleType,
								R.id.countryIDs, R.id.matchAdminLevel,
								R.id.mappedNames, R.id.latitude,
								R.id.longitude, R.id.proximity,
								R.id.defaultFolder, R.id.folderStatus,
								R.id.seqNoFrefix, R.id.runningSeqNo,
								R.id.createdDate, R.id.lUDate });
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// String tag = null;
		switch (parent.getId()) {

		case R.id.folderFilter:
			folderRuleID = ((TextView) view.findViewById(R.id.masterID))
					.getText().toString();
			// tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
			// .toString();
			// String percentage = ((TextView) view
			// .findViewById(R.id.masterPercentage)).getText().toString();
			// showToast("statusFilter: masterId=" + folderRuleID + ", tag= "
			// + tag + ", percentage=" + percentage);
			break;

		case R.id.statusFilter:
			statusID = ((TextView) view.findViewById(R.id.masterId)).getText()
					.toString();
			// tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
			// .toString();
			// showToast("statusFilter: masterId=" + statusID + ", tag= " +
			// tag);
			break;

		case R.id.actionFilter:
			nextActionID = ((TextView) view.findViewById(R.id.masterId))
					.getText().toString();
			// tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
			// .toString();
			// showToast("statusFilter: masterId=" + nextActionID + ", tag= "
			// + tag);
			break;

		default:
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// showToast("onNothingSelected()");
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		// YYYY-MM-DDThh:mm:ss.sTZD
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss",
				Locale.getDefault());
		// String timeZoneId = TimeZone.getDefault().getID();
		// df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String formattedDate = df.format(calendar.getTime());
		formattedDate = formattedDate.substring(17, formattedDate.length());

		String hourStr = "", minStr = "";
		int hour = String.valueOf(hourOfDay).trim().length();
		if (hour == 1) {
			hourStr = "0" + hourOfDay;
		} else {
			hourStr = "" + hourOfDay;
		}
		int min = String.valueOf(minute).trim().length();
		if (min == 1) {
			minStr = "0" + minute;
		} else {
			minStr = "" + minute;
		}

		timeStr = hourStr + ":" + minStr + ":" + formattedDate;
		prefDateTimeStr = dateStr + " " + timeStr;
		nextActionDateBox.setVisibility(View.VISIBLE);
		nextActionDateBox.setText("" + PreferedDateNormal(prefDateTimeStr));
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		dateStr = "";
		timeStr = "";
		nextActionDateBox.setText("");
		month = month + 1;
		int mon = String.valueOf(month).trim().length();
		if (mon == 1) {
			dateStr = "0" + month + "/" + day + "/" + year;
		} else {
			dateStr = month + "/" + day + "/" + year;
		}
		GetTimePicker();
	}

	private void SetCalendar() {
		calendar = Calendar.getInstance();
		datePickerDialog = DatePickerDialog.newInstance(this,
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), false);
		timePickerDialog = TimePickerDialog.newInstance(this,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), true, false);
	}

	private void GetDatePicker() {
		datePickerDialog.setVibrate(false);
		datePickerDialog.setYearRange(1985, 2028);
		datePickerDialog.setCloseOnSingleTapDay(false);
		datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
	}

	private void GetTimePicker() {
		timePickerDialog.setVibrate(false);
		timePickerDialog.setCloseOnSingleTapMinute(false);
		timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
	}

	private String PreferedDateNormal(String preferedDate) {
		Date date;// 02/26/2015 02:00:29
		try {
			SimpleDateFormat normalFormat = new SimpleDateFormat(
					"MM/dd/yyyy KK:mm:ss", Locale.getDefault());
			date = normalFormat.parse(preferedDate);

			SimpleDateFormat destFormat = new SimpleDateFormat(
					"MMM dd yyyy HH:mm", Locale.ENGLISH);

			return destFormat.format(date);// Feb 26 2015 02:00 AM
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	private String GetCurrentUTCTime() {
		SimpleDateFormat utcFormat = new SimpleDateFormat(
				"MM/dd/yyyy HH:mm:ss", Locale.getDefault());
		utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return utcFormat.format(Calendar.getInstance().getTime());
	}

	private String NextActionDateUTC() {
		Date date;// 02/25/2015 03:00:56
		if (prefDateTimeStr.equalsIgnoreCase("")) {
			prefDateTimeStr = nextActionDateBox.getText().toString();
		}
		try {
			SimpleDateFormat utcFormat = new SimpleDateFormat(
					"MM/dd/yyyy KK:mm:ss", Locale.getDefault());
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			date = utcFormat.parse(prefDateTimeStr);// Wed Feb 25 08:30:56
													// GMT+05:30 2015
			SimpleDateFormat destFormat = new SimpleDateFormat(
					"MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
			return destFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String NextActionUpDateUTC(String dateS) {
		Date date;// 02/25/2015 03:00:56

		try {
			// 07 Apr 2015 18:41:44 PM, "dd MMM yyyy KK:mm:ss aa"
			SimpleDateFormat utcFormat = new SimpleDateFormat(
					"dd MMM yyyy KK:mm:ss aa", Locale.getDefault());
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			date = utcFormat.parse(dateS);// Wed Feb 25 08:30:56
											// GMT+05:30 2015
			SimpleDateFormat destFormat = new SimpleDateFormat(
					"MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
			return destFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void SaveExternalOrder() {

		try {
			String ezeid = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("EZEID");
			String to_ezeid = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("BusinessEZEID");
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			if (isRequesterAdded == true) {

				messageText = item_update_notes.getText().toString();

				taskDateTimeStr = GetCurrentUTCTime();
				String locID = ezeidPreferenceHelper
						.GetValueFromSharedPrefs("LocID");

				if (isItemsEditAgain == true) {
					contactInfo = ezeidPreferenceHelper
							.GetValueFromSharedPrefs("ContactInfo");
				} else {
					contactInfo = ezeidPreferenceHelper
							.GetValueFromSharedPrefs("USER_NAME")
							+ " - "
							+ ezeidPreferenceHelper
									.GetValueFromSharedPrefs("USER_MOBILE");
				}
				// salesItemListType = ezeidPreferenceHelper
				// .GetValueFromSharedPrefs("SalesItemListType");
				update_ezistingNotes = notesBox.getText().toString();
				if (rlatitude.equalsIgnoreCase("")) {
					rlatitude = "0";
				}
				if (rlongitude.equalsIgnoreCase("")) {
					rlongitude = "0";
				}

				taskDateTimeStr = GetCurrentUTCTime();
				String duration = "0";
				String durationScales = "0";
				HashMap<String, String> hashMap = new HashMap<String, String>();
				hashMap.put("Token", token);
				hashMap.put("TID", "0");
				hashMap.put("MessageText", messageText);
				hashMap.put("item_list_type", "0");
				hashMap.put("Status", "0");
				hashMap.put("TaskDateTime", taskDateTimeStr);
				hashMap.put("Notes", "");
				hashMap.put("LocID", locID);
				hashMap.put("Country", country);
				hashMap.put("State", state);
				hashMap.put("City", city);
				hashMap.put("Area", area);
				hashMap.put("FunctionType", "4");
				hashMap.put("Latitude", rlatitude);
				hashMap.put("Longitude", rlongitude);
				hashMap.put("EZEID", ezeid);
				hashMap.put("ToEZEID", to_ezeid);
				hashMap.put("ContactInfo", contactInfo);
				hashMap.put("FolderRuleID", "0");
				hashMap.put("Duration", duration);
				hashMap.put("DurationScales", durationScales);
				hashMap.put("DeliveryAddress", addressBox.getText().toString());
				hashMap.put("NextAction", "0");
				hashMap.put("NextActionDateTime", taskDateTimeStr);
				hashMap.put("ItemsList", "[]");

				// Log.i("TAG", "hashMap items: " + hashMap.toString());

				SaveTransactions(hashMap);

			} else {
				EzeidUtil.showToast(HREnquiry.this,
						"Customer information is not selected!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			EzeidUtil.showToast(HREnquiry.this,
					"Exception during saving transaction");
		}

	}

	private void SaveItems() {
		try {
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			if (isRequesterAdded == true && isItemsEditAgain == false) {
				String to_ezeid = ezeidPreferenceHelper
						.GetValueFromSharedPrefs("EZEID");
				messageText = item_update_notes.getText().toString();
				taskDateTimeStr = GetCurrentUTCTime();
				String locID = ezeidPreferenceHelper
						.GetValueFromSharedPrefs("LocID");//
				if (locID.equalsIgnoreCase("")) {
					locID = "0";
				}
				contactInfo = salesRequester.getText().toString();
				if (rlatitude.equalsIgnoreCase("")) {
					rlatitude = "0";
				}
				if (rlongitude.equalsIgnoreCase("")) {
					rlongitude = "0";
				}

				String folderRule = "";
				if (!edit_floderID.equalsIgnoreCase("")) {
					folderRule = edit_floderID;
				} else {
					folderRule = "0";
				}
				String status = "";
				if (!edit_statusID.equalsIgnoreCase("")) {
					status = edit_statusID;
				} else {
					status = "0";
				}
				String nextAction = "";
				if (!edit_nextActionID.equalsIgnoreCase("")) {
					nextAction = edit_nextActionID;
				} else {
					nextAction = "0";
				}

				String duration = "0";
				String durationScales = "0";
				String nextActionkDateTime = NextActionDateUTC();
				if (edit_nextActionID.equalsIgnoreCase("")) {
					EzeidUtil.showToast(HREnquiry.this,
							"Next Action status is mandatory!");
					GetRequester();
				} else if (edit_statusID.equalsIgnoreCase("")) {
					EzeidUtil.showToast(HREnquiry.this,
							"Delivery status is mandatory!");
					GetRequester();
				} else {

					HashMap<String, String> hashMap = new HashMap<String, String>();
					hashMap.put("Token", token);
					hashMap.put("TID", "0");
					hashMap.put("MessageText", messageText);
					hashMap.put("item_list_type", "0");
					hashMap.put("Status", status);
					hashMap.put("TaskDateTime", taskDateTimeStr);
					hashMap.put("Notes", "");
					hashMap.put("LocID", locID);
					hashMap.put("Country", country);
					hashMap.put("State", state);
					hashMap.put("City", city);
					hashMap.put("Area", area);
					hashMap.put("FunctionType", "4");
					hashMap.put("Latitude", rlatitude);
					hashMap.put("Longitude", rlongitude);
					hashMap.put("EZEID", ezeid);
					hashMap.put("ToEZEID", to_ezeid);
					hashMap.put("ContactInfo", contactInfo);
					hashMap.put("FolderRuleID", folderRule);
					hashMap.put("Duration", duration);
					hashMap.put("DurationScales", durationScales);
					hashMap.put("DeliveryAddress", addressBox.getText()
							.toString());
					hashMap.put("NextAction", nextAction);
					hashMap.put("NextActionDateTime", nextActionkDateTime);
					hashMap.put("ItemsList", "[]");

					// Log.e("TAG", "hashMap items: " + hashMap.toString());

					SaveTransactions(hashMap);
				}

			} else if (isItemsEditAgain == true
					&& !messageID.equalsIgnoreCase("")) {

				/* EDIT UPDATE MESSAGE */

				String status = "0";
				if (isEdited == true) {
					status = edit_statusID;
				} else {
					status = bundle.getString("StatusID");
				}

				String folderRule = "0";
				if (isEdited == true) {
					folderRule = edit_floderID;
				} else {
					folderRule = bundle.getString("FolderID");
				}

				String nextAction = "0";
				if (isEdited == true) {
					nextAction = edit_nextActionID;
				} else {
					nextAction = bundle.getString("ActionID");
				}

				String taskDateTime = NextActionUpDateUTC(bundle
						.getString("TaskDateTime"));
				String nextactionDateTime = NextActionUpDateUTC(bundle
						.getString("Next_action_date"));

				if (!item_update_notes.getText().toString().isEmpty()) {
					update_messageText = item_update_notes.getText().toString();

					HashMap<String, String> hashMap = new HashMap<String, String>();
					hashMap.put("Token", token);
					hashMap.put("TID", bundle.getString("TID"));
					hashMap.put("MessageText", update_messageText);
					hashMap.put("item_list_type", "0");
					hashMap.put("Status", status);
					hashMap.put("TaskDateTime", taskDateTime);
					hashMap.put("Notes", "");
					hashMap.put("LocID", bundle.getString("LocID"));
					hashMap.put("Country", "");
					hashMap.put("State", "");
					hashMap.put("City", "");
					hashMap.put("Area", "");
					hashMap.put("FunctionType", "4");
					hashMap.put("Latitude", "0");
					hashMap.put("Longitude", "0");
					hashMap.put("EZEID", bundle.getString("RequesterEZEID"));
					hashMap.put("ToEZEID", bundle.getString("EZEID"));
					hashMap.put("ContactInfo", bundle.getString("ContactInfo"));
					hashMap.put("FolderRuleID", folderRule);
					hashMap.put("Duration", "0");
					hashMap.put("DurationScales", "0");
					hashMap.put("DeliveryAddress",
							bundle.getString("DeliveryAddress"));
					hashMap.put("NextAction", nextAction);
					hashMap.put("NextActionDateTime", nextactionDateTime);
					hashMap.put("ItemsList", "[]");
					// Log.e("TAG", "update hashMap items: " +
					// hashMap.toString());
					SaveTransactions(hashMap);

				} else {
					EzeidUtil.showToast(HREnquiry.this, "Please enter notes");
				}

			} else {
				GetRequester();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void SaveTransactions(HashMap<String, String> hashMap) {
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
				Request.Method.POST, EZEIDUrlManager.getAPIUrl()
						+ "ewtSaveTranscation", new JSONObject(hashMap),
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						try {
							if (null != response) {
								String result = response
										.getString("IsSuccessfull");
								if (result.equalsIgnoreCase("true"))
									EzeidUtil.showToast(HREnquiry.this,
											"Transaction saved Successfully");
								NavToTrList();
							} else {
								EzeidUtil.showToast(HREnquiry.this,
										"Transaction saved failed");
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
						// Log.e(TAG, "hashMap items: " + response.toString());

					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						if (error instanceof TimeoutError) {
							EzeidUtil.showToast(HREnquiry.this,
									"Connection timeout. Please try again");
						} else if (error instanceof ServerError
								|| error instanceof AuthFailureError) {
							EzeidUtil
									.showToast(HREnquiry.this,
											"Unable to connect server. Please try later");
						} else if (error instanceof NetworkError
								|| error instanceof NoConnectionError) {
							EzeidUtil.showToast(HREnquiry.this,
									"Network is unreachable");
						} else {
							EzeidUtil.showToast(HREnquiry.this, "Send failed");
						}
					}
				});
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);
	}

	private void NavToTrList() {
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				finish();
			}
		}, 1000);
	}
}
