package com.ezeone.manager.salesenquiry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.SplashScreen;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.listeners.RecyclerItemClickListener;
import com.ezeone.manager.adapter.MainAdapter;
import com.ezeone.manager.backend.ItemDBhelper;
import com.ezeone.manager.backend.SaleItemModel;
import com.ezeone.manager.pojo.AddedItems;
import com.ezeone.manager.pojo.TrItems;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.animations.FadeInUpAnimator;
import com.ezeonelib.animations.SlideInUpAnimator;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.date.time.RadialPickerLayout;
import com.ezeonelib.date.time.TimePickerDialog;
import com.ezeonelib.dialog.MaterialDialog;
import com.ezeonelib.floatingaction.ButtonFloatSmall;

public class EzeidAddItems extends AppCompatActivity implements
        OnClickListener, OnItemSelectedListener,
        TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private static final String TAG = EzeidAddItems.class.getSimpleName();
    private Toolbar toolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private AppCompatEditText item_search_filter;
    private Button submit_order, cancel_order;
    private MainAdapter adapter;
    private List<TrItems> items;
    private EzeidPreferenceHelper ezeidPreferenceHelper;
    private int sessionRetry = 0;
    private EzeidLoadingProgress ezeidLoadingProgress;
    private Handler handlerDialog;
    private Runnable runnableDialog;
    private RelativeLayout contact_info;
    private ArrayList<HashMap<String, String>> status_array, action_array,
            folder_array;
    private EditText salesRequester, addressBox, nextActionDateBox, notesBox;
    private Bundle bundle;
    private ButtonFloatSmall editActionDate, checkEzeidAvailability;
    private Button requseterOk, requseterCancel;
    private Spinner statusFilter, actionFilter, folderFilter, locationFilter;
    private SimpleAdapter statusAdapter, actionAdapter, folderAdapter,
            locationAdapter;
    private LinearLayout folderLayout, checkEzeidStatusLayout;
    private TextView checkEzeidStatusText, checkEzeidStatusLine,
            panel_item_selected, contact_info_text;
    private String GET_ITEMS_URL = "";
    private TextView switchLoc;
    private RadioButton custEzeid, custOther;
    private RadioGroup userType;
    private ProgressBar contactInfoProgress;
    private String firstName = "", lastName = "", mobileNumber = "",
            latitude = "", longitude = "", dateStr = "", timeStr = "",
            prefDateTimeStr = "", country = "", state = "", city = "",
            area = "", rlatitude = "0", rlongitude = "0", folderRuleID = "",
            statusID = "", nextActionID = "", messageID = "",
            taskDateTimeStr = "", messageText = "", contactInfo = "",
            update_statusID = "", update_nextActionID = "",
            update_folderRuleID = "", update_locID = "",
            update_messageText = "", update_taskDateTimeStr = "",
            update_nextActionDateStr = "", update_contactInfo = "",
            update_ezeid = "", update_ezistingNotes = "", req_ezeid = "",
            deliveryAddress = "", salesItemListType = "", edit_statusID = "",
            edit_nextActionID = "", edit_floderID = "";
    private String edit_itemid = "", edit_tid = "", edit_item_name = "",
            edit_rate = "", edit_qty = "", edit_option = "",
            item_durations = "", message_id = "";
    private Bitmap edit_bitmap;
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private boolean isSessionDialogShown = false, isRequesterAdded = false,
            isItemsEditAgain = false;
    private boolean isEdited = false;
    private int status_count = 0, action_count = 0, folder_count = 0,
            location_count = 0;

    private ItemDBhelper dBhelper;
    private Dialog overlayInfo;

    private ImageView noresults_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sales_items_only);

        toolbar = (Toolbar) findViewById(R.id.toolbar_addItem);
        toolbar.setTitle(Html.fromHtml("SALES ORDER"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(
                R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dBhelper.open();
                dBhelper.DeleteAllItems();
                dBhelper.close();
                EzeidAddItems.this.finish();
            }
        });

        InitComponents();
        SwipeOverlay();
        ActivityStatus();
        // if (isSessionDialogShown == false) {
        // CheckSessionExpiredStatus();
        // }

        if (savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog) getSupportFragmentManager()
                    .findFragmentByTag(DATEPICKER_TAG);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
            }

            TimePickerDialog tpd = (TimePickerDialog) getSupportFragmentManager()
                    .findFragmentByTag(TIMEPICKER_TAG);
            if (tpd != null) {
                tpd.setOnTimeSetListener(this);
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (EzeidUtil.isConnectedToInternet(getApplicationContext()) == false) {

            EzeidUtil.showToast(EzeidAddItems.this, getResources()
                    .getString(R.string.internet_warning));
        }
    }


    private void SwipeOverlay() {

        String overlay = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("OVERLAY");
        if (!overlay.equalsIgnoreCase("false")) {

            overlayInfo = new Dialog(EzeidAddItems.this);
            overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
            overlayInfo.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            overlayInfo.getWindow().setFlags(
                    WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
            overlayInfo.setContentView(R.layout.ezeid_add_sales_overlay_view);
            overlayInfo.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            if (overlayInfo != null) {
                overlayInfo.show();
            }
            TextView overlayText = (TextView) overlayInfo
                    .findViewById(R.id.overlayText);

            overlayText.setText("Tap to select or unselect.");

            overlayText.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    overlayInfo.cancel();
                    // overlayInfo.dismiss();
                    ezeidPreferenceHelper.SaveValueToSharedPrefs("OVERLAY",
                            "false");
                }
            });

        }
    }

    private void InitComponents() {
        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) findViewById(R.id.filter_items_list);
        mRecyclerView.setHasFixedSize(true);
        // The number of Columns
        mLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new FadeInUpAnimator());
        mRecyclerView.getItemAnimator().setAddDuration(300);
        mRecyclerView.getItemAnimator().setRemoveDuration(200);

        ezeidPreferenceHelper = new EzeidPreferenceHelper(EzeidAddItems.this);
        dBhelper = new ItemDBhelper(EzeidAddItems.this);

        item_search_filter = (AppCompatEditText) findViewById(R.id.item_search_filter);
        submit_order = (Button) findViewById(R.id.submit_order);
        cancel_order = (Button) findViewById(R.id.cancel_order);

        submit_order.setOnClickListener(this);
        cancel_order.setOnClickListener(this);
        panel_item_selected = (TextView) findViewById(R.id.panel_item_selected);
        contact_info_text = (TextView) findViewById(R.id.contact_info_text);
        contact_info = (RelativeLayout) findViewById(R.id.contact_info);
        contact_info.setOnClickListener(this);
        panel_item_selected.setText("0 Items Selected");
        noresults_icon = (ImageView) findViewById(R.id.noresults_icon);
        noresults_icon.setVisibility(View.INVISIBLE);
        item_search_filter.setHintTextColor(getResources().getColor(
                R.color.met_green));
        mRecyclerView.setItemAnimator(new SlideInUpAnimator());

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(
                EzeidAddItems.this, mRecyclerView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // Log.i("TAG", "Item clicked");
                        adapter.remove(position);
                        EzeidGlobal.LAST_SELECTED_POS = position;

                        TextView itemid = (TextView) view
                                .findViewById(R.id.item_id_);
                        TextView tid = (TextView) view
                                .findViewById(R.id.item_tid);
                        TextView qty = (TextView) view
                                .findViewById(R.id.item_qty);
                        TextView rate = (TextView) view
                                .findViewById(R.id.item_rate);
                        TextView amt = (TextView) view
                                .findViewById(R.id.item_amt);
                        TextView item_name = (TextView) view
                                .findViewById(R.id.item_name);
                        TextView item_status_edit = (TextView) view
                                .findViewById(R.id.item_status_);
                        TextView item_duration_edit = (TextView) view
                                .findViewById(R.id.item_durations);
                        TextView item_message_id = (TextView) view
                                .findViewById(R.id.item_message_id);
                        ImageView item_img = (ImageView) view
                                .findViewById(R.id.item_img);

                        BitmapDrawable drawable = (BitmapDrawable) item_img
                                .getDrawable();
                        edit_bitmap = drawable.getBitmap();

                        edit_itemid = itemid.getText().toString();
                        edit_tid = tid.getText().toString();
                        edit_item_name = item_name.getText().toString();
                        edit_qty = qty.getText().toString();
                        edit_rate = rate.getText().toString();
                        String edit_amt = amt.getText().toString();
                        String edit_state = item_status_edit.getText()
                                .toString();
                        item_durations = item_duration_edit.getText()
                                .toString();
                        message_id = item_message_id.getText().toString();

                        if (!salesItemListType.equalsIgnoreCase("1")
                                || !salesItemListType.equalsIgnoreCase("2")) {
                            if (isItemsEditAgain == true
                                    && !messageID.equalsIgnoreCase("")) {
                                edit_option = "edit";
                            } else {

                                if (edit_state.equalsIgnoreCase("new")) {
                                    edit_option = "edit";
                                } else {
                                    edit_option = "new";
                                }
                            }
                        }

                        SelectItem(edit_itemid, edit_tid, edit_item_name,
                                edit_bitmap, edit_rate, edit_rate, edit_qty,
                                edit_option, item_durations, message_id);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        // ...
                        // Log.i("TAG", "onItemLongClick");
                    }
                }));
        item_search_filter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
                if (s.length() != 0) {
                    BindFilterTrItems(s.toString());
                } else {
                    BindTrItems();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        salesItemListType = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("SalesItemListType");

    }

    public void SelectItem(String edit_itemid, String edit_tid,
                           String edit_item_name, Bitmap bitmap, String edit_rate,
                           String edit_amt, String edit_qty, String status,
                           String item_durations, String item_message_id) {
        // String update_item = "itemid: " + edit_itemid + ", tid:" + edit_tid
        // + ", name:" + edit_item_name + ", img:" + bitmap + ", rate:"
        // + edit_rate + ", amt:" + edit_amt + ",qty:" + edit_qty
        // + " status:" + status + ", item_durations:" + item_durations
        // + ", item_message_id:" + item_message_id;
        // Log.i(TAG, "SelectItem() edit:" + update_item);
        UpdateItem(edit_itemid, edit_tid, edit_item_name, bitmap, edit_rate,
                edit_amt, edit_qty, status, item_durations, item_message_id);

    }

    private void UpdateTotal() {
        // panel_item_selected
        dBhelper.open();
        int items = dBhelper.GetTotalAddedItems();
        panel_item_selected.setText("" + items + " Items Selected");
        dBhelper.close();
    }

    private void BindTrItems() {
        // TODO Auto-generated method stub
        items = new ArrayList<TrItems>();
        dBhelper.open();
        List<SaleItemModel> models = dBhelper.GetAllItems();
        if (models.size() > 0) {

            for (SaleItemModel model : models) {

                TrItems trItems = new TrItems();
                trItems.item_id = model.getItem_id();
                trItems.item_tid = model.getItem_tid();
                trItems.item_name = model.getItem_name();
                trItems.item_rate = model.getItem_rate();
                trItems.item_amt = model.getItem_amt();
                trItems.item_qty = model.getItem_qty();
                trItems.item_status = model.getItem_status();
                trItems.item_img = model.getItem_img();
                trItems.item_durations = model.getItem_durations();
                trItems.item_message_id = model.getItem_message_id();
                items.add(trItems);
            }
            if (items.size() > 0) {
                // Log.d("Result: ", "BindTrItems(): " + items.toString());
                mRecyclerView.setVisibility(View.VISIBLE);
                adapter = new MainAdapter(EzeidAddItems.this, items);
                mRecyclerView.setAdapter(adapter);

            }
        } else {
            mRecyclerView.setVisibility(View.INVISIBLE);
            EzeidUtil.showToast(EzeidAddItems.this, "No items found!");
            noresults_icon.setVisibility(View.VISIBLE);
        }
        dBhelper.close();

    }

    private JSONArray BindAddedTrItems() throws JSONException {

        JSONObject jsonObject = null;
        JSONArray jsonArray = new JSONArray();
        dBhelper.open();
        List<AddedItems> addedItems = dBhelper.GetAllAddedItems();
        // ArrayList<HashMap<String, String>> titleList = EzeidGlobal.titleList;
        // if (titleList.size() != 0) {
        // salesItemListType = titleList.get(0).get("SalesItemListType");
        // }
        if (addedItems.size() > 0) {
            for (AddedItems items : addedItems) {

                jsonObject = new JSONObject();

                if (isItemsEditAgain == true && !messageID.equalsIgnoreCase("")) {
                    jsonObject.put("TID", items.getItem_tid());
                } else {
                    jsonObject.put("TID", "0");
                }
                jsonObject.put("ItemID", items.getItem_id());
                jsonObject.put("Durations", items.getItem_durations());
                jsonObject.put("Amount", "0");
                jsonObject.put("Qty", "0");
                jsonObject.put("Rate", "0");

                jsonArray.put(jsonObject);
            }

        } else {
            jsonArray = new JSONArray("[]");
        }
        dBhelper.close();

        JSONObject object = new JSONObject();
        object.put("ItemsList", jsonArray);
        return jsonArray;

    }

    private String GetSelectedOrders() {

        String selected_items = "";
        StringBuilder builder = null;
        dBhelper.open();
        List<AddedItems> addedItems = dBhelper.GetAllAddedItems();

        if (addedItems.size() > 0) {
            builder = new StringBuilder();
            for (AddedItems items : addedItems) {
                builder.append(items.getItem_name());
                builder.append(",");
                // builder.append(items.getItem_qty());
                // builder.append(",");
            }

            selected_items = builder.toString();
            if (selected_items.length() > 0
                    && selected_items.charAt(selected_items.length() - 1) == ',') {
                selected_items = selected_items.substring(0,
                        selected_items.length() - 1);
            }
        } else {
            selected_items = "";
        }

        return selected_items;
    }

    private void BindFilterTrItems(String name) {

        items = new ArrayList<TrItems>();
        dBhelper.open();
        List<SaleItemModel> models = dBhelper.GetAllFilteredItems(name);

        if (models.size() > 0) {
            for (SaleItemModel model : models) {

                TrItems trItems = new TrItems();
                trItems.item_id = model.getItem_id();
                trItems.item_tid = model.getItem_tid();
                trItems.item_name = model.getItem_name();
                trItems.item_rate = model.getItem_rate();
                trItems.item_amt = model.getItem_amt();
                trItems.item_qty = model.getItem_qty();
                trItems.item_status = model.getItem_status();
                trItems.item_img = model.getItem_img();
                items.add(trItems);
            }
            if (items.size() > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                adapter = new MainAdapter(EzeidAddItems.this, items);
                mRecyclerView.setAdapter(adapter);
            }

        } else {
            // no items found
            mRecyclerView.setVisibility(View.INVISIBLE);
            EzeidUtil.showToast(EzeidAddItems.this, "No items found!");
        }
        dBhelper.close();

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        dBhelper.open();
        dBhelper.DeleteAllItems();
        dBhelper.close();
        finish();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.submit_order:
            /*
			 * if (isRequesterAdded == false) { contact_info_text
			 * .setText("Contact Information is not selected"); expand(); } else
			 * { collapse(); SaveItems(); }
			 */

                if (ezeidPreferenceHelper.GetValueFromSharedPrefs(
                        "ITEM_TO_ADD_USER").equalsIgnoreCase("business_mgr")) {
                    SaveItems();
                } else {
                    SaveExternalOrder();
                }

                break;

            case R.id.cancel_order:
                dBhelper.open();
                dBhelper.DeleteAllItems();
                dBhelper.close();
                finish();
                break;

            case R.id.itemAdd:
                // AddSelectedItems();
                break;

            case R.id.saveSaleItemBtn:
                // GetRequesterDetails();
                // SaveItems();
                break;

            case R.id.nextActionEdit:
                SetCalendar();
                GetDatePicker();
                break;

            case R.id.contact_info:
                GetRequester();
                break;

        }
    }

    private void GetRequester() {
        // isItemsEditAgain = false;
        if (ezeidPreferenceHelper.GetValueFromSharedPrefs("ITEM_TO_ADD_USER")
                .equalsIgnoreCase("business_mgr")) {
            // if (EzeidGlobal.EZEID_TYPE.equalsIgnoreCase("1")) {
            // GetRequesterDetails();
            // } else {
            ShowEzeidDialog("Please wait!");
            BindActionStatusFilters();
            BindNextActionFilters();
            BindFolderFilters();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    DismissEzeidDialog();
                    GetRequesterDetails();
                    if (isItemsEditAgain == true
                            && !messageID.equalsIgnoreCase("")) {
                        if (null != salesRequester) {
                            if (bundle.getString("EZEID").isEmpty()
                                    || bundle.getString("EZEID") == null
                                    || bundle.getString("EZEID")
                                    .equalsIgnoreCase("null")) {
                                salesRequester.setText("");
                            } else {
                                salesRequester.setText(bundle
                                        .getString("EZEID"));
                                GetPrimaryDetails(bundle.getString("EZEID"));
                            }
                        }
                    }
                }
            }, 3000);
            // }
        } else {
            // GetPrimaryDetails(ezeidPreferenceHelper
            // .GetValueFromSharedPrefs("EZEID"));
            isRequesterAdded = true;

            GetConsumerDetails();
        }
    }

    private void GetConsumerDetails() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(
                R.layout.ezeid_add_requester_info_layout_external,
                (ViewGroup) findViewById(R.id.requester_external));
        final Dialog dialog = new Dialog(EzeidAddItems.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialoglayout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        locationFilter = (Spinner) dialog.findViewById(R.id.locFilter);
        LinearLayout reqphoneLayout = (LinearLayout) dialog
                .findViewById(R.id.reqphoneLayout);
        TextView ezeids = (TextView) dialog.findViewById(R.id.contact_ezeid);
        TextView reqName = (TextView) dialog.findViewById(R.id.reqName);
        TextView reqMobile = (TextView) dialog.findViewById(R.id.reqMobile);
        TextView reqphone = (TextView) dialog.findViewById(R.id.reqphone);
        addressBox = (EditText) dialog.findViewById(R.id.addressBox);
        notesBox = (EditText) dialog.findViewById(R.id.notesBox);
        requseterOk = (Button) dialog.findViewById(R.id.requseterOk);
        requseterCancel = (Button) dialog.findViewById(R.id.requseterCancel);
        ezeids.setText(ezeidPreferenceHelper.GetValueFromSharedPrefs("EZEID"));
        reqName.setText(ezeidPreferenceHelper
                .GetValueFromSharedPrefs("USER_NAME"));
        reqMobile.setText(ezeidPreferenceHelper
                .GetValueFromSharedPrefs("USER_MOBILE"));
        if (!ezeidPreferenceHelper.GetValueFromSharedPrefs("USER_PHONE")
                .equalsIgnoreCase("")) {
            reqphone.setText(ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("USER_PHONE"));
        } else {
            reqphoneLayout.setVisibility(View.GONE);
        }

        if (locationAdapter != null) {
            locationFilter.setAdapter(locationAdapter);
        }
        locationFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                rlatitude = ((TextView) view.findViewById(R.id.lati)).getText()
                        .toString();
                rlongitude = ((TextView) view.findViewById(R.id.longi))
                        .getText().toString();

                String masterTag = ((TextView) view
                        .findViewById(R.id.masterTag)).getText().toString();
                if (masterTag.equalsIgnoreCase("< Other >")) {
                    addressBox.setVisibility(View.VISIBLE);
                } else {
                    addressBox.setVisibility(View.GONE);
                    if (!rlatitude.equalsIgnoreCase("")
                            && !rlongitude.equalsIgnoreCase("")) {
                        GetAddress(rlatitude, rlongitude);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });
        requseterCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
        requseterOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();
                isRequesterAdded = true;
                contact_info_text.setText(Html.fromHtml(getResources()
                        .getString(R.string.customer_info_msg)));

            }
        });
        dialog.show();
    }

    private void GetRequesterDetails() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(
                R.layout.ezeid_add_requester_info_layout,
                (ViewGroup) findViewById(R.id.requesterRoot));
        final Dialog dialog = new Dialog(EzeidAddItems.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialoglayout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        salesRequester = (EditText) dialog.findViewById(R.id.salesRequesters);
        addressBox = (EditText) dialog.findViewById(R.id.addressBox);

        notesBox = (EditText) dialog.findViewById(R.id.notesBox);
        nextActionDateBox = (EditText) dialog
                .findViewById(R.id.nextActionDateBox);
        nextActionDateBox.setEnabled(false);
        nextActionDateBox.setTextColor(Color.rgb(0, 122, 165));
        editActionDate = (ButtonFloatSmall) dialog
                .findViewById(R.id.nextActionEdit);
        checkEzeidAvailability = (ButtonFloatSmall) dialog
                .findViewById(R.id.checkEzeidAvailability);
        requseterOk = (Button) dialog.findViewById(R.id.requseterOk);
        requseterCancel = (Button) dialog.findViewById(R.id.requseterCancel);
        statusFilter = (Spinner) dialog.findViewById(R.id.statusFilter);
        actionFilter = (Spinner) dialog.findViewById(R.id.actionFilter);
        folderFilter = (Spinner) dialog.findViewById(R.id.folderFilter);
        locationFilter = (Spinner) dialog.findViewById(R.id.locFilter);
        checkEzeidStatusText = (TextView) dialog
                .findViewById(R.id.checkEzeidStatus);
        checkEzeidStatusLine = (TextView) dialog
                .findViewById(R.id.checkEzeidStatusBar);
        // notesLayout = (LinearLayout) dialog.findViewById(R.id.notesLayout);

        folderLayout = (LinearLayout) dialog.findViewById(R.id.folderLayout);
        // reqStatuslayout = (LinearLayout)
        // dialog.findViewById(R.id.statusLayout);
        // reqActionLayout = (LinearLayout)
        // dialog.findViewById(R.id.actionLayout);
        // reqActionDateLayout = (LinearLayout) dialog
        // .findViewById(R.id.actionDateLayout);
        // reqAvailabilityLayout = (LinearLayout) dialog
        // .findViewById(R.id.salesRequesterLayout);
        // userTypeLayout = (LinearLayout) dialog
        // .findViewById(R.id.userTypeLayout);

        // reqBar1 = (TextView) dialog.findViewById(R.id.bar1);
        // reqBar2 = (TextView) dialog.findViewById(R.id.bar2);

        checkEzeidStatusLayout = (LinearLayout) dialog
                .findViewById(R.id.checkEzeidStatusLayout);
        switchLoc = (TextView) dialog.findViewById(R.id.switchCurloc);

        userType = (RadioGroup) dialog.findViewById(R.id.userType);
        custEzeid = (RadioButton) dialog.findViewById(R.id.custEzeid);
        custOther = (RadioButton) dialog.findViewById(R.id.custOther);
        editActionDate.setOnClickListener(this);
        custEzeid.setChecked(true);
        // toggle delivery locations
        // switchLoc.setChecked(true);
        // EzeidUtil.ChangeEditEffect(folderLayout, EzeidAddItems.this);
        // folderLayout.setBackgroundColor(Color.rgb(216, 216, 216));
        // folderFilter.setEnabled(false);
        if (bundle != null) {
            addressBox.setText(bundle.getString("DeliveryAddress"));
            notesBox.setText(bundle.getString("Notes_lbl"));
            nextActionDateBox.setText(bundle.getString("Next_action_date"));
            // statusFilter.setSelection(Integer.parseInt(bundle
            // .getString("StatusID")));
            // actionFilter.setSelection(Integer.parseInt(bundle
            // .getString("ActionID")));
            // folderFilter.setSelection(Integer.parseInt(bundle
            // .getString("FolderID")));

            addressBox.setText(bundle.getString("DeliveryAddress"));
            if (bundle.getString("EZEID").isEmpty()
                    || bundle.getString("EZEID") == null) {
                salesRequester.setText("");
            } else {
                salesRequester.setText(bundle.getString("EZEID"));
                GetPrimaryDetails(bundle.getString("EZEID"));
            }
            requseterOk.setText("OK");

        }
        if (custEzeid.isChecked() == true) {
            checkEzeidAvailability.setVisibility(View.VISIBLE);
        } else {
            checkEzeidAvailability.setVisibility(View.GONE);
        }
        // if (switchLoc.isChecked() == true) {
        //
        // folderLayout.setBackgroundColor(Color.rgb(255, 255, 255));
        // folderFilter.setEnabled(true);
        // } else {
        // EzeidUtil.ChangeEditEffect(folderLayout, EzeidAddItems.this);
        // folderLayout.setBackgroundColor(Color.rgb(216, 216, 216));
        // folderFilter.setEnabled(false);
        // }
        userType.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (custEzeid.isChecked() == true) {
                    salesRequester.setHint("Contact Informations");
                    addressBox.setVisibility(View.GONE);
                    checkEzeidStatusLine.setVisibility(View.VISIBLE);
                    checkEzeidAvailability.setVisibility(View.VISIBLE);
                    checkEzeidStatusLayout.setVisibility(View.VISIBLE);
                } else if (custOther.isChecked()) {
                    salesRequester.setHint("Contact name and phone no");
                    addressBox.setVisibility(View.GONE);
                    checkEzeidStatusLine.setVisibility(View.GONE);
                    checkEzeidAvailability.setVisibility(View.GONE);
                    checkEzeidStatusLayout.setVisibility(View.GONE);
                }
            }
        });

        contactInfoProgress = (ProgressBar) dialog
                .findViewById(R.id.contactInfoProgress);
        contactInfoProgress.getIndeterminateDrawable().setColorFilter(
                Color.rgb(50, 175, 230),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        // BindUserFilter();

        statusFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                edit_statusID = ((TextView) view.findViewById(R.id.masterId))
                        .getText().toString();
                if (status_count == 0) {
                    isEdited = false;
                    status_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });
        actionFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                edit_nextActionID = ((TextView) view
                        .findViewById(R.id.masterId)).getText().toString();
                if (action_count == 0) {
                    isEdited = false;
                    action_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });
        folderFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                edit_floderID = ((TextView) view.findViewById(R.id.folderTID))
                        .getText().toString();
                if (folder_count == 0) {
                    isEdited = false;
                    folder_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });

        locationFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                rlatitude = ((TextView) view.findViewById(R.id.lati)).getText()
                        .toString();
                rlongitude = ((TextView) view.findViewById(R.id.longi))
                        .getText().toString();
                // showToast("locationFilter: location=" + rlatitude + " "
                // + rlongitude);
                String masterTag = ((TextView) view
                        .findViewById(R.id.masterTag)).getText().toString();
                if (masterTag.equalsIgnoreCase("< Other >")) {
                    addressBox.setVisibility(View.VISIBLE);
                } else {
                    addressBox.setVisibility(View.GONE);
                    if (!rlatitude.equalsIgnoreCase("")
                            && !rlongitude.equalsIgnoreCase("")) {
                        GetAddress(rlatitude, rlongitude);
                    }
                }
                if (location_count == 0) {
                    isEdited = false;
                    location_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        editActionDate.setOnClickListener(this);
        // salesRequester.addTextChangedListener(requesterWatcher);

        requseterCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // menu_deliver_persion.getItem(0).setIcon(
                // R.drawable.ic_action_requester_normal);
                contact_info_text.setText(Html.fromHtml(getResources()
                        .getString(R.string.customer_info_warn)));
                isRequesterAdded = false;
            }
        });
        requseterOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // validation here.
                if (salesRequester.getText().toString().equalsIgnoreCase("")) {
                    checkEzeidStatusText
                            .setText("Requester EZEID is mandatory!");
                } else if (nextActionDateBox.getText().toString()
                        .equalsIgnoreCase("")) {
                    showToast("Next action date is mandatory!");
                } else {
                    isRequesterAdded = true;
                    // menu_deliver_persion.getItem(0).setIcon(
                    // R.drawable.ic_action_requester_selected);
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                    dialog.dismiss();
                }
            }
        });

        if (statusAdapter != null) {
            statusFilter.setAdapter(statusAdapter);
            if (bundle != null) {
                SetDefaultStatus();
            }
        } else {
            BindActionStatusFilters();
        }
        if (actionAdapter != null) {
            actionFilter.setAdapter(actionAdapter);
            if (bundle != null) {
                SetDefaultActions();
            }
        } else {
            BindNextActionFilters();
        }
        if (folderAdapter != null) {
            folderFilter.setAdapter(folderAdapter);
            if (bundle != null) {
                SetDefaultFolder();
            }
        } else {
            BindFolderFilters();
        }
        if (locationAdapter != null) {
            locationFilter.setAdapter(locationAdapter);
        }

		/*
		 * String masterID = ezeidPreferenceHelper
		 * .GetValueFromSharedPrefs("MasterIDLog"); if
		 * (masterID.equalsIgnoreCase("0")) { if (locationAdapter != null) {
		 * locationLayout.setVisibility(View.VISIBLE);
		 * locationFilter.setAdapter(locationAdapter); } } else {
		 * locationLayout.setVisibility(View.GONE); }
		 */

        switchLoc.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is switchLoc checked?
                if (((CheckBox) v).isChecked()) {
                    EzeidUtil
                            .ChangeEditEffect(folderLayout, EzeidAddItems.this);
                    folderLayout.setBackgroundColor(Color.rgb(216, 216, 216));
                    folderFilter.setEnabled(false);
                } else {
                    folderLayout.setBackgroundColor(Color.rgb(255, 255, 255));
                    folderFilter.setEnabled(true);
                }

            }
        });
        checkEzeidAvailability.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!salesRequester.getText().toString().equalsIgnoreCase("")) {
                    GetPrimaryDetails(salesRequester.getText().toString());
                }
            }
        });
        // if (EzeidGlobal.EZEID_TYPE.equalsIgnoreCase("2")) {
        // folderLayout.setVisibility(View.VISIBLE);
        // reqStatuslayout.setVisibility(View.VISIBLE);
        // reqActionLayout.setVisibility(View.VISIBLE);
        // reqActionDateLayout.setVisibility(View.VISIBLE);
        // reqAvailabilityLayout.setVisibility(View.VISIBLE);
        // userTypeLayout.setVisibility(View.VISIBLE);
        // reqBar1.setVisibility(View.VISIBLE);
        // reqBar2.setVisibility(View.VISIBLE);
        // } else {
        // folderLayout.setVisibility(View.GONE);
        // reqStatuslayout.setVisibility(View.GONE);
        // reqActionLayout.setVisibility(View.GONE);
        // reqActionDateLayout.setVisibility(View.GONE);
        // reqAvailabilityLayout.setVisibility(View.GONE);
        // userTypeLayout.setVisibility(View.GONE);
        // reqBar1.setVisibility(View.GONE);
        // reqBar2.setVisibility(View.GONE);
        // }

        dialog.show();
    }

    private void SetDefaultStatus() {
        // statusID
        if (status_array != null) {
            if (status_array.size() > 0) {
                int count = 0;
                for (HashMap<String, String> hashMap : status_array) {
                    String status = hashMap.get("TID");
                    if (update_statusID.equalsIgnoreCase(status)) {
                        statusFilter.setSelection(count);
                    } else {
                        count++;
                    }
                }
            }
        }
    }

    private void SetDefaultActions() {
        if (action_array != null) {
            if (action_array.size() > 0) {
                int count = 0;
                for (HashMap<String, String> hashMap : action_array) {
                    String status = hashMap.get("TID");
                    if (update_nextActionID.equalsIgnoreCase(status)) {
                        actionFilter.setSelection(count);
                    } else {
                        count++;
                    }
                }
            }
        }
    }

    private void SetDefaultFolder() {
        // statusID
        if (folder_array != null) {
            if (folder_array.size() > 0) {
                int count = 0;
                for (HashMap<String, String> hashMap : folder_array) {
                    String status = hashMap.get("TID");
                    if (update_folderRuleID.equalsIgnoreCase(status)) {
                        folderFilter.setSelection(count);
                    } else {
                        count++;
                    }
                }
            }
        }
    }

    void showToast(CharSequence msg) {
        Toast.makeText(EzeidAddItems.this, msg, Toast.LENGTH_SHORT).show();
    }

    private void ShowEzeidDialog(String content) {
        ezeidLoadingProgress = new EzeidLoadingProgress(EzeidAddItems.this,
                content);
        handlerDialog = new Handler();
        runnableDialog = new Runnable() {

            @Override
            public void run() {
                if (ezeidLoadingProgress != null) {
                    if (ezeidLoadingProgress.isShowing()) {
                        ezeidLoadingProgress.dismiss();
                    }
                }
            }
        };
        ezeidLoadingProgress.show();
    }

    private void DismissEzeidDialog() {
        handlerDialog.removeCallbacks(runnableDialog);
        if (ezeidLoadingProgress.isShowing()) {
            ezeidLoadingProgress.dismiss();
        }
    }

    protected void GetAddress(String lati, String longi) {

        try {

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    "http://maps.googleapis.com/maps/api/geocode/json?latlng="
                            + lati + "," + longi + "&sensor=true",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // TODO Auto-generated method stub
                            BindAddress(response);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    VolleyLog.e(TAG, "error:" + error);
                    BindAddress(null);
                }
            });

            // JSONObject jsonObj = EzeidUtil
            // .getJSONfromURL("http://maps.googleapis.com/maps/api/geocode/json?latlng="
            // + lati + "," + longi + "&sensor=true");
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void BindAddress(JSONObject jsonObj) {
        // TODO Auto-generated method stub
        String address1 = "";
        String address2 = "";
        String PIN = "";
        // String full_address = "";
        try {
            if (null != jsonObj) {
                String Status = jsonObj.getString("status");
                if (Status.equalsIgnoreCase("OK")) {
                    JSONArray Results = jsonObj.getJSONArray("results");
                    JSONObject zero = Results.getJSONObject(0);
                    JSONArray address_components = zero
                            .getJSONArray("address_components");

                    JSONObject geoObj = zero.getJSONObject("geometry");
                    JSONObject locObj = geoObj.getJSONObject("location");
                    rlatitude = locObj.getString("lat");
                    rlongitude = locObj.getString("lng");

                    for (int i = 0; i < address_components.length(); i++) {
                        JSONObject zero2 = address_components.getJSONObject(i);
                        String long_name = zero2.getString("long_name");
                        JSONArray mtypes = zero2.getJSONArray("types");
                        String Type = mtypes.getString(0);

                        if (TextUtils.isEmpty(long_name) == false
                                || !long_name.equals(null)
                                || long_name.length() > 0 || long_name != "") {
                            if (Type.equalsIgnoreCase("street_number")) {
                                address1 = long_name + " ";
                            } else if (Type.equalsIgnoreCase("route")) {
                                address1 = address1 + long_name;
                            } else if (Type.equalsIgnoreCase("sublocality")) {
                                address2 = long_name;
                            } else if (Type.equalsIgnoreCase("locality")) {
                                // Address2 = Address2 + long_name + ", ";
                                city = long_name;
                            } else if (Type
                                    .equalsIgnoreCase("administrative_area_level_2")) {
                                country = long_name;
                            } else if (Type
                                    .equalsIgnoreCase("administrative_area_level_1")) {
                                state = long_name;
                            } else if (Type.equalsIgnoreCase("country")) {
                                country = long_name;
                            } else if (Type.equalsIgnoreCase("postal_code")) {
                                PIN = long_name;
                            }

                        }

                        // full_address = address1 + "," + address2 + "," + city
                        // + "," + state + "," + country + "," + PIN;
                        // Log.i(TAG, "full_address: " + full_address);
                    }
                }
            } else {
                // Log.i(TAG, "null response");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetPrimaryDetails(String ezeid) {
        try {
            if (!ezeid.equalsIgnoreCase("")) {
                String token = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("Token");
                String url = EZEIDUrlManager.getAPIUrl()
                        + "ewtEZEIDPrimaryDetails?Token=" + token + "&EZEID="
                        + ezeid;
                contactInfoProgress.setVisibility(View.VISIBLE);
                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {

                            @Override
                            public void onResponse(JSONArray response) {
                                PrimaryDetail(response);
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        PrimaryDetail(null);
                    }
                });
                jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void PrimaryDetail(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len > 0) {
                    for (int i = 0; i < len; i++) {

                        JSONObject jsonObject = response.getJSONObject(i);
                        String tID = jsonObject.getString("TID");
                        update_ezeid = jsonObject.getString("EZEID");
                        firstName = jsonObject.getString("FirstName");
                        lastName = jsonObject.getString("LastName");
                        mobileNumber = jsonObject.getString("MobileNumber");
                        latitude = jsonObject.getString("Latitude");
                        longitude = jsonObject.getString("Longitude");
                        salesRequester.setText("" + firstName + " " + lastName
                                + " - " + mobileNumber);

                    }
                    if (!latitude.equalsIgnoreCase("")
                            && !longitude.equalsIgnoreCase("")) {
                        // GetGeoAddress(Double.parseDouble(latitude),
                        // Double.parseDouble(longitude));
                        GetAddress(latitude, longitude);
                    }
                }
                checkEzeidStatusText.setVisibility(View.GONE);
            } else {
                // salesRequester.setHint("Requester");
                // salesRequester.setHint("Contact info");
                checkEzeidStatusText.setText("Invalid EZEID!");
            }
            contactInfoProgress.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CheckSessionExpiredStatus() {
        String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
        if (!token.equalsIgnoreCase("") && sessionRetry < 5) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                    EzeidGlobal.EzeidUrl + "ewtGetLoginCheck?Token=" + token,
                     new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    SessionStatus(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    sessionRetry += 1;
                    if (error instanceof TimeoutError) {
                        SessionTryAgain();
                    } else if (error instanceof ServerError
                            || error instanceof AuthFailureError) {
                        EzeidUtil.showToast(EzeidAddItems.this,
                                "Network is unreachable!");
                    } else if (error instanceof NetworkError
                            || error instanceof NoConnectionError) {
                        SessionTryAgain();
                    } else {
                        SessionTryAgain();
                    }
                }
            });
            req.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
        } else {
            // loader.setVisibility(View.INVISIBLE);
        }
    }

    private void SessionTryAgain() {
        CheckSessionExpiredStatus();
    }

    private void SessionStatus(JSONObject response) {

        if (!response.isNull("IsAvailable")) {
            try {
                String status = response.getString("IsAvailable");
                if (status.equalsIgnoreCase("true")) {
                    ActivityStatus();
                } else {
                    SessionDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void SessionDialog() {
        isSessionDialogShown = true;
        final MaterialDialog materialDialog = new MaterialDialog(this);
        materialDialog
                .setBackgroundResource(R.drawable.abc_cab_background_internal_bg);
        materialDialog.setTitle(R.string.sessionExpired)
                .setMessage("Your session has expired. Please login again!")
                .setPositiveButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        ezeidPreferenceHelper.SaveValueToSharedPrefs("Token",
                                "");
                        startActivity(new Intent(EzeidAddItems.this,
                                SplashScreen.class));
                        EzeidAddItems.this.finish();
                    }
                });
        materialDialog.setCanceledOnTouchOutside(true).show();
    }

    private void ActivityStatus() {
        // GetTrItems();

        // GetSalesItemList("");
        // GetAddItemStatus();
        bundle = getIntent().getExtras();
        BindBundle();
        String masterID = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("MasterIDLog");
        if (masterID.equalsIgnoreCase("0")) {
            BindLocationsFilters();
        } else {
            locationAdapter = null;
        }

        GetEditedItems();
    }

    private void BindBundle() {
        if (bundle != null) {

            messageID = bundle.getString("MessageID");

            if (messageID.equalsIgnoreCase("")) {
                contact_info_text.setText(Html.fromHtml(getResources()
                        .getString(R.string.customer_info_warn)));
                isItemsEditAgain = false;
                isRequesterAdded = false;
            } else {
                contact_info_text.setText(Html.fromHtml(getResources()
                        .getString(R.string.customer_info_msg)));
                isItemsEditAgain = true;
                isRequesterAdded = true;
            }

        } else {
            contact_info_text.setText(Html.fromHtml(getResources().getString(
                    R.string.customer_info_warn)));
        }
    }

    private void GetEditedItems() {
        Intent intent = getIntent();
        if (null != intent) {
            if (intent.getStringExtra("MessageID") != null) {

                messageID = intent.getStringExtra("MessageID");
                String edit = intent.getStringExtra("ITEM_EDIT");
                update_ezistingNotes = intent.getStringExtra("Notes_lbl");
                update_statusID = intent.getStringExtra("StatusID");
                update_nextActionID = intent.getStringExtra("ActionID");
                update_folderRuleID = intent.getStringExtra("FolderID");
                update_locID = intent.getStringExtra("LocID");
                update_messageText = intent.getStringExtra("Message");
                update_taskDateTimeStr = intent.getStringExtra("TaskDateTime");
                update_nextActionDateStr = intent
                        .getStringExtra("Next_action_date");
                update_contactInfo = intent.getStringExtra("ContactInfo");
                deliveryAddress = intent.getStringExtra("DeliveryAddress");
                update_ezeid = intent.getStringExtra("EZEID");
                req_ezeid = intent.getStringExtra("RequesterEZEID");
                if (edit.equalsIgnoreCase("") || edit.equalsIgnoreCase(null)) {
                    isItemsEditAgain = false;
                    isRequesterAdded = false;
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_warn)));
                } else {
                    isItemsEditAgain = true;
                    isRequesterAdded = true;
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                }

                GetEditedSalesItemList(messageID);
            } else {
                String get_local_user_type = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("ITEM_TO_ADD_USER");
                String token = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("Token");
                String bus_ezeid = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("BusinessEZEID");
                if (get_local_user_type.equalsIgnoreCase("business_mgr")) {
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_warn)));
                    GET_ITEMS_URL = EZEIDUrlManager.getAPIUrl()
                            + "ewtGetItemList?Token=" + token
                            + "&FunctionType=0";
                } else {
                    GET_ITEMS_URL = EZEIDUrlManager.getAPIUrl()
                            + "ewtGetItemListForEZEID?Token=" + token
                            + "&FunctionType=0&EZEID=" + bus_ezeid;
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                }

                GetSalesItemList(GET_ITEMS_URL);
                // GetSalesItemList();
                // BindTrItems();
				/*
				 * if (EzeidGlobal.EZEID_TYPE.equalsIgnoreCase("2")) {
				 * BindActionStatusFilters(); BindNextActionFilters();
				 * BindFolderFilters(); }
				 */

            }

        }
    }

    private void BindActionStatusFilters() {

        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetStatusType?Token=" + token + "&MasterID="
                    + masterID + "&FunctionType=0";
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindStatus(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindStatus(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void BindLocationsFilters() {

        try {

            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetLocationList?Token=" + token;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindLocations(response);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindLocations(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void BindNextActionFilters() {

        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetActionType?Token=" + token + "&MasterID="
                    + masterID + "&FunctionType=0";

            // String url = "http://10.0.100.199:3001/ewtGetActionType?Token="
            // + token + "&MasterID=" + masterID + "&FunctionType=1";
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindActions(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindActions(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void BindActions(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    action_array = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String masterID = jsonObject.getString("MasterID");
                        String actionTitle = jsonObject
                                .getString("ActionTitle");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("MasterID", masterID);
                        map.put("ActionTitle", actionTitle);
                        action_array.add(map);

                        actionAdapter = new SimpleAdapter(
                                EzeidAddItems.this,
                                action_array,
                                R.layout.ezeid_tagid_row,
                                new String[]{"TID", "MasterID", "ActionTitle"},
                                new int[]{R.id.masterId,
                                        R.id.masterPercentage, R.id.masterTag});
                        // actionFilter.setAdapter(actionAdapter);
                    }
                }
            } else {
                action_array = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("TID", "0");
                map.put("MasterID", "0");
                map.put("ActionTitle", "No next actions added");
                action_array.add(map);

                actionAdapter = new SimpleAdapter(EzeidAddItems.this,
                        action_array, R.layout.ezeid_tagid_row, new String[]{
                        "TID", "MasterID", "ActionTitle"}, new int[]{
                        R.id.masterId, R.id.masterPercentage,
                        R.id.masterTag});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindLocations(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String masterID = jsonObject.getString("MasterID");
                        String locTitle = jsonObject.getString("LocTitle");
                        String latitude = jsonObject.getString("Latitude");
                        String longitude = jsonObject.getString("Longitude");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("MasterID", masterID);
                        map.put("LocTitle", locTitle);
                        map.put("Latitude", latitude);
                        map.put("Longitude", longitude);
                        arrayList.add(map);

                        // actionFilter.setAdapter(actionAdapter);
                    }

                    HashMap<String, String> map1 = new HashMap<String, String>();
                    map1.put("TID", "0");
                    map1.put("MasterID", "0");
                    map1.put("LocTitle", "Current Location");
                    map1.put("Latitude", ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Latitude"));
                    map1.put("Longitude", ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Longitude"));
                    arrayList.add(map1);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("TID", "0");
                    map.put("MasterID", "0");
                    map.put("LocTitle", "< Other >");
                    map.put("Latitude", "");
                    map.put("Longitude", "");
                    arrayList.add(map);

                    locationAdapter = new SimpleAdapter(EzeidAddItems.this,
                            arrayList, R.layout.ezeid_tagid_row, new String[]{
                            "TID", "MasterID", "LocTitle", "Latitude",
                            "Longitude"}, new int[]{R.id.masterId,
                            R.id.masterPercentage, R.id.masterTag,
                            R.id.lati, R.id.longi});
                }
            } else {
                ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("TID", "0");
                map.put("MasterID", "0");
                map.put("LocTitle", "No locations added");
                map.put("Latitude", "");
                map.put("Longitude", "");
                arrayList.add(map);

                locationAdapter = new SimpleAdapter(EzeidAddItems.this,
                        arrayList, R.layout.ezeid_tagid_row, new String[]{
                        "TID", "MasterID", "LocTitle", "Latitude",
                        "Longitude"}, new int[]{R.id.masterId,
                        R.id.masterPercentage, R.id.masterTag,
                        R.id.lati, R.id.longi});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindStatus(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    status_array = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String statusTitle = jsonObject
                                .getString("StatusTitle");
                        String progressPercent = jsonObject
                                .getString("ProgressPercent");
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("StatusTitle", statusTitle);
                        map.put("ProgressPercent", progressPercent);
                        status_array.add(map);

                        statusAdapter = new SimpleAdapter(EzeidAddItems.this,
                                status_array, R.layout.ezeid_tagid_row,
                                new String[]{"TID", "StatusTitle",
                                        "ProgressPercent"}, new int[]{
                                R.id.masterId, R.id.masterTag,
                                R.id.masterPercentage});
                        // statusFilter.setAdapter(statusAdapter);
                    }
                }
            } else {
                status_array = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("TID", "0");
                map.put("StatusTitle", "No stages added");
                map.put("ProgressPercent", "");
                status_array.add(map);

                statusAdapter = new SimpleAdapter(EzeidAddItems.this,
                        status_array, R.layout.ezeid_tagid_row, new String[]{
                        "TID", "StatusTitle", "ProgressPercent"},
                        new int[]{R.id.masterId, R.id.masterTag,
                                R.id.masterPercentage});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BindFolderFilters() {
        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetFolderList?Token=" + token + "&MasterID="
                    + masterID + "&FunctionType=0";
            // String url = "http://10.0.100.199:3001/ewtGetFolderList?Token="
            // + token + "&MasterID=" + masterID + "&FunctionType=1";
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindFolders(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindFolders(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindFolders(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    folder_array = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String masterID = jsonObject.getString("MasterID");
                        String folderTitle = jsonObject
                                .getString("FolderTitle");
                        String ruleFunction = jsonObject
                                .getString("RuleFunction");
                        String ruleType = jsonObject.getString("RuleType");
                        String countryIDs = jsonObject.getString("CountryIDs");
                        String matchAdminLevel = jsonObject
                                .getString("MatchAdminLevel");
                        String mappedNames = jsonObject
                                .getString("MappedNames");
                        String latitude = jsonObject.getString("Latitude");
                        String longitude = jsonObject.getString("Longitude");

                        String proximity = jsonObject.getString("Proximity");
                        String defaultFolder = jsonObject
                                .getString("DefaultFolder");
                        String folderStatus = jsonObject
                                .getString("FolderStatus");
                        String seqNoFrefix = jsonObject
                                .getString("SeqNoFrefix");
                        String runningSeqNo = jsonObject
                                .getString("RunningSeqNo");
                        String createdDate = jsonObject
                                .getString("CreatedDate");
                        String lUDate = jsonObject.getString("LUDate");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("MasterID", masterID);
                        map.put("FolderTitle", folderTitle);
                        map.put("RuleFunction", ruleFunction);
                        map.put("RuleType", ruleType);
                        map.put("CountryIDs", countryIDs);
                        map.put("MatchAdminLevel", matchAdminLevel);
                        map.put("MappedNames", mappedNames);
                        map.put("Latitude", latitude);
                        map.put("Longitude", longitude);
                        map.put("Proximity", proximity);
                        map.put("DefaultFolder", defaultFolder);
                        map.put("FolderStatus", folderStatus);
                        map.put("SeqNoFrefix", seqNoFrefix);
                        map.put("RunningSeqNo", runningSeqNo);
                        map.put("CreatedDate", createdDate);
                        map.put("LUDate", lUDate);
                        folder_array.add(map);

                        folderAdapter = new SimpleAdapter(EzeidAddItems.this,
                                folder_array, R.layout.ezeid_folder_row,
                                new String[]{"TID", "MasterID",
                                        "FolderTitle", "RuleFunction",
                                        "RuleType", "CountryIDs",
                                        "MatchAdminLevel", "MappedNames",
                                        "Latitude", "Longitude", "Proximity",
                                        "DefaultFolder", "FolderStatus",
                                        "SeqNoFrefix", "RunningSeqNo",
                                        "CreatedDate", "LUDate"}, new int[]{
                                R.id.folderTID, R.id.masterID,
                                R.id.folderTitle, R.id.ruleFunction,
                                R.id.ruleType, R.id.countryIDs,
                                R.id.matchAdminLevel, R.id.mappedNames,
                                R.id.latitude, R.id.longitude,
                                R.id.proximity, R.id.defaultFolder,
                                R.id.folderStatus, R.id.seqNoFrefix,
                                R.id.runningSeqNo, R.id.createdDate,
                                R.id.lUDate});
                        // folderFilter.setAdapter(folderAdapter);
                    }
                }
            } else {
                folder_array = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map_ = new HashMap<String, String>();
                map_.put("TID", "");
                map_.put("MasterID", "");
                map_.put("FolderTitle", "No folder rule added");
                map_.put("RuleFunction", "");
                map_.put("RuleType", "");
                map_.put("CountryIDs", "");
                map_.put("MatchAdminLevel", "");
                map_.put("MappedNames", "");
                map_.put("Latitude", "");
                map_.put("Longitude", "");
                map_.put("Proximity", "");
                map_.put("DefaultFolder", "");
                map_.put("FolderStatus", "");
                map_.put("SeqNoFrefix", "");
                map_.put("RunningSeqNo", "");
                map_.put("CreatedDate", "");
                map_.put("LUDate", "");
                folder_array.add(map_);
                folderAdapter = new SimpleAdapter(EzeidAddItems.this,
                        folder_array, R.layout.ezeid_folder_row, new String[]{
                        "TID", "MasterID", "FolderTitle",
                        "RuleFunction", "RuleType", "CountryIDs",
                        "MatchAdminLevel", "MappedNames", "Latitude",
                        "Longitude", "Proximity", "DefaultFolder",
                        "FolderStatus", "SeqNoFrefix", "RunningSeqNo",
                        "CreatedDate", "LUDate"}, new int[]{
                        R.id.folderTID, R.id.masterID,
                        R.id.folderTitle, R.id.ruleFunction,
                        R.id.ruleType, R.id.countryIDs,
                        R.id.matchAdminLevel, R.id.mappedNames,
                        R.id.latitude, R.id.longitude, R.id.proximity,
                        R.id.defaultFolder, R.id.folderStatus,
                        R.id.seqNoFrefix, R.id.runningSeqNo,
                        R.id.createdDate, R.id.lUDate});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        String tag = null;
        switch (parent.getId()) {

            case R.id.folderFilter:
                folderRuleID = ((TextView) view.findViewById(R.id.masterID))
                        .getText().toString();
                tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
                        .toString();
                String percentage = ((TextView) view
                        .findViewById(R.id.masterPercentage)).getText().toString();
                // showToast("statusFilter: masterId=" + folderRuleID + ", tag= "
                // + tag + ", percentage=" + percentage);
                break;

            case R.id.statusFilter:
                statusID = ((TextView) view.findViewById(R.id.masterId)).getText()
                        .toString();
                // tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
                // .toString();
                // showToast("statusFilter: masterId=" + statusID + ", tag= " +
                // tag);
                break;

            case R.id.actionFilter:
                nextActionID = ((TextView) view.findViewById(R.id.masterId))
                        .getText().toString();
                // tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
                // .toString();
                // showToast("statusFilter: masterId=" + nextActionID + ", tag= "
                // + tag);
                break;

            default:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // showToast("onNothingSelected()");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        // YYYY-MM-DDThh:mm:ss.sTZD
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss",
                Locale.getDefault());
        // String timeZoneId = TimeZone.getDefault().getID();
        // df.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = df.format(calendar.getTime());
        formattedDate = formattedDate.substring(17, formattedDate.length());

        String hourStr = "", minStr = "";
        int hour = String.valueOf(hourOfDay).trim().length();
        if (hour == 1) {
            hourStr = "0" + hourOfDay;
        } else {
            hourStr = "" + hourOfDay;
        }
        int min = String.valueOf(minute).trim().length();
        if (min == 1) {
            minStr = "0" + minute;
        } else {
            minStr = "" + minute;
        }

        timeStr = hourStr + ":" + minStr + ":" + formattedDate;
        prefDateTimeStr = dateStr + " " + timeStr;
        nextActionDateBox.setVisibility(View.VISIBLE);
        nextActionDateBox.setText("" + PreferedDateNormal(prefDateTimeStr));

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year,
                          int month, int day) {
        dateStr = "";
        timeStr = "";
        nextActionDateBox.setText("");
        month = month + 1;
        int mon = String.valueOf(month).trim().length();
        if (mon == 1) {
            dateStr = "0" + month + "/" + day + "/" + year;
        } else {
            dateStr = month + "/" + day + "/" + year;
        }
        GetTimePicker();
    }

    private void SetCalendar() {
        calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), false);
        timePickerDialog = TimePickerDialog.newInstance(this,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), true, false);
    }

    private void GetDatePicker() {
        datePickerDialog.setVibrate(false);
        datePickerDialog.setYearRange(1985, 2028);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
    }

    private void GetTimePicker() {
        timePickerDialog.setVibrate(false);
        timePickerDialog.setCloseOnSingleTapMinute(false);
        timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
    }

    private String PreferedDateNormal(String preferedDate) {
        Date date;// 02/26/2015 02:00:29
        try {
            SimpleDateFormat normalFormat = new SimpleDateFormat(
                    "MM/dd/yyyy KK:mm:ss", Locale.getDefault());
            date = normalFormat.parse(preferedDate);

            SimpleDateFormat destFormat = new SimpleDateFormat(
                    "MMM dd yyyy HH:mm", Locale.ENGLISH);

            return destFormat.format(date);// Feb 26 2015 02:00 AM
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        dBhelper.open();
        dBhelper.DeleteAllItems();
        dBhelper.close();
    }

    private void UpdateItem(String item_id, String item_tid, String item_name,
                            Bitmap img, String item_rate, String item_amt, String item_qty,
                            String item_status, String item_durations, String item_message_id) {
        try {

            dBhelper.open();
            dBhelper.UpdateItemData(new SaleItemModel(item_id, item_tid,
                    item_name, img, item_rate, item_amt, item_qty, item_status,
                    item_durations, item_message_id), item_id);
            dBhelper.close();

            BindSelectTrItems(item_id);
            UpdateTotal();

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }

    }

    private void BindSelectTrItems(String item_id) {

        items = new ArrayList<TrItems>();
        dBhelper.open();
        List<SaleItemModel> models = dBhelper.GetSelectedItem(item_id);

        if (models.size() > 0) {
            for (SaleItemModel model : models) {

                TrItems items = new TrItems();
                items.item_id = model.getItem_id();
                items.item_tid = model.getItem_tid();
                items.item_name = model.getItem_name();
                items.item_rate = model.getItem_rate();
                items.item_amt = model.getItem_amt();
                items.item_qty = model.getItem_qty();
                items.item_status = model.getItem_status();
                items.item_img = model.getItem_img();

                adapter.add(items, EzeidGlobal.LAST_SELECTED_POS);
            }
        }
        dBhelper.close();
    }

    private void GetSalesItemList(String url) {
        try {
            ShowEzeidDialog("Please wait!");
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindSalesItems(response);
                            DismissEzeidDialog();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    BindSalesItems(null);
                    DismissEzeidDialog();
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BindSalesItems(JSONArray salesItems) {

        try {

            if (null != salesItems) {
                int len = salesItems.length();
                if (len != 0) {

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = salesItems.getJSONObject(i);
                        String item_tid = jsonObject.getString("TID");
                        String item_id = jsonObject.getString("TID");
                        String item_name = jsonObject.getString("ItemName");
                        // String item_desc =
                        // jsonObject.getString("Description");
                        String item_rate = "0";
                        String item_amt = "0";
                        String item_qty = "0";
                        String item_img = jsonObject.getString("Pic");
                        String item_status = "new";
                        String item_durations = jsonObject
                                .getString("Duration");
                        String item_message_id = "0";
                        Bitmap itemBitmap = null;

                        if (!item_img.equalsIgnoreCase("")) {
                            String itemImag = EzeidUtil.GetFileType(item_img);
                            itemBitmap = EzeidUtil.convertBitmap(itemImag);
                        } else {
                            itemBitmap = null;
                        }

                        SaleItemModel saleItemModel = new SaleItemModel(
                                item_id, item_tid, item_name, itemBitmap,
                                item_rate, item_amt, item_qty, item_status,
                                item_durations, item_message_id);

                        String insert_item = "itemid: " + item_id + ", tid:"
                                + item_tid + ", name:" + item_name + ", img:"
                                + itemBitmap.toString() + ", rate:" + item_rate
                                + ", amt:" + item_amt + ",qty:" + item_qty
                                + " status:" + item_status + "item_durations:"
                                + item_durations + ", item_message_id:"
                                + item_message_id;

                        // Log.i(TAG, "insert_item:" + insert_item);

                        dBhelper.open();
                        dBhelper.insertItemDetails(saleItemModel);
                        dBhelper.close();

                    }
                    BindTrItems();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void GetEditedSalesItemList(String msgid) {
        try {
            ShowEzeidDialog("Please wait!");
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            // token = "294f4b0c8c847333b1b8";
            // String url = EZEIDUrlManager.getAPIUrl()
            // + "ewtGetTranscationItems?Token=" + token + "&MessageID="
            // + msgid;
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetTranscationItems?Token=" + token + "&MessageID="
                    + msgid;

            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindEditedSalesItems(response);
                            DismissEzeidDialog();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindEditedSalesItems(null);
                    DismissEzeidDialog();
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindEditedSalesItems(JSONArray response) {
        // TODO Auto-generated method stub
        try {

            if (null != response) {
                int len = response.length();
                if (len != 0) {

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String item_tid = jsonObject.getString("TID");
                        String item_id = jsonObject.getString("ItemID");
                        String item_name = jsonObject.getString("ItemName");
                        String item_rate = jsonObject.getString("Rate");
                        String item_amt = jsonObject.getString("Amount");
                        String item_qty = jsonObject.getString("Qty");
                        String item_img = jsonObject.getString("Pic");
                        String item_status = "edit";
                        String item_durations = jsonObject
                                .getString("Duration");
                        String item_message_id = jsonObject
                                .getString("MessageID");
                        Bitmap itemBitmap = null;

                        if (!item_img.equalsIgnoreCase("")) {
                            String itemImag = EzeidUtil.GetFileType(item_img);
                            itemBitmap = EzeidUtil.convertBitmap(itemImag);
                        } else {
                            itemBitmap = null;
                        }

                        SaleItemModel saleItemModel = new SaleItemModel(
                                item_id, item_tid, item_name, itemBitmap,
                                item_rate, item_amt, item_qty, item_status,
                                item_durations, item_message_id);

                        String insert_item = "itemid: " + item_id + ", tid:"
                                + item_tid + ", name:" + item_name + ", img:"
                                + itemBitmap.toString() + ", rate:" + item_rate
                                + ", amt:" + item_amt + ",qty:" + item_qty
                                + " status:" + item_status + "item_durations:"
                                + item_durations + ", item_message_id:"
                                + item_message_id;

                        // Log.i(TAG, "insert_item:" + insert_item);

                        dBhelper.open();
                        dBhelper.insertItemDetails(saleItemModel);
                        dBhelper.close();

                    }
                    BindTrItems();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private String GetCurrentUTCTime() {
        SimpleDateFormat utcFormat = new SimpleDateFormat(
                "MM/dd/yyyy HH:mm:ss", Locale.getDefault());
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return utcFormat.format(Calendar.getInstance().getTime());
    }

    private String NextActionDateUTC() {
        Date date;// 02/25/2015 03:00:56
        if (prefDateTimeStr.equalsIgnoreCase("")) {
            prefDateTimeStr = nextActionDateBox.getText().toString();
        }
        try {
            SimpleDateFormat utcFormat = new SimpleDateFormat(
                    "MM/dd/yyyy KK:mm:ss", Locale.getDefault());
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = utcFormat.parse(prefDateTimeStr);// Wed Feb 25 08:30:56
            // GMT+05:30 2015
            SimpleDateFormat destFormat = new SimpleDateFormat(
                    "MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
            return destFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String NextActionUpDateUTC(String dateS) {
        Date date;// 02/25/2015 03:00:56

        try {
            // 07 Apr 2015 18:41:44 PM, "dd MMM yyyy KK:mm:ss aa"
            SimpleDateFormat utcFormat = new SimpleDateFormat(
                    "dd MMM yyyy KK:mm:ss aa", Locale.getDefault());
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = utcFormat.parse(dateS);// Wed Feb 25 08:30:56
            // GMT+05:30 2015
            SimpleDateFormat destFormat = new SimpleDateFormat(
                    "MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
            return destFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void SaveExternalOrder() {

        try {
            salesItemListType = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("SalesItemListType");
            String ezeid = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("EZEID");
            String to_ezeid = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("BusinessEZEID");
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            if (isRequesterAdded == true) {
                messageText = GetSelectedOrders();
                taskDateTimeStr = GetCurrentUTCTime();
                String locID = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("LocID");
                contactInfo = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("USER_NAME")
                        + " - "
                        + ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("USER_MOBILE");
                update_ezistingNotes = notesBox.getText().toString();
                JSONArray jsonArray = BindAddedTrItems();
                String itemList = jsonArray.toString();
                if (rlatitude.equalsIgnoreCase("")) {
                    rlatitude = "0";
                }
                if (rlongitude.equalsIgnoreCase("")) {
                    rlongitude = "0";
                }
                if (jsonArray.length() > 0) {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("Token", token);
                    hashMap.put("TID", "0");
                    hashMap.put("MessageText", messageText);
                    hashMap.put("item_list_type", salesItemListType);
                    hashMap.put("Status", "0");
                    hashMap.put("TaskDateTime", taskDateTimeStr);
                    hashMap.put("Notes", update_ezistingNotes);
                    hashMap.put("LocID", locID);
                    hashMap.put("Country", country);
                    hashMap.put("State", state);
                    hashMap.put("City", city);
                    hashMap.put("Area", area);
                    hashMap.put("FunctionType", "0");
                    hashMap.put("Latitude", rlatitude);
                    hashMap.put("Longitude", rlongitude);
                    hashMap.put("EZEID", ezeid);
                    hashMap.put("ToEZEID", to_ezeid);
                    hashMap.put("ContactInfo", contactInfo);
                    hashMap.put("FolderRuleID", "0");
                    hashMap.put("Duration", "0");
                    hashMap.put("DurationScales", "0");
                    hashMap.put("DeliveryAddress", addressBox.getText()
                            .toString());
                    hashMap.put("NextAction", "0");
                    hashMap.put("NextActionDateTime", taskDateTimeStr);
                    hashMap.put("ItemsList", itemList);

                    // Log.i("TAG", "hashMap items: " + hashMap.toString());

                    SaveTransactions(hashMap);
                } else {
                    EzeidUtil.showToast(EzeidAddItems.this,
                            "Please add some orders!");
                }
            } else {
                EzeidUtil.showToast(EzeidAddItems.this,
                        "Customer information is not selected!");
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            EzeidUtil.showToast(EzeidAddItems.this,
                    "Exception during saving transaction");
        }

    }

    private void SaveItems() {

        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            salesItemListType = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("SalesItemListType");
            if (isRequesterAdded == true && isItemsEditAgain == false) {

                String to_ezeid = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("EZEID");

                messageText = GetSelectedOrders();
                taskDateTimeStr = GetCurrentUTCTime();
                String locID = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("LocID");
                if (locID.equalsIgnoreCase("")) {
                    locID = "0";
                }

                contactInfo = salesRequester.getText().toString();
                String notes = notesBox.getText().toString();
                if (rlatitude.equalsIgnoreCase("")) {
                    rlatitude = "0";
                }
                if (rlongitude.equalsIgnoreCase("")) {
                    rlongitude = "0";
                }

                String folderRule = "";
                if (!edit_floderID.equalsIgnoreCase("")) {
                    folderRule = edit_floderID;
                } else {
                    folderRule = "0";
                }
                String status = "";
                if (!edit_statusID.equalsIgnoreCase("")) {
                    status = edit_statusID;//
                } else {
                    status = "0";
                }
                String nextAction = "";
                if (!edit_nextActionID.equalsIgnoreCase("")) {
                    nextAction = edit_nextActionID;//
                } else {
                    nextAction = "0";
                }
                String duration = "0";
                String durationScales = "0";
                String nextActionkDateTime = NextActionDateUTC();

                JSONArray jsonArray = BindAddedTrItems();
                String itemList = jsonArray.toString();

                if (jsonArray.length() > 0) {
                    if (nextAction.equalsIgnoreCase("")) {
                        EzeidUtil.showToast(EzeidAddItems.this,
                                "Next Action status is mandatory!");
                        GetRequester();
                    } else if (status.equalsIgnoreCase("")) {
                        EzeidUtil.showToast(EzeidAddItems.this,
                                "Delivery status is mandatory!");
                        GetRequester();
                    } else if (jsonArray.length() == 0) {
                        EzeidUtil.showToast(EzeidAddItems.this,
                                "Please add some transactions!");
                    } else {

                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("Token", token);
                        hashMap.put("TID", "0");
                        hashMap.put("MessageText", messageText);
                        hashMap.put("item_list_type", salesItemListType);
                        hashMap.put("Status", status);
                        hashMap.put("TaskDateTime", taskDateTimeStr);
                        hashMap.put("Notes", notes);
                        hashMap.put("LocID", locID);
                        hashMap.put("Country", country);
                        hashMap.put("State", state);
                        hashMap.put("City", city);
                        hashMap.put("Area", area);
                        hashMap.put("FunctionType", "0");
                        hashMap.put("Latitude", rlatitude);
                        hashMap.put("Longitude", rlongitude);
                        hashMap.put("EZEID", update_ezeid);
                        hashMap.put("ToEZEID", to_ezeid);
                        hashMap.put("ContactInfo", contactInfo);
                        hashMap.put("FolderRuleID", folderRule);
                        hashMap.put("Duration", duration);
                        hashMap.put("DurationScales", durationScales);
                        hashMap.put("DeliveryAddress", addressBox.getText()
                                .toString());
                        hashMap.put("NextAction", nextAction);
                        hashMap.put("NextActionDateTime", nextActionkDateTime);
                        hashMap.put("ItemsList", itemList);

                        // Log.i("TAG", "hashMap items: " + hashMap.toString());

                        SaveTransactions(hashMap);
                    }
                } else {
                    EzeidUtil.showToast(EzeidAddItems.this,
                            "Please add some transactions!");
                }
            } else if (isItemsEditAgain == true
                    && !messageID.equalsIgnoreCase("")) {

                String taskDateTime = NextActionUpDateUTC(update_taskDateTimeStr);
                String nextactionDateTime = NextActionUpDateUTC(update_nextActionDateStr);

                String status = "0";
                if (isEdited == true) {
                    status = statusID;
                } else {
                    status = update_statusID;
                }

                String folderRule = "0";
                if (isEdited == true) {
                    folderRule = folderRuleID;
                } else {
                    folderRule = update_folderRuleID;
                }

                String nextAction = "0";
                if (isEdited == true) {
                    nextAction = nextActionID;
                } else {
                    nextAction = update_nextActionID;
                }
                update_messageText = GetSelectedOrders();

                JSONArray jsonArray = BindAddedTrItems();
                String itemList = jsonArray.toString();

                if (jsonArray.length() > 0) {

                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("Token", token);
                    hashMap.put("TID", messageID);
                    hashMap.put("MessageText", update_messageText);
                    hashMap.put("item_list_type", salesItemListType);
                    hashMap.put("Status", status);
                    hashMap.put("TaskDateTime", taskDateTime);
                    hashMap.put("Notes", update_ezistingNotes);
                    hashMap.put("LocID", update_locID);
                    hashMap.put("Country", "");
                    hashMap.put("State", "");
                    hashMap.put("City", "");
                    hashMap.put("Area", "");
                    hashMap.put("FunctionType", "0");
                    hashMap.put("Latitude", "0");
                    hashMap.put("Longitude", "0");
                    hashMap.put("EZEID", "");
                    hashMap.put("ToEZEID", req_ezeid);
                    hashMap.put("ContactInfo", update_contactInfo);
                    hashMap.put("FolderRuleID", folderRule);
                    hashMap.put("Duration", "0");
                    hashMap.put("DurationScales", "0");
                    hashMap.put("DeliveryAddress", deliveryAddress);
                    hashMap.put("NextAction", nextAction);
                    hashMap.put("NextActionDateTime", nextactionDateTime);
                    hashMap.put("ItemsList", itemList);

                    // Log.i("TAG", "update hashMap items: " +
                    // hashMap.toString());

                    SaveTransactions(hashMap);
                } else {

                    // cancel confirmations
                    // CancelConfirmations();
                    // Log.i("TAG", "ItemsList is empty!");
                }
            } else {
                GetRequester();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SaveTransactions(HashMap<String, String> hashMap) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, EZEIDUrlManager.getAPIUrl()
                + "ewtSaveTranscation", new JSONObject(hashMap),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // TODO Auto-generated method stub
                        try {
                            if (null != response) {
                                String result = response
                                        .getString("IsSuccessfull");
                                if (result.equalsIgnoreCase("true"))
                                    EzeidUtil.showToast(EzeidAddItems.this,
                                            "Transaction saved Successfully");
                                NavToTrList();
                            } else {
                                EzeidUtil.showToast(EzeidAddItems.this,
                                        "Transaction saved failed");
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        // Log.i(TAG, "hashMap items: " + response.toString());

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                if (error instanceof TimeoutError) {
                    EzeidUtil.showToast(EzeidAddItems.this,
                            "Connection timeout. Please try again");
                } else if (error instanceof ServerError
                        || error instanceof AuthFailureError) {
                    EzeidUtil
                            .showToast(EzeidAddItems.this,
                                    "Unable to connect server. Please try later");
                } else if (error instanceof NetworkError
                        || error instanceof NoConnectionError) {
                    EzeidUtil.showToast(EzeidAddItems.this,
                            "Network is unreachable");
                } else {
                    EzeidUtil.showToast(EzeidAddItems.this,
                            "Send failed");
                }
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);
    }

    private void NavToTrList() {

        dBhelper.open();
        dBhelper.DeleteAllItems();
        dBhelper.close();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
            }
        }, 2000);

    }

}