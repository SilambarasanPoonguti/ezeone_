package com.ezeone.manager.salesenquiry;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.SplashScreen;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.manager.adapter.MainAdapter;
import com.ezeone.manager.backend.ItemDBhelper;
import com.ezeone.manager.backend.SaleItemModel;
import com.ezeone.manager.pojo.TrItems;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.animations.adapter.AlphaInAnimationAdapter;
import com.ezeonelib.animations.adapter.ScaleInAnimationAdapter;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.date.time.RadialPickerLayout;
import com.ezeonelib.date.time.TimePickerDialog;
import com.ezeonelib.dialog.MaterialDialog;
import com.ezeonelib.floatingaction.FloatingActionButton;
import com.ezeonelib.floatingaction.FloatingActionsMenu;

public class SalesTransactionDetail extends AppCompatActivity implements
		OnClickListener,
		TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

	private String TAG = SalesTransactionDetail.class.getSimpleName();
	private Toolbar toolbar;
	private RecyclerView mRecyclerView;
	private EzeidPreferenceHelper ezeidPreferenceHelper;
	private TextView transRequirements;
	private AppCompatEditText nextActionDate;
	private int sessionRetry = 0;
	private TextView requesterName, transdAmount, transRequirementsMore,
			folderLbl, notesEdit, updateUser, updateDate, notes,
			deliveryAddressTxt;
	private String dateStr = "", timeStr = "", prefDateTimeStr = "",
			edit_folderRuleID = "0", folderRuleID = "", statusID = "",
			edit_statusID = "0", edit_nextActionID = "0", nextActionID = "",
			messageID = "", locID = "", editNotes = "", ezistingNotes = "",
			nextActionDateStr = "", taskDateTimeStr, contactInfo = "",
			messageText = "", deliveryAddress = "", salesItemListType = "",
			ezeid = "", req_ezeid = "";
	// private Spinner statusFilter, actionFilter, folderFilter;
	// private Spinner folderFilter;
	// private SimpleAdapter statusAdapter, actionAdapter, folderAdapter;
	private TextView nextActionDateBtn;
	// private LinearLayout folderLayout, statusLayout, nextActionDateLayout;
	RelativeLayout collapse_info_layout;
	private ArrayList<HashMap<String, String>> status_array, action_array,
			folder_array;
	public static final String DATEPICKER_TAG = "datepicker";
	public static final String TIMEPICKER_TAG = "timepicker";
	private Calendar calendar;
	private DatePickerDialog datePickerDialog;
	private TimePickerDialog timePickerDialog;
	private boolean isSessionDialogShown = false;
	private EzeidLoadingProgress ezeidLoadingProgress;
	private Handler handler;
	private Runnable runnable;
	private ItemDBhelper dBhelper;
	private List<TrItems> items;
	private MainAdapter trItemsAdapter;
	private RecyclerView.LayoutManager mLayoutManager;
	private FloatingActionsMenu statusMenu, nextActionMenu, folder_menu;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_sales_transactions_details);

		toolbar = (Toolbar) findViewById(R.id.toolbar_transDetail);
		toolbar.setTitle(Html.fromHtml("TRANSACTION DETAIL"));
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		toolbar.setNavigationIcon(getResources().getDrawable(
				R.drawable.ic_action_back));
		toolbar.setNavigationOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ezistingNotes = "";
				dBhelper.open();
				dBhelper.DeleteAllItems();
				dBhelper.close();
				SalesTransactionDetail.this.finish();
			}
		});

		InitDefaultComponents();
		if (isSessionDialogShown == false) {
			CheckSessionExpiredStatus();
		}

		if (savedInstanceState != null) {
			DatePickerDialog dpd = (DatePickerDialog) getSupportFragmentManager()
					.findFragmentByTag(DATEPICKER_TAG);
			if (dpd != null) {
				dpd.setOnDateSetListener(this);
			}

			TimePickerDialog tpd = (TimePickerDialog) getSupportFragmentManager()
					.findFragmentByTag(TIMEPICKER_TAG);
			if (tpd != null) {
				tpd.setOnTimeSetListener(this);
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (EzeidUtil.isConnectedToInternet(getApplicationContext()) == false) {

			EzeidUtil.showToast(SalesTransactionDetail.this, getResources()
					.getString(R.string.internet_warning));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.transaction_detail, menu);
		// if (isMenuState == false) {
		// menu.findItem(R.id.menu_save_msgs).setVisible(false);
		// } else {
		// menu.findItem(R.id.menu_save_msgs).setVisible(true);
		// }
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * On selecting action bar icons
	 * */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
		switch (item.getItemId()) {
		case R.id.menu_save_msgs:
			// msg action
			SaveItems(ezistingNotes);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void InitDefaultComponents() {
		// TODO Auto-generated method stub
		ezeidPreferenceHelper = new EzeidPreferenceHelper(
				SalesTransactionDetail.this);
		LinearLayoutManager layoutManager;
		mRecyclerView = (RecyclerView) findViewById(R.id.filter_items_list);
		mRecyclerView.setHasFixedSize(true);
		transdAmount = (TextView) findViewById(R.id.transdAmount);
		salesItemListType = ezeidPreferenceHelper
				.GetValueFromSharedPrefs("SalesItemListType");
		EzeidGlobal.SALES_TYPE = salesItemListType;
		transRequirementsMore = (TextView) findViewById(R.id.transRequirementsMore);
		if (salesItemListType.equalsIgnoreCase("1")) {
			layoutManager = new LinearLayoutManager(SalesTransactionDetail.this);
			layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
			mRecyclerView.setLayoutManager(layoutManager);
			transdAmount.setVisibility(View.GONE);
			transRequirementsMore.setVisibility(View.GONE);
		} else if (salesItemListType.equalsIgnoreCase("2")) {
			mLayoutManager = new GridLayoutManager(this, 3);
			mRecyclerView.setLayoutManager(mLayoutManager);
			transRequirementsMore.setVisibility(View.GONE);
			transdAmount.setVisibility(View.GONE);
		} else if (salesItemListType.equalsIgnoreCase("3")) {
			layoutManager = new LinearLayoutManager(SalesTransactionDetail.this);
			layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
			mRecyclerView.setLayoutManager(layoutManager);
			transdAmount.setVisibility(View.GONE);
			transRequirementsMore.setVisibility(View.VISIBLE);
			transRequirementsMore.setOnClickListener(this);
		} else {
			layoutManager = new LinearLayoutManager(SalesTransactionDetail.this);
			layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
			mRecyclerView.setLayoutManager(layoutManager);
			transdAmount.setVisibility(View.VISIBLE);
			transRequirementsMore.setVisibility(View.VISIBLE);
			transRequirementsMore.setOnClickListener(this);
		}
		// }

		dBhelper = new ItemDBhelper(SalesTransactionDetail.this);
		requesterName = (TextView) findViewById(R.id.transRequesterName);
		statusMenu = (FloatingActionsMenu) findViewById(R.id.statusmenu);
		nextActionMenu = (FloatingActionsMenu) findViewById(R.id.nextAction_menu);
		folder_menu = (FloatingActionsMenu) findViewById(R.id.folder_menu);

		statusMenu.setVisibility(View.INVISIBLE);
		nextActionMenu.setVisibility(View.INVISIBLE);
		folder_menu.setVisibility(View.INVISIBLE);

		transRequirements = (TextView) findViewById(R.id.transRequirements);
		folderLbl = (TextView) findViewById(R.id.folderLbl);
		nextActionDate = (AppCompatEditText) findViewById(R.id.nextActionText);
		nextActionDate.setEnabled(false);
		nextActionDate.setTextColor(Color.rgb(0, 150, 136));
		nextActionDateBtn = (TextView) findViewById(R.id.nextActionDateBtn);
		notesEdit = (TextView) findViewById(R.id.notesEdit);
		notes = (TextView) findViewById(R.id.notes);
		deliveryAddressTxt = (TextView) findViewById(R.id.itemupDeliveryAddress);
		updateUser = (TextView) findViewById(R.id.updateBy);
		updateDate = (TextView) findViewById(R.id.updateOnDate);
		nextActionDateBtn.setOnClickListener(this);
		notesEdit.setOnClickListener(this);
		GetSavedInstanses();
	}

	private void SetDefaultStatus() {
		// statusID
		if (status_array.size() > 0) {
			statusMenu.setVisibility(View.VISIBLE);
			// Log.i(TAG, "status_array: " + status_array.toString());
			for (HashMap<String, String> hashMap : status_array) {

				final FloatingActionButton command_one = new FloatingActionButton(
						SalesTransactionDetail.this);

				String status = hashMap.get("TID");
				final String statusTitle = hashMap.get("StatusTitle");
				command_one.setId(Integer.parseInt(status));

				if (edit_statusID.equalsIgnoreCase(status)) {
					// statusFilter.setSelection(count);
					command_one.setTitle("" + statusTitle);
					command_one.setIcon(R.drawable.ic_action_settings);
					command_one.setColorNormal(getResources().getColor(
							R.color.highlightDarkOrange));
					command_one.setColorPressed(getResources().getColor(
							R.color.ff7a32Orange));
					command_one.setSize(FloatingActionButton.SIZE_MINI);
					statusMenu.addButton(command_one);
				} else {
					command_one.setTitle("" + statusTitle);
					command_one.setIcon(R.drawable.ic_action_settings);
					command_one.setColorNormal(getResources().getColor(
							R.color.highlightColorOrangeLight1));
					command_one.setColorPressed(getResources().getColor(
							R.color.ff7a32Orange));
					command_one.setSize(FloatingActionButton.SIZE_MINI);
					statusMenu.addButton(command_one);
				}
				command_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int id = command_one.getId();
						edit_statusID = String.valueOf(id);
						// Log.i(TAG, "Status_id: " + edit_statusID);
						statusMenu.collapse();
						SaveItems(ezistingNotes);

					}
				});

			}

		} else {
			statusMenu.setVisibility(View.INVISIBLE);
			// Log.i(TAG, "action_array: " + status_array.toString());
		}
	}

	private void SetDefaultActions() {
		if (action_array.size() > 0) {
			nextActionMenu.setVisibility(View.VISIBLE);
			// Log.i(TAG, "action_array: " + action_array.toString());
			for (HashMap<String, String> hashMap : action_array) {

				final FloatingActionButton command_one = new FloatingActionButton(
						SalesTransactionDetail.this);

				String status = hashMap.get("TID");
				final String actionTitle = hashMap.get("ActionTitle");
				command_one.setId(Integer.parseInt(status));

				if (edit_nextActionID.equalsIgnoreCase(status)) {
					// statusFilter.setSelection(count);
					command_one.setTitle("" + actionTitle);
					command_one.setIcon(R.drawable.ic_action_settings);
					command_one.setColorNormal(getResources().getColor(
							R.color.highlightDarkOrange));
					command_one.setColorPressed(getResources().getColor(
							R.color.ff7a32Orange));
					command_one.setSize(FloatingActionButton.SIZE_MINI);
					nextActionMenu.addButton(command_one);
				} else {
					command_one.setTitle("" + actionTitle);
					command_one.setIcon(R.drawable.ic_action_settings);
					command_one.setColorNormal(getResources().getColor(
							R.color.highlightColorOrangeLight1));
					command_one.setColorPressed(getResources().getColor(
							R.color.ff7a32Orange));
					command_one.setSize(FloatingActionButton.SIZE_MINI);
					nextActionMenu.addButton(command_one);
				}
				command_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int id = command_one.getId();
						edit_nextActionID = String.valueOf(id);
						// Log.i(TAG, "edit_nextActionID : " +
						// edit_nextActionID);
						nextActionMenu.collapse();
						SaveItems(ezistingNotes);
					}
				});

			}

		} else {
			nextActionMenu.setVisibility(View.INVISIBLE);
			// Log.i(TAG, "action_array: " + action_array.toString());
		}

		// if (action_array.size() > 0) {
		// int count = 0;
		// for (HashMap<String, String> hashMap : action_array) {
		// String status = hashMap.get("TID");
		// if (nextActionID.equalsIgnoreCase(status)) {
		// actionFilter.setSelection(count);
		// } else {
		// count++;
		// }
		// }
		// }
	}

	private void SetDefaultFolder() {
		// statusID
		if (folder_array.size() > 0) {
			folder_menu.setVisibility(View.VISIBLE);
			// Log.i(TAG, "folder_array: " + folder_array.toString());
			for (HashMap<String, String> hashMap : folder_array) {

				final FloatingActionButton command_one = new FloatingActionButton(
						SalesTransactionDetail.this);

				String status = hashMap.get("TID");
				final String folderTitle = hashMap.get("FolderTitle");
				command_one.setId(Integer.parseInt(status));

				if (edit_folderRuleID.equalsIgnoreCase(status)) {
					// statusFilter.setSelection(count);
					command_one.setTitle("" + folderTitle);
					command_one.setIcon(R.drawable.ic_action_location_found);
					command_one.setColorNormal(getResources().getColor(
							R.color.highlightDarkOrange));
					command_one.setColorPressed(getResources().getColor(
							R.color.ff7a32Orange));
					command_one.setSize(FloatingActionButton.SIZE_MINI);
					folder_menu.addButton(command_one);
				} else {
					command_one.setTitle("" + folderTitle);
					command_one.setIcon(R.drawable.ic_action_location_found);
					command_one.setColorNormal(getResources().getColor(
							R.color.highlightColorOrangeLight1));
					command_one.setColorPressed(getResources().getColor(
							R.color.ff7a32Orange));
					command_one.setSize(FloatingActionButton.SIZE_MINI);
					folder_menu.addButton(command_one);
				}
				command_one.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int id = command_one.getId();
						edit_folderRuleID = String.valueOf(id);
						// Log.i(TAG, "edit_folderRuleID : " +
						// edit_folderRuleID);
						folder_menu.collapse();
						SaveItems(ezistingNotes);
					}
				});

			}

		} else {
			folder_menu.setVisibility(View.INVISIBLE);
			// Log.i(TAG, "folder_array: " + folder_array.toString());
		}
		// if (folder_array.size() > 0) {
		// int count = 0;
		// for (HashMap<String, String> hashMap : folder_array) {
		// String status = hashMap.get("TID");
		// if (folderRuleID.equalsIgnoreCase(status)) {
		// folderFilter.setSelection(count);
		// } else {
		// count++;
		// }
		// }
		// }
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		dBhelper.open();
		dBhelper.DeleteAllItems();
		dBhelper.close();
		finish();
	}

	private void GetSavedInstanses() {

		Intent intent = getIntent();
		if (null != intent) {
			ShowEzeidDialog("Loading transaction details");
			requesterName.setText("" + intent.getStringExtra("Requester"));
			String amt = intent.getStringExtra("Amount");
			if (salesItemListType.equalsIgnoreCase("4")) {
				if (amt.equalsIgnoreCase("0")) {
					transdAmount.setVisibility(View.GONE);
				} else {
					transdAmount.setVisibility(View.VISIBLE);
					transdAmount.setText("Amount: " + amt);
				}
			} else {
				transdAmount.setVisibility(View.GONE);
			}

			// transRequirements.setText(""+intent.getStringExtra("Message"));
			nextActionDate.setText(""
					+ intent.getStringExtra("Next_action_date"));
			ezistingNotes = intent.getStringExtra("Notes_lbl");
			edit_statusID = intent.getStringExtra("StatusID");
			edit_nextActionID = intent.getStringExtra("ActionID");
			edit_folderRuleID = intent.getStringExtra("FolderID");
			locID = intent.getStringExtra("LocID");
			messageID = intent.getStringExtra("TID");
			taskDateTimeStr = intent.getStringExtra("TaskDateTime");
			nextActionDateStr = intent.getStringExtra("Next_action_date");
			contactInfo = intent.getStringExtra("ContactInfo");
			deliveryAddress = intent.getStringExtra("DeliveryAddress");
			ezeid = intent.getStringExtra("EZEID");
			req_ezeid = intent.getStringExtra("RequesterEZEID");
			// ezeidPreferenceHelper
			// .SaveValueToSharedPrefs("BusinessEZEID", ezeid);
			ezeidPreferenceHelper.SaveValueToSharedPrefs("ContactInfo",
					contactInfo);

			if (ezistingNotes.equalsIgnoreCase("")) {
				// notesMore.setVisibility(View.GONE);
				notes.setText("Notes not found");
				notesEdit.setText("Add Notes");
			} else {
				notes.setText("Notes: " + ezistingNotes);
				notesEdit.setText("Edit");

			}
			if (edit_folderRuleID.equalsIgnoreCase("null")) {
				folderLbl.setVisibility(View.GONE);
				// folderFilter.setVisibility(View.GONE);
				folder_menu.setVisibility(View.GONE);
			}
			messageText = intent.getStringExtra("Message");

			String updateBy = intent.getStringExtra("UpdatedUser");
			String updateOn = intent.getStringExtra("updatedDate");
			updateUser.setText(updateBy);
			updateDate.setText(updateOn);
			deliveryAddressTxt.setText(deliveryAddress);
			CheckEditStatus();
			DismissEzeidDialog();

			// if(){
			// GetEditedSalesItemList(String msgid)
			// }
		}
	}

	private void CheckEditStatus() {
		if (salesItemListType.equalsIgnoreCase("3")
				|| salesItemListType.equalsIgnoreCase("4")) {

			if (messageText.equalsIgnoreCase("")) {
				// transRequirements.setVisibility(View.GONE);
				transRequirements.setText("No Items found");
				transRequirementsMore.setText("Add items");
				transRequirementsMore.setVisibility(View.GONE);
			} else {
				transRequirements.setVisibility(View.VISIBLE);
				transRequirementsMore.setVisibility(View.VISIBLE);
				transRequirementsMore.setText("Edit");
				transRequirements.setText("" + messageText);
			}
		} else if (salesItemListType.equalsIgnoreCase("1")
				|| salesItemListType.equalsIgnoreCase("2")) {
			if (messageText.equalsIgnoreCase("")) {
				transRequirements.setText("No Items found");
			} else {
				transRequirements.setText("" + messageText);
			}
		}
	}

	private void ShowEzeidDialog(String content) {
		ezeidLoadingProgress = new EzeidLoadingProgress(
				SalesTransactionDetail.this, content);
		handler = new Handler();
		runnable = new Runnable() {

			@Override
			public void run() {
				if (ezeidLoadingProgress != null) {
					if (ezeidLoadingProgress.isShowing()) {
						ezeidLoadingProgress.dismiss();
					}
				}
			}
		};
		ezeidLoadingProgress.show();
	}

	private void DismissEzeidDialog() {
		handler.removeCallbacks(runnable);
		if (ezeidLoadingProgress.isShowing()) {
			ezeidLoadingProgress.dismiss();
		}
	}

	private void CheckSessionExpiredStatus() {
		// loader.setVisibility(View.VISIBLE);
		String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
		if (!token.equalsIgnoreCase("") && sessionRetry < 5) {
			JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
					EzeidGlobal.EzeidUrl + "ewtGetLoginCheck?Token=" + token,
					new Response.Listener<JSONObject>() {
						@Override
						public void onResponse(JSONObject response) {
							// loader.setVisibility(View.INVISIBLE);
							SessionStatus(response);
						}
					}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							// loader.setVisibility(View.INVISIBLE);
							sessionRetry += 1;
							if (error instanceof TimeoutError) {
								SessionTryAgain();
							} else if (error instanceof ServerError
									|| error instanceof AuthFailureError) {
								EzeidUtil.showToast(
										SalesTransactionDetail.this,
										"Network is unreachable!");
							} else if (error instanceof NetworkError
									|| error instanceof NoConnectionError) {
								SessionTryAgain();
							} else {
								SessionTryAgain();
							}
						}
					});
			req.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
		}
	}

	private void SessionTryAgain() {
		CheckSessionExpiredStatus();
	}

	private void SessionStatus(JSONObject response) {

		if (!response.isNull("IsAvailable")) {
			try {
				String status = response.getString("IsAvailable");
				if (status.equalsIgnoreCase("true")) {
					ActivityStatus();
				} else {
					SessionDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void ActivityStatus() {
		BindActionStatusFilters();
		BindNextActionFilters();
		BindFolderFilters();

		// BindTransactionItems();
		GetEditedSalesItemList();
		SetCalendar();

		// ShowEzeidDialog("Please wait!");
		// BindActionStatusFilters();
		// BindNextActionFilters();
		// BindFolderFilters();
		// new Handler().postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// DismissEzeidDialog();
		// }
		// }, 3000);

	}

	private void SessionDialog() {
		isSessionDialogShown = true;
		final MaterialDialog materialDialog = new MaterialDialog(this);
		materialDialog
				.setBackgroundResource(R.drawable.abc_cab_background_internal_bg);
		materialDialog.setTitle(R.string.sessionExpired)
				.setMessage("Your session has expired. Please login again!")
				.setPositiveButton(R.string.ok, new OnClickListener() {
					@Override
					public void onClick(View v) {
						materialDialog.dismiss();
						ezeidPreferenceHelper.SaveValueToSharedPrefs("Token",
								"");
						startActivity(new Intent(SalesTransactionDetail.this,
								SplashScreen.class));
						SalesTransactionDetail.this.finish();
					}
				});
		materialDialog.setCanceledOnTouchOutside(true).show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

	}


	private void BindActionStatusFilters() {

		try {
			// ezeidLoadingProgress.show();
			// ShowEzeidDialog("Loading status");
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String masterID = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("MasterID");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetStatusType?Token=" + token + "&MasterID="
					+ masterID + "&FunctionType=0";
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {

						@Override
						public void onResponse(JSONArray response) {
							// DismissEzeidDialog();
							BindStatus(response);

						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							// DismissEzeidDialog();
							statusMenu.setVisibility(View.GONE);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void GetEditedSalesItemList() {
		try {
			ShowEzeidDialog("Please wait!");
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			// token = "294f4b0c8c847333b1b8";
			// String url = EZEIDUrlManager.getAPIUrl()
			// + "ewtGetTranscationItems?Token=" + token + "&MessageID="
			// + msgid;
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetTranscationItems?Token=" + token + "&MessageID="
					+ messageID;

			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {

						@Override
						public void onResponse(JSONArray response) {
							BindEditedSalesItems(response);
							DismissEzeidDialog();
						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							BindEditedSalesItems(null);
							DismissEzeidDialog();
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindEditedSalesItems(JSONArray response) {
		// TODO Auto-generated method stub
		try {

			if (null != response) {
				int len = response.length();
				if (len != 0) {

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String item_tid = jsonObject.getString("TID");
						String item_id = jsonObject.getString("ItemID");
						String item_name = jsonObject.getString("ItemName");
						String item_rate = jsonObject.getString("Rate");
						String item_amt = jsonObject.getString("Amount");
						String item_qty = jsonObject.getString("Qty");
						String item_img = jsonObject.getString("Pic");
						String item_status = "new";
						String item_durations = jsonObject
								.getString("Duration");
						String item_message_id = jsonObject
								.getString("MessageID");
						Bitmap itemBitmap = null;

						if (!item_img.equalsIgnoreCase("")) {
							String itemImag = EzeidUtil.GetFileType(item_img);
							itemBitmap = EzeidUtil.convertBitmap(itemImag);
						} else {
							itemBitmap = BitmapFactory.decodeResource(
									getResources(), R.drawable.ic_launcher);
						}

						SaleItemModel saleItemModel = new SaleItemModel(
								item_id, item_tid, item_name, itemBitmap,
								item_rate, item_amt, item_qty, item_status,
								item_durations, item_message_id);



						dBhelper.open();
						dBhelper.insertItemDetails(saleItemModel);
						dBhelper.close();

					}
					BindTrItems();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private void BindTrItems() {
		// TODO Auto-generated method stub
		items = new ArrayList<TrItems>();
		dBhelper.open();
		List<SaleItemModel> models = dBhelper.GetAllItems();
		if (models.size() > 0) {

			for (SaleItemModel model : models) {

				TrItems trItems = new TrItems();
				trItems.item_id = model.getItem_id();
				trItems.item_tid = model.getItem_tid();
				trItems.item_name = model.getItem_name();
				trItems.item_rate = model.getItem_rate();
				trItems.item_amt = model.getItem_amt();
				trItems.item_qty = model.getItem_qty();
				trItems.item_status = model.getItem_status();
				trItems.item_img = model.getItem_img();
				trItems.item_durations = model.getItem_durations();
				trItems.item_message_id = model.getItem_message_id();
				items.add(trItems);
			}
			if (items.size() > 0) {
				// Log.d("Result: ", "BindTrItems(): " + items.toString());
				mRecyclerView.setVisibility(View.VISIBLE);
				trItemsAdapter = new MainAdapter(SalesTransactionDetail.this,
						items);

				AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(
						trItemsAdapter);
				mRecyclerView.setAdapter(new ScaleInAnimationAdapter(
						alphaAdapter));

			}
		} else {
			mRecyclerView.setVisibility(View.INVISIBLE);
			EzeidUtil.showToast(SalesTransactionDetail.this, "No items found!");
		}
		dBhelper.close();

	}

	private void BindNextActionFilters() {

		try {
			// ShowEzeidDialog("Loading next actions");
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String masterID = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("MasterID");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetActionType?Token=" + token + "&MasterID="
					+ masterID + "&FunctionType=0";

			// String url = "http://10.0.100.199:3001/ewtGetActionType?Token="
			// + token + "&MasterID=" + masterID + "&FunctionType=1";
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {

						@Override
						public void onResponse(JSONArray response) {
							// DismissEzeidDialog();
							BindActions(response);

						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							// DismissEzeidDialog();
							nextActionMenu.setVisibility(View.GONE);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void BindActions(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					action_array = new ArrayList<HashMap<String, String>>();

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String masterID = jsonObject.getString("MasterID");
						String actionTitle = jsonObject
								.getString("ActionTitle");

						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("MasterID", masterID);
						map.put("ActionTitle", actionTitle);
						action_array.add(map);

						// actionAdapter = new SimpleAdapter(
						// SalesTransactionDetail.this, action_array,
						// R.layout.ezeid_tagid_row, new String[] { "TID",
						// "MasterID", "ActionTitle" }, new int[] {
						// R.id.masterId, R.id.masterPercentage,
						// R.id.masterTag });
						// actionFilter.setAdapter(actionAdapter);
					}
					SetDefaultActions();
				} else {
					nextActionMenu.setVisibility(View.GONE);
				}
			} else {
				nextActionMenu.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindStatus(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					status_array = new ArrayList<HashMap<String, String>>();

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String statusTitle = jsonObject
								.getString("StatusTitle");
						String progressPercent = jsonObject
								.getString("ProgressPercent");
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("StatusTitle", statusTitle);
						map.put("ProgressPercent", progressPercent);
						status_array.add(map);

					}
					SetDefaultStatus();
				} else {
					statusMenu.setVisibility(View.GONE);
				}
			} else {
				statusMenu.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void BindFolderFilters() {
		try {
			// ShowEzeidDialog("Loading folders");
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			String masterID = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("MasterID");
			String url = EZEIDUrlManager.getAPIUrl()
					+ "ewtGetFolderList?Token=" + token + "&MasterID="
					+ masterID + "&FunctionType=0";
			// String url = "http://10.0.100.199:3001/ewtGetFolderList?Token="
			// + token + "&MasterID=" + masterID + "&FunctionType=1";
			JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
					new Response.Listener<JSONArray>() {

						@Override
						public void onResponse(JSONArray response) {
							// DismissEzeidDialog();
							BindFolders(response);

						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							// DismissEzeidDialog();
							folder_menu.setVisibility(View.GONE);
						}
					});
			jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void BindFolders(JSONArray response) {
		try {
			if (null != response) {
				int len = response.length();
				if (len != 0) {
					folder_array = new ArrayList<HashMap<String, String>>();

					for (int i = 0; i < len; i++) {
						JSONObject jsonObject = response.getJSONObject(i);
						String tid = jsonObject.getString("TID");
						String masterID = jsonObject.getString("MasterID");
						String folderTitle = jsonObject
								.getString("FolderTitle");
						String ruleFunction = jsonObject
								.getString("RuleFunction");
						String ruleType = jsonObject.getString("RuleType");
						String countryIDs = jsonObject.getString("CountryIDs");
						String matchAdminLevel = jsonObject
								.getString("MatchAdminLevel");
						String mappedNames = jsonObject
								.getString("MappedNames");
						String latitude = jsonObject.getString("Latitude");
						String longitude = jsonObject.getString("Longitude");

						String proximity = jsonObject.getString("Proximity");
						String defaultFolder = jsonObject
								.getString("DefaultFolder");
						String folderStatus = jsonObject
								.getString("FolderStatus");
						String seqNoFrefix = jsonObject
								.getString("SeqNoFrefix");
						String runningSeqNo = jsonObject
								.getString("RunningSeqNo");
						String createdDate = jsonObject
								.getString("CreatedDate");
						String lUDate = jsonObject.getString("LUDate");

						HashMap<String, String> map = new HashMap<String, String>();
						map.put("TID", tid);
						map.put("MasterID", masterID);
						map.put("FolderTitle", folderTitle);
						map.put("RuleFunction", ruleFunction);
						map.put("RuleType", ruleType);
						map.put("CountryIDs", countryIDs);
						map.put("MatchAdminLevel", matchAdminLevel);
						map.put("MappedNames", mappedNames);
						map.put("Latitude", latitude);
						map.put("Longitude", longitude);
						map.put("Proximity", proximity);
						map.put("DefaultFolder", defaultFolder);
						map.put("FolderStatus", folderStatus);
						map.put("SeqNoFrefix", seqNoFrefix);
						map.put("RunningSeqNo", runningSeqNo);
						map.put("CreatedDate", createdDate);
						map.put("LUDate", lUDate);
						folder_array.add(map);

					}
					SetDefaultFolder();
				} else {
					folder_menu.setVisibility(View.GONE);
				}
			} else {
				folder_menu.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




	void showToast(CharSequence msg) {
		Toast.makeText(SalesTransactionDetail.this, msg, Toast.LENGTH_SHORT)
				.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.nextActionDateBtn:
			GetDatePicker();
			break;

		case R.id.transRequirementsMore:
			EditItems();
			break;

		case R.id.notesEdit:
			MessageUpdate(ezistingNotes);
			break;



		default:
			break;
		}

	}

	private void EditItems() {


		dBhelper.open();
		dBhelper.DeleteAllItems();
		dBhelper.close();
		// ArrayList<HashMap<String, String>> titleList = EzeidGlobal.titleList;
		// if (titleList.size() != 0) {
		// String salesItemListType = titleList.get(0)
		// .get("SalesItemListType");
		if (salesItemListType.equalsIgnoreCase("3")
				|| salesItemListType.equalsIgnoreCase("4")) {
			/*
			 * Intent intent = new Intent(SalesTransactionDetail.this,
			 * EzeidAddItems.class); intent.putExtra("EZEID", ezeid);
			 * intent.putExtra("MessageID", messageID);
			 * intent.putExtra("ITEM_EDIT", "Edit"); intent.putExtra("Message",
			 * messageText); intent.putExtra("StatusID", statusID);
			 * intent.putExtra("ActionID", nextActionID);
			 * intent.putExtra("FolderID", folderRuleID);
			 * intent.putExtra("LocID", locID); intent.putExtra("TaskDateTime",
			 * taskDateTimeStr); intent.putExtra("Next_action_date",
			 * nextActionDateStr); intent.putExtra("ContactInfo", contactInfo);
			 * intent.putExtra("Notes_lbl", ezistingNotes);
			 * intent.putExtra("DeliveryAddress", deliveryAddressTxt.getText()
			 * .toString()); startActivity(intent); } else {
			 */

			ezeidPreferenceHelper.SaveValueToSharedPrefs("ITEM_TO_ADD_USER",
					"business_mgr");
			Intent intent = new Intent(SalesTransactionDetail.this,
					EzeidAddSalesItems.class);
			intent.putExtra("EZEID", ezeid);
			intent.putExtra("RequesterEZEID", req_ezeid);
			intent.putExtra("MessageID", messageID);
			intent.putExtra("ITEM_EDIT", "Edit");
			intent.putExtra("Message", messageText);
			intent.putExtra("StatusID", statusID);
			intent.putExtra("ActionID", nextActionID);
			intent.putExtra("FolderID", folderRuleID);
			intent.putExtra("LocID", locID);
			intent.putExtra("TaskDateTime", taskDateTimeStr);
			intent.putExtra("Next_action_date", nextActionDateStr);
			intent.putExtra("ContactInfo", contactInfo);
			intent.putExtra("Notes_lbl", ezistingNotes);
			intent.putExtra("DeliveryAddress", deliveryAddressTxt.getText()
					.toString());
			startActivity(intent);
		}
		// }

	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		// YYYY-MM-DDThh:mm:ss.sTZD
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss",
				Locale.getDefault());
		// String timeZoneId = TimeZone.getDefault().getID();
		// df.setTimeZone(TimeZone.getTimeZone("UTC"));
		String formattedDate = df.format(calendar.getTime());
		formattedDate = formattedDate.substring(17, formattedDate.length());

		String hourStr = "", minStr = "";
		int hour = String.valueOf(hourOfDay).trim().length();
		if (hour == 1) {
			hourStr = "0" + hourOfDay;
		} else {
			hourStr = "" + hourOfDay;
		}
		int min = String.valueOf(minute).trim().length();
		if (min == 1) {
			minStr = "0" + minute;
		} else {
			minStr = "" + minute;
		}

		timeStr = hourStr + ":" + minStr + ":" + formattedDate;
		prefDateTimeStr = dateStr + " " + timeStr;
		nextActionDate.setVisibility(View.VISIBLE);
		nextActionDate.setText("" + PreferedDateNormal(prefDateTimeStr));

	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		dateStr = "";
		timeStr = "";
		nextActionDate.setText("");
		month = month + 1;
		int mon = String.valueOf(month).trim().length();
		if (mon == 1) {
			dateStr = "0" + month + "/" + day + "/" + year;
		} else {
			dateStr = month + "/" + day + "/" + year;
		}
		GetTimePicker();
	}

	private void SetCalendar() {
		calendar = Calendar.getInstance();
		datePickerDialog = DatePickerDialog.newInstance(this,
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), false);
		timePickerDialog = TimePickerDialog.newInstance(this,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), true, false);
	}

	private void GetDatePicker() {
		datePickerDialog.setVibrate(false);
		datePickerDialog.setYearRange(1985, 2028);
		datePickerDialog.setCloseOnSingleTapDay(false);
		datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
	}

	private void GetTimePicker() {
		timePickerDialog.setVibrate(false);
		timePickerDialog.setCloseOnSingleTapMinute(false);
		timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
	}

	private String PreferedDateNormal(String preferedDate) {
		Date date;// 02/26/2015 02:00:29
		try {
			SimpleDateFormat normalFormat = new SimpleDateFormat(
					"MM/dd/yyyy KK:mm:ss", Locale.getDefault());
			date = normalFormat.parse(preferedDate);

			SimpleDateFormat destFormat = new SimpleDateFormat(
					"dd MMM yyyy HH:mm:ss aa", Locale.ENGLISH);

			return destFormat.format(date);// Feb 26 2015 02:00 AM
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void SaveItems(String notes) {

		try {

			ShowEzeidDialog("Updating...");
			String token = ezeidPreferenceHelper
					.GetValueFromSharedPrefs("Token");
			// String ezeid = ezeidPreferenceHelper
			// .GetValueFromSharedPrefs("BusinessEZEID");
			String status = "0";
			// if (isEdited == true) {
			// status = edit_statusID;
			// } else {
			// status = statusID;
			// }
			// if (status.equalsIgnoreCase("")) {
			// status = "0";
			// }

			// String taskDateTime = taskDateTimeStr;

			String folderRule = "0";
			// if (isEdited == true) {
			// folderRule = edit_folderRuleID;
			// } else {
			// folderRule = folderRuleID;
			// }

			// if (!folderRuleID.equalsIgnoreCase("")) {
			// folderRule = folderRuleID;
			// } else {
			// folderRule = "0";
			// }

			String nextAction = "0";
			// if (isEdited == true) {
			// nextAction = edit_nextActionID;
			// } else {
			// nextAction = nextActionID;
			// }
			// if (!actionID.equalsIgnoreCase("")) {
			// nextAction = actionID;//
			// } else {
			// nextAction = "0";
			// }
			String nextactionDateTime = "";
			nextActionDateStr = nextActionDate.getText().toString();
			// if (isEditActionDate == true)
			// {
			// // nextActionDateStr = NextActionDateUTC();
			// nextactionDateTime=NextActionUpDateUTC(nextActionDateStr);
			// } else
			// {
			// nextActionDateStr = nextActionDate.getText().toString();
			// }

			String taskDateTime = NextActionUpDateUTC(taskDateTimeStr);
			nextactionDateTime = NextActionUpDateUTC(nextActionDateStr);

			String itemList = "[]";
			if (!salesItemListType.equalsIgnoreCase("0")) {
				JSONArray jsonArray = BindAddedTrItems();
				dBhelper.open();
				itemList = jsonArray.toString();
				messageText = GetSelectedOrders();
			}

			HashMap<String, String> hashMap = new HashMap<String, String>();

			hashMap.put("Token", token);
			hashMap.put("TID", messageID);
			hashMap.put("MessageText", messageText);
			hashMap.put("item_list_type", salesItemListType);
			hashMap.put("Status", edit_statusID);
			hashMap.put("TaskDateTime", taskDateTime);
			hashMap.put("Notes", notes);
			hashMap.put("LocID", locID);
			hashMap.put("Country", "");
			hashMap.put("State", "");
			hashMap.put("City", "");
			hashMap.put("Area", "");
			hashMap.put("FunctionType", "0");
			hashMap.put("Latitude", "0");
			hashMap.put("Longitude", "0");
			hashMap.put("FolderRuleID", edit_folderRuleID);
			hashMap.put("EZEID", req_ezeid);
			hashMap.put("ToEZEID", ezeid);
			hashMap.put("ContactInfo", contactInfo);
			hashMap.put("Duration", "0");
			hashMap.put("DeliveryAddress", deliveryAddress);
			hashMap.put("DurationScales", "0");
			hashMap.put("NextAction", edit_nextActionID);
			hashMap.put("NextActionDateTime", nextactionDateTime);
			hashMap.put("ItemsList", itemList);

			 Log.i("TAG", "hashMap items: " + hashMap.toString());

			JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
					Request.Method.POST, EZEIDUrlManager.getAPIUrl()
							+ "ewtSaveTranscation", new JSONObject(hashMap),
					new Response.Listener<JSONObject>() {

						@Override
						public void onResponse(JSONObject response) {
							// TODO Auto-generated method stub
							try {
								DismissEzeidDialog();
								if (null != response) {
									String result = response
											.getString("IsSuccessfull");
									if (result.equalsIgnoreCase("true"))
										EzeidUtil
												.showToast(
														SalesTransactionDetail.this,
														"Transaction saved Successfully");
								} else {
									EzeidUtil.showToast(
											SalesTransactionDetail.this,
											"Transaction saved failed");
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							// Log.i(TAG, "hashMap items: " +
							// response.toString());

						}
					}, new Response.ErrorListener() {

						@Override
						public void onErrorResponse(VolleyError error) {
							// TODO Auto-generated method stub
							DismissEzeidDialog();
							if (error instanceof TimeoutError) {
								EzeidUtil.showToast(
										SalesTransactionDetail.this,
										"Connection timeout. Please try again");
							} else if (error instanceof ServerError
									|| error instanceof AuthFailureError) {
								EzeidUtil
										.showToast(SalesTransactionDetail.this,
												"Unable to connect server. Please try later");
							} else if (error instanceof NetworkError
									|| error instanceof NoConnectionError) {
								EzeidUtil.showToast(
										SalesTransactionDetail.this,
										"Network is unreachable");
							} else {
								EzeidUtil.showToast(
										SalesTransactionDetail.this,
										"Send failed");
							}
						}
					});
			jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);
			// } else {
			// EzeidUtil.showToast(SalesTransactionDetail.this,
			// "Transaction items mismatched!");
			// }
			dBhelper.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String NextActionUpDateUTC(String dateS) {
		Date date;// 02/25/2015 03:00:56

		try {
			// 07 Apr 2015 18:41:44 PM, "dd MMM yyyy KK:mm:ss aa"
			SimpleDateFormat utcFormat = new SimpleDateFormat(
					"dd MMM yyyy KK:mm:ss aa", Locale.getDefault());
			utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			date = utcFormat.parse(dateS);// Wed Feb 25 08:30:56
											// GMT+05:30 2015
			SimpleDateFormat destFormat = new SimpleDateFormat(
					"MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
			return destFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private String GetSelectedOrders() {

		String selected_items = "";
		StringBuilder builder = null;
		dBhelper.open();
		List<SaleItemModel> addedItems = dBhelper.GetAllItems();

		if (addedItems.size() > 0) {
			builder = new StringBuilder();
			for (SaleItemModel items : addedItems) {

				if (salesItemListType.equalsIgnoreCase("1")) {
					builder.append(items.getItem_name());
					builder.append(",");
				} else if (salesItemListType.equalsIgnoreCase("2")) {
					builder.append(items.getItem_name());
					builder.append(",");
				} else {
					builder.append(items.getItem_name());
					builder.append("(");
					builder.append(items.getItem_qty());
					builder.append(")");
					builder.append(",");
				}

			}

			selected_items = builder.toString();
			if (selected_items.length() > 0
					&& selected_items.charAt(selected_items.length() - 1) == ',') {
				selected_items = selected_items.substring(0,
						selected_items.length() - 1);
			}
		} else {
			selected_items = "";
		}

		return selected_items;
	}

	private void MessageUpdate(String notes) {

		final MaterialDialog msgDialog = new MaterialDialog(
				SalesTransactionDetail.this);
		if (msgDialog != null) {
			RelativeLayout parent = new RelativeLayout(
					SalesTransactionDetail.this);
			View view = LayoutInflater.from(SalesTransactionDetail.this)
					.inflate(R.layout.row_sales_save_transaction_notes, parent);
			final AppCompatEditText content = (AppCompatEditText) view
					.findViewById(R.id.siltm_enquiry_msg);
			final TextView lable = (TextView) view.findViewById(R.id.transMgs);
			final Button ok = (Button) view.findViewById(R.id.saveMsg);
			final Button cancel = (Button) view.findViewById(R.id.cancelMsg);

			lable.setText(Html.fromHtml("<body><b>Message: </b>" + "\n" + notes
					+ "</body>"));

			float scale = getResources().getDisplayMetrics().density;
			int dpAsPixels = (int) (2 * scale + 0.2f);
			view.setPadding(0, dpAsPixels, 0, dpAsPixels);
			msgDialog.setContentView(view);
			ok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					msgDialog.dismiss();
					editNotes = content.getText().toString();
					SaveItems(ezistingNotes + " " + editNotes);
				}
			});
			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					msgDialog.dismiss();
				}
			});

			msgDialog.show();
		}
	}

	private JSONArray BindAddedTrItems() throws JSONException {

		JSONObject jsonObject = null;
		JSONArray jsonArray = new JSONArray();

		List<SaleItemModel> addedItems = dBhelper.GetAllItems();

		if (addedItems.size() > 0) {
			for (SaleItemModel items : addedItems) {

				jsonObject = new JSONObject();

				// jsonObject.put("TID", items.getItem_tid());
				// jsonObject.put("ItemID", items.getItem_id());
				// jsonObject.put("Durations", items.getItem_durations());
				// jsonObject.put("Amount", items.getItem_amt());
				// jsonObject.put("Qty", items.getItem_qty());
				// jsonObject.put("Rate", items.getItem_rate());

				String durations = items.getItem_durations();
				if (durations.equalsIgnoreCase("null")) {
					durations = "0";
				}
				jsonObject.put("TID", items.getItem_tid());
				jsonObject.put("ItemID", items.getItem_id());
				jsonObject.put("Durations", durations);

				if (salesItemListType.equalsIgnoreCase("1")) {
					jsonObject.put("Amount", "0");
					jsonObject.put("Qty", "0");
					jsonObject.put("Rate", "0");
				} else if (salesItemListType.equalsIgnoreCase("2")) {
					jsonObject.put("Amount", "0");
					jsonObject.put("Qty", "0");
					jsonObject.put("Rate", "0");
				} else if (salesItemListType.equalsIgnoreCase("3")) {
					jsonObject.put("Amount", "0");
					jsonObject.put("Qty", items.getItem_qty());
					jsonObject.put("Rate", items.getItem_rate());
				} else {
					jsonObject.put("Amount", items.getItem_amt());
					jsonObject.put("Qty", items.getItem_qty());
					jsonObject.put("Rate", items.getItem_rate());
				}

				jsonArray.put(jsonObject);
			}

		} else {
			jsonArray = new JSONArray("[]");
		}
		dBhelper.close();

		JSONObject object = new JSONObject();
		object.put("ItemsList", jsonArray);
		return jsonArray;

	}

}