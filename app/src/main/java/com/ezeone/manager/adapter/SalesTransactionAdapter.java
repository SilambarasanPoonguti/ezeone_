package com.ezeone.manager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.manager.pojo.SalesTransactionItem;

import java.util.List;

public class SalesTransactionAdapter extends
		RecyclerView.Adapter<SalesTransactionAdapter.ListItemViewHolder> {

	private List<SalesTransactionItem> items;
	private Context context;

	public SalesTransactionAdapter(List<SalesTransactionItem> modelData) {
		if (modelData == null) {
			throw new IllegalArgumentException("modelData must not be null");
		}
		this.items = modelData;
	}

	@Override
	public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup,
			int viewType) {
		View itemView;

		itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
				R.layout.row_get_transaction_items, viewGroup, false);
		this.context = viewGroup.getContext();
		return new ListItemViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {

		SalesTransactionItem model = items.get(position);

		if (String.valueOf(model.itemRequester).equalsIgnoreCase("null")) {
			viewHolder.itemRequester.setText(String
					.valueOf(model.itemContactInfo));
		} else {
			viewHolder.itemRequester.setText(String
					.valueOf(model.itemRequester));
		}

		viewHolder.itemTID.setText(String.valueOf(model.itemTID));
		// viewHolder.itemUpdatedDateUser.setText(String
		// .valueOf(model.itemUpdatedDateUser));
		viewHolder.itemAmount.setText(String.valueOf(model.itemAmount));
		viewHolder.itemNextActionDate.setText(String
				.valueOf(model.itemNextActionDate));
		viewHolder.itemNotes.setText(String.valueOf(model.itemNotes));
		viewHolder.itemStatus.setText(String.valueOf(model.itemStatus));
		viewHolder.itemNextActionID.setText(String
				.valueOf(model.itemNextActionID));
		viewHolder.itemTaskDateTime.setText(String
				.valueOf(model.itemTaskDateTime));
		viewHolder.itemTrnNo.setText(String.valueOf(model.itemTrnNo));
		viewHolder.itemFolderRuleID.setText(String
				.valueOf(model.itemFolderRuleID));
		viewHolder.itemMessage.setText(String.valueOf(model.itemMessage));
		viewHolder.itemContactInfo.setText(String
				.valueOf(model.itemContactInfo));
		viewHolder.itemLocID.setText(String.valueOf(model.itemLocID));
		viewHolder.itemUpdatedUser.setText(String
				.valueOf(model.itemUpdatedUser));
		viewHolder.itemupdatedDate.setText(String
				.valueOf(model.itemupdatedDate));
		viewHolder.itemDeliveryAddress.setText(String
				.valueOf(model.itemDeliveryAddress));
		viewHolder.item_trans_status_title.setText(String
				.valueOf(model.transStatusTitle));
		if (model.transStatusTitle.equalsIgnoreCase("New")) {
			viewHolder.item_trans_status_title.setTextColor(context
					.getResources().getColor(R.color.highlightColorOrange));
		} else {
			viewHolder.item_trans_status_title.setTextColor(context
					.getResources().getColor(R.color.met_green));
		}

		viewHolder.transMsgEZEID.setText(String.valueOf(model.transMsgEZEID));
		viewHolder.item_req_ezeid.setText(String.valueOf(model.transReqEZEID));
//		setAnimation(context, viewHolder.trans_items_container, position);
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	public final static class ListItemViewHolder extends
			RecyclerView.ViewHolder {
		TextView itemRequester, itemTID, itemUpdatedDateUser, itemAmount,
				itemNextActionDate, itemNotes, itemStatus, itemNextActionID,
				itemTaskDateTime, itemTrnNo, itemFolderRuleID, itemMessage,
				itemContactInfo, itemLocID, itemUpdatedUser, itemupdatedDate,
				item_trans_status_title, itemDeliveryAddress, transMsgEZEID,
				item_req_ezeid;
		private RelativeLayout trans_items_container;

		public ListItemViewHolder(View itemView) {
			super(itemView);
			trans_items_container = (RelativeLayout) itemView
					.findViewById(R.id.trans_items_container);
			itemRequester = (TextView) itemView
					.findViewById(R.id.trans_requester_name);
			itemTID = (TextView) itemView.findViewById(R.id.trans_tID);
			itemUpdatedDateUser = (TextView) itemView
					.findViewById(R.id.trans_updated_date);
			itemAmount = (TextView) itemView.findViewById(R.id.trans_amount);
			itemNextActionDate = (TextView) itemView
					.findViewById(R.id.trans_next_action_date);
			itemNotes = (TextView) itemView.findViewById(R.id.trans_notes_lbl);
			itemStatus = (TextView) itemView.findViewById(R.id.trans_statusID);
			itemNextActionID = (TextView) itemView
					.findViewById(R.id.trans_actionID);
			itemTaskDateTime = (TextView) itemView
					.findViewById(R.id.trans_taskDateTime);
			itemTrnNo = (TextView) itemView.findViewById(R.id.trans_trnId);
			itemFolderRuleID = (TextView) itemView
					.findViewById(R.id.trans_folderID);
			itemMessage = (TextView) itemView
					.findViewById(R.id.trans_requirements);
			itemContactInfo = (TextView) itemView
					.findViewById(R.id.trans_contact_info);
			itemLocID = (TextView) itemView.findViewById(R.id.trans_locID);
			itemUpdatedUser = (TextView) itemView.findViewById(R.id.updateBy);
			itemupdatedDate = (TextView) itemView
					.findViewById(R.id.updateOnDate);
			item_trans_status_title = (TextView) itemView
					.findViewById(R.id.trans_status_title);
			itemDeliveryAddress = (TextView) itemView
					.findViewById(R.id.itemupDeliveryAddress);
			transMsgEZEID = (TextView) itemView
					.findViewById(R.id.trans_contact_ezeid);
			item_req_ezeid = (TextView) itemView
					.findViewById(R.id.trans_req_ezeid);
		}
	}

}