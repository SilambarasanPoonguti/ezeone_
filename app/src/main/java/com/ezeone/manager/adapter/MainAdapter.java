package com.ezeone.manager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.manager.pojo.TrItems;
import com.ezeone.utils.EzeidGlobal;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private Context mContext;
    private List<TrItems> mDataSet;
    private String salesItemListType = "";

    public MainAdapter(Context context, List<TrItems> dataSet) {
        mContext = context;
        mDataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;

        salesItemListType = EzeidGlobal.SALES_TYPE;
        if (salesItemListType.equalsIgnoreCase("1")) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_ezeid_sales_item_only, parent, false);
        } else if (salesItemListType.equalsIgnoreCase("2")) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_add_sales_item_only_grid_item, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_ezeid_sales_items, parent, false);
        }
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        TrItems items = mDataSet.get(position);
        viewHolder.item_id.setText(String.valueOf(items.item_id));
        viewHolder.item_tid.setText(String.valueOf(items.item_tid));
        viewHolder.item_name.setText(String.valueOf(items.item_name));
        viewHolder.item_rate.setText(String.valueOf(items.item_rate));
        viewHolder.item_amt.setText(String.valueOf(items.item_amt));
        viewHolder.item_qty.setText(String.valueOf(items.item_qty));
        viewHolder.item_durations.setText(String.valueOf(items.item_durations));

        viewHolder.item_status_.setText(String.valueOf(items.item_status));
        // viewHolder.status.setTag(trItems.get(position));
        if (null != items.item_img) {
            viewHolder.item_pic.setImageBitmap(items.item_img);
        } else {
            viewHolder.item_pic.setImageResource(R.drawable.ic_action_loader);
        }

        if (items.item_status.equalsIgnoreCase("new")) {
            // viewHolder.status.setIconDrawable(context.getResources()
            // .getDrawable(R.drawable.ic_action_item_add));
            viewHolder.item_container
                    .setBackgroundResource(R.drawable.abc_popup_background_mtrl_mult);
        } else if (items.item_status.equalsIgnoreCase("hide")) {
            // viewHolder.status.setIconDrawable(context.getResources()
            // .getDrawable(R.drawable.ic_action_item_add));
            viewHolder.item_container
                    .setBackgroundResource(R.drawable.abc_popup_background_mtrl_mult);
        } else {
            // viewHolder.status.setIconDrawable(context.getResources()
            // .getDrawable(R.drawable.ic_action_item_edit));
            viewHolder.item_container
                    .setBackgroundResource(R.drawable.abc_row_selected);
        }
        int LAST_SELECTED_POS = EzeidGlobal.LAST_SELECTED_POS;
        salesItemListType = EzeidGlobal.SALES_TYPE;
//        Log.i("Tag","salesItemListType: "+salesItemListType);

        int text_bg_disable = 0, text_bg_enable = 0;
        if (salesItemListType.equalsIgnoreCase("1")) {
            text_bg_disable = mContext.getResources().getColor(
                    R.color.highlightTransparent);
            text_bg_enable = mContext.getResources()
                    .getColor(R.color.met_green);

            viewHolder.item_id_selected_bar.setVisibility(View.VISIBLE);

            if (position == LAST_SELECTED_POS) {
                viewHolder.item_id_selected_bar.setBackgroundColor(mContext
                        .getResources().getColor(R.color.highlightColorOrange));
                viewHolder.item_name.setBackgroundColor(mContext.getResources()
                        .getColor(android.R.color.transparent));
            } else {
                viewHolder.item_id_selected_bar.setBackgroundColor(mContext
                        .getResources().getColor(android.R.color.transparent));
                viewHolder.item_name.setBackgroundColor(mContext.getResources()
                        .getColor(android.R.color.transparent));
            }
        } else if (salesItemListType.equalsIgnoreCase("2")) {
            text_bg_disable = mContext.getResources().getColor(
                    R.color.highlightTransparent);
            text_bg_enable = mContext.getResources()
                    .getColor(R.color.met_green);
            viewHolder.item_id_selected_bar.setVisibility(View.GONE);
            if (position == LAST_SELECTED_POS) {
                viewHolder.item_name.setBackgroundColor(text_bg_enable);
            } else {
                viewHolder.item_name.setBackgroundColor(text_bg_disable);
            }
        } else {
            viewHolder.item_id_selected_bar.setVisibility(View.VISIBLE);

            if (position == LAST_SELECTED_POS) {
                viewHolder.item_id_selected_bar.setBackgroundColor(mContext
                        .getResources().getColor(R.color.met_green));
            } else {
                viewHolder.item_id_selected_bar.setBackgroundColor(mContext
                        .getResources().getColor(android.R.color.transparent));
            }
        }

        if (salesItemListType.equalsIgnoreCase("2")) {
            viewHolder.amt_row.setVisibility(View.GONE);
            viewHolder.item_amt.setVisibility(View.GONE);
            viewHolder.item_qty_layout.setVisibility(View.GONE);
            viewHolder.item_rate_layout.setVisibility(View.GONE);
        } else if (salesItemListType.equalsIgnoreCase("3")) {
            viewHolder.item_amt.setVisibility(View.GONE);
            viewHolder.amt_row.setVisibility(View.GONE);
        } else {
            viewHolder.item_amt.setVisibility(View.VISIBLE);
            viewHolder.item_image_layout.setVisibility(View.VISIBLE);
            viewHolder.amt_row.setVisibility(View.VISIBLE);
            viewHolder.item_rate_layout.setVisibility(View.VISIBLE);
            viewHolder.item_qty_layout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void remove(int position) {
        mDataSet.remove(position);
        notifyItemRemoved(position);
    }

    public void add(TrItems items, int position) {
        mDataSet.add(position, items);
        notifyItemInserted(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView item_id, item_tid, item_name, item_rate, item_amt,
                item_qty, item_status_, item_durations, item_id_selected_bar;
        private ImageView item_pic;
        private RelativeLayout item_image_layout;
        private TableRow amt_row;
        private LinearLayout item_container, item_rate_layout, item_qty_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            item_container = (LinearLayout) itemView
                    .findViewById(R.id.item_container);
            item_rate_layout = (LinearLayout) itemView
                    .findViewById(R.id.item_rate_layout);
            item_qty_layout = (LinearLayout) itemView
                    .findViewById(R.id.item_qty_layout);
            item_image_layout = (RelativeLayout) itemView
                    .findViewById(R.id.item_img_layout);
            amt_row = (TableRow) itemView.findViewById(R.id.tableRow2);
            item_id = (TextView) itemView.findViewById(R.id.item_id_);
            item_tid = (TextView) itemView.findViewById(R.id.item_tid);
            item_name = (TextView) itemView.findViewById(R.id.item_name);
            item_rate = (TextView) itemView.findViewById(R.id.item_rate);
            item_amt = (TextView) itemView.findViewById(R.id.item_amt);
            item_qty = (TextView) itemView.findViewById(R.id.item_qty);
            item_status_ = (TextView) itemView.findViewById(R.id.item_status_);
            item_durations = (TextView) itemView
                    .findViewById(R.id.item_durations);
            item_id_selected_bar = (TextView) itemView
                    .findViewById(R.id.item_id_selected_bar);
            item_pic = (ImageView) itemView.findViewById(R.id.item_img);
        }
    }
}
