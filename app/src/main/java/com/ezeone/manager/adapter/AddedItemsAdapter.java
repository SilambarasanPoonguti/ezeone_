package com.ezeone.manager.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ezeone.R;
import com.ezeone.manager.pojo.AddedItems;

import java.util.List;

public class AddedItemsAdapter extends ArrayAdapter<AddedItems> {

    private Context context;

    public AddedItemsAdapter(Context context, List<AddedItems> addedItems) {
        super(context, 0, addedItems);
        // TODO Auto-generated constructor stub
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // get the data item for this position
        AddedItems items = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (null == convertView) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_ezeid_sales_items, parent, false);
        }

        final int pos = position;
        final TextView item_id = (TextView) convertView
                .findViewById(R.id.item_id_);
        final TextView item_tid = (TextView) convertView
                .findViewById(R.id.item_tid);
        final TextView item_name = (TextView) convertView
                .findViewById(R.id.item_name);
        final TextView item_rate = (TextView) convertView
                .findViewById(R.id.item_rate);
        final TextView item_amt = (TextView) convertView
                .findViewById(R.id.item_amt);
        final TextView item_qty = (TextView) convertView
                .findViewById(R.id.item_qty);
        final TextView item_status = (TextView) convertView
                .findViewById(R.id.item_status_);
        final TextView item_durations = (TextView) convertView
                .findViewById(R.id.item_durations);
        final TextView item_message_id = (TextView) convertView
                .findViewById(R.id.item_message_id);
        ImageView item_img = (ImageView) convertView
                .findViewById(R.id.item_img);
        item_id.setText(items.getItem_id());
        item_tid.setText(items.getItem_tid());
        item_name.setText(items.getItem_name());
        item_rate.setText(items.getItem_rate());
        item_amt.setText(items.getItem_amt());
        item_qty.setText(items.getItem_qty());
        item_status.setText(items.getItem_status());
        item_durations.setText(items.getItem_durations());
        item_message_id.setText(items.getItem_message_id());
        item_img.setImageBitmap(items.getItem_img());

        BitmapDrawable drawable = (BitmapDrawable) item_img.getDrawable();
        final Bitmap bitmap = drawable.getBitmap();

        return convertView;
    }
}
