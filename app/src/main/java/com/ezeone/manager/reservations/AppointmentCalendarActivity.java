package com.ezeone.manager.reservations;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ezeone.R;
import com.ezeone.helpers.Constant;
import com.ezeone.manager.reservations.services.WeekView;
import com.ezeone.manager.reservations.services.WeekViewEvent;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.floatingaction.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

public class AppointmentCalendarActivity extends AppCompatActivity implements
        WeekView.MonthChangeListener, WeekView.EventClickListener,
        WeekView.EventLongPressListener, WeekView.OnDateChanged,
        DatePickerDialog.OnDateSetListener, WeekView.EmptyViewClickListener {

    private static final int TYPE_DAY_VIEW = 1;
    private int mWeekViewType = TYPE_DAY_VIEW;
    private WeekView mWeekView;
    private Calendar c;
    private FloatingActionButton addAppointment, calendarBtn;
    private int hour;
    private List<HashMap<String, String>> resources;
    private Spinner resourceSpinner;
    private String TAG = getClass().getSimpleName();
    private Button goToday;

    public static final String DATEPICKER_TAG = "datepicker";
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private TextView dateText;
    private SimpleDateFormat sdf, sdf2, timeFormat, formatForCalendar;
    private EzeidPreferenceHelper mgr;
    private String[] colors = new String[]{"#1abc9c", "#2ecc71", "#e67e22",
            "#3498db", "#c0392b", "#9b59b6", "#2c3e50", "#EC407A", "#4DD0E1",
            "#FF8A65"};
    private EzeidLoadingProgress ezeidLoadingProgress;
    private Handler handlerDialog;
    private Runnable runnableDialog;
    private Date selectedDate;
    private Toolbar toolbar;
    private EzeidPreferenceHelper ezeidPreferenceHelper;
    List<WeekViewEvent> events = new ArrayList<>();
    WeekViewEvent event;
    private Date fromTime1, toTime1, fromTime2, toTime2;
    private String reserverName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_appointment);

        sdf = new SimpleDateFormat("dd/MMM");
        sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        timeFormat = new SimpleDateFormat("HH:mm:ss");

        toolbar = (Toolbar) findViewById(R.id.appointment_toolbar);
        goToday = (Button) findViewById(R.id.goToday);

        toolbar.setTitle(Html.fromHtml("RESERVATION"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(
                R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ezeidPreferenceHelper = new EzeidPreferenceHelper(
                AppointmentCalendarActivity.this);
        calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), false);

        formatForCalendar = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        mgr = new EzeidPreferenceHelper(getApplicationContext());
        resources = new ArrayList<HashMap<String, String>>();
        if (EzeidUtil.isConnectedToInternet(getApplicationContext())) {
            ShowEzeidDialog("Loading Resources. Please wait");
            String bus_ezeid = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("BusinessEZEID");
            NetworkHandler.GetResources(TAG, bus_ezeid, handler);
        }

        mWeekView = (WeekView) findViewById(R.id.resWeekView);
        calendarBtn = (FloatingActionButton) findViewById(R.id.resvCalender);

        c = Calendar.getInstance();
        hour = c.get(c.HOUR_OF_DAY);

        // Show a toast message about the touched event.
        mWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide
        // the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);
        mWeekView.setOnDateChangedListener(this);
        mWeekView.setEmptyViewClickListener(this);

        mWeekViewType = TYPE_DAY_VIEW;
        mWeekView.setNumberOfVisibleDays(1);
        mWeekView.setColumnGap((int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 8, getResources()
                        .getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, 12, getResources()
                        .getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, 12, getResources()
                        .getDisplayMetrics()));
        mWeekView.goToHour(hour);

        goToday.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mWeekView.goToToday();

                ShowEzeidDialog("Loading");

                String bus_ezeid = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("BusinessEZEID");
                NetworkHandler.GetReservation(
                        TAG,
                        bus_ezeid,
                        resources.get(resourceSpinner.getSelectedItemPosition()).get(
                                "tid"), sdf2.format(new Date()), handler);
            }
        });

        resourceSpinner = (Spinner) findViewById(R.id.resourcesSpinner);

        addAppointment = (FloatingActionButton) findViewById(R.id.resvAddAppointment);
//		addAppointment.setColorNormal(Color.parseColor("#d14233"));
//		addAppointment.setColorPressed(Color.parseColor("#e74c3c"));
//		addAppointment.setIcon(R.drawable.ic_action_increase);

        addAppointment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addAppointment = new Intent(getApplicationContext(),
                        AddAppointment.class);
                addAppointment.putExtra("time", Calendar.getInstance());
                addAppointment.putExtra(
                        "resource",
                        ""
                                + resources.get(
                                resourceSpinner
                                        .getSelectedItemPosition())
                                .get("title"));
                addAppointment.putExtra(
                        "resourceId",
                        ""
                                + resources.get(
                                resourceSpinner
                                        .getSelectedItemPosition())
                                .get("tid"));
                addAppointment.putExtra("internal", false);
                addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("BusinessEZEID"));
                startActivity(addAppointment);
            }
        });

        calendarBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDatePicker();
            }
        });

    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {

		/*
         * // Populate the week view with some events. List<WeekViewEvent>
		 * events = new ArrayList<WeekViewEvent>(); WeekViewEvent event;
		 * //WeekViewEvent k = new WeekViewEvent(id, name, startYear,
		 * startMonth, startDay, startHour, startMinute, endYear, endMonth,
		 * endDay, endHour, endMinute) if(newMonth==5){ event = new
		 * WeekViewEvent(1, "Meeting", 2015, 05, 22, 3, 0, 2015, 05, 22, 4, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(2, "Webinar", 2015, 05, 22, 6, 0, 2015, 05,
		 * 22, 8, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_02));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(3, "JAVA", 2015, 05, 22, 10, 0, 2015, 05,
		 * 22, 12, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_03));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(4, "Lunch", 2015, 05, 22, 13, 0, 2015, 05,
		 * 22, 14, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 22, 17, 0, 2015,
		 * 05, 22, 19, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_03));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(6, "IOS", 2015, 05, 22, 22, 0, 2015, 05,
		 * 22, 24, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * 
		 * event = new WeekViewEvent(4, "Lunch", 2015, 05, 20, 4, 0, 2015, 05,
		 * 20, 5, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 20, 17, 0, 2015,
		 * 05,20, 19, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(6, "IOS", 2015, 05, 20, 19, 0, 2015, 05,
		 * 20, 20, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(4, "IOS", 2015, 05, 21, 15, 0, 2015, 05,
		 * 21, 20, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 21, 21, 0, 2015,
		 * 05,21, 23, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 26, 12, 32, 2015,
		 * 05,26, 12, 40);
		 * event.setColor(getResources().getColor(R.color.event_color_02));
		 * events.add(event);
		 * 
		 * }
		 */

        return events;
    }

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.arg1) {
                case Constant.MessageState.RESOURCES_AVAILABLE:
                    parseResources((JSONObject) msg.obj);
                    break;

                case Constant.MessageState.RESOURCES_UNAVAILABLE:
                    DismissEzeidDialog();
                    break;

                case Constant.MessageState.RESERVATION_DATA_AVAILABLE:
                    parseReservationData((JSONObject) msg.obj);
                    break;

                case Constant.MessageState.RESERVATION_DATA_NOT_AVAILABLE:
                    DismissEzeidDialog();
                    break;


                case Constant.MessageState.RESERVATION_DETAILS_AVAILABLE:
                    parseReservationDetails((JSONObject) msg.obj);
                    break;


                case Constant.MessageState.RESERVATION_DETAILS_NOT_AVAILABLE:
                    break;

                default:
                    break;
            }
        }

        ;
    };

    @SuppressLint("DefaultLocale")
    private String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d",
                time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE),
                time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {

        if (event.getName().toUpperCase().contains(
                mgr.GetValueFromSharedPrefs("EZEID").toUpperCase())) {

            ShowEzeidDialog("Loading");

            NetworkHandler.GetReservationDetails(TAG, "" + event.getId(), handler);

        } else {
            Toast.makeText(AppointmentCalendarActivity.this,
                    "This slot is reserved", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
    }

    private void parseResources(JSONObject obj) {
        DismissEzeidDialog();

        try {
            HashMap<String, String> map;

            if (!obj.isNull("data")) {
                JSONArray arr = obj.getJSONArray("data");
                if (arr.length() > 0) {
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj1 = arr.getJSONObject(i);
                        map = new HashMap<String, String>();
                        map.put("tid", obj1.getString("tid"));
                        map.put("title", obj1.getString("title"));
                        resources.add(map);
                    }

                    final SimpleAdapter adapter = new SimpleAdapter(
                            getApplicationContext(), resources,
                            R.layout.country_list_item, new String[]{"title",
                            "tid"}, new int[]{R.id.countryTitle,
                            R.id.countryId});
                    resourceSpinner.setAdapter(adapter);


                    resourceSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parent,
                                                   View view, int position, long id) {

                            String bus_ezeid = ezeidPreferenceHelper
                                    .GetValueFromSharedPrefs("BusinessEZEID");

                            ShowEzeidDialog("Loading Data..");
                            NetworkHandler.GetReservation(TAG, bus_ezeid,
                                    resources.get(resourceSpinner.getSelectedItemPosition())
                                            .get("tid"), sdf2.format(new Date()), handler);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                } else {
                    EzeidUtil.showToast(AppointmentCalendarActivity.this, "There are no resources to serve you.");
                }
            } else {
                EzeidUtil.showToast(AppointmentCalendarActivity.this, "There are no resources to serve you.");
            }
            selectedDate = new Date();

            String bus_ezeid = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("BusinessEZEID");
            NetworkHandler.GetReservation(TAG, bus_ezeid,
                    resources.get(resourceSpinner.getSelectedItemPosition())
                            .get("tid"), sdf2.format(new Date()), handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void parseReservationDetails(JSONObject obj) {
        try {
//			Log.e("Response", ""+obj);

			/*{"message":"Reservation Trans details Send successfully",
			 * "error":null,"data":
			 * [{"endtime":"11:33:00",
			 * "reserverName":"MMindustries",
			 * "Status":0,
			 * "duration":10,
			 * "TID":1,
			 * "reserverId":171,
			 * "ContactInfo":"9886902446",
			 * "service":"Service2,Service3",
			 * "serviceids":"16,17",
			 * "Starttime":"11:23:00",
			 * "mResResourceID":21}],
			 * "status":true}*/

            DismissEzeidDialog();

            if (!obj.isNull("status")) {
                if (obj.getBoolean("status")) {
                    if (!obj.isNull("data")) {
                        JSONArray dataArr = obj.getJSONArray("data");
                        for (int i = 0; i < dataArr.length(); i++) {
                            String resourceId = dataArr.getJSONObject(i).getString("mResResourceID");
                            String contactDetails = dataArr.getJSONObject(i).getString("ContactInfo");
                            String Status = dataArr.getJSONObject(i).getString("Status");
                            String startTime = dataArr.getJSONObject(i).getString("Starttime");
                            String endTime = dataArr.getJSONObject(i).getString("endtime");
                            String tid = dataArr.getJSONObject(i).getString("TID");

                            Intent intent = new Intent(getApplicationContext(), AddAppointment.class);
                            intent.putExtra("resourceId", resourceId);
                            intent.putExtra("contactDetails", contactDetails);
                            intent.putExtra("Status", Status);
                            intent.putExtra("startTime", "" + timeFormat.format(new Date(timeFormat.parse(startTime)
                                    .getTime()
                                    + TimeZone.getDefault().getOffset(
                                    new Date().getTime()))));
                            intent.putExtra("endTime", "" + timeFormat.format(new Date(timeFormat.parse(endTime)
                                    .getTime()
                                    + TimeZone.getDefault().getOffset(
                                    new Date().getTime()))));
                            intent.putExtra("tid", tid);
                            intent.putExtra("resourceId", resourceId);
                            intent.putExtra("internal", true);
                            intent.putExtra(
                                    "resource",
                                    ""
                                            + resources.get(
                                            resourceSpinner
                                                    .getSelectedItemPosition())
                                            .get("title"));
                            intent.putExtra("isUpdate", true);
                            intent.putExtra("service", dataArr.getJSONObject(i).getString("service"));
                            intent.putExtra("ownReservation", true);
                            startActivity(intent);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void parseReservationData(JSONObject obj) {
        events.clear();
        DismissEzeidDialog();

        try {
            Date startDate, endDate;
            timeFormat.setTimeZone(TimeZone.getDefault());
            if (obj.getBoolean("status")) {
                if (!obj.isNull("data")) {
                    JSONArray dataArr = obj.getJSONArray("data");
                    for (int i = 0; i < dataArr.length(); i++) {
                        JSONObject dataObj = dataArr.getJSONObject(i);

                        fromTime1 = timeFormat.parse(dataObj.getString("W1"));
                        toTime1 = timeFormat.parse(dataObj.getString("W2"));
                        fromTime2 = timeFormat.parse(dataObj.getString("W3"));
                        toTime2 = timeFormat.parse(dataObj.getString("W4"));

                        startDate = timeFormat.parse(dataObj
                                .getString("Starttime"));
                        endDate = timeFormat
                                .parse(dataObj.getString("endtime"));

                        Calendar startCal = Calendar.getInstance();
                        startCal.setTime(new Date(startDate.getTime()
                                + TimeZone.getDefault().getOffset(
                                new Date().getTime())));

                        Calendar endCal = Calendar.getInstance();
                        endCal.setTime(new Date(endDate.getTime()
                                + TimeZone.getDefault().getOffset(
                                new Date().getTime())));

                        Calendar selected = Calendar.getInstance();
                        selected.setTime(selectedDate);
                        selected.add(selected.MONTH, 1);

                        reserverName = dataObj.getString("reserverName");

                        if (dataObj.getString("reserverName").equalsIgnoreCase(
                                mgr.GetValueFromSharedPrefs("EZEID"))) {
                            event = new WeekViewEvent(dataObj.getInt("TID"), ""
                                    + dataObj.getString("ContactInfo") + " - "
                                    + dataObj.getString("service") + " - " + dataObj.getString("reserverName"),
                                    selected.get(selected.YEAR),
                                    selected.get(selected.MONTH),
                                    selected.get(selected.DATE),
                                    startCal.get(startCal.HOUR_OF_DAY),
                                    startCal.get(startCal.MINUTE),
                                    selected.get(selected.YEAR),
                                    selected.get(selected.MONTH),
                                    selected.get(selected.DATE),
                                    endCal.get(endCal.HOUR_OF_DAY),
                                    endCal.get(endCal.MINUTE));

                            int idx = new Random().nextInt(colors.length);
                            String random = (colors[idx]);
                            event.setColor(Color.parseColor(random));
                            events.add(event);

                        } else {
                            event = new WeekViewEvent(dataObj.getInt("TID"),
                                    "RESERVED", selected.get(selected.YEAR),
                                    selected.get(selected.MONTH),
                                    selected.get(selected.DATE),
                                    startCal.get(startCal.HOUR_OF_DAY),
                                    startCal.get(startCal.MINUTE),
                                    selected.get(selected.YEAR),
                                    selected.get(selected.MONTH),
                                    selected.get(selected.DATE),
                                    endCal.get(endCal.HOUR_OF_DAY),
                                    endCal.get(endCal.MINUTE));

                            event.setColor(getResources().getColor(
                                    R.color.event_color_02));
                            events.add(event);
                        }
                    }
                    mWeekView.notifyDatasetChanged();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDateChanged(Date date) {
        ShowEzeidDialog("Loading data.");
        selectedDate = date;
        String bus_ezeid = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("BusinessEZEID");
        NetworkHandler.GetReservation(
                TAG,
                bus_ezeid,
                resources.get(resourceSpinner.getSelectedItemPosition()).get(
                        "tid"), sdf2.format(date), handler);
    }

    private void ShowEzeidDialog(String content) {
        ezeidLoadingProgress = new EzeidLoadingProgress(
                AppointmentCalendarActivity.this, content);
        handlerDialog = new Handler();
        runnableDialog = new Runnable() {
            @Override
            public void run() {
                if (ezeidLoadingProgress != null) {
                    if (ezeidLoadingProgress.isShowing()) {
                        ezeidLoadingProgress.dismiss();
                    }
                }
            }
        };
        ezeidLoadingProgress.show();
    }

    private void DismissEzeidDialog() {
        handlerDialog.removeCallbacks(runnableDialog);
        if (ezeidLoadingProgress.isShowing()) {
            ezeidLoadingProgress.dismiss();
        }
    }

    private void GetDatePicker() {
        datePickerDialog.setVibrate(false);
        datePickerDialog.setYearRange(1985, 2028);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year,
                          int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        mWeekView.goToDate(cal);

        ShowEzeidDialog("Loading.");

        String bus_ezeid = mgr
                .GetValueFromSharedPrefs("EZEID");
        NetworkHandler.GetReservation(
                TAG,
                bus_ezeid,
                resources.get(resourceSpinner.getSelectedItemPosition()).get(
                        "tid"), sdf2.format(cal.getTime()), handler);

    }

    @Override
    public void onEmptyViewClicked(Calendar time) {
        try {

            if (mWeekView.setAvailableSlots(timeFormat.parse(timeFormat.format(new Date(fromTime1.getTime()
                    + TimeZone.getDefault().getOffset(
                    new Date().getTime())))), timeFormat.parse(timeFormat.format(new Date(toTime1.getTime()
                    + TimeZone.getDefault().getOffset(
                    new Date().getTime())))), timeFormat.parse(timeFormat.format(time.getTime())))) {

                if (resources.size() > 0) {
                    Intent addAppointment = new Intent(getApplicationContext(),
                            AddAppointment.class);
                    addAppointment.putExtra("BUSINESS_ADD", "internal");
                    addAppointment.putExtra("time", time);
                    addAppointment.putExtra(
                            "resource",
                            ""
                                    + resources.get(
                                    resourceSpinner.getSelectedItemPosition())
                                    .get("title"));
                    addAppointment.putExtra(
                            "resourceId",
                            ""
                                    + resources.get(
                                    resourceSpinner.getSelectedItemPosition())
                                    .get("tid"));
                    addAppointment.putExtra("internal", true);
                    addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("EZEID"));
                    addAppointment.putExtra("isUpdate", false);
                    startActivity(addAppointment);
                }


            } else {
                Toast.makeText(getApplicationContext(), "This time is not available for reservation", Toast.LENGTH_SHORT).show();

                if (mWeekView.setAvailableSlots(timeFormat.parse(timeFormat.format(new Date(fromTime2.getTime()
                        + TimeZone.getDefault().getOffset(
                        new Date().getTime())))), timeFormat.parse(timeFormat.format(new Date(toTime2.getTime()
                        + TimeZone.getDefault().getOffset(
                        new Date().getTime())))), timeFormat.parse(timeFormat.format(time.getTime())))) {

                    if (resources.size() > 0) {
                        Intent addAppointment = new Intent(getApplicationContext(),
                                AddAppointment.class);
                        addAppointment.putExtra("BUSINESS_ADD", "internal");
                        addAppointment.putExtra("time", time);
                        addAppointment.putExtra(
                                "resource",
                                ""
                                        + resources.get(
                                        resourceSpinner.getSelectedItemPosition())
                                        .get("title"));
                        addAppointment.putExtra(
                                "resourceId",
                                ""
                                        + resources.get(
                                        resourceSpinner.getSelectedItemPosition())
                                        .get("tid"));
                        addAppointment.putExtra("internal", true);
                        addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("EZEID"));
                        addAppointment.putExtra("isUpdate", false);
                        startActivity(addAppointment);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "This time is not available for reservation", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (resources.size() > 0) {
            ShowEzeidDialog("Loading data.");
            NetworkHandler.GetReservation(TAG, mgr
                            .GetValueFromSharedPrefs("EZEID"),
                    resources.get(resourceSpinner.getSelectedItemPosition())
                            .get("tid"), sdf2.format(selectedDate), handler);
        }
    }

}
