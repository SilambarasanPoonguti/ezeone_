package com.ezeone.manager.reservations;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.adapters.DropdownAdapter;
import com.ezeone.helpers.Constant;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.manager.reservations.adapter.Service;
import com.ezeone.manager.reservations.adapter.ServiceAdapter;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.networkhandler.ResponseListener;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.date.time.RadialPickerLayout;
import com.ezeonelib.date.time.TimePickerDialog;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class AddAppointment extends AppCompatActivity implements
		DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

	private Bundle bundle;
	private TextView dateTime, details, resourceName;
	private Calendar time;
	private AppCompatButton save, checkEzeid;
	private EditText appointEzeid, appointmentDateTime;
	private EzeidPreferenceHelper mgr;
	private String TAG = getClass().getSimpleName();
	private DatePickerDialog datePickerDialog;
	private TimePickerDialog timePickerDialog;
	private Calendar calendar;
	public static final String DATEPICKER_TAG = "datepicker";
	public static final String TIMEPICKER_TAG = "timepicker";
	private String dateStr, timeStr;
	private List<Service> services;
	private Button serviceSpinner;
	private int TID = 0;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	private SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
	private ServiceAdapter adapter;
	private Gson gson;
	private AlertDialog alert;
	private Builder builder;
	private StringBuilder serviceArray;
	private int totalAmount, totalDuration, status;
	private int[] statusArray = new int[] { 0, 10, 11,12};
	private Spinner statusSpinner;
	private Toolbar toolbar;
	private EzeidLoadingProgress ezeidLoadingProgress;
	private Handler handlerDialog;
	private Runnable runnableDialog;
	private TextView statusLabel;
	private String ezeid;
	private TextView startTime,endTime,serviceText;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_appointment);

		bundle = getIntent().getExtras();
		builder = new Builder(this);

		toolbar = (Toolbar) findViewById(R.id.appointment_toolbar_actionbar);

		toolbar.setTitle(Html.fromHtml("RESERVATION"));
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		toolbar.setNavigationIcon(getResources().getDrawable(
				R.drawable.ic_action_back));
		toolbar.setNavigationOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AddAppointment.this.finish();
			}
		});

		dateTime = (TextView) findViewById(R.id.dateTime);
		details = (TextView) findViewById(R.id.details);
		resourceName = (TextView) findViewById(R.id.resourceName);
		services = new ArrayList<Service>();
		serviceSpinner = (Button) findViewById(R.id.serviceSpinner);
		statusSpinner = (Spinner) findViewById(R.id.appointmentStatus);
		statusLabel = (TextView) findViewById(R.id.statusLabel);
		appointEzeid = (EditText) findViewById(R.id.appointmentFor);
		
		startTime = (TextView) findViewById(R.id.startTime);
		endTime = (TextView) findViewById(R.id.endTime);
		serviceText = (TextView) findViewById(R.id.servicesText);
		appointmentDateTime = (EditText) findViewById(R.id.appointmentDateTime);
		

		save = (AppCompatButton) findViewById(R.id.saveAppointment);
		gson = new Gson();
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

		mgr = new EzeidPreferenceHelper(getApplicationContext());
		serviceArray = new StringBuilder();

//		Log.e("Token", mgr.GetValueFromSharedPrefs("Token"));

		serviceSpinner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showServiceDialog();
			}
		});

		statusSpinner.setAdapter(new DropdownAdapter(getApplicationContext(),
				R.layout.category_list_item, getResources().getStringArray(
						R.array.appointmentStatus)));

		statusSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				status = statusArray[position];
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		if (bundle != null) {
			time = (Calendar) bundle.getSerializable("time");
//			Log.e("Bundle raja", "" + bundle.getString("resource") + " - "
//					+ bundle.getString("resourceId"));
			
			ShowEzeidDialog("Loading services");
			
			ezeid = bundle.getString("ezeid");
			
			if(bundle.getBoolean("ownReservation")){
				
				NetworkHandler.GetMappedServices(TAG,
						ezeid,
						bundle.getString("resourceId"), handler);
				resourceName.setText("Selected: " + bundle.getString("resource"));

				if (bundle.getBoolean("internal") == false) {
					statusSpinner.setVisibility(View.INVISIBLE);
					statusLabel.setVisibility(View.INVISIBLE);
				}
				
				
				appointEzeid.setText(bundle.getString("contactDetails"));
				if(bundle.getBoolean("isUpdate")){
					appointmentDateTime.setVisibility(View.GONE);
					startTime.setVisibility(View.VISIBLE);
					endTime.setVisibility(View.VISIBLE);
					startTime.setText("Start time - "+bundle.getString("startTime"));
					endTime.setText("End time - "+bundle.getString("endTime"));
					serviceSpinner.setVisibility(View.GONE);
					serviceText.setVisibility(View.VISIBLE);
					serviceText.setText(""+ bundle.getString("service"));
					appointEzeid.setEnabled(false);
					
//					Log.e("Status", bundle.getString("Status"));
					
					for(int i=0;i<statusArray.length;i++){
						if(statusArray[i]==Integer.parseInt(bundle.getString("Status"))){
							statusSpinner.setSelection(i);
						}
					}
				}
			}else{
				NetworkHandler.GetMappedServices(TAG,
						ezeid,
						bundle.getString("resourceId"), handler);
				resourceName.setText("Selected: " + bundle.getString("resource"));

				if (bundle.getBoolean("internal") == false) {
					statusSpinner.setVisibility(View.INVISIBLE);
					statusLabel.setVisibility(View.INVISIBLE);
				}
				
				
				appointEzeid.setText(bundle.getString("contactDetails"));
				if(bundle.getBoolean("isUpdate")){
					appointmentDateTime.setVisibility(View.GONE);
					startTime.setVisibility(View.VISIBLE);
					endTime.setVisibility(View.VISIBLE);
					startTime.setText("Start time - "+bundle.getString("startTime"));
					endTime.setText("End time - "+bundle.getString("endTime"));
					serviceSpinner.setVisibility(View.GONE);
					serviceText.setVisibility(View.VISIBLE);
					serviceText.setText(""+ bundle.getString("service"));
					appointEzeid.setEnabled(false);
					
//					Log.e("Status", bundle.getString("Status"));
					
					for(int i=0;i<statusArray.length;i++){
						if(statusArray[i]==Integer.parseInt(bundle.getString("Status"))){
							statusSpinner.setSelection(i);
						}
					}
				}
			}
			
			/*NetworkHandler.GetMappedServices(TAG,
					ezeid,
					bundle.getString("resourceId"), handler);
			resourceName.setText("Selected: " + bundle.getString("resource"));

			if (bundle.getBoolean("internal") == false) {
				statusSpinner.setVisibility(View.INVISIBLE);
				statusLabel.setVisibility(View.INVISIBLE);
			}
			
			appointEzeid.setText(bundle.getString("contactDetails"));
			if(bundle.getBoolean("isUpdate")){
				appointmentDateTime.setVisibility(View.GONE);
				startTime.setVisibility(View.VISIBLE);
				endTime.setVisibility(View.VISIBLE);
				startTime.setText("Start time - "+bundle.getString("startTime"));
				endTime.setText("End time - "+bundle.getString("endTime"));
				serviceSpinner.setVisibility(View.GONE);
				serviceText.setVisibility(View.VISIBLE);
				serviceText.setText(""+ bundle.getString("service"));
				appointEzeid.setEnabled(false);
				
				Log.e("Status", bundle.getString("Status"));
				
				for(int i=0;i<statusArray.length;i++){
					if(statusArray[i]==Integer.parseInt(bundle.getString("Status"))){
						statusSpinner.setSelection(i);
					}
				}
			}*/
		}else{
//			Log.e("Bundle ", "is null");
		}

		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(bundle.getBoolean("isUpdate")){
					
					ShowEzeidDialog("Saving");
					Map<String,String> params = new HashMap<String, String>();
					params.put("Token", mgr.GetValueFromSharedPrefs("Token"));
					params.put("tid", bundle.getString("tid"));
					params.put("status", ""+statusArray[statusSpinner.getSelectedItemPosition()]);
					
//					Log.e("params", ""+params);
					
					NetworkHandler.UpdateReservatioStatus(TAG, handler, params);
				}else{
					
					if (!appointEzeid.getText().toString().isEmpty()) {
						if (serviceArray.length() != 0) {

							ShowEzeidDialog("Saving");
							String parseService = serviceArray.toString();
							if (parseService.length() > 0
									&& parseService.charAt(parseService.length() - 1) == ',') {
								parseService = parseService.substring(0, parseService.length() - 1);
							}
							sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
							Map<String, String> params = new HashMap<String, String>();
							params.put("TID", "" + TID);
							params.put("Token",
									mgr.GetValueFromSharedPrefs("Token"));
							params.put("contactinfo", ""
									+ appointEzeid.getText().toString());
							params.put("toEzeid",
									"" + ezeid);
							params.put("resourceid", bundle.getString("resourceId"));
							params.put(
									"res_datetime",
									""
											+ sdf.format(new Date(
													appointmentDateTime.getText()
															.toString())));
							params.put("duration", "" + totalDuration);
							params.put("status", "" + status);
							params.put("serviceid", "" + serviceArray);

//							Log.e("----", "" + params);

//							NetworkHandler.SaveReservation(TAG, handler, params);
							saveTransaction(params);
							
						} else {
							EzeidUtil.showToast(AddAppointment.this,
									"Select atleast one service");
						}
					} else {
						appointEzeid.setError("Required");
					}
				}
			}
		});

		calendar = Calendar.getInstance();
		datePickerDialog = DatePickerDialog.newInstance(this,
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), false);

		timePickerDialog = TimePickerDialog.newInstance(this,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), true);

		checkEzeid = (AppCompatButton) findViewById(R.id.checkAppointEzeid);
		
		appointEzeid = (EditText) findViewById(R.id.appointmentFor);
		if(time!=null){
			appointmentDateTime.setText("" + sdf2.format(time.getTime()));
		}
		appointmentDateTime.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GetDatePicker();
			}
		});

		checkEzeid.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				details.setVisibility(View.INVISIBLE);

				if (!appointEzeid.getText().toString().isEmpty()) {
					NetworkHandler.GetPrimaryDetails(TAG, mgr
							.GetValueFromSharedPrefs("Token"), appointEzeid
							.getText().toString(), handler);
				} else {
					appointEzeid.setError("Required");
				}
			}
		});

		appointEzeid.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.toString().length() == 0) {
					details.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.arg1) {
			case Constant.MessageState.PRIMARY_DETAILS_AVAILABLE:
				parsePrimaryDetails((JSONArray) msg.obj);
				break;

			case Constant.MessageState.PRIMARY_DETAILS_NOTAVAILABLE:
				DismissEzeidDialog();
				parseError((VolleyError) msg.obj);
				break;

			case Constant.MessageState.SERVICES_AVAILABLE:
				parseServices((JSONObject) msg.obj);
				break;

			case Constant.MessageState.SERVICES_UNAVAILABLE:
				DismissEzeidDialog();
//				Log.e("SERVICES", "" + (VolleyError) msg.obj);
				parseError((VolleyError) msg.obj);
				break;

			case Constant.MessageState.RESERVATION_SAVED:
//				DismissEzeidDialog();
				parseSaveResponse((JSONObject) msg.obj);
				break;

			case Constant.MessageState.RESERVATION_SAVE_FAILED:
				DismissEzeidDialog();
				break;
				
			case Constant.MessageState.RESERVATION_DETAILS_UPDATED:
				parseUpdateResponse((JSONObject)msg.obj);
				break;
				
			case Constant.MessageState.RESERVATION_DETAILS_UPDATE_FAILED:
				break;

			default:
				break;
			}
		};
	};

	private void parsePrimaryDetails(JSONArray arr) {
		try {
			if (arr.length() > 0) {
				details.setVisibility(View.VISIBLE);
				for (int i = 0; i < arr.length(); i++) {
					JSONObject obj = arr.getJSONObject(i);
					details.setText("Name: " + obj.getString("FirstName") + " "
							+ obj.getString("LastName") + "- Phone No: "
							+ obj.getString("MobileNumber"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void GetDatePicker() {
		datePickerDialog.setCancelable(false);
		datePickerDialog.setVibrate(false);
		datePickerDialog.setYearRange(1985, 2028);
		datePickerDialog.setCloseOnSingleTapDay(false);
		datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		dateStr = (month + 1) + "/" + day + "/" + year;
		timePickerDialog.setVibrate(false);
		timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		timeStr = hourOfDay + ":" + minute;
		appointmentDateTime.setText(dateStr + " " + timeStr);
	}

	private void parseError(VolleyError e) {
		if (e instanceof ParseError) {
			appointEzeid.setError("That Ezeid is not available");
		}
	}

	private void parseServices(JSONObject obj) {
		DismissEzeidDialog();

		try {
//			Log.e("Response parse service", ""+obj);
			if (!obj.isNull("status")) {
				if (obj.getBoolean("status")) {
					JSONArray arr = obj.getJSONArray("data");
					if (arr.length() != 0) {
						for (int i = 0; i < arr.length(); i++) {
							JSONObject dataObj = arr.getJSONObject(i);
							Service service = gson.fromJson(dataObj.toString(),
									Service.class);
							services.add(service);
						}

						/*
						 * adapter = new SimpleAdapter( getApplicationContext(),
						 * services, R.layout.service_list_item, new String[] {
						 * "title","tid","rate","duration" }, new int[] {
						 * R.id.serviceTitle,
						 * R.id.serviceTid,R.id.serviceRate,R.id.serviceDuration
						 * });
						 */

						adapter = new ServiceAdapter(getApplicationContext(),
								R.layout.service_list_item, services);

						builder.setTitle("Select Services");
						builder.setPositiveButton("Select",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										totalAmount = 0;
										totalDuration = 0;
										serviceArray = new StringBuilder();

										for (int i = 0; i < services.size(); i++) {
											if (services.get(i).isChecked()) {
												serviceArray.append(services
														.get(i).getTid());
												serviceArray.append(",");
												totalAmount += services.get(i)
														.getRate();
												totalDuration += services
														.get(i).getDuration();
											}
										}
//										Log.e("Amount and duration", ""
//												+ totalAmount + " - "
//												+ totalDuration);
//										Log.e("Service array", ""
//												+ serviceArray);
									}
								});
						builder.setNegativeButton("cancel",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});

						builder.setAdapter(adapter, null);
						alert = builder.create();

						// serviceSpinner.setAdapter(adapter);
					}
				}else{
					serviceSpinner.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							EzeidUtil.showToast(AddAppointment.this, "There are no services available");
						}
					});
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void parseSaveResponse(JSONObject obj) {
		try {
			if (obj.getBoolean("status") == true) {
				if (!obj.isNull("data")) {
					DismissEzeidDialog();
					Toast.makeText(getApplicationContext(),
							"" + obj.getString("message"), Toast.LENGTH_SHORT)
							.show();
					finish();
				}
			} else {
				DismissEzeidDialog();
				String msg = "" + obj.getString("message");
				EzeidUtil.showToast(getApplicationContext(), "" + msg);
				
			}
		} catch (Exception e) {
			DismissEzeidDialog();
			e.printStackTrace();
		}
	}

	private void showServiceDialog() {
		alert.show();
	}

	private void ShowEzeidDialog(String content) {
		ezeidLoadingProgress = new EzeidLoadingProgress(AddAppointment.this,
				content);
		handlerDialog = new Handler();
		runnableDialog = new Runnable() {

			@Override
			public void run() {
				if (ezeidLoadingProgress != null) {
					if (ezeidLoadingProgress.isShowing()) {
						ezeidLoadingProgress.dismiss();
					}
				}
			}
		};
		ezeidLoadingProgress.show();
	}

	private void DismissEzeidDialog() {
		handlerDialog.removeCallbacks(runnableDialog);
		if (ezeidLoadingProgress.isShowing()) {
			ezeidLoadingProgress.dismiss();
		}
	}
	
	private void saveTransaction(Map<String,String> params){
		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
				Request.Method.POST, EZEIDUrlManager.getAPIUrl() + "reservation_transaction",
				new JSONObject(params),
				ResponseListener.<JSONObject> createGenericReqSuccessListener(
						handler, Constant.MessageState.RESERVATION_SAVED),
				new Response.ErrorListener() {
					public void onErrorResponse(VolleyError error) {
						if(error.networkResponse != null && error.networkResponse.data != null){
							try{
								
								DismissEzeidDialog();
								
				                JSONObject err = new JSONObject(new String(error.networkResponse.data));
				                EzeidUtil.showToast(AddAppointment.this, "" + err.getJSONObject("message"));
							}catch(Exception e){
								
							}
			            }
					}
				});
		
		jsonObjectRequest.setShouldCache(false);
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3 * 1000, 5, 1.0f));
		EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest);
	}
	
	private void parseUpdateResponse(JSONObject obj){
		try{
			/* 06-10 11:40:01.875: E/Update(8751): {"error":null,
			 * "message":"Status changed successfully",
			 * "data":{"status":12,"tid":17},
			 * "status":true}*/
			
			if(!obj.isNull("status")){
				if(obj.getBoolean("status")){
					if(!obj.isNull("data")){
						DismissEzeidDialog();
						Toast.makeText(getApplicationContext(), ""+obj.getString("message"), Toast.LENGTH_SHORT).show();
						finish();
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
