package com.ezeone.manager.reservations.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.ezeone.R;

public class ServiceAdapter extends ArrayAdapter<Service> {

	private List<Service> services;
	private int resource;
	private Context context;

	Service service;

	public ServiceAdapter(Context context, int resource, List<Service> services) {
		super(context, resource, services);
		this.services = services;
		this.resource = resource;
		this.context = context;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		service = getItem(position);

		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(resource, null);

			holder = new ViewHolder();

			holder.title = (TextView) convertView
					.findViewById(R.id.serviceTitle);
			holder.rate = (TextView) convertView.findViewById(R.id.serviceRate);
			holder.duration = (TextView) convertView
					.findViewById(R.id.serviceDuration);
			holder.isSelected = (CheckBox) convertView
					.findViewById(R.id.selectService);
			holder.isSelected.setTag(position);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.title.setText(service.getTitle());
		holder.rate.setText("Rs. "+service.getRate());
		holder.duration.setText(service.getDuration()+" mins");
		holder.isSelected.setSelected(service.isChecked());
		holder.isSelected.setTag(position);

		/*holder.isSelected.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ViewHolder holder = new ViewHolder();
				holder.isSelected = (CheckBox) v
						.findViewById(R.id.selectService);

				int pos = Integer.parseInt((String) holder.isSelected.getTag());
				Log.e("TAG", "" + holder.isSelected.isChecked() + ", Pos: "
						+ pos);
				if (holder.isSelected.isChecked()) {
					service.setChecked(true);
				} else {
					holder.isSelected.setChecked(false);
				}

			}
		});*/

		holder.isSelected
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						Log.e("TAG", "" + buttonView.getTag());
						 services.get((Integer)buttonView.getTag()).setChecked(buttonView.isChecked());
					}
				});

		return convertView;
	}

	private class ViewHolder {
		TextView title, rate, duration;
		CheckBox isSelected;
	}

}
