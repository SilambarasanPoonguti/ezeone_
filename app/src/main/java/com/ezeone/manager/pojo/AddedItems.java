package com.ezeone.manager.pojo;

import android.graphics.Bitmap;

public class AddedItems {

	public String item_id;
	public String item_tid;
	public String item_name;
	public String item_rate;
	public String item_amt;
	public String item_qty;
	public String item_status;
	public String item_durations;
	public Bitmap item_img;
	public String item_message_id;

	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_tid() {
		return item_tid;
	}

	public void setItem_tid(String item_tid) {
		this.item_tid = item_tid;
	}

	public String getItem_name() {
		return item_name;
	}

	public Bitmap getItem_img() {
		return item_img;
	}

	public void setItem_img(Bitmap item_img) {
		this.item_img = item_img;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getItem_rate() {
		return item_rate;
	}

	public void setItem_rate(String item_rate) {
		this.item_rate = item_rate;
	}

	public String getItem_amt() {
		return item_amt;
	}

	public void setItem_amt(String item_amt) {
		this.item_amt = item_amt;
	}

	public String getItem_qty() {
		return item_qty;
	}

	public void setItem_qty(String item_qty) {
		this.item_qty = item_qty;
	}

	public String getItem_status() {
		return item_status;
	}

	public void setItem_status(String item_status) {
		this.item_status = item_status;
	}

	public String getItem_durations() {
		return item_durations;
	}

	public void setItem_durations(String item_durations) {
		this.item_durations = item_durations;
	}

	public String getItem_message_id() {
		return item_message_id;
	}

	public void setItem_message_id(String item_message_id) {
		this.item_message_id = item_message_id;
	}

}
