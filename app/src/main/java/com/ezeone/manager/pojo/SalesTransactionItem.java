package com.ezeone.manager.pojo;

public class SalesTransactionItem {

	public String itemRequester = "";
	public String itemTID = "";
	// public String itemUpdatedDateUser = "";
	public String itemAmount = "";
	public String itemNextActionDate = "";
	public String itemNotes = "";
	public String itemStatus = "";
	public String itemNextActionID = "";
	public String itemTaskDateTime = "";
	public String itemTrnNo = "";
	public String itemFolderRuleID = "";
	public String itemMessage = "";
	public String itemContactInfo = "";
	public String itemLocID = "";
	public String itemUpdatedUser = "";
	public String itemupdatedDate = "";
	public String itemDeliveryAddress = "";
	public String transMsgEZEID = "";
	public String transReqEZEID = "";
	public String transStatusTitle = "";

	public String itemTotalCount = "";
	public String itemFolderTitle = "";
	public String itemStatusTitle = "";
	public String itemEZEID = "";
	public String itemReqEZEID = "";
	public String actionTitle = "";
}