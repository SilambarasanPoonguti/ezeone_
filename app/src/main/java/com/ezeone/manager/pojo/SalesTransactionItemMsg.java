package com.ezeone.manager.pojo;

public class SalesTransactionItemMsg
{
	public String itemAmount = "";
	public String itemLocID = "";
	public String itemRequester = "";
	public String itemTotalCount = "";
	public String itemMessage = "";
	public String itemupdatedDate = "";
	public String itemDeliveryAddress = "";
	public String transStatusTitle ="";
	public String itemNextActionID = "";
	public String itemTrnNo = "";
	public String itemNotes = "";
	public String itemStatus = "";
	public String itemTID = "";
	public String itemUpdatedUser = "";
	public String itemTaskDateTime = "";
	public String itemFolderTitle  ="";
	public String itemNextActionDate = "";
	public String itemContactInfo = "";
	public String itemStatusTitle = "";
	public String itemEZEID = "";
	public String itemReqEZEID = "";
	public String actionTitle = "";
	public String itemFolderRuleID = "";
}
