package com.ezeone.manager.businessfragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.adapters.CountNoAdapter;
import com.ezeone.adapters.EzeidFindByAdapter;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.listeners.RecyclerItemClickListener;
import com.ezeone.manager.adapter.SalesTransactionAdapter;
import com.ezeone.manager.pojo.SalesTransactionItem;
import com.ezeone.manager.salesenquiry.EzeidAddItems;
import com.ezeone.manager.salesenquiry.EzeidAddSalesItems;
import com.ezeone.manager.salesenquiry.EzeidAddSalesMessages;
import com.ezeone.manager.salesenquiry.SalesTransactionDetail;
import com.ezeone.pojo.CountNo;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeonelib.animations.SlideInUpAnimator;
import com.ezeonelib.animations.adapter.AlphaInAnimationAdapter;
import com.ezeonelib.animations.adapter.ScaleInAnimationAdapter;
import com.ezeonelib.floatingaction.ButtonFloatSmall;
import com.ezeonelib.slidepanel.EZEOneSlideUpPanel;

public class ActionSalesFragment extends Fragment implements
        OnItemSelectedListener, OnClickListener, OnRefreshListener,
        RecyclerItemClickListener.OnItemClickListener, EZEOneSlideUpPanel.PanelSlideListener {

    private static final String TAG = ActionSalesFragment.class.getSimpleName();
    private Spinner action_status_filter, action_sort_filter;
    private EzeidPreferenceHelper ezeidPreferenceHelper;
    private ButtonFloatSmall searchKeywordBtn;
    private View searchView_layout_panel, addTransactionView;
    private ImageView noresults_icon, hideSearchImg;
    private ProgressBar transListProgress;
    public EZEOneSlideUpPanel searchPanel;
    private TextView fragmentMsg;
    private ImageButton addTrViewLayout;

    private String salesItemListType = "";

    private SimpleAdapter statusAdapter;

    private String requesterName = "", amount = "", nextActionDate = "",
            notes = "", statusTypeID = "", actionTypeID = "";
    private View countView;
    private RecyclerView countList;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private EditText searchKeywords;
    private EzeidFindByAdapter spinAdapter;
    private RecyclerView transList;
    private static List<SalesTransactionItem> transItems;
    private ArrayList<HashMap<String, String>> statusArray;
    private int page_count = 1;
    private String statusID = "", sort_by = "0";
    private LinearLayoutManager count_lm, tr_lm;

//	private String title;
//	private int page;

    public ActionSalesFragment() {
    }

    public static ActionSalesFragment newInstance(int page, String title) {
        ActionSalesFragment salesFragment = new ActionSalesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("Page", page);
        bundle.putString("Title", title);
        salesFragment.setArguments(bundle);
        return salesFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		page = getArguments().getInt("Page");
//		title = getArguments().getString("Title");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_action_sales_enquiry, container, false);
        ezeidPreferenceHelper = new EzeidPreferenceHelper(getActivity());
        count_lm = new LinearLayoutManager(getActivity());
        count_lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        tr_lm = new LinearLayoutManager(getActivity());
        tr_lm.setOrientation(LinearLayoutManager.VERTICAL);

        countView = rootView.findViewById(R.id.footbar_count);
        countList = (RecyclerView) countView.findViewById(R.id.countList);
        countList.setLayoutManager(count_lm);

        transList = (RecyclerView) rootView.findViewById(R.id.transList);
        transList.setLayoutManager(tr_lm);

        transList.setItemAnimator(new SlideInUpAnimator());
        countList.setItemAnimator(new SlideInUpAnimator());
        countList.setHasFixedSize(true);
        transList.setHasFixedSize(true);

//		Log.i(TAG, "Inflate fragment values:" + title + ", #" + page);
        ezeidPreferenceHelper = new EzeidPreferenceHelper(getActivity());
        InitFragmentComponents(rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        if (isAdded() && activity != null) {
            // InitFragmentComponents();
        }
    }

    private void InitFragmentComponents(View rootView) {

        ezeidPreferenceHelper = new EzeidPreferenceHelper(getActivity());
        searchPanel = (EZEOneSlideUpPanel) rootView.findViewById(
                R.id.sliding_layoutchildse);
        searchPanel.setPanelSlideListener(this);
        searchView_layout_panel = rootView.findViewById(R.id.searchView_layoutse);
        addTransactionView = rootView.findViewById(R.id.addTrBtn);
        addTrViewLayout = (ImageButton) addTransactionView.findViewById(R.id.addtr_main_btn);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView
                .findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.highlightColorOrange, R.color.ff7a32Orange,
                R.color.highlightOrange);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        action_status_filter = (Spinner) searchView_layout_panel
                .findViewById(R.id.action_status_filter);
        action_sort_filter = (Spinner) searchView_layout_panel
                .findViewById(R.id.action_sort_filter);
        noresults_icon = (ImageView) rootView.findViewById(R.id.noresults_icon);
        searchKeywords = (EditText) searchView_layout_panel.findViewById(R.id.search_keywords);
        transListProgress = (ProgressBar) rootView
                .findViewById(R.id.transListProgress);
        fragmentMsg = (TextView) searchView_layout_panel.findViewById(R.id.fragmentMsg);
        hideSearchImg = (ImageView) rootView.findViewById(R.id.hideSearchImg);
//		addSalesItem = (ButtonFloatSmall) rootView
//				.findViewById(R.id.addSalesItemBtn);
        searchKeywordBtn = (ButtonFloatSmall) searchView_layout_panel
                .findViewById(R.id.searchKeywordBtn);


        addTrViewLayout.setOnClickListener(this);
        searchKeywordBtn.setOnClickListener(this);
        noresults_icon.setVisibility(View.INVISIBLE);
        action_status_filter.setOnItemSelectedListener(this);

        transList.addOnItemTouchListener(new RecyclerItemClickListener(
                getActivity(), transList, this));

        countList.addOnItemTouchListener(new RecyclerItemClickListener(
                getActivity(), countList,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        page_count = position + 1;
                        GetTransactionList(page_count, statusID);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        page_count = position + 1;
                        GetTransactionList(page_count, statusID);
                    }
                }));
        addItemsToSpinner();
        BindActionStatusFilters();
        BindPageContent();
    }

    private List<CountNo> createList(int size) {

        List<CountNo> result = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            CountNo ci = new CountNo();
            ci.count = "" + i;
            result.add(ci);
        }
        return result;
    }

    public void addItemsToSpinner() {

        ArrayList<String> list = new ArrayList<String>();
        list.add("By Task Date");
        list.add("By Action Date");
        spinAdapter = new EzeidFindByAdapter(getActivity(), list);

        action_sort_filter.setAdapter(spinAdapter);
        action_sort_filter.setSelection(0);
        action_sort_filter.setOnItemSelectedListener(this);

    }

    private void BindPageContent() {
        String salesFormMsg = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("SalesFormMsg");

        if (salesFormMsg.equalsIgnoreCase("")) {
            fragmentMsg.setVisibility(View.GONE);
        } else {
            fragmentMsg.setText("" + salesFormMsg);
        }

    }

    private void BindActionStatusFilters() {

        try {
            // ShowEzeidDialog("Loading status");
            ezeidPreferenceHelper = new EzeidPreferenceHelper(getActivity());
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl() + "ewmStatusType?Token="
                    + token + "&MasterID=" + masterID + "&FunctionType=0";
//			Log.i(TAG, "Status url:" + url);
            JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url,
                    null, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    // DismissEzeidDialog();
                    BindStatus(response);
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // DismissEzeidDialog();
                    BindStatus(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void GetTransactionList(int pagecount, String status) {

        if (!status.equalsIgnoreCase("")) {
            String searchKeyword = searchKeywords.getText().toString();
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetTranscation?Token=" + token
                    + "&FunctionType=0&Page=" + pagecount + "&Status=" + status
                    + "&searchkeyword=" + searchKeyword + "&sort_by=" + sort_by;
            transListProgress.setVisibility(View.VISIBLE);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET, url,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            transList.setVisibility(View.VISIBLE);
                            BindTransactionList(response);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    transList.setVisibility(View.INVISIBLE);
                    BindTransactionList(null);
                }
            });

            int socketTimeout = 5000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);
        }
    }

    @SuppressLint("SimpleDateFormat")
    protected void BindTransactionList(JSONObject response) {
        try {
            if (null != response) {
                mSwipeRefreshLayout.setRefreshing(false);
                String page = response.getString("TotalPage");
                if (!page.equalsIgnoreCase("")) {
                    BindPageCount(Integer.parseInt(page));
                }

                JSONArray responseArray = response.getJSONArray("Result");

                int len = responseArray.length();
                if (len > 0) {
                    noresults_icon.setVisibility(View.INVISIBLE);
                    salesItemListType = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("SalesItemListType");

                    transItems = new ArrayList<SalesTransactionItem>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = responseArray.getJSONObject(i);
                        String trnNo = jsonObject.getString("TrnNo");
                        String taskDateTime = jsonObject
                                .getString("TaskDateTime");
                        String folderRuleID = jsonObject
                                .getString("FolderRuleID");
                        String contactInfo = jsonObject
                                .getString("ContactInfo");
                        String tid = jsonObject.getString("TID");
                        requesterName = jsonObject.getString("Requester");
                        String message = jsonObject.getString("Message");
                        amount = jsonObject.getString("Amount");
                        nextActionDate = jsonObject.getString("NextActionDate");
                        notes = jsonObject.getString("Notes");
                        statusTypeID = jsonObject.getString("Status");
                        String statusTitle = jsonObject
                                .getString("statustitle");
                        actionTypeID = jsonObject.getString("NextActionID");
                        String locID = jsonObject.getString("LocID");
                        String updatedUser = jsonObject
                                .getString("UpdatedUser");
                        String updatedDate = jsonObject
                                .getString("updatedDate");
                        String deliveryAddress = jsonObject
                                .getString("DeliveryAddress");
                        String ezeid = jsonObject.getString("EZEID");
                        String req_ezeid = jsonObject
                                .getString("RequesterEZEID");

                        String formattedDate = "", updatedDateS = "";
                        try {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                                    "dd MMM yyyy KK:mm:ss a");
                            simpleDateFormat.setTimeZone(TimeZone
                                    .getTimeZone("UTC"));
                            Date parsed = simpleDateFormat.parse(taskDateTime);
                            Date updateDate = simpleDateFormat
                                    .parse(updatedDate);
                            SimpleDateFormat destFormat = new SimpleDateFormat(
                                    "dd MMM yyyy HH:mm:ss a",
                                    Locale.getDefault());
                            destFormat.setTimeZone(TimeZone.getDefault());
                            formattedDate = destFormat.format(parsed);
                            updatedDateS = destFormat.format(updateDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        SalesTransactionItem item = new SalesTransactionItem();
                        item.itemTID = tid;
                        item.itemRequester = requesterName;
                        item.itemAmount = amount;
                        item.itemNextActionDate = nextActionDate;
                        item.itemNotes = notes;
                        item.itemStatus = statusTypeID;
                        item.itemNextActionID = actionTypeID;
                        item.itemTaskDateTime = formattedDate;
                        item.itemTrnNo = trnNo;
                        item.itemFolderRuleID = folderRuleID;
                        item.itemMessage = message;
                        item.itemContactInfo = contactInfo;
                        item.itemLocID = locID;
                        item.itemUpdatedUser = updatedUser;
                        item.itemupdatedDate = updatedDateS;
                        item.itemDeliveryAddress = deliveryAddress;
                        item.transMsgEZEID = ezeid;
                        item.transReqEZEID = req_ezeid;
                        item.transStatusTitle = statusTitle;
                        transItems.add(item);

                        SalesTransactionAdapter adapter = new SalesTransactionAdapter(
                                transItems);

                        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(
                                adapter);
                        transList.setAdapter(new ScaleInAnimationAdapter(
                                alphaAdapter));
                    }
                } else {
                    noresults_icon.setVisibility(View.VISIBLE);
//					Log.i(TAG, "BindTransactionList size is 0");
                }

            } else {
//				Log.i(TAG, "BindTransactionList(): null");
                noresults_icon.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);
            }
            transListProgress.setVisibility(View.INVISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BindPageCount(int page) {
        CountNoAdapter countNoAdapter = new CountNoAdapter(createList(page));
        countList.setAdapter(countNoAdapter);
    }

    private void BindTransactionDetail(View view) {
        Intent intent = null;
        salesItemListType = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("SalesItemListType");
        EzeidGlobal.SALES_TYPE = salesItemListType;
        String requester = ((TextView) view
                .findViewById(R.id.trans_requester_name)).getText().toString();
        // String updated_date = ((TextView) view
        // .findViewById(R.id.trans_updated_date)).getText()
        // .toString();
        String amount = ((TextView) view.findViewById(R.id.trans_amount))
                .getText().toString();
        String next_action_date = ((TextView) view
                .findViewById(R.id.trans_next_action_date)).getText()
                .toString();
        String notes_lbl = ((TextView) view.findViewById(R.id.trans_notes_lbl))
                .getText().toString();
        String statusID = ((TextView) view.findViewById(R.id.trans_statusID))
                .getText().toString();
        String actionID = ((TextView) view.findViewById(R.id.trans_actionID))
                .getText().toString();
        String taskDateTime = ((TextView) view
                .findViewById(R.id.trans_taskDateTime)).getText().toString();
        String trnId = ((TextView) view.findViewById(R.id.trans_trnId))
                .getText().toString();
        String folderID = ((TextView) view.findViewById(R.id.trans_folderID))
                .getText().toString();
        String message = ((TextView) view.findViewById(R.id.trans_requirements))
                .getText().toString();
        String contact_info = ((TextView) view
                .findViewById(R.id.trans_contact_info)).getText().toString();
        String tid = ((TextView) view.findViewById(R.id.trans_tID)).getText()
                .toString();

        String loc_id = ((TextView) view.findViewById(R.id.trans_locID))
                .getText().toString();

        String updateUser = ((TextView) view.findViewById(R.id.updateBy))
                .getText().toString();

        String updateDate = ((TextView) view.findViewById(R.id.updateOnDate))
                .getText().toString();
        String itemupDeliveryAddress = ((TextView) view
                .findViewById(R.id.itemupDeliveryAddress)).getText().toString();
        String ezeid = ((TextView) view.findViewById(R.id.trans_contact_ezeid))
                .getText().toString();
        String req_ezeid = ((TextView) view.findViewById(R.id.trans_req_ezeid))
                .getText().toString();

        if (salesItemListType.equalsIgnoreCase("0")) {
            intent = new Intent(getActivity(), EzeidAddSalesMessages.class);
        } else {
            intent = new Intent(getActivity(), SalesTransactionDetail.class);
        }

        intent.putExtra("Requester", requester);
        intent.putExtra("TID", tid);
        intent.putExtra("Amount", amount);
        intent.putExtra("Next_action_date", next_action_date);
        intent.putExtra("Notes_lbl", notes_lbl);
        intent.putExtra("StatusID", statusID);
        intent.putExtra("ActionID", actionID);
        intent.putExtra("TaskDateTime", taskDateTime);
        intent.putExtra("TrnId", trnId);
        intent.putExtra("FolderID", folderID);
        intent.putExtra("Message", message);
        intent.putExtra("ContactInfo", contact_info);
        intent.putExtra("LocID", loc_id);
        intent.putExtra("UpdatedUser", updateUser);
        intent.putExtra("updatedDate", updateDate);
        intent.putExtra("DeliveryAddress", itemupDeliveryAddress);
        intent.putExtra("EZEID", ezeid);
        intent.putExtra("RequesterEZEID", req_ezeid);

        startActivity(intent);

    }

    protected void BindStatus(JSONObject response) {
        try {
            if (null != response) {
                JSONArray jsonArray = response.getJSONArray("Result");
                int len = jsonArray.length();
                if (len != 0) {
                    statusArray = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String statusTitle = jsonObject
                                .getString("StatusTitle");
                        String progressPercent = jsonObject
                                .getString("ProgressPercent");
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("StatusTitle", statusTitle);
                        map.put("ProgressPercent", progressPercent);
                        statusArray.add(map);

                        statusAdapter = new SimpleAdapter(getActivity(),
                                statusArray, R.layout.ezeid_tagid_row,
                                new String[]{"TID", "StatusTitle",
                                        "ProgressPercent"}, new int[]{
                                R.id.masterId, R.id.masterTag,
                                R.id.masterPercentage});
                        action_status_filter.setAdapter(statusAdapter);
                    }
                    // BindTransactions();
                    GetTransactionList(page_count, statusID);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        // String tag = null;
        switch (parent.getId()) {

            case R.id.action_sort_filter:
                String item = parent.getItemAtPosition(position).toString();
                if (item.equalsIgnoreCase("By Task Date")) {
                    sort_by = "0";
                } else {
                    sort_by = "1";
                }
                GetTransactionList(page_count, statusID);
                break;

            case R.id.action_status_filter:
                if (null != view) {
                    TextView txt = (TextView) view.findViewById(R.id.masterId);
                    if (null != txt) {
                        statusID = ((TextView) view.findViewById(R.id.masterId))
                                .getText().toString();
                        GetTransactionList(page_count, statusID);
                    }
                }
                break;

            default:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addtr_main_btn:
                SetSaleItemType();
                break;
            case R.id.searchKeywordBtn:
                GetTransactionList(page_count, statusID);
                break;

            default:
                break;
        }

    }

    private void SetSaleItemType() {
        salesItemListType = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("SalesItemListType");
        ezeidPreferenceHelper.SaveValueToSharedPrefs("ITEM_TO_ADD_USER",
                "business_mgr");
        salesItemListType = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("SalesItemListType");
        EzeidGlobal.SALES_TYPE = salesItemListType;
        if (salesItemListType.equalsIgnoreCase("0")) {
            startActivity(new Intent(getActivity(), EzeidAddSalesMessages.class));
        } else if (salesItemListType.equalsIgnoreCase("2")) {
            startActivity(new Intent(getActivity(), EzeidAddItems.class));
        } else {
            startActivity(new Intent(getActivity(), EzeidAddSalesItems.class));
        }
    }

    @Override
    public void onRefresh() {
        refreshContent();
    }

    private void refreshContent() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                GetTransactionList(page_count, statusID);
            }
        }, 3000);

    }

    @Override
    public void onItemClick(View view, int position) {
        BindTransactionDetail(view);
    }

    @Override
    public void onItemLongClick(View view, int position) {
        BindTransactionDetail(view);
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPanelCollapsed(View panel) {
        // TODO Auto-generated method stub
        hideSearchImg.setImageResource(R.drawable.ic_action_expand);
    }

    @Override
    public void onPanelExpanded(View panel) {
        // TODO Auto-generated method stub
        hideSearchImg.setImageResource(R.drawable.ic_action_collapse);
    }

    @Override
    public void onPanelAnchored(View panel) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPanelHidden(View panel) {
        // TODO Auto-generated method stub

    }

}