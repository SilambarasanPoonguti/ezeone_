package com.ezeone.manager.businessfragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ezeone.R;
import com.ezeone.helpers.Constant;
import com.ezeone.manager.reservations.AddAppointment;
import com.ezeone.manager.reservations.services.WeekView;
import com.ezeone.manager.reservations.services.WeekViewEvent;
import com.ezeone.networkhandler.NetworkHandler;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.floatingaction.ButtonFloatSmall;
import com.ezeonelib.floatingaction.FloatingActionButton;

public class ActionReservationFragment extends Fragment implements
		WeekView.MonthChangeListener, WeekView.EventClickListener,
		WeekView.EventLongPressListener, DatePickerDialog.OnDateSetListener,
		WeekView.EmptyViewClickListener, WeekView.OnDateChanged {

	private static final int TYPE_DAY_VIEW = 1;
	private int mWeekViewType = TYPE_DAY_VIEW;
	public static final String DATEPICKER_TAG = "datepicker";
	private WeekView mWeekView;
	private Calendar c;
	int hour;
	private Spinner resourceSpinner;
	private DatePickerDialog datePickerDialog;
	private ButtonFloatSmall calendarBtn;
	private Calendar calendar;
	private TextView dateText;
	private SimpleDateFormat sdf, sdf2, timeFormat, formatForCalendar;
	private FloatingActionButton addAppointment;
	private String TAG = getClass().getSimpleName();
	private EzeidPreferenceHelper mgr;
	private List<HashMap<String, String>> resources;
	public static Calendar startTime1, endTime1, startTime2, endTime2;
	private String[] colors = new String[] { "#1abc9c", "#2ecc71", "#e67e22",
			"#3498db", "#c0392b", "#9b59b6", "#2c3e50", "#EC407A", "#4DD0E1",
			"#FF8A65" };
	private EzeidLoadingProgress ezeidLoadingProgress;
	private Handler handlerDialog;
	private Runnable runnableDialog;
	private Date selectedDate;
	private String title;
	private int page;
	private Button goToday;
	private Date fromTime1,toTime1,fromTime2,toTime2;

	List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
	WeekViewEvent event;

	public ActionReservationFragment() {
	}

	public static ActionReservationFragment newInstance() {
		ActionReservationFragment reservationFragment = new ActionReservationFragment();
//		Bundle bundle = new Bundle();
//		bundle.putInt("Page", page);
//		bundle.putString("Title", title);
//		reservationFragment.setArguments(bundle);
		return reservationFragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		page = getArguments().getInt("Page");
//		title = getArguments().getString("Title");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// View rootView = inflater.inflate(
		// R.layout.fragment_action_home_delivery, container, false);
		// View rootView = inflater.inflate(R.layout.fragment_ezeid_chat,
		// container, false);
		View rootView = inflater.inflate(R.layout.fragment_action_reservation,
				container, false);

		resources = new ArrayList<>();

		sdf = new SimpleDateFormat("dd/MMM");
		sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		timeFormat = new SimpleDateFormat("HH:mm:ss");

		formatForCalendar = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		mWeekView = (WeekView) rootView.findViewById(R.id.weekView);

		mgr = new EzeidPreferenceHelper(getActivity());

		Log.e("Token", mgr.GetValueFromSharedPrefs("Token"));

		resourceSpinner = (Spinner) rootView.findViewById(R.id.resources);

		goToday = (Button) rootView.findViewById(R.id.goTodayFrag);

		/*calenderViewType = (Spinner) rootView
				.findViewById(R.id.calenderViewType);*/
		calendarBtn = (ButtonFloatSmall) rootView.findViewById(R.id.calender);
		dateText = (TextView) rootView.findViewById(R.id.dateText);

		addAppointment = (FloatingActionButton) rootView
				.findViewById(R.id.addAppointment);
//		addAppointment.setColorNormal(Color.parseColor("#d14233"));
//		addAppointment.setColorPressed(Color.parseColor("#e74c3c"));
//		addAppointment.setIcon(R.drawable.ic_action_increase);

		dateText.setText("" + sdf.format(new Date()));

		c = Calendar.getInstance();
		hour = c.get(c.HOUR_OF_DAY);

		calendar = Calendar.getInstance();
		datePickerDialog = DatePickerDialog.newInstance(this,
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH), false);
		
		goToday.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mWeekView.goToToday();
				
				ShowEzeidDialog("Loading");
				
				String bus_ezeid = mgr
						.GetValueFromSharedPrefs("EZEID");
				NetworkHandler.GetReservation(
						TAG,
						bus_ezeid,
						resources.get(resourceSpinner.getSelectedItemPosition()).get(
								"tid"), sdf2.format(new Date()), handler);
			}
		});

		addAppointment.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (resources.size() > 0) {
					Intent addAppointment = new Intent(getActivity(),
							AddAppointment.class);
					addAppointment.putExtra("BUSINESS_ADD", "internal");
					addAppointment.putExtra("time", Calendar.getInstance());
					addAppointment.putExtra(
							"resource",
							""
									+ resources.get(
											resourceSpinner
													.getSelectedItemPosition())
											.get("title"));
					addAppointment.putExtra(
							"resourceId",
							""
									+ resources.get(
											resourceSpinner
													.getSelectedItemPosition())
											.get("tid"));
					addAppointment.putExtra("internal", true);
					addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("EZEID"));
					addAppointment.putExtra("isUpdate", false);
					startActivity(addAppointment);
				}
			}
		});

		if (mWeekView == null) {
			Log.e("weekview", "IS NULL");
		} else {

			mWeekViewType = TYPE_DAY_VIEW;
			mWeekView.setNumberOfVisibleDays(1);
			mWeekView.setColumnGap((int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 8, getResources()
							.getDisplayMetrics()));
			mWeekView.setTextSize((int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_SP, 12, getResources()
							.getDisplayMetrics()));
			mWeekView.setEventTextSize((int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_SP, 12, getResources()
							.getDisplayMetrics()));

			// Show a toast message about the touched event.
			mWeekView.setOnEventClickListener(ActionReservationFragment.this);

			// The week view has infinite scrolling horizontally. We have to
			// provide
			// the events of a
			// month every time the month changes on the week view.
			mWeekView.setMonthChangeListener(ActionReservationFragment.this);

			mWeekView.setOnDateChangedListener(ActionReservationFragment.this);

			// Set long press listener for events.
			mWeekView.setEventLongPressListener(ActionReservationFragment.this);
			mWeekView.setEmptyViewClickListener(ActionReservationFragment.this);
			mWeekView.goToHour(hour);

		}

		calendarBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GetDatePicker();
			}
		});

		/*calenderViewType
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						switch (position) {

						case 0:
							mWeekView.goToToday();
							break;

						case 1:
							mWeekViewType = TYPE_DAY_VIEW;
							mWeekView.setNumberOfVisibleDays(1);
							mWeekView.setColumnGap((int) TypedValue
									.applyDimension(
											TypedValue.COMPLEX_UNIT_DIP, 8,
											getResources().getDisplayMetrics()));
							mWeekView.setTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											12, getResources()
													.getDisplayMetrics()));
							mWeekView.setEventTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											12, getResources()
													.getDisplayMetrics()));
							break;

						case 2:
							mWeekViewType = TYPE_THREE_DAY_VIEW;
							mWeekView.setNumberOfVisibleDays(3);
							mWeekView.setColumnGap((int) TypedValue
									.applyDimension(
											TypedValue.COMPLEX_UNIT_DIP, 8,
											getResources().getDisplayMetrics()));
							mWeekView.setTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											12, getResources()
													.getDisplayMetrics()));
							mWeekView.setEventTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											12, getResources()
													.getDisplayMetrics()));
							break;

						case 3:
							mWeekViewType = TYPE_WEEK_VIEW;
							mWeekView.setNumberOfVisibleDays(7);

							// Lets change some dimensions to best fit the view.
							mWeekView.setColumnGap((int) TypedValue
									.applyDimension(
											TypedValue.COMPLEX_UNIT_DIP, 2,
											getResources().getDisplayMetrics()));
							mWeekView.setTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											10, getResources()
													.getDisplayMetrics()));
							mWeekView.setEventTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											10, getResources()
													.getDisplayMetrics()));
							break;

						case 4:
							mWeekViewType = TYPE_WEEK2_VIEW;
							mWeekView.setNumberOfVisibleDays(14);

							// Lets change some dimensions to best fit the view.
							mWeekView.setColumnGap((int) TypedValue
									.applyDimension(
											TypedValue.COMPLEX_UNIT_DIP, 8,
											getResources().getDisplayMetrics()));
							mWeekView.setTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											5, getResources()
													.getDisplayMetrics()));
							mWeekView.setEventTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											5, getResources()
													.getDisplayMetrics()));
							break;

						default:
							mWeekView.goToToday();
							mWeekViewType = TYPE_DAY_VIEW;
							mWeekView.setNumberOfVisibleDays(1);
							mWeekView.setColumnGap((int) TypedValue
									.applyDimension(
											TypedValue.COMPLEX_UNIT_DIP, 8,
											getResources().getDisplayMetrics()));
							mWeekView.setTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											12, getResources()
													.getDisplayMetrics()));
							mWeekView.setEventTextSize((int) TypedValue
									.applyDimension(TypedValue.COMPLEX_UNIT_SP,
											12, getResources()
													.getDisplayMetrics()));
							break;
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {

					}
				});*/
		
		if (EzeidUtil.isConnectedToInternet(getActivity())) {
			ShowEzeidDialog("Loading Resources. Please wait");
			NetworkHandler.GetResources(TAG,
					mgr.GetValueFromSharedPrefs("EZEID"), handler);
		}else{
			Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
		}
		return rootView;
	}

	@SuppressLint("DefaultLocale")
	private String getEventTitle(Calendar time) {
		return String.format("Event of %02d:%02d %s/%d",
				time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE),
				time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
	}

	@Override
	public void onEventClick(WeekViewEvent event, RectF eventRect) {
		/*Toast.makeText(getActivity(),
				"Clicked "+ event.getId(),
				Toast.LENGTH_SHORT).show();*/
		
		ShowEzeidDialog("Loading");
		
		NetworkHandler.GetReservationDetails(TAG, ""+event.getId(), handler);
	}

	@Override
	public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
		/*Toast.makeText(getActivity(), "Long pressed event: " + event.getName(),
				Toast.LENGTH_SHORT).show();*/
	}

	@Override
	public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
		// Populate the week view with some events.

		// WeekViewEvent k = new WeekViewEvent(id, name, startYear, startMonth,
		// startDay, startHour, startMinute, endYear, endMonth, endDay, endHour,
		// endMinute)

		/*
		 * if (newMonth == 5) { event = new WeekViewEvent(1, "Meeting", 2015,
		 * 05, 22, 3, 0, 2015, 05, 22, 4, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(2, "Webinar", 2015, 05, 22, 6, 0, 2015, 05,
		 * 22, 8, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_02));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(3, "JAVA", 2015, 05, 22, 10, 0, 2015, 05,
		 * 22, 12, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_03));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(4, "Lunch", 2015, 05, 22, 13, 0, 2015, 05,
		 * 22, 14, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 22, 17, 0, 2015,
		 * 05, 22, 19, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_03));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(6, "IOS", 2015, 05, 22, 22, 0, 2015, 05,
		 * 22, 24, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(4, "Lunch", 2015, 05, 20, 4, 0, 2015, 05,
		 * 20, 5, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 20, 17, 0, 2015,
		 * 05, 20, 19, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_03));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(6, "IOS", 2015, 05, 20, 19, 0, 2015, 05,
		 * 20, 20, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_01));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(4, "IOS", 2015, 05, 21, 15, 0, 2015, 05,
		 * 21, 20, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 21, 21, 0, 2015,
		 * 05, 21, 23, 0);
		 * event.setColor(getResources().getColor(R.color.event_color_03));
		 * events.add(event);
		 * 
		 * event = new WeekViewEvent(5, "Android", 2015, 05, 26, 12, 32, 2015,
		 * 05, 26, 12, 40);
		 * event.setColor(getResources().getColor(R.color.event_color_04));
		 * events.add(event); }
		 */
		if (newMonth == 5) {
			
		}
			return events;
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, day);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.YEAR, year);
		mWeekView.goToDate(cal);
		
		ShowEzeidDialog("Loading.");
		
		String bus_ezeid = mgr
				.GetValueFromSharedPrefs("EZEID");
		NetworkHandler.GetReservation(
				TAG,
				bus_ezeid,
				resources.get(resourceSpinner.getSelectedItemPosition()).get(
						"tid"), sdf2.format(cal.getTime()), handler);
		
		
	}

	private void GetDatePicker() {
		datePickerDialog.setVibrate(false);
		datePickerDialog.setYearRange(1985, 2028);
		datePickerDialog.setCloseOnSingleTapDay(false);
		datePickerDialog.show(getChildFragmentManager(), DATEPICKER_TAG);
	}

	@Override
	public void onEmptyViewClicked(Calendar time) {
		try{
			
			
//			Log.e("Date", ""+fromTime1);
			
			Log.e("Date", ""+fromTime1);
			Log.e("Date", "" + fromTime2);
			
		if(mWeekView.setAvailableSlots(timeFormat.parse(timeFormat.format(new Date(fromTime1.getTime()
				+ TimeZone.getDefault().getOffset(
				new Date().getTime())))), timeFormat.parse(timeFormat.format(new Date(toTime1.getTime()
				+ TimeZone.getDefault().getOffset(
				new Date().getTime())))), timeFormat.parse(timeFormat.format(time.getTime())))){
			
//			Log.e("Slot", "Available");
			
			
			if (resources.size() > 0) {
				Intent addAppointment = new Intent(getActivity(),
						AddAppointment.class);
				addAppointment.putExtra("BUSINESS_ADD", "internal");
				addAppointment.putExtra("time", time);
				addAppointment.putExtra(
						"resource",
						""
								+ resources.get(
										resourceSpinner.getSelectedItemPosition())
										.get("title"));
				addAppointment.putExtra(
						"resourceId",
						""
								+ resources.get(
										resourceSpinner.getSelectedItemPosition())
										.get("tid"));
				addAppointment.putExtra("internal", true);
				addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("EZEID"));
				addAppointment.putExtra("isUpdate", false);
				startActivity(addAppointment);
			}
			
			
		}else{
			Toast.makeText(getActivity(), "This time is not available for reservation", Toast.LENGTH_SHORT).show();
			
			if(mWeekView.setAvailableSlots(timeFormat.parse(timeFormat.format(new Date(fromTime2.getTime()
					+ TimeZone.getDefault().getOffset(
							new Date().getTime())))), timeFormat.parse(timeFormat.format(new Date(toTime2.getTime()
									+ TimeZone.getDefault().getOffset(
											new Date().getTime())))), timeFormat.parse(timeFormat.format(time.getTime())))){
				
//				Log.e("Slot", "Available 2");
				
				
				if (resources.size() > 0) {
					Intent addAppointment = new Intent(getActivity(),
							AddAppointment.class);
					addAppointment.putExtra("BUSINESS_ADD", "internal");
					addAppointment.putExtra("time", time);
					addAppointment.putExtra(
							"resource",
							""
									+ resources.get(
											resourceSpinner.getSelectedItemPosition())
											.get("title"));
					addAppointment.putExtra(
							"resourceId",
							""
									+ resources.get(
											resourceSpinner.getSelectedItemPosition())
											.get("tid"));
					addAppointment.putExtra("internal", true);
					addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("EZEID"));
					addAppointment.putExtra("isUpdate", false);
					startActivity(addAppointment);
				}
				
				
			}else{
//				Log.e("Slot", "NOT Available 2");
				Toast.makeText(getActivity(), "This time is not available for reservation", Toast.LENGTH_SHORT).show();
			}
		}
		
		
		/*if(mWeekView.setAvailableSlots(timeFormat.parse(timeFormat.format(new Date(fromTime2.getTime()
				+ TimeZone.getDefault().getOffset(
						new Date().getTime())))), timeFormat.parse(timeFormat.format(new Date(toTime2.getTime()
								+ TimeZone.getDefault().getOffset(
										new Date().getTime())))), timeFormat.parse(timeFormat.format(time.getTime())))){
			
			Log.e("Slot", "Available 2");
			
		}else{
			
			Log.e("Slot", "NOT Available 2");
			
		}*/
		
		
		}catch(Exception e){
			e.printStackTrace();
		}
		/*if (resources.size() > 0) {
			Intent addAppointment = new Intent(getActivity(),
					AddAppointment.class);
			addAppointment.putExtra("BUSINESS_ADD", "internal");
			addAppointment.putExtra("time", time);
			addAppointment.putExtra(
					"resource",
					""
							+ resources.get(
									resourceSpinner.getSelectedItemPosition())
									.get("title"));
			addAppointment.putExtra(
					"resourceId",
					""
							+ resources.get(
									resourceSpinner.getSelectedItemPosition())
									.get("tid"));
			addAppointment.putExtra("internal", true);
			addAppointment.putExtra("ezeid", mgr.GetValueFromSharedPrefs("EZEID"));
			addAppointment.putExtra("isUpdate", false);
			startActivity(addAppointment);
		}*/
	}

	Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.arg1) {
			case Constant.MessageState.RESOURCES_AVAILABLE:
				parseResources((JSONObject) msg.obj);
				break;

			case Constant.MessageState.RESOURCES_UNAVAILABLE:

				break;

			case Constant.MessageState.RESERVATION_DATA_AVAILABLE:
				parseReservationData((JSONObject) msg.obj);
				break;

			case Constant.MessageState.RESERVATION_DATA_NOT_AVAILABLE:
				break;
				
			case Constant.MessageState.RESERVATION_DETAILS_AVAILABLE:
				parseReservationDetails((JSONObject)msg.obj);
				break;
				
				
			case Constant.MessageState.RESERVATION_DETAILS_NOT_AVAILABLE:
				break;

			default:
				break;
			}
			return false;
		}
	});

	private void parseResources(JSONObject obj) {
		DismissEzeidDialog();

		try {
			HashMap<String, String> map;

			if (!obj.isNull("data")) {
				JSONArray arr = obj.getJSONArray("data");
				if (arr.length() > 0) {
					for (int i = 0; i < arr.length(); i++) {
						JSONObject obj1 = arr.getJSONObject(i);
						map = new HashMap<String, String>();
						map.put("tid", obj1.getString("tid"));
						map.put("title", obj1.getString("title"));
						resources.add(map);
					}

					final SimpleAdapter adapter = new SimpleAdapter(
							getActivity(), resources,
							R.layout.country_list_item, new String[] { "title",
									"tid" }, new int[] { R.id.countryTitle,
									R.id.countryId });
					resourceSpinner.setAdapter(adapter);
					
					resourceSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							
							String bus_ezeid = mgr
									.GetValueFromSharedPrefs("EZEID");
							
							ShowEzeidDialog("Loading Data..");
							NetworkHandler.GetReservation(TAG, bus_ezeid,
									resources.get(resourceSpinner.getSelectedItemPosition())
											.get("tid"), sdf2.format(new Date()), handler);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							
						}
					});
				}
			}
			// ShowEzeidDialog("Loading data");

			selectedDate = new Date();
			if (resources.size() > 0) {

				NetworkHandler.GetReservation(TAG, mgr
						.GetValueFromSharedPrefs("EZEID"),
						resources
								.get(resourceSpinner.getSelectedItemPosition())
								.get("tid"), sdf2.format(new Date()), handler);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void parseReservationData(JSONObject obj) {

		events.clear();
		 DismissEzeidDialog();

		try {
			Date startDate, endDate;
			timeFormat.setTimeZone(TimeZone.getDefault());
			if (obj.getBoolean("status")) {
				if (!obj.isNull("data")) {
					JSONArray dataArr = obj.getJSONArray("data");

					/*fromTime1 = timeFormat.parse("06:00:00");
					toTime1 = timeFormat.parse("15:00:00");
					fromTime2 = timeFormat.parse("20:00:00");
					toTime2 = timeFormat.parse("22:00:00");*/
					
					/*Calendar workHrsStart1 = Calendar.getInstance();
					Calendar workHrsStart2 = Calendar.getInstance();
					Calendar workHrsEnd1 = Calendar.getInstance();
					Calendar workHrsEnd2 = Calendar.getInstance();
					
					workHrsStart1.setTime(new Date(fromTime1.getTime()
							+ TimeZone.getDefault().getOffset(
									new Date().getTime())));
					
					workHrsStart2.setTime(new Date(fromTime2.getTime()
							+ TimeZone.getDefault().getOffset(
									new Date().getTime())));
					
					workHrsEnd1.setTime(new Date(toTime1.getTime()
							+ TimeZone.getDefault().getOffset(
									new Date().getTime())));
					
					workHrsEnd2.setTime(new Date(toTime2.getTime()
							+ TimeZone.getDefault().getOffset(
									new Date().getTime())));
					
					Calendar selected2 = Calendar.getInstance();
					selected2.setTime(selectedDate);
					selected2.add(selected2.MONTH, 1);*/
					
					/*WeekViewEvent notAvailableEvent1 = new WeekViewEvent(0, "Not Working 1",
							selected2.get(selected2.YEAR),
							selected2.get(selected2.MONTH),
							selected2.get(selected2.DATE),
							workHrsEnd2.get(workHrsStart2.HOUR_OF_DAY),
							workHrsEnd2.get(workHrsStart2.MINUTE),
							selected2.get(selected2.YEAR),
							selected2.get(selected2.MONTH),
							selected2.get(selected2.DATE),
							workHrsStart1.get(workHrsStart1.HOUR_OF_DAY),
							workHrsStart1.get(workHrsStart1.MINUTE));
					
					notAvailableEvent1.setColor(Color.parseColor("#eaeaea"));
					events.add(notAvailableEvent1);
					
					WeekViewEvent notAvailableEvent2 = new WeekViewEvent(0, "Not Working 2",
							selected2.get(selected2.YEAR),
							selected2.get(selected2.MONTH),
							selected2.get(selected2.DATE),
							workHrsEnd1.get(workHrsStart2.HOUR_OF_DAY),
							workHrsEnd1.get(workHrsStart2.MINUTE),
							selected2.get(selected2.YEAR),
							selected2.get(selected2.MONTH),
							selected2.get(selected2.DATE),
							workHrsStart2.get(workHrsEnd2.HOUR_OF_DAY),
							workHrsStart2.get(workHrsEnd2.MINUTE)); 
						
					notAvailableEvent2.setColor(Color.parseColor("#eaeaea"));
					events.add(notAvailableEvent2);*/
							
					for (int i = 0; i < dataArr.length(); i++) {
						JSONObject dataObj = dataArr.getJSONObject(i);
						
						fromTime1 = timeFormat.parse(dataObj.getString("W1"));
						toTime1 = timeFormat.parse(dataObj.getString("W2"));
						fromTime2 = timeFormat.parse(dataObj.getString("W3"));
						toTime2 = timeFormat.parse(dataObj.getString("W4"));
						String d = dataObj
								.getString("Starttime");
						String e =dataObj.getString("endtime");
						if(!d.equalsIgnoreCase("null")||!e.equalsIgnoreCase("null")) {

							startDate = timeFormat.parse(d);
							endDate = timeFormat
									.parse(dataObj.getString("endtime"));

							Calendar startCal = Calendar.getInstance();
							startCal.setTime(new Date(startDate.getTime()
									+ TimeZone.getDefault().getOffset(
									new Date().getTime())));

							Calendar endCal = Calendar.getInstance();
							endCal.setTime(new Date(endDate.getTime()
									+ TimeZone.getDefault().getOffset(
									new Date().getTime())));

							Calendar selected = Calendar.getInstance();
							selected.setTime(selectedDate);
							selected.add(selected.MONTH, 1);

						/*Log.e("Date format", String.format("%td", selectedDate));

						Log.e("Selected date",
								""
										+ sdf2.format(selectedDate)
										+ " "
										+ timeFormat.format(new Date(
												startDate.getTime()
														+ TimeZone
																.getDefault()
																.getOffset(
																		new Date()
																				.getTime()))));
						Log.e("Selected end date 2",
								""
										+ sdf2.format(selectedDate)
										+ " "
										+ timeFormat.format(new Date(
												endDate.getTime()
														+ TimeZone
																.getDefault()
																.getOffset(
																		new Date()
																				.getTime()))));*/

						/*Calendar cal1 = Calendar.getInstance();
						Calendar cal2 = Calendar.getInstance();

						formatForCalendar.parse(""
								+ sdf2.format(selectedDate)
								+ " "
								+ timeFormat.format(new Date(endDate.getTime()
										+ TimeZone.getDefault().getOffset(
												new Date().getTime()))));
						cal1.setTime(formatForCalendar.parse(""
								+ sdf2.format(selectedDate)
								+ " "
								+ timeFormat.format(new Date(startDate
										.getTime()
										+ TimeZone.getDefault().getOffset(
												new Date().getTime())))));
						cal2.setTime(formatForCalendar.parse(""
								+ sdf2.format(selectedDate)
								+ " "
								+ timeFormat.format(new Date(endDate.getTime()
										+ TimeZone.getDefault().getOffset(
												new Date().getTime())))));*/

						/*
						 * Log.e("Date ",
						 * ""+selected.get(selected.YEAR)+"-"+selected
						 * .get(selected.MONTH)+"-"+
						 * selected.get(selected.DAY_OF_MONTH));
						 * Log.e("Time of calendar",
						 * ""+startCal.get(startCal.HOUR_OF_DAY
						 * )+":"+startCal.get(startCal.MINUTE));
						 * 
						 * Log.e("end Time of calendar",
						 * ""+endCal.get(endCal.HOUR_OF_DAY
						 * )+":"+endCal.get(endCal.MINUTE));
						 */

							event = new WeekViewEvent(dataObj.getInt("TID"), ""
									+ dataObj.getString("ContactInfo") + " - "
									+ dataObj.getString("service"),
									selected.get(selected.YEAR),
									selected.get(selected.MONTH),
									selected.get(selected.DATE),
									startCal.get(startCal.HOUR_OF_DAY),
									startCal.get(startCal.MINUTE),
									selected.get(selected.YEAR),
									selected.get(selected.MONTH),
									selected.get(selected.DATE),
									endCal.get(endCal.HOUR_OF_DAY),
									endCal.get(endCal.MINUTE));

							// event = new WeekViewEvent(dataObj.getInt("TID"),
							// dataObj.getString("ContactInfo"), cal1, cal2);

							int idx = new Random().nextInt(colors.length);
							String random = (colors[idx]);
							event.setColor(Color.parseColor(random));
							events.add(event);
						/*
						 * event = new WeekViewEvent(1,
						 * ""+dataObj.getString("reserverName"
						 * )+" - "+dataObj.getString("service"), 2015, 06, 03,
						 * 3, 0, 2015, 06, 03, 4, 0);
						 * event.setColor(getResources
						 * ().getColor(R.color.event_color_01));
						 * events.add(event);
						 */
						}
					}

					mWeekView.notifyDatasetChanged();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDateChanged(Date date) {
		ShowEzeidDialog("Loading data.");
		selectedDate = date;
		if (resources.size() > 0) {
			NetworkHandler.GetReservation(TAG, mgr
					.GetValueFromSharedPrefs("EZEID"),
					resources.get(resourceSpinner.getSelectedItemPosition())
							.get("tid"), sdf2.format(date), handler);
		}
	}

	private void ShowEzeidDialog(String content) {
		ezeidLoadingProgress = new EzeidLoadingProgress(getActivity(), content);
		handlerDialog = new Handler();
		runnableDialog = new Runnable() {

			@Override
			public void run() {
				if (ezeidLoadingProgress != null) {
					if (ezeidLoadingProgress.isShowing()) {
						ezeidLoadingProgress.dismiss();
					}
				}
			}
		};
		ezeidLoadingProgress.show();
	}

	private void DismissEzeidDialog() {
		handlerDialog.removeCallbacks(runnableDialog);
		if (ezeidLoadingProgress.isShowing()) {
			ezeidLoadingProgress.dismiss();
		}
	}
	
	private void parseReservationDetails(JSONObject obj){
		try{
//			Log.e("Response", ""+obj);
			
			/*{"message":"Reservation Trans details Send successfully",
			 * "error":null,"data":
			 * [{"endtime":"11:33:00",
			 * "reserverName":"MMindustries",
			 * "Status":0,
			 * "duration":10,
			 * "TID":1,
			 * "reserverId":171,
			 * "ContactInfo":"9886902446",
			 * "service":"Service2,Service3",
			 * "serviceids":"16,17",
			 * "Starttime":"11:23:00",
			 * "mResResourceID":21}],
			 * "status":true}*/
			
			DismissEzeidDialog();
			
			if(!obj.isNull("status")){
				if(obj.getBoolean("status")){
					if(!obj.isNull("data")){
						JSONArray dataArr = obj.getJSONArray("data");
						for(int i=0;i<dataArr.length();i++){
							String resourceId = dataArr.getJSONObject(i).getString("mResResourceID");
							String contactDetails = dataArr.getJSONObject(i).getString("ContactInfo");
							String Status = dataArr.getJSONObject(i).getString("Status");
							String startTime = dataArr.getJSONObject(i).getString("Starttime");
							String endTime = dataArr.getJSONObject(i).getString("endtime");
							String tid = dataArr.getJSONObject(i).getString("TID");
							
							Intent intent = new Intent(getActivity(),AddAppointment.class);
							intent.putExtra("resourceId", resourceId);
							intent.putExtra("contactDetails", contactDetails);
							intent.putExtra("Status", Status);
							intent.putExtra("startTime", ""+timeFormat.format(new Date(timeFormat.parse(startTime)
									.getTime()
									+ TimeZone.getDefault().getOffset(
											new Date().getTime()))));
							intent.putExtra("endTime", ""+timeFormat.format(new Date(timeFormat.parse(endTime)
									.getTime()
									+ TimeZone.getDefault().getOffset(
											new Date().getTime()))));
							intent.putExtra("tid", tid);
							intent.putExtra("resourceId", resourceId);
							intent.putExtra("internal", true);
							intent.putExtra(
									"resource",
									""
											+ resources.get(
													resourceSpinner
															.getSelectedItemPosition())
													.get("title"));
							intent.putExtra("isUpdate", true);
							intent.putExtra("service", dataArr.getJSONObject(i).getString("service"));
							intent.putExtra("ownReservation",false);
							startActivity(intent);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		if(resources.size()>0){
			
		ShowEzeidDialog("Loading data.");
			
		NetworkHandler.GetReservation(TAG, mgr
				.GetValueFromSharedPrefs("EZEID"),
				resources.get(resourceSpinner.getSelectedItemPosition())
						.get("tid"), sdf2.format(selectedDate), handler);
		}
		super.onResume();
	}
	
}
