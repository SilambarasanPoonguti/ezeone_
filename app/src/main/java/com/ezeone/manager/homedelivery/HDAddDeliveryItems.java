package com.ezeone.manager.homedelivery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.ezeone.R;
import com.ezeone.SplashScreen;
import com.ezeone.helpers.EZEIDUrlManager;
import com.ezeone.listeners.RecyclerItemClickListener;
import com.ezeone.listeners.RecyclerItemClickListener.OnItemClickListener;
import com.ezeone.manager.adapter.MainAdapter;
import com.ezeone.manager.backend.ItemDBhelper;
import com.ezeone.manager.backend.SaleItemModel;
import com.ezeone.manager.pojo.AddedItems;
import com.ezeone.manager.pojo.TrItems;
import com.ezeone.utils.EzeidGlobal;
import com.ezeone.utils.EzeidLoadingProgress;
import com.ezeone.utils.EzeidPreferenceHelper;
import com.ezeone.utils.EzeidUtil;
import com.ezeonelib.animations.FlipInRightYAnimator;
import com.ezeonelib.date.DatePickerDialog;
import com.ezeonelib.date.time.RadialPickerLayout;
import com.ezeonelib.date.time.TimePickerDialog;
import com.ezeonelib.dialog.MaterialDialog;
import com.ezeonelib.floatingaction.ButtonFloatSmall;

public class HDAddDeliveryItems extends AppCompatActivity implements
        OnItemClickListener, OnClickListener,
        TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener,
        OnItemSelectedListener {

    private static final String TAG = HDAddDeliveryItems.class.getSimpleName();
    private Toolbar toolbar;
    private EzeidPreferenceHelper ezeidPreferenceHelper;
    private ItemDBhelper dBhelper;
    private EzeidLoadingProgress ezeidLoadingProgress;
    private Handler handlerDialog;
    private Runnable runnableDialog;
    private MainAdapter adapter;
    private List<TrItems> trItems;
    private RecyclerView recyclerView;
    private boolean isRequesterAdded = false, isItemsEditAgain = false;
    private String salesItemListType = "", edit_itemid = "", edit_tid = "",
            edit_item_name = "", edit_rate = "", edit_amt = "", edit_qty = "",
            edit_option = "", item_durations = "", message_id = "";
    private Bitmap edit_bitmap;
    private EditText item_edit_rate;
    private ImageView item_edit_img;
    private LinearLayout item_edit_amt_layout, folderLayout,
            checkEzeidStatusLayout;
    private TextView switchLoc;
    private RadioButton custEzeid, custOther;
    private RadioGroup userType;
    private ProgressBar contactInfoProgress;
    private RelativeLayout contact_info;
    private TextView checkEzeidStatusText, checkEzeidStatusLine, itemName_edit,
            item_edit_amt, item_edit_amt_equal, item_edit_tid,
            item_edit_itemid, item_edit_item_durations, item_edit_message_id,
            panel_item_selected, panel_total_amt, contact_info_text;
    private EditText salesRequester, addressBox, nextActionDateBox, notesBox,
            item_edit_qty;
    private ImageView item_edit_increaseQty, item_edit_decreaseQty;
    private Button requseterOk, requseterCancel, item_edit_ok,
            item_edit_remove;
    private ButtonFloatSmall item_edit_close, editActionDate,
            checkEzeidAvailability;
    private String firstName = "", lastName = "", mobileNumber = "",
            latitude = "", longitude = "", dateStr = "", timeStr = "",
            prefDateTimeStr = "", country = "", state = "", city = "",
            area = "", rlatitude = "0", rlongitude = "0", messageID = "",
            taskDateTimeStr = "", messageText = "", contactInfo = "",
            update_statusID = "", update_nextActionID = "",
            update_folderRuleID = "", update_locID = "",
            update_messageText = "", update_taskDateTimeStr = "",
            update_nextActionDateStr = "", update_contactInfo = "",
            update_ezistingNotes = "", ezeid = "", req_ezeid = "",
            deliveryAddress = "", edit_statusID = "", edit_nextActionID = "",
            edit_floderID = "", GET_ITEMS_URL = "";
    ;
    public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private AppCompatEditText item_search_filter;
    private Button submit_order, cancel_order;
    private Bundle bundle;
    private Dialog overlayInfo;
    private ArrayList<HashMap<String, String>> status_array, action_array,
            folder_array;
    private Spinner statusFilter, actionFilter, folderFilter, locationFilter;
    private SimpleAdapter statusAdapter, actionAdapter, folderAdapter,
            locationAdapter;
    private boolean isEdited = false;
    private int status_count = 0, action_count = 0, folder_count = 0,
            sessionRetry = 0;


    private ImageView noresults_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_delivery_orders);

        toolbar = (Toolbar) findViewById(R.id.toolbar_addItem);
        toolbar.setTitle(Html.fromHtml("DELIVERY ORDER"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(
                R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dBhelper.open();
                dBhelper.DeleteAllItems();
                dBhelper.close();
                HDAddDeliveryItems.this.finish();
            }
        });

        InitComponents();
        SwipeOverlay();
        ActivityStatus();
        // if (isSessionDialogShown == false) {
        // CheckSessionExpiredStatus();
        // }

        if (savedInstanceState != null) {
            DatePickerDialog dpd = (DatePickerDialog) getSupportFragmentManager()
                    .findFragmentByTag(DATEPICKER_TAG);
            if (dpd != null) {
                dpd.setOnDateSetListener(this);
            }

            TimePickerDialog tpd = (TimePickerDialog) getSupportFragmentManager()
                    .findFragmentByTag(TIMEPICKER_TAG);
            if (tpd != null) {
                tpd.setOnTimeSetListener(this);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (EzeidUtil.isConnectedToInternet(getApplicationContext()) == false) {

            EzeidUtil.showToast(HDAddDeliveryItems.this, getResources()
                    .getString(R.string.internet_warning));
        }
    }

    private void InitComponents() {

        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new FlipInRightYAnimator());
        recyclerView.getItemAnimator().setAddDuration(300);
        recyclerView.getItemAnimator().setRemoveDuration(100);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(
                HDAddDeliveryItems.this, recyclerView, this));

        dBhelper = new ItemDBhelper(HDAddDeliveryItems.this);
        ezeidPreferenceHelper = new EzeidPreferenceHelper(
                HDAddDeliveryItems.this);
        salesItemListType = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("HomeDeliveryItemListType");
        EzeidGlobal.SALES_TYPE = salesItemListType;

        item_search_filter = (AppCompatEditText) findViewById(R.id.item_search_filter);
        submit_order = (Button) findViewById(R.id.submit_order);
        cancel_order = (Button) findViewById(R.id.cancel_order);

        submit_order.setOnClickListener(this);
        cancel_order.setOnClickListener(this);
        panel_item_selected = (TextView) findViewById(R.id.panel_item_selected);
        contact_info_text = (TextView) findViewById(R.id.contact_info_text);
        contact_info = (RelativeLayout) findViewById(R.id.contact_info);
        contact_info.setOnClickListener(this);
        panel_total_amt = (TextView) findViewById(R.id.panel_total_amt);
        panel_item_selected.setText("0 Items Selected");

        noresults_icon = (ImageView) findViewById(R.id.noresults_icon);
        noresults_icon.setVisibility(View.INVISIBLE);
        salesItemListType = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("HomeDeliveryItemListType");

        if (salesItemListType.equalsIgnoreCase("4")) {
            panel_total_amt.setVisibility(View.VISIBLE);
        } else {
            panel_total_amt.setVisibility(View.GONE);
        }

        item_search_filter.setHintTextColor(getResources().getColor(
                R.color.met_green));

        item_search_filter.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.length() != 0) {
                    BindFilterTrItems(s.toString());
                } else {
                    BindTrItems();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

    }

    private void SwipeOverlay() {

        String overlay = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("OVERLAY");

        if (!overlay.equalsIgnoreCase("false")) {

            overlayInfo = new Dialog(HDAddDeliveryItems.this);
            overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
            overlayInfo.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            overlayInfo.getWindow().setFlags(
                    WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
            overlayInfo.setContentView(R.layout.ezeid_add_sales_overlay_view);
            overlayInfo.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            if (overlayInfo != null) {
                overlayInfo.show();
            }
            TextView overlayText = (TextView) overlayInfo
                    .findViewById(R.id.overlayText);
            salesItemListType = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("HomeDeliveryItemListType");
            if (salesItemListType.equalsIgnoreCase("3")) {
                overlayText.setText(getResources().getString(
                        R.string.additemsOverlayText));
            } else if (salesItemListType.equalsIgnoreCase("4")) {
                panel_total_amt.setVisibility(View.VISIBLE);
                panel_total_amt.setText("Rs.0");
                overlayText.setText(getResources().getString(
                        R.string.additemsOverlayText));
            } else {
                panel_total_amt.setVisibility(View.GONE);
                overlayText.setText("Tap to select or unselect.");
            }

            overlayText.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    overlayInfo.cancel();
                    // overlayInfo.dismiss();
                    ezeidPreferenceHelper.SaveValueToSharedPrefs("OVERLAY",
                            "false");
                }
            });
        }
    }

    private void ActivityStatus() {

        String masterID = ezeidPreferenceHelper
                .GetValueFromSharedPrefs("MasterIDLog");
        if (masterID.equalsIgnoreCase("0")) {
            BindLocationsFilters();
        } else {
            locationAdapter = null;
        }

        GetEditedItems();
        BindBundle();
    }

    private void BindBundle() {

        bundle = getIntent().getExtras();
        if (null != bundle) {

            messageID = bundle.getString("MessageID");

            if (messageID.equalsIgnoreCase("")) {
                contact_info_text.setText(Html.fromHtml(getResources()
                        .getString(R.string.customer_info_warn)));
                isItemsEditAgain = false;
                isRequesterAdded = false;
            } else {
                isItemsEditAgain = true;
                isRequesterAdded = true;
            }
        }
    }

    private void GetEditedItems() {
        Intent intent = getIntent();
        if (null != intent) {
            if (intent.getStringExtra("MessageID") != null) {

                messageID = intent.getStringExtra("MessageID");
                String edit = intent.getStringExtra("ITEM_EDIT");
                update_ezistingNotes = intent.getStringExtra("Notes_lbl");
                update_statusID = intent.getStringExtra("StatusID");
                update_nextActionID = intent.getStringExtra("ActionID");
                update_folderRuleID = intent.getStringExtra("FolderID");
                update_locID = intent.getStringExtra("LocID");
                update_messageText = intent.getStringExtra("Message");
                update_taskDateTimeStr = intent.getStringExtra("TaskDateTime");
                update_nextActionDateStr = intent
                        .getStringExtra("Next_action_date");
                update_contactInfo = intent.getStringExtra("ContactInfo");
                deliveryAddress = intent.getStringExtra("DeliveryAddress");
                ezeid = intent.getStringExtra("EZEID");
                req_ezeid = intent.getStringExtra("RequesterEZEID");
                if (edit.equalsIgnoreCase("") || edit.equalsIgnoreCase(null)) {
                    isItemsEditAgain = false;
                    isRequesterAdded = false;
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_warn)));
                } else {
                    isItemsEditAgain = true;
                    isRequesterAdded = true;
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                }

                GetEditedSalesItemList(messageID);
            } else {
                String get_local_user_type = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("ITEM_TO_ADD_USER");
                String token = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("Token");
                String bus_ezeid = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("BusinessEZEID");
                if (get_local_user_type.equalsIgnoreCase("business_mgr")) {
                    GET_ITEMS_URL = EZEIDUrlManager.getAPIUrl()
                            + "ewtGetItemList?Token=" + token
                            + "&FunctionType=2";
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_warn)));
                } else {
                    GET_ITEMS_URL = EZEIDUrlManager.getAPIUrl()
                            + "ewtGetItemListForEZEID?Token=" + token
                            + "&FunctionType=2&EZEID=" + bus_ezeid;
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                }

                GetSalesItemList(GET_ITEMS_URL);
            }

        }
    }

    private void GetEditedSalesItemList(String msgid) {
        try {
            ShowEzeidDialog("Please wait!");
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            // token = "294f4b0c8c847333b1b8";
            // String url = EZEIDUrlManager.getAPIUrl()
            // + "ewtGetTranscationItems?Token=" + token + "&MessageID="
            // + msgid;
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetTranscationItems?Token=" + token + "&MessageID="
                    + msgid;

            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindEditedSalesItems(response);
                            DismissEzeidDialog();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindEditedSalesItems(null);
                    DismissEzeidDialog();
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindEditedSalesItems(JSONArray response) {
        // TODO Auto-generated method stub
        try {

            if (null != response) {
                int len = response.length();
                if (len != 0) {

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String item_tid = jsonObject.getString("TID");
                        String item_id = jsonObject.getString("ItemID");
                        String item_name = jsonObject.getString("ItemName");
                        String item_rate = jsonObject.getString("Rate");
                        String item_amt = jsonObject.getString("Amount");
                        String item_qty = jsonObject.getString("Qty");
                        String item_img = jsonObject.getString("Pic");
                        String item_status = "edit";
                        String item_durations = jsonObject
                                .getString("Duration");
                        String item_message_id = jsonObject
                                .getString("MessageID");
                        Bitmap itemBitmap = null;

                        if (!item_img.equalsIgnoreCase("")) {
                            String itemImag = EzeidUtil.GetFileType(item_img);
                            itemBitmap = EzeidUtil.convertBitmap(itemImag);
                        } else {
                            itemBitmap = null;
                        }

                        SaleItemModel saleItemModel = new SaleItemModel(
                                item_id, item_tid, item_name, itemBitmap,
                                item_rate, item_amt, item_qty, item_status,
                                item_durations, item_message_id);

                        String insert_item = "itemid: " + item_id + ", tid:"
                                + item_tid + ", name:" + item_name + ", img:"
                                + itemBitmap.toString() + ", rate:" + item_rate
                                + ", amt:" + item_amt + ",qty:" + item_qty
                                + " status:" + item_status + "item_durations:"
                                + item_durations + ", item_message_id:"
                                + item_message_id;

                        // Log.i(TAG, "insert_item:" + insert_item);

                        dBhelper.open();
                        dBhelper.insertItemDetails(saleItemModel);
                        dBhelper.close();

                    }
                    BindTrItems();
                    UpdateTotal();
                    if (salesItemListType.equalsIgnoreCase("4")) {
                        UpdateAmount();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void UpdateTotal() {
        // panel_item_selected
        dBhelper.open();
        int items = dBhelper.GetTotalAddedItems();
        panel_item_selected.setText("" + items + " Items Selected");
        dBhelper.close();
    }

    private void UpdateAmount() {
        // panel_item_selected
        if (salesItemListType.equalsIgnoreCase("4")) {
            dBhelper.open();
            float amount = dBhelper.GetTotalAmount();
            panel_total_amt.setVisibility(View.VISIBLE);
            panel_total_amt.setText("Rs." + amount);
            dBhelper.close();
        } else {
            panel_total_amt.setVisibility(View.GONE);
        }
    }

    private void GetSalesItemList(String url) {
        try {
            ShowEzeidDialog("Please wait!");
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindSalesItems(response);
                            DismissEzeidDialog();
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    BindSalesItems(null);
                    DismissEzeidDialog();
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BindSalesItems(JSONArray salesItems) {

        try {

            if (null != salesItems) {
                int len = salesItems.length();
                if (len != 0) {

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = salesItems.getJSONObject(i);
                        String item_tid = jsonObject.getString("TID");
                        String item_id = jsonObject.getString("TID");
                        String item_name = jsonObject.getString("ItemName");
                        // String item_desc =
                        // jsonObject.getString("Description");
                        String item_rate = jsonObject.getString("Rate");
                        String item_amt = jsonObject.getString("Rate");
                        String item_qty = "1";
                        String item_img = jsonObject.getString("Pic");
                        String item_status = "new";
                        String item_durations = jsonObject
                                .getString("Duration");
                        String item_message_id = "0";
                        Bitmap itemBitmap = null;

                        if (!item_img.equalsIgnoreCase("")) {
                            String itemImag = EzeidUtil.GetFileType(item_img);
                            itemBitmap = EzeidUtil.convertBitmap(itemImag);
                        } else {
                            itemBitmap = BitmapFactory.decodeResource(
                                    getResources(), R.drawable.ic_launcher);
                        }

                        SaleItemModel saleItemModel = new SaleItemModel(
                                item_id, item_tid, item_name, itemBitmap,
                                item_rate, item_amt, item_qty, item_status,
                                item_durations, item_message_id);
                        String insert_item = "";
                        if (null != itemBitmap) {
                            insert_item = "itemid: " + item_id + ", tid:"
                                    + item_tid + ", name:" + item_name
                                    + ", img:" + itemBitmap.toString()
                                    + ", rate:" + item_rate + ", amt:"
                                    + item_amt + ",qty:" + item_qty
                                    + " status:" + item_status
                                    + "item_durations:" + item_durations
                                    + ", item_message_id:" + item_message_id;
                        }
                        // Log.i(TAG, "insert_item:" + insert_item);

                        dBhelper.open();
                        dBhelper.insertItemDetails(saleItemModel);
                        dBhelper.close();

                    }
                    BindTrItems();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void BindTrItems() {

        trItems = new ArrayList<TrItems>();
        dBhelper.open();
        List<SaleItemModel> models = dBhelper.GetAllItems();

        if (models.size() > 0) {

            for (SaleItemModel model : models) {

                TrItems items = new TrItems();
                items.item_id = model.getItem_id();
                items.item_tid = model.getItem_tid();
                items.item_name = model.getItem_name();
                items.item_rate = model.getItem_rate();
                items.item_amt = model.getItem_amt();
                items.item_qty = model.getItem_qty();
                items.item_status = model.getItem_status();
                items.item_img = model.getItem_img();
                items.item_durations = model.getItem_durations();
                items.item_message_id = model.getItem_message_id();
                trItems.add(items);
            }
            if (trItems.size() > 0) {

                // Log.d("Result: ", "BindTrItems(): " + trItems.toString());
                recyclerView.setVisibility(View.VISIBLE);
                adapter = new MainAdapter(HDAddDeliveryItems.this, trItems);
                recyclerView.setAdapter(adapter);

            }
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
            EzeidUtil.showToast(HDAddDeliveryItems.this, "No items found!");
            noresults_icon.setVisibility(View.VISIBLE);
        }
        dBhelper.close();

    }

    private void ShowEzeidDialog(String content) {
        ezeidLoadingProgress = new EzeidLoadingProgress(
                HDAddDeliveryItems.this, content);
        handlerDialog = new Handler();
        runnableDialog = new Runnable() {

            @Override
            public void run() {
                if (ezeidLoadingProgress != null) {
                    if (ezeidLoadingProgress.isShowing()) {
                        ezeidLoadingProgress.dismiss();
                    }
                }
            }
        };
        ezeidLoadingProgress.show();
    }

    private void DismissEzeidDialog() {
        handlerDialog.removeCallbacks(runnableDialog);
        if (ezeidLoadingProgress.isShowing()) {
            ezeidLoadingProgress.dismiss();
        }
    }

    @Override
    public void finish() {
        super.finish();
        dBhelper.open();
        dBhelper.DeleteAllItems();
        dBhelper.close();
        // Log.i(TAG, "Removed all items from the sales table.");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onItemClick(View view, int position) {
        // TODO Auto-generated method stub

        adapter.remove(position);

        EzeidGlobal.LAST_SELECTED_POS = position;

        ImageView item_img = (ImageView) view.findViewById(R.id.item_img);
        BitmapDrawable drawable = (BitmapDrawable) item_img.getDrawable();
        edit_bitmap = drawable.getBitmap();

        edit_itemid = ((TextView) view.findViewById(R.id.item_id_)).getText()
                .toString();
        edit_tid = ((TextView) view.findViewById(R.id.item_tid)).getText()
                .toString();
        edit_item_name = ((TextView) view.findViewById(R.id.item_name))
                .getText().toString();
        edit_qty = ((TextView) view.findViewById(R.id.item_qty)).getText()
                .toString();
        edit_rate = ((TextView) view.findViewById(R.id.item_rate)).getText()
                .toString();
        edit_amt = ((TextView) view.findViewById(R.id.item_amt)).getText()
                .toString();
        String edit_state = ((TextView) view.findViewById(R.id.item_status_))
                .getText().toString();
        item_durations = ((TextView) view.findViewById(R.id.item_durations))
                .getText().toString();
        message_id = ((TextView) view.findViewById(R.id.item_message_id))
                .getText().toString();

        if (edit_state.equalsIgnoreCase("new")) {
            edit_option = "edit";
        } else {
            edit_option = "new";
        }

        if (salesItemListType.equalsIgnoreCase("3")
                || salesItemListType.equalsIgnoreCase("4")) {

            LinearLayout item_container = (LinearLayout) view
                    .findViewById(R.id.item_container);
            Drawable r = item_container.getBackground();
            if (r.equals(getResources()
                    .getDrawable(R.drawable.abc_row_selected))) {

            }
        }
        UpdateSelectedItem();

    }

    @Override
    public void onItemLongClick(View view, int position) {

        adapter.remove(position);

        EzeidGlobal.LAST_SELECTED_POS = position;

        ImageView item_img = (ImageView) view.findViewById(R.id.item_img);
        BitmapDrawable drawable = (BitmapDrawable) item_img.getDrawable();
        Bitmap bitmap = drawable.getBitmap();

        String edit_itemid = ((TextView) view.findViewById(R.id.item_id_))
                .getText().toString();
        String edit_tid = ((TextView) view.findViewById(R.id.item_tid))
                .getText().toString();
        String edit_item_name = ((TextView) view.findViewById(R.id.item_name))
                .getText().toString();
        String edit_qty = ((TextView) view.findViewById(R.id.item_qty))
                .getText().toString();
        String edit_rate = ((TextView) view.findViewById(R.id.item_rate))
                .getText().toString();
        String edit_amt = ((TextView) view.findViewById(R.id.item_amt))
                .getText().toString();
        String edit_state = ((TextView) view.findViewById(R.id.item_status_))
                .getText().toString();
        String item_durations = ((TextView) view
                .findViewById(R.id.item_durations)).getText().toString();
        String message_id = ((TextView) view.findViewById(R.id.item_message_id))
                .getText().toString();
        if (!salesItemListType.equalsIgnoreCase("1")
                || !salesItemListType.equalsIgnoreCase("2")) {

            EditTransactionItem(edit_tid, edit_itemid, edit_item_name,
                    edit_qty, edit_rate, edit_amt, position, bitmap,
                    edit_state, item_durations, message_id);
        }

    }

    private void UpdateSelectedItem() {

        if (edit_option.equalsIgnoreCase("new")) {
            edit_qty = "1";
            SelectItem(edit_itemid, edit_tid, edit_item_name, edit_bitmap,
                    edit_rate, edit_rate, edit_qty, edit_option,
                    item_durations, message_id);
        } else {
            // edit_qty = item_swipe_edit_qty.getText().toString();
            if (edit_qty.equalsIgnoreCase("")) {
                edit_qty = "1";
            }
            if (edit_rate.equalsIgnoreCase("")) {
                edit_rate = "1";
            }
            int qty = Integer.parseInt(edit_qty);
            float rate = Float.parseFloat(edit_rate);
            float amt = (qty) * (rate);
            edit_amt = String.valueOf(amt);
            SelectItem(edit_itemid, edit_tid, edit_item_name, edit_bitmap,
                    edit_rate, edit_amt, edit_qty, "edit", item_durations,
                    message_id);
        }
    }

    public void SelectItem(String edit_itemid, String edit_tid,
                           String edit_item_name, Bitmap bitmap, String edit_rate,
                           String edit_amt, String edit_qty, String status,
                           String item_durations, String item_message_id) {
        String update_item = "itemid: " + edit_itemid + ", tid:" + edit_tid
                + ", name:" + edit_item_name + ", img:" + bitmap + ", rate:"
                + edit_rate + ", amt:" + edit_amt + ",qty:" + edit_qty
                + " status:" + status + ", item_durations:" + item_durations
                + ", item_message_id:" + item_message_id;
        // Log.i(TAG, "SelectItem() edit:" + update_item);
        UpdateItem(edit_itemid, edit_tid, edit_item_name, bitmap, edit_rate,
                edit_amt + "0", edit_qty, status, item_durations,
                item_message_id);

    }

    private void UpdateItem(String item_id, String item_tid, String item_name,
                            Bitmap img, String item_rate, String item_amt, String item_qty,
                            String item_status, String item_durations, String item_message_id) {
        try {

            dBhelper.open();
            dBhelper.UpdateItemData(new SaleItemModel(item_id, item_tid,
                    item_name, img, item_rate, item_amt, item_qty, item_status,
                    item_durations, item_message_id), item_id);
            dBhelper.close();

            BindSelectTrItems(item_id);
            UpdateTotal();

            if (salesItemListType.equalsIgnoreCase("4")) {
                UpdateAmount();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void BindSelectTrItems(String item_id) {

        trItems = new ArrayList<TrItems>();
        dBhelper.open();
        List<SaleItemModel> models = dBhelper.GetSelectedItem(item_id);

        if (models.size() > 0) {
            for (SaleItemModel model : models) {

                TrItems items = new TrItems();
                items.item_id = model.getItem_id();
                items.item_tid = model.getItem_tid();
                items.item_name = model.getItem_name();
                items.item_rate = model.getItem_rate();
                items.item_amt = model.getItem_amt();
                items.item_qty = model.getItem_qty();
                items.item_status = model.getItem_status();
                items.item_img = model.getItem_img();

                adapter.add(items, EzeidGlobal.LAST_SELECTED_POS);
            }
        }
        dBhelper.close();
    }

    public void EditTransactionItem(String edit_tid, String edit_itemid,
                                    String edit_item_name, String edit_qty, String edit_rate,
                                    String edit_amt, final int position, Bitmap img, String status,
                                    String item_durations, String item_msgid) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.ezeid_transaction_edit,
                (ViewGroup) findViewById(R.id.item_edit_root));
        final Dialog dialog = new Dialog(HDAddDeliveryItems.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialoglayout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        item_edit_amt_layout = (LinearLayout) dialog
                .findViewById(R.id.item_edit_amt_layouts);
        // item_edit_qty_layout = (LinearLayout) dialog
        // .findViewById(R.id.item_edit_qty_layout);
        item_edit_rate = (EditText) dialog.findViewById(R.id.item_edit_rate);
        item_edit_amt = (TextView) dialog.findViewById(R.id.item_edit_amt);
        item_edit_amt_equal = (TextView) dialog
                .findViewById(R.id.item_edit_amt_equal);
        item_edit_qty = (EditText) dialog.findViewById(R.id.item_edit_qty);
        // item_edit_status = (TextView) dialog
        // .findViewById(R.id.item_edit_status);
        item_edit_remove = (Button) dialog.findViewById(R.id.item_edit_remove);
        item_edit_close = (ButtonFloatSmall) dialog
                .findViewById(R.id.item_edit_layout_close);
        item_edit_ok = (Button) dialog.findViewById(R.id.item_edit_ok);
        item_edit_increaseQty = (ImageView) dialog
                .findViewById(R.id.item_edit_increaseQty);
        item_edit_decreaseQty = (ImageView) dialog
                .findViewById(R.id.item_edit_decreaseQty);
        itemName_edit = (TextView) dialog.findViewById(R.id.itemName_edit);
        item_edit_tid = (TextView) dialog.findViewById(R.id.item_edit_tid);
        item_edit_itemid = (TextView) dialog
                .findViewById(R.id.item_edit_itemid);
        item_edit_item_durations = (TextView) dialog
                .findViewById(R.id.item_edit_item_durations);
        item_edit_message_id = (TextView) dialog
                .findViewById(R.id.item_edit_message_id);
        // item_edit_img_progress = (ProgressBar) dialog
        // .findViewById(R.id.item_edit_img_progress);
        item_edit_img = (ImageView) dialog.findViewById(R.id.item_edit_img);

        item_edit_qty.addTextChangedListener(qtyWatcher);

        // edit_qty = edit_qty.substring(5, edit_qty.length());

        item_edit_itemid.setText(edit_itemid);
        item_edit_tid.setText(edit_tid);
        itemName_edit.setText(edit_item_name);
        item_edit_amt.setText(edit_amt + "0");
        item_edit_rate.setText(edit_rate);
        item_edit_qty.setText(edit_qty);
        item_edit_img.setImageBitmap(img);
        item_edit_item_durations.setText(item_durations);
        item_edit_message_id.setText(item_msgid);

        if (status.equalsIgnoreCase("new")) {
            item_edit_remove.setVisibility(View.INVISIBLE);
            item_edit_ok.setText(getResources().getString(R.string.save_order));
        } else {
            item_edit_remove.setVisibility(View.VISIBLE);
            item_edit_ok.setText(getResources().getString(
                    R.string.item_edit_update));
        }
        if (salesItemListType.equalsIgnoreCase("2")) {
            item_edit_amt_layout.setVisibility(View.GONE);
            item_edit_rate.setEnabled(false);
        } else if (salesItemListType.equalsIgnoreCase("3")) {
            item_edit_amt.setVisibility(View.GONE);
            item_edit_amt_equal.setVisibility(View.GONE);
            item_edit_rate.setEnabled(false);
        } else {
            item_edit_rate.setEnabled(true);
            item_edit_amt_equal.setVisibility(View.VISIBLE);
            item_edit_amt.setVisibility(View.VISIBLE);
        }

        item_edit_increaseQty.setOnClickListener(this);
        item_edit_decreaseQty.setOnClickListener(this);

        // GetSalesItemDetailEdit(edit_itemid, edit_tid);
        item_edit_close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                String name = itemName_edit.getText().toString();
                String itemid = item_edit_itemid.getText().toString();
                String tid = item_edit_tid.getText().toString();
                String qty = item_edit_qty.getText().toString();
                String rate = item_edit_rate.getText().toString();
                String amt = item_edit_amt.getText().toString();
                String durations = item_edit_item_durations.getText()
                        .toString();
                String msgid = item_edit_message_id.getText().toString();
                // AddEditedItems(name, itemid, tid, qty, rate, amt, durations);
                BitmapDrawable drawable = (BitmapDrawable) item_edit_img
                        .getDrawable();
                Bitmap img = drawable.getBitmap();
                String update_item = "itemid: " + itemid + ", tid:" + tid
                        + ", name:" + name + ", img:" + img + ", rate:" + rate
                        + ", amt:" + amt + ",qty:" + qty + " edit"
                        + ", duration:" + durations + ", msgid:" + msgid;
                // Log.i(TAG, "update_item:" + update_item);
                UpdateItem(itemid, tid, name, img, rate, amt, qty, "new",
                        durations, msgid);
            }
        });

        if (isItemsEditAgain == true && !messageID.equalsIgnoreCase("")) {
            item_edit_remove.setVisibility(View.GONE);
        }

        item_edit_ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

                String name = itemName_edit.getText().toString();
                String itemid = item_edit_itemid.getText().toString();
                String tid = item_edit_tid.getText().toString();
                String qty = item_edit_qty.getText().toString();
                String rate = item_edit_rate.getText().toString();
                String amt = item_edit_amt.getText().toString();
                String durations = item_edit_item_durations.getText()
                        .toString();
                String msgid = item_edit_message_id.getText().toString();
                // AddEditedItems(name, itemid, tid, qty, rate, amt, durations);
                BitmapDrawable drawable = (BitmapDrawable) item_edit_img
                        .getDrawable();
                Bitmap img = drawable.getBitmap();
                // String update_item = "itemid: " + itemid + ", tid:" + tid
                // + ", name:" + name + ", img:" + img + ", rate:" + rate
                // + ", amt:" + amt + ",qty:" + qty + " edit"
                // + ", duration:" + durations + ", msgid:" + msgid;
                // Log.i(TAG, "update_item:" + update_item);
                UpdateItem(itemid, tid, name, img, rate, amt, qty, "edit",
                        durations, msgid);
            }
        });

        item_edit_remove.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

                String name = itemName_edit.getText().toString();
                String itemid = item_edit_itemid.getText().toString();
                String tid = item_edit_tid.getText().toString();
                // String qty = item_edit_qty.getText().toString();
                // String rate = item_edit_rate.getText().toString();
                String amt = item_edit_amt.getText().toString();
                String durations = item_edit_item_durations.getText()
                        .toString();
                String msgid = item_edit_message_id.getText().toString();
                String qty = "1";
                String rate = item_edit_rate.getText().toString();
                BitmapDrawable drawable = (BitmapDrawable) item_edit_img
                        .getDrawable();
                Bitmap img = drawable.getBitmap();
                String update_item = "itemid: " + itemid + ", tid:" + tid
                        + ", name:" + name + ", img:" + img + ", rate:" + rate
                        + ", amt:" + amt + ",qty:" + qty + " edit"
                        + ", duration:" + durations + ", msgid:" + msgid;
                // Log.i(TAG, "update_item:" + update_item);
                // if (isItemsEditAgain == true &&
                // !messageID.equalsIgnoreCase("")) {
                // UpdateItem(itemid, tid, name, img, rate, amt, qty, "hide",
                // durations, msgid);
                // } else {
                UpdateItem(itemid, tid, name, img, rate, amt, qty, "new",
                        durations, msgid);
                // }
            }
        });

        dialog.show();
    }

    private TextWatcher qtyWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            // TODO Auto-generated method stub
            AmtCalculate();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub

        }
    };

    private void AmtCalculate() {
        if (null != item_edit_amt && null != item_edit_qty
                && null != item_edit_rate) {
            float total = 0;
            String rat = item_edit_rate.getText().toString();
            String quantityS = item_edit_qty.getText().toString();
            if (!quantityS.equalsIgnoreCase("") && !rat.equalsIgnoreCase("")) {
                int quantity = Integer.parseInt(quantityS);
                // String r = rat.substring(6, rat.length());
                float rate = Float.parseFloat(item_edit_rate.getText()
                        .toString());
                // int amt = Integer.parseInt(r);
                if (quantity > 0) {
                    total = (rate) * (quantity);
                    item_edit_amt.setText("" + total + "0");
                }

            } else {
                item_edit_amt.setText("" + rat + "0");
            }
        }
    }

    private String GetCurrentUTCTime() {
        SimpleDateFormat utcFormat = new SimpleDateFormat(
                "MM/dd/yyyy HH:mm:ss", Locale.getDefault());
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return utcFormat.format(Calendar.getInstance().getTime());
    }

    private String NextActionDateUTC() {
        Date date;// 02/25/2015 03:00:56
        if (prefDateTimeStr.equalsIgnoreCase("")) {
            prefDateTimeStr = nextActionDateBox.getText().toString();
        }
        try {
            SimpleDateFormat utcFormat = new SimpleDateFormat(
                    "MM/dd/yyyy KK:mm:ss", Locale.getDefault());
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = utcFormat.parse(prefDateTimeStr);// Wed Feb 25 08:30:56
            // GMT+05:30 2015
            SimpleDateFormat destFormat = new SimpleDateFormat(
                    "MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
            return destFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String NextActionUpDateUTC(String dateS) {
        Date date;// 02/25/2015 03:00:56

        try {
            // 07 Apr 2015 18:41:44 PM, "dd MMM yyyy KK:mm:ss aa"
            SimpleDateFormat utcFormat = new SimpleDateFormat(
                    "dd MMM yyyy KK:mm:ss aa", Locale.getDefault());
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = utcFormat.parse(dateS);// Wed Feb 25 08:30:56
            // GMT+05:30 2015
            SimpleDateFormat destFormat = new SimpleDateFormat(
                    "MM/dd/yyyy HH:mm:ss", Locale.ENGLISH);
            return destFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void SaveExternalOrder() {

        try {
            String ezeid = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("EZEID");
            String to_ezeid = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("BusinessEZEID");
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            if (isRequesterAdded == true) {
                messageText = GetSelectedOrders();
                taskDateTimeStr = GetCurrentUTCTime();
                String locID = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("LocID");

                if (isItemsEditAgain == true) {
                    contactInfo = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("ContactInfo");
                } else {
                    contactInfo = ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("USER_NAME")
                            + " - "
                            + ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("USER_MOBILE");
                }
                salesItemListType = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("HomeDeliveryItemListType");
                update_ezistingNotes = notesBox.getText().toString();
                JSONArray jsonArray = BindAddedTrItems();
                String itemList = jsonArray.toString();
                if (rlatitude.equalsIgnoreCase("")) {
                    rlatitude = "0";
                }
                if (rlongitude.equalsIgnoreCase("")) {
                    rlongitude = "0";
                }
                if (jsonArray.length() > 0) {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("Token", token);
                    hashMap.put("TID", "0");
                    hashMap.put("MessageText", messageText);
                    hashMap.put("item_list_type", salesItemListType);
                    hashMap.put("Status", "0");
                    hashMap.put("TaskDateTime", taskDateTimeStr);
                    hashMap.put("Notes", update_ezistingNotes);
                    hashMap.put("LocID", locID);
                    hashMap.put("Country", country);
                    hashMap.put("State", state);
                    hashMap.put("City", city);
                    hashMap.put("Area", area);
                    hashMap.put("FunctionType", "2");
                    hashMap.put("Latitude", rlatitude);
                    hashMap.put("Longitude", rlongitude);
                    hashMap.put("EZEID", ezeid);
                    hashMap.put("ToEZEID", to_ezeid);
                    hashMap.put("ContactInfo", contactInfo);
                    hashMap.put("FolderRuleID", "0");
                    hashMap.put("Duration", "0");
                    hashMap.put("DurationScales", "0");
                    hashMap.put("DeliveryAddress", addressBox.getText()
                            .toString());
                    hashMap.put("NextAction", "0");
                    hashMap.put("NextActionDateTime", taskDateTimeStr);
                    hashMap.put("ItemsList", itemList);

                    // Log.i("TAG", "hashMap items: " + hashMap.toString());

                    SaveTransactions(hashMap);
                } else {
                    EzeidUtil.showToast(HDAddDeliveryItems.this,
                            "Please add some orders!");
                }
            } else {
                EzeidUtil.showToast(HDAddDeliveryItems.this,
                        "Customer information is not selected!");
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            EzeidUtil.showToast(HDAddDeliveryItems.this,
                    "Exception during saving transaction");
        }

    }

    private JSONArray BindAddedTrItems() throws JSONException {

        JSONObject jsonObject = null;
        JSONArray jsonArray = new JSONArray();
        dBhelper.open();
        List<AddedItems> addedItems = dBhelper.GetAllAddedItems();
        if (addedItems.size() > 0) {
            for (AddedItems items : addedItems) {

                jsonObject = new JSONObject();

                if (isItemsEditAgain == true && !messageID.equalsIgnoreCase("")) {
                    jsonObject.put("TID", items.getItem_tid());
                } else {
                    jsonObject.put("TID", "0");
                }
                String durations = items.getItem_durations();
                if (durations.equalsIgnoreCase("null")) {
                    durations = "0";
                }
                jsonObject.put("ItemID", items.getItem_id());
                jsonObject.put("Durations", durations);

                if (salesItemListType.equalsIgnoreCase("1")) {
                    jsonObject.put("Amount", "0");
                    jsonObject.put("Qty", "0");
                    jsonObject.put("Rate", "0");
                } else if (salesItemListType.equalsIgnoreCase("2")) {
                    jsonObject.put("Amount", "0");
                    jsonObject.put("Qty", "0");
                    jsonObject.put("Rate", "0");
                } else if (salesItemListType.equalsIgnoreCase("3")) {
                    jsonObject.put("Amount", "0");
                    jsonObject.put("Qty", items.getItem_qty());
                    jsonObject.put("Rate", items.getItem_rate());
                } else {
                    jsonObject.put("Amount", items.getItem_amt());
                    jsonObject.put("Qty", items.getItem_qty());
                    jsonObject.put("Rate", items.getItem_rate());
                }

                jsonArray.put(jsonObject);
            }

        } else {
            jsonArray = new JSONArray("[]");
        }
        dBhelper.close();

        JSONObject object = new JSONObject();
        object.put("ItemsList", jsonArray);
        return jsonArray;

    }

    private void SaveItems() {

        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            // String ezeid = ezeidPreferenceHelper
            // .GetValueFromSharedPrefs("EZEID");
            salesItemListType = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("HomeDeliveryItemListType");
            if (isRequesterAdded == true && isItemsEditAgain == false) {
                String to_ezeid = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("EZEID");
                messageText = GetSelectedOrders();
                taskDateTimeStr = GetCurrentUTCTime();
                String locID = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("LocID");

                if (locID.equalsIgnoreCase("")) {
                    locID = "0";
                }

                contactInfo = salesRequester.getText().toString();
                update_ezistingNotes = notesBox.getText().toString();
                if (rlatitude.equalsIgnoreCase("")) {
                    rlatitude = "0";
                }
                if (rlongitude.equalsIgnoreCase("")) {
                    rlongitude = "0";
                }

                String folderRule = "";
                if (!edit_floderID.equalsIgnoreCase("")) {
                    folderRule = edit_floderID;
                } else {
                    folderRule = "0";
                }
                String status = "";
                if (!edit_statusID.equalsIgnoreCase("")) {
                    status = edit_statusID;
                } else {
                    status = "0";
                }
                String nextAction = "";
                if (!edit_nextActionID.equalsIgnoreCase("")) {
                    nextAction = edit_nextActionID;
                } else {
                    nextAction = "0";
                }
                String duration = "0";
                String durationScales = "0";
                update_taskDateTimeStr = NextActionDateUTC();

                JSONArray jsonArray = BindAddedTrItems();
                String itemList = jsonArray.toString();

                if (jsonArray.length() > 0) {
                    if (edit_nextActionID.equalsIgnoreCase("")) {
                        EzeidUtil.showToast(HDAddDeliveryItems.this,
                                "Next Action status is mandatory!");
                        GetRequester();
                    } else if (edit_statusID.equalsIgnoreCase("")) {
                        EzeidUtil.showToast(HDAddDeliveryItems.this,
                                "Delivery status is mandatory!");
                        GetRequester();
                    } else if (jsonArray.length() == 0) {
                        EzeidUtil.showToast(HDAddDeliveryItems.this,
                                "Please add some transactions!");
                    } else {

                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("Token", token);
                        hashMap.put("TID", "0");
                        hashMap.put("MessageText", messageText);
                        hashMap.put("item_list_type", salesItemListType);
                        hashMap.put("Status", status);
                        hashMap.put("TaskDateTime", taskDateTimeStr);
                        hashMap.put("Notes", update_ezistingNotes);
                        hashMap.put("LocID", locID);
                        hashMap.put("Country", country);
                        hashMap.put("State", state);
                        hashMap.put("City", city);
                        hashMap.put("Area", area);
                        hashMap.put("FunctionType", "2");
                        hashMap.put("Latitude", rlatitude);
                        hashMap.put("Longitude", rlongitude);
                        hashMap.put("EZEID", ezeid);
                        hashMap.put("ToEZEID", to_ezeid);
                        hashMap.put("ContactInfo", contactInfo);
                        hashMap.put("FolderRuleID", folderRule);
                        hashMap.put("Duration", duration);
                        hashMap.put("DurationScales", durationScales);
                        hashMap.put("DeliveryAddress", addressBox.getText()
                                .toString());
                        hashMap.put("NextAction", nextAction);
                        hashMap.put("NextActionDateTime",
                                update_taskDateTimeStr);
                        hashMap.put("ItemsList", itemList);

                        // Log.i("TAG", "hashMap items: " + hashMap.toString());

                        SaveTransactions(hashMap);
                    }
                } else {
                    EzeidUtil.showToast(HDAddDeliveryItems.this,
                            "Please add some transactions!");
                }
            } else if (isItemsEditAgain == true
                    && !messageID.equalsIgnoreCase("")) {

                // String status = "";
                // if (!update_statusID.equalsIgnoreCase("")) {
                // status = update_statusID;//
                // } else {
                // status = "0";
                // }

                String taskDateTime = NextActionUpDateUTC(update_taskDateTimeStr);
                String nextactionDateTime = NextActionUpDateUTC(update_nextActionDateStr);

                // String folderRule = "";
                // if (!update_folderRuleID.equalsIgnoreCase("")) {
                // folderRule = update_folderRuleID;
                // } else {
                // folderRule = "0";
                // }

                // String nextAction = "";
                // if (!update_nextActionID.equalsIgnoreCase("")) {
                // nextAction = update_nextActionID;//
                // } else {
                // nextAction = "0";
                // }
                String status = "0";
                if (isEdited == true) {
                    status = edit_statusID;
                } else {
                    status = update_statusID;
                }
                // if (status.equalsIgnoreCase("")) {
                // status = "0";
                // }

                // String taskDateTime = taskDateTimeStr;

                String folderRule = "0";
                if (isEdited == true) {
                    folderRule = edit_floderID;
                } else {
                    folderRule = update_folderRuleID;
                }

                // if (!folderRuleID.equalsIgnoreCase("")) {
                // folderRule = folderRuleID;
                // } else {
                // folderRule = "0";
                // }

                String nextAction = "0";
                if (isEdited == true) {
                    nextAction = edit_nextActionID;
                } else {
                    nextAction = update_nextActionID;
                }
                update_messageText = GetSelectedOrders();

                JSONArray jsonArray = BindAddedTrItems();
                String itemList = jsonArray.toString();

                if (jsonArray.length() > 0) {

                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("Token", token);
                    hashMap.put("TID", messageID);
                    hashMap.put("MessageText", update_messageText);
                    hashMap.put("item_list_type", salesItemListType);
                    hashMap.put("Status", status);
                    hashMap.put("TaskDateTime", taskDateTime);
                    hashMap.put("Notes", update_ezistingNotes);
                    hashMap.put("LocID", update_locID);
                    hashMap.put("Country", "");
                    hashMap.put("State", "");
                    hashMap.put("City", "");
                    hashMap.put("Area", "");
                    hashMap.put("FunctionType", "0");
                    hashMap.put("Latitude", "0");
                    hashMap.put("Longitude", "0");
                    hashMap.put("EZEID", ezeid);
                    hashMap.put("ToEZEID", req_ezeid);
                    hashMap.put("ContactInfo", update_contactInfo);
                    hashMap.put("FolderRuleID", folderRule);
                    hashMap.put("Duration", "0");
                    hashMap.put("DurationScales", "0");
                    hashMap.put("DeliveryAddress", deliveryAddress);
                    hashMap.put("NextAction", nextAction);
                    hashMap.put("NextActionDateTime", nextactionDateTime);
                    hashMap.put("ItemsList", itemList);

                    // Log.i("TAG", "update hashMap items: " +
                    // hashMap.toString());

                    SaveTransactions(hashMap);
                } else {

                    // cancel confirmations
                    // CancelConfirmations();
                    // Log.i("TAG", "ItemsList is empty!");
                }
            } else {
                GetRequester();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SaveTransactions(HashMap<String, String> hashMap) {

        ShowEzeidDialog("Saving your order...");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, EZEIDUrlManager.getAPIUrl()
                + "ewtSaveTranscation", new JSONObject(hashMap),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        // TODO Auto-generated method stub
                        try {
                            DismissEzeidDialog();
                            if (null != response) {
                                String result = response
                                        .getString("IsSuccessfull");
                                if (result.equalsIgnoreCase("true"))
                                    EzeidUtil.showToast(
                                            HDAddDeliveryItems.this,
                                            "Transaction saved Successfully");
                                NavToTrList();
                            } else {
                                EzeidUtil.showToast(HDAddDeliveryItems.this,
                                        "Transaction saved failed");
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        // Log.i(TAG, "hashMap items: " + response.toString());

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub
                DismissEzeidDialog();
                if (error instanceof TimeoutError) {
                    EzeidUtil.showToast(HDAddDeliveryItems.this,
                            "Connection timeout. Please try again");
                } else if (error instanceof ServerError
                        || error instanceof AuthFailureError) {
                    EzeidUtil
                            .showToast(HDAddDeliveryItems.this,
                                    "Unable to connect server. Please try later");
                } else if (error instanceof NetworkError
                        || error instanceof NoConnectionError) {
                    EzeidUtil.showToast(HDAddDeliveryItems.this,
                            "Network is unreachable");
                } else {
                    EzeidUtil.showToast(HDAddDeliveryItems.this,
                            "Send failed");
                }
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);
    }

    private void NavToTrList() {

        dBhelper.open();
        dBhelper.DeleteAllItems();
        dBhelper.close();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                finish();
            }
        }, 2500);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            case R.id.submit_order:
            /*
			 * if (isRequesterAdded == false) { contact_info_text
			 * .setText("Contact Information is not selected"); expand(); } else
			 * { collapse(); SaveItems(); }
			 */
                // if (swipe_count_layout.getVisibility() != View.VISIBLE) {
                //
                // }
                if (ezeidPreferenceHelper.GetValueFromSharedPrefs(
                        "ITEM_TO_ADD_USER").equalsIgnoreCase("business_mgr")) {

                    SaveItems();
                } else {
                    SaveExternalOrder();
                }
                break;

            // case R.id.item_swipe_qty_close:
            // collapse();
            // break;

            case R.id.cancel_order:
                // dBhelper.open();
                // dBhelper.DeleteAllItems();
                // dBhelper.close();
                finish();
                break;

            case R.id.itemAdd:
                // AddSelectedItems();
                break;

            case R.id.saveSaleItemBtn:
                // GetRequesterDetails();
                // SaveItems();
                break;

            case R.id.nextActionEdit:
                SetCalendar();
                GetDatePicker();
                break;

            case R.id.item_edit_increaseQty:
                SetQtyDynamic("plus");
                break;
            case R.id.item_edit_decreaseQty:
                SetQtyDynamic("minus");
                break;

            // case R.id.item_swipe_increase_Qty:
            // SetQtySwipe("plus");
            // break;
            // case R.id.item_swipe_decrease_Qty:
            // SetQtySwipe("minus");
            // break;

            case R.id.contact_info:
                GetRequester();
                break;

            // case R.id.item_swipe_qty_update:
            // collapse();
            // UpdateSwipeItem();
            // break;

        }
    }

    private void SetQtyDynamic(String opt) {

        if (null != item_edit_qty) {
            String qtyStr = item_edit_qty.getText().toString();
            // float rate =
            // Float.parseFloat(item_edit_rate.getText().toString());
            if (!qtyStr.equalsIgnoreCase("")) {
                int qty = Integer.parseInt(qtyStr);
                if (qty < 100) {
                    // float amt = 0;
                    if (qty == 0) {
                        qty = 1;
                    }
                    if (qty >= 1) {
                        if (opt.equalsIgnoreCase("plus")) {
                            qty++;
                            // amt = (rate) * (qty);
                        } else {
                            qty--;
                            // amt = (rate) * (qty);
                        }
                    }
                    if (qty != 0) {
                        item_edit_qty.setText("" + qty);
                        // item_edit_amt.setText("Rs. " + amt);
                    } else {
                        item_edit_qty.setText(qtyStr);
                        // item_edit_amt.setText("Rs. "
                        // + item_edit_rate.getText().toString());
                    }
                }
            }
        }

    }

    private void GetRequester() {
        // isItemsEditAgain = false;

        if (ezeidPreferenceHelper.GetValueFromSharedPrefs("ITEM_TO_ADD_USER")
                .equalsIgnoreCase("business_mgr")) {
            // if (EzeidGlobal.EZEID_TYPE.equalsIgnoreCase("1")) {
            // GetRequesterDetails();
            // } else {
            ShowEzeidDialog("Please wait!");
            BindActionStatusFilters();
            BindNextActionFilters();
            BindFolderFilters();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    DismissEzeidDialog();
                    GetRequesterDetails();
                    if (isItemsEditAgain == true
                            && !messageID.equalsIgnoreCase("")) {
                        if (null != salesRequester) {
                            if (bundle.getString("EZEID").isEmpty()
                                    || bundle.getString("EZEID") == null
                                    || bundle.getString("EZEID")
                                    .equalsIgnoreCase("null")) {
                                salesRequester.setText("");
                            } else {
                                salesRequester.setText(bundle
                                        .getString("EZEID"));
                                GetPrimaryDetails(bundle.getString("EZEID"));
                            }
                        }
                    }
                }
            }, 3000);
            // }
        } else {
            // GetPrimaryDetails(ezeidPreferenceHelper
            // .GetValueFromSharedPrefs("EZEID"));
            isRequesterAdded = true;

            GetConsumerDetails();
        }
    }

    private void GetConsumerDetails() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(
                R.layout.ezeid_add_requester_info_layout_external,
                (ViewGroup) findViewById(R.id.requester_external));
        final Dialog dialog = new Dialog(HDAddDeliveryItems.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialoglayout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        locationFilter = (Spinner) dialog.findViewById(R.id.locFilter);
        LinearLayout reqphoneLayout = (LinearLayout) dialog
                .findViewById(R.id.reqphoneLayout);
        TextView ezeids = (TextView) dialog.findViewById(R.id.contact_ezeid);
        TextView reqName = (TextView) dialog.findViewById(R.id.reqName);
        TextView reqMobile = (TextView) dialog.findViewById(R.id.reqMobile);
        TextView reqphone = (TextView) dialog.findViewById(R.id.reqphone);
        addressBox = (EditText) dialog.findViewById(R.id.addressBox);
        notesBox = (EditText) dialog.findViewById(R.id.notesBox);
        requseterOk = (Button) dialog.findViewById(R.id.requseterOk);
        requseterCancel = (Button) dialog.findViewById(R.id.requseterCancel);
        ezeids.setText(ezeidPreferenceHelper.GetValueFromSharedPrefs("EZEID"));
        reqName.setText(ezeidPreferenceHelper
                .GetValueFromSharedPrefs("USER_NAME"));
        reqMobile.setText(ezeidPreferenceHelper
                .GetValueFromSharedPrefs("USER_MOBILE"));
        if (!ezeidPreferenceHelper.GetValueFromSharedPrefs("USER_PHONE")
                .equalsIgnoreCase("")) {
            reqphone.setText(ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("USER_PHONE"));
        } else {
            reqphoneLayout.setVisibility(View.GONE);
        }

        if (locationAdapter != null) {
            locationFilter.setAdapter(locationAdapter);
        }
        locationFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                rlatitude = ((TextView) view.findViewById(R.id.lati)).getText()
                        .toString();
                rlongitude = ((TextView) view.findViewById(R.id.longi))
                        .getText().toString();

                String masterTag = ((TextView) view
                        .findViewById(R.id.masterTag)).getText().toString();
                if (masterTag.equalsIgnoreCase("< Other >")) {
                    addressBox.setVisibility(View.VISIBLE);
                } else {
                    addressBox.setVisibility(View.GONE);
                    if (!rlatitude.equalsIgnoreCase("")
                            && !rlongitude.equalsIgnoreCase("")) {
                        GetAddress(rlatitude, rlongitude);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });
        requseterCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // menu_deliver_persion.getItem(0).setIcon(
                // R.drawable.ic_action_requester_normal);
                // if (isItemsEditAgain == false) {
                // contact_info_text.setText(Html.fromHtml(getResources()
                // .getString(R.string.customer_info_warn)));
                // isRequesterAdded = false;
                // } else {
                // contact_info_text.setText(Html.fromHtml(getResources()
                // .getString(R.string.customer_info_msg)));
                // isRequesterAdded = true;
                // }

            }
        });
        requseterOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                dialog.dismiss();
                isRequesterAdded = true;
                contact_info_text.setText(Html.fromHtml(getResources()
                        .getString(R.string.customer_info_msg)));

            }
        });
        dialog.show();
    }

    private void GetRequesterDetails() {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(
                R.layout.ezeid_add_requester_info_layout,
                (ViewGroup) findViewById(R.id.requesterRoot));
        final Dialog dialog = new Dialog(HDAddDeliveryItems.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialoglayout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        salesRequester = (EditText) dialog.findViewById(R.id.salesRequesters);
        addressBox = (EditText) dialog.findViewById(R.id.addressBox);

        notesBox = (EditText) dialog.findViewById(R.id.notesBox);
        nextActionDateBox = (EditText) dialog
                .findViewById(R.id.nextActionDateBox);
        nextActionDateBox.setEnabled(false);
        nextActionDateBox.setTextColor(Color.rgb(0, 122, 165));
        nextActionDateBox.setText(update_taskDateTimeStr);
        notesBox.setText(update_ezistingNotes);
        editActionDate = (ButtonFloatSmall) dialog
                .findViewById(R.id.nextActionEdit);
        checkEzeidAvailability = (ButtonFloatSmall) dialog
                .findViewById(R.id.checkEzeidAvailability);
        requseterOk = (Button) dialog.findViewById(R.id.requseterOk);
        requseterCancel = (Button) dialog.findViewById(R.id.requseterCancel);
        statusFilter = (Spinner) dialog.findViewById(R.id.statusFilter);
        actionFilter = (Spinner) dialog.findViewById(R.id.actionFilter);
        folderFilter = (Spinner) dialog.findViewById(R.id.folderFilter);
        locationFilter = (Spinner) dialog.findViewById(R.id.locFilter);
        checkEzeidStatusText = (TextView) dialog
                .findViewById(R.id.checkEzeidStatus);
        checkEzeidStatusLine = (TextView) dialog
                .findViewById(R.id.checkEzeidStatusBar);
        // notesLayout = (LinearLayout) dialog.findViewById(R.id.notesLayout);

        folderLayout = (LinearLayout) dialog.findViewById(R.id.folderLayout);
        // reqStatuslayout = (LinearLayout)
        // dialog.findViewById(R.id.statusLayout);
        // reqActionLayout = (LinearLayout)
        // dialog.findViewById(R.id.actionLayout);
        // reqActionDateLayout = (LinearLayout) dialog
        // .findViewById(R.id.actionDateLayout);
        // reqAvailabilityLayout = (LinearLayout) dialog
        // .findViewById(R.id.salesRequesterLayout);
        // userTypeLayout = (LinearLayout) dialog
        // .findViewById(R.id.userTypeLayout);

        // reqBar1 = (TextView) dialog.findViewById(R.id.bar1);
        // reqBar2 = (TextView) dialog.findViewById(R.id.bar2);

        checkEzeidStatusLayout = (LinearLayout) dialog
                .findViewById(R.id.checkEzeidStatusLayout);
        switchLoc = (TextView) dialog.findViewById(R.id.switchCurloc);

        userType = (RadioGroup) dialog.findViewById(R.id.userType);
        custEzeid = (RadioButton) dialog.findViewById(R.id.custEzeid);
        custOther = (RadioButton) dialog.findViewById(R.id.custOther);
        editActionDate.setOnClickListener(this);
        custEzeid.setChecked(true);
        // toggle delivery locations
        // switchLoc.setChecked(true);
        // EzeidUtil.ChangeEditEffect(folderLayout, EzeidAddSalesItems.this);
        // folderLayout.setBackgroundColor(Color.rgb(216, 216, 216));
        // folderFilter.setEnabled(false);

        if (bundle != null) {
            addressBox.setText(bundle.getString("DeliveryAddress"));
            notesBox.setText(bundle.getString("Notes_lbl"));
            nextActionDateBox.setText(bundle.getString("Next_action_date"));
            // statusFilter.setSelection(Integer.parseInt(bundle
            // .getString("StatusID")));
            // actionFilter.setSelection(Integer.parseInt(bundle
            // .getString("ActionID")));
            // folderFilter.setSelection(Integer.parseInt(bundle
            // .getString("FolderID")));

            addressBox.setText(bundle.getString("DeliveryAddress"));
            if (bundle.getString("EZEID").isEmpty()
                    || bundle.getString("EZEID") == null
                    || bundle.getString("EZEID").equalsIgnoreCase("null")) {
                salesRequester.setText("");
            } else {
                salesRequester.setText(bundle.getString("EZEID"));
                GetPrimaryDetails(bundle.getString("EZEID"));
            }
            requseterOk.setText("OK");

        }

        if (custEzeid.isChecked() == true) {
            checkEzeidAvailability.setVisibility(View.VISIBLE);
        } else {
            checkEzeidAvailability.setVisibility(View.GONE);
        }

        userType.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (custEzeid.isChecked() == true) {
                    salesRequester.setHint("Contact Informations");
                    addressBox.setVisibility(View.GONE);
                    checkEzeidStatusLine.setVisibility(View.VISIBLE);
                    checkEzeidAvailability.setVisibility(View.VISIBLE);
                    checkEzeidStatusLayout.setVisibility(View.VISIBLE);
                } else if (custOther.isChecked()) {
                    salesRequester.setHint("Contact name and phone no");
                    addressBox.setVisibility(View.GONE);
                    checkEzeidStatusLine.setVisibility(View.GONE);
                    checkEzeidAvailability.setVisibility(View.GONE);
                    checkEzeidStatusLayout.setVisibility(View.GONE);
                }
            }
        });

        contactInfoProgress = (ProgressBar) dialog
                .findViewById(R.id.contactInfoProgress);
        contactInfoProgress.getIndeterminateDrawable().setColorFilter(
                Color.rgb(50, 175, 230),
                android.graphics.PorterDuff.Mode.MULTIPLY);
        // BindUserFilter();

        statusFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                edit_statusID = ((TextView) view.findViewById(R.id.masterId))
                        .getText().toString();
                if (status_count == 0) {
                    isEdited = false;
                    status_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });
        actionFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                edit_nextActionID = ((TextView) view
                        .findViewById(R.id.masterId)).getText().toString();
                if (action_count == 0) {
                    isEdited = false;
                    action_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });
        folderFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                edit_floderID = ((TextView) view.findViewById(R.id.folderTID))
                        .getText().toString();
                if (folder_count == 0) {
                    isEdited = false;
                    folder_count++;
                } else {
                    isEdited = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                isEdited = false;
            }
        });

        locationFilter.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                rlatitude = ((TextView) view.findViewById(R.id.lati)).getText()
                        .toString();
                rlongitude = ((TextView) view.findViewById(R.id.longi))
                        .getText().toString();
                // showToast("locationFilter: location=" + rlatitude + " "
                // + rlongitude);
                String masterTag = ((TextView) view
                        .findViewById(R.id.masterTag)).getText().toString();
                if (masterTag.equalsIgnoreCase("< Other >")) {
                    addressBox.setVisibility(View.VISIBLE);
                } else {
                    addressBox.setVisibility(View.GONE);
                    if (!rlatitude.equalsIgnoreCase("")
                            && !rlongitude.equalsIgnoreCase("")) {
                        GetAddress(rlatitude, rlongitude);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        editActionDate.setOnClickListener(this);
        // salesRequester.addTextChangedListener(requesterWatcher);

        requseterCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // menu_deliver_persion.getItem(0).setIcon(
                // R.drawable.ic_action_requester_normal);
                if (isItemsEditAgain == false) {
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_warn)));
                    isRequesterAdded = false;
                } else {
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                    isRequesterAdded = true;
                }

                // messageID = bundle.getString("MessageID");
                // if (messageID.equalsIgnoreCase("")) {
                // contact_info_text.setText(Html.fromHtml(getResources()
                // .getString(R.string.customer_info_warn)));
                // isRequesterAdded = false;
                // } else {
                // contact_info_text.setText(Html.fromHtml(getResources()
                // .getString(R.string.customer_info_msg)));
                // isRequesterAdded = true;
                // }
            }
        });
        requseterOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // validation here.
                if (salesRequester.getText().toString().equalsIgnoreCase("")) {
                    checkEzeidStatusText
                            .setText("Requester EZEID is mandatory!");
                } else if (nextActionDateBox.getText().toString()
                        .equalsIgnoreCase("")) {
                    showToast("Next action date is mandatory!");
                } else {
                    isRequesterAdded = true;
                    // menu_deliver_persion.getItem(0).setIcon(
                    // R.drawable.ic_action_requester_selected);
                    contact_info_text.setText(Html.fromHtml(getResources()
                            .getString(R.string.customer_info_msg)));
                    dialog.dismiss();
                }
            }
        });
        if (statusAdapter != null) {
            statusFilter.setAdapter(statusAdapter);
            if (bundle != null) {
                SetDefaultStatus();
            }
        } else {
            BindActionStatusFilters();
        }
        if (actionAdapter != null) {
            actionFilter.setAdapter(actionAdapter);
            if (bundle != null) {
                SetDefaultActions();
            }
        } else {
            BindNextActionFilters();
        }
        if (folderAdapter != null) {
            folderFilter.setAdapter(folderAdapter);
            if (bundle != null) {
                SetDefaultFolder();
            }
        } else {
            BindFolderFilters();
        }

        if (locationAdapter != null) {
            locationFilter.setAdapter(locationAdapter);
        } else {
            BindLocationsFilters();
        }

		/*
		 * String masterID = ezeidPreferenceHelper
		 * .GetValueFromSharedPrefs("MasterIDLog"); if
		 * (masterID.equalsIgnoreCase("0")) { if (locationAdapter != null) {
		 * locationLayout.setVisibility(View.VISIBLE);
		 * locationFilter.setAdapter(locationAdapter); } } else {
		 * locationLayout.setVisibility(View.GONE); }
		 */

        switchLoc.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // is switchLoc checked?
                if (((CheckBox) v).isChecked()) {
                    EzeidUtil.ChangeEditEffect(folderLayout,
                            HDAddDeliveryItems.this);
                    folderLayout.setBackgroundColor(Color.rgb(216, 216, 216));
                    folderFilter.setEnabled(false);
                } else {
                    folderLayout.setBackgroundColor(Color.rgb(255, 255, 255));
                    folderFilter.setEnabled(true);
                }

            }
        });
        checkEzeidAvailability.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!salesRequester.getText().toString().equalsIgnoreCase("")) {
                    GetPrimaryDetails(salesRequester.getText().toString());
                }
            }
        });

        dialog.show();
    }

    void showToast(CharSequence msg) {
        Toast.makeText(HDAddDeliveryItems.this, msg, Toast.LENGTH_SHORT).show();
    }

    private void SetDefaultStatus() {
        // statusID
        if (status_array != null) {
            if (status_array.size() > 0) {
                int count = 0;
                for (HashMap<String, String> hashMap : status_array) {
                    String status = hashMap.get("TID");
                    if (bundle.getString("StatusID").equalsIgnoreCase(status)) {
                        statusFilter.setSelection(count);
                    } else {
                        count++;
                    }
                }
            }
        }
    }

    private void SetDefaultActions() {
        if (action_array != null) {
            if (action_array.size() > 0) {
                int count = 0;
                for (HashMap<String, String> hashMap : action_array) {
                    String status = hashMap.get("TID");
                    if (bundle.getString("ActionID").equalsIgnoreCase(status)) {
                        actionFilter.setSelection(count);
                    } else {
                        count++;
                    }
                }
            }
        }
    }

    private void SetDefaultFolder() {
        // statusID
        if (folder_array != null) {
            if (folder_array.size() > 0) {
                int count = 0;
                for (HashMap<String, String> hashMap : folder_array) {
                    String status = hashMap.get("TID");
                    if (bundle.getString("FolderID").equalsIgnoreCase(status)) {
                        folderFilter.setSelection(count);
                    } else {
                        count++;
                    }
                }
            }
        }
    }

    private void SetCalendar() {
        calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), false);
        timePickerDialog = TimePickerDialog.newInstance(this,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), true, false);
    }

    private void GetDatePicker() {
        datePickerDialog.setVibrate(false);
        datePickerDialog.setYearRange(1985, 2028);
        datePickerDialog.setCloseOnSingleTapDay(false);
        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
    }

    private void GetTimePicker() {
        timePickerDialog.setVibrate(false);
        timePickerDialog.setCloseOnSingleTapMinute(false);
        timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
    }

    private String PreferedDateNormal(String preferedDate) {
        Date date;// 02/26/2015 02:00:29
        try {
            SimpleDateFormat normalFormat = new SimpleDateFormat(
                    "MM/dd/yyyy KK:mm:ss", Locale.getDefault());
            date = normalFormat.parse(preferedDate);

            SimpleDateFormat destFormat = new SimpleDateFormat(
                    "MMM dd yyyy HH:mm", Locale.ENGLISH);

            return destFormat.format(date);// Feb 26 2015 02:00 AM
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String GetSelectedOrders() {

        String selected_items = "";
        StringBuilder builder = null;
        dBhelper.open();
        List<AddedItems> addedItems = dBhelper.GetAllAddedItems();
        // ArrayList<HashMap<String, String>> titleList = EzeidGlobal.titleList;
        // if (titleList.size() != 0) {
        // salesItemListType = titleList.get(0).get("SalesItemListType");
        // }
        if (addedItems.size() > 0) {
            builder = new StringBuilder();
            for (AddedItems items : addedItems) {

                if (salesItemListType.equalsIgnoreCase("1")) {
                    builder.append(items.getItem_name());
                    builder.append(",");
                } else if (salesItemListType.equalsIgnoreCase("2")) {
                    builder.append(items.getItem_name());
                    // builder.append("(");
                    // // builder.append(items.getItem_qty());
                    // builder.append(")");
                    builder.append(",");
                } else {
                    builder.append(items.getItem_name());
                    builder.append("(");
                    builder.append(items.getItem_qty());
                    builder.append(")");
                    builder.append(",");
                }

                // builder.append(items.getItem_name());
                // builder.append("(");
                // builder.append(items.getItem_qty());
                // builder.append(")");
                // builder.append(",");
            }

            selected_items = builder.toString();
            if (selected_items.length() > 0
                    && selected_items.charAt(selected_items.length() - 1) == ',') {
                selected_items = selected_items.substring(0,
                        selected_items.length() - 1);
            }
        } else {
            selected_items = "";
        }

        return selected_items;
    }

    private void BindFilterTrItems(String name) {

        trItems = new ArrayList<TrItems>();
        dBhelper.open();
        List<SaleItemModel> models = dBhelper.GetAllFilteredItems(name);

        if (models.size() > 0) {
            for (SaleItemModel model : models) {

                TrItems items = new TrItems();
                items.item_id = model.getItem_id();
                items.item_tid = model.getItem_tid();
                items.item_name = model.getItem_name();
                items.item_rate = model.getItem_rate();
                items.item_amt = model.getItem_amt();
                items.item_qty = model.getItem_qty();
                items.item_status = model.getItem_status();
                items.item_img = model.getItem_img();
                trItems.add(items);
            }
            if (trItems.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                adapter = new MainAdapter(HDAddDeliveryItems.this, trItems);
                recyclerView.setAdapter(adapter);
            }

        } else {
            // no items found
            recyclerView.setVisibility(View.INVISIBLE);
            EzeidUtil.showToast(HDAddDeliveryItems.this, "No items found!");
        }
        dBhelper.close();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,
                               long id) {
        // String tag = null;
        switch (parent.getId()) {

            case R.id.folderFilter:
                edit_floderID = ((TextView) view.findViewById(R.id.folderTID))
                        .getText().toString();
                // tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
                // .toString();
                // String percentage = ((TextView) view
                // .findViewById(R.id.masterPercentage)).getText().toString();
                // showToast("statusFilter: masterId=" + folderRuleID + ", tag= "
                // + tag + ", percentage=" + percentage);
                break;

            case R.id.statusFilter:
                edit_statusID = ((TextView) view.findViewById(R.id.masterId))
                        .getText().toString();
                // tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
                // .toString();
                // showToast("statusFilter: masterId=" + statusID + ", tag= " +
                // tag);
                break;

            case R.id.actionFilter:
                edit_nextActionID = ((TextView) view.findViewById(R.id.masterId))
                        .getText().toString();
                // tag = ((TextView) view.findViewById(R.id.masterTag)).getText()
                // .toString();
                // showToast("statusFilter: masterId=" + nextActionID + ", tag= "
                // + tag);
                break;

            default:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        // YYYY-MM-DDThh:mm:ss.sTZD
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss",
                Locale.getDefault());
        // String timeZoneId = TimeZone.getDefault().getID();
        // df.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = df.format(calendar.getTime());
        formattedDate = formattedDate.substring(17, formattedDate.length());

        String hourStr = "", minStr = "";
        int hour = String.valueOf(hourOfDay).trim().length();
        if (hour == 1) {
            hourStr = "0" + hourOfDay;
        } else {
            hourStr = "" + hourOfDay;
        }
        int min = String.valueOf(minute).trim().length();
        if (min == 1) {
            minStr = "0" + minute;
        } else {
            minStr = "" + minute;
        }

        timeStr = hourStr + ":" + minStr + ":" + formattedDate;
        prefDateTimeStr = dateStr + " " + timeStr;
        nextActionDateBox.setVisibility(View.VISIBLE);
        nextActionDateBox.setText("" + PreferedDateNormal(prefDateTimeStr));

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year,
                          int month, int day) {
        dateStr = "";
        timeStr = "";
        nextActionDateBox.setText("");
        month = month + 1;
        int mon = String.valueOf(month).trim().length();
        if (mon == 1) {
            dateStr = "0" + month + "/" + day + "/" + year;
        } else {
            dateStr = month + "/" + day + "/" + year;
        }
        GetTimePicker();
    }

    private void BindLocationsFilters() {

        try {

            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetLocationList?Token=" + token;
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindLocations(response);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindLocations(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void BindNextActionFilters() {

        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetActionType?Token=" + token + "&MasterID="
                    + masterID + "&FunctionType=2";

            // String url = "http://10.0.100.199:3001/ewtGetActionType?Token="
            // + token + "&MasterID=" + masterID + "&FunctionType=1";
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindActions(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindActions(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void BindActionStatusFilters() {

        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetStatusType?Token=" + token + "&MasterID="
                    + masterID + "&FunctionType=2";
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindStatus(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindStatus(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void BindActions(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    action_array = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String masterID = jsonObject.getString("MasterID");
                        String actionTitle = jsonObject
                                .getString("ActionTitle");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("MasterID", masterID);
                        map.put("ActionTitle", actionTitle);
                        action_array.add(map);

                        actionAdapter = new SimpleAdapter(
                                HDAddDeliveryItems.this, action_array,
                                R.layout.ezeid_tagid_row, new String[]{"TID",
                                "MasterID", "ActionTitle"}, new int[]{
                                R.id.masterId, R.id.masterPercentage,
                                R.id.masterTag});
                        // actionFilter.setAdapter(actionAdapter);
                    }
                }
            } else {
                action_array = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("TID", "0");
                map.put("MasterID", "0");
                map.put("ActionTitle", "No next actions added");
                action_array.add(map);

                actionAdapter = new SimpleAdapter(HDAddDeliveryItems.this,
                        action_array, R.layout.ezeid_tagid_row, new String[]{
                        "TID", "MasterID", "ActionTitle"}, new int[]{
                        R.id.masterId, R.id.masterPercentage,
                        R.id.masterTag});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindLocations(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();

                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String masterID = jsonObject.getString("MasterID");
                        String locTitle = jsonObject.getString("LocTitle");
                        String latitude = jsonObject.getString("Latitude");
                        String longitude = jsonObject.getString("Longitude");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("MasterID", masterID);
                        map.put("LocTitle", locTitle);
                        map.put("Latitude", latitude);
                        map.put("Longitude", longitude);
                        arrayList.add(map);

                        // actionFilter.setAdapter(actionAdapter);
                    }

                    HashMap<String, String> map1 = new HashMap<String, String>();
                    map1.put("TID", "0");
                    map1.put("MasterID", "0");
                    map1.put("LocTitle", "Current Location");
                    map1.put("Latitude", ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Latitude"));
                    map1.put("Longitude", ezeidPreferenceHelper
                            .GetValueFromSharedPrefs("Longitude"));
                    arrayList.add(map1);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("TID", "0");
                    map.put("MasterID", "0");
                    map.put("LocTitle", "< Other >");
                    map.put("Latitude", "");
                    map.put("Longitude", "");
                    arrayList.add(map);

                    locationAdapter = new SimpleAdapter(
                            HDAddDeliveryItems.this, arrayList,
                            R.layout.ezeid_tagid_row, new String[]{"TID",
                            "MasterID", "LocTitle", "Latitude",
                            "Longitude"}, new int[]{R.id.masterId,
                            R.id.masterPercentage, R.id.masterTag,
                            R.id.lati, R.id.longi});
                }
            } else {
                ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("TID", "0");
                map.put("MasterID", "0");
                map.put("LocTitle", "No locations added");
                map.put("Latitude", "");
                map.put("Longitude", "");
                arrayList.add(map);

                locationAdapter = new SimpleAdapter(HDAddDeliveryItems.this,
                        arrayList, R.layout.ezeid_tagid_row, new String[]{
                        "TID", "MasterID", "LocTitle", "Latitude",
                        "Longitude"}, new int[]{R.id.masterId,
                        R.id.masterPercentage, R.id.masterTag,
                        R.id.lati, R.id.longi});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindStatus(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    status_array = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String statusTitle = jsonObject
                                .getString("StatusTitle");
                        String progressPercent = jsonObject
                                .getString("ProgressPercent");
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("StatusTitle", statusTitle);
                        map.put("ProgressPercent", progressPercent);
                        status_array.add(map);

                        statusAdapter = new SimpleAdapter(
                                HDAddDeliveryItems.this, status_array,
                                R.layout.ezeid_tagid_row, new String[]{"TID",
                                "StatusTitle", "ProgressPercent"},
                                new int[]{R.id.masterId, R.id.masterTag,
                                        R.id.masterPercentage});
                        // statusFilter.setAdapter(statusAdapter);
                    }
                }
            } else {
                status_array = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("TID", "0");
                map.put("StatusTitle", "No stages added");
                map.put("ProgressPercent", "");
                status_array.add(map);

                statusAdapter = new SimpleAdapter(HDAddDeliveryItems.this,
                        status_array, R.layout.ezeid_tagid_row, new String[]{
                        "TID", "StatusTitle", "ProgressPercent"},
                        new int[]{R.id.masterId, R.id.masterTag,
                                R.id.masterPercentage});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void BindFolderFilters() {
        try {
            String token = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("Token");
            String masterID = ezeidPreferenceHelper
                    .GetValueFromSharedPrefs("MasterID");
            String url = EZEIDUrlManager.getAPIUrl()
                    + "ewtGetFolderList?Token=" + token + "&MasterID="
                    + masterID + "&FunctionType=2";
            // String url = "http://10.0.100.199:3001/ewtGetFolderList?Token="
            // + token + "&MasterID=" + masterID + "&FunctionType=1";
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            BindFolders(response);

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    BindFolders(null);
                }
            });
            jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void BindFolders(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len != 0) {
                    folder_array = new ArrayList<HashMap<String, String>>();
                    for (int i = 0; i < len; i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String tid = jsonObject.getString("TID");
                        String masterID = jsonObject.getString("MasterID");
                        String folderTitle = jsonObject
                                .getString("FolderTitle");
                        String ruleFunction = jsonObject
                                .getString("RuleFunction");
                        String ruleType = jsonObject.getString("RuleType");
                        String countryIDs = jsonObject.getString("CountryIDs");
                        String matchAdminLevel = jsonObject
                                .getString("MatchAdminLevel");
                        String mappedNames = jsonObject
                                .getString("MappedNames");
                        String latitude = jsonObject.getString("Latitude");
                        String longitude = jsonObject.getString("Longitude");

                        String proximity = jsonObject.getString("Proximity");
                        String defaultFolder = jsonObject
                                .getString("DefaultFolder");
                        String folderStatus = jsonObject
                                .getString("FolderStatus");
                        String seqNoFrefix = jsonObject
                                .getString("SeqNoFrefix");
                        String runningSeqNo = jsonObject
                                .getString("RunningSeqNo");
                        String createdDate = jsonObject
                                .getString("CreatedDate");
                        String lUDate = jsonObject.getString("LUDate");

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("TID", tid);
                        map.put("MasterID", masterID);
                        map.put("FolderTitle", folderTitle);
                        map.put("RuleFunction", ruleFunction);
                        map.put("RuleType", ruleType);
                        map.put("CountryIDs", countryIDs);
                        map.put("MatchAdminLevel", matchAdminLevel);
                        map.put("MappedNames", mappedNames);
                        map.put("Latitude", latitude);
                        map.put("Longitude", longitude);
                        map.put("Proximity", proximity);
                        map.put("DefaultFolder", defaultFolder);
                        map.put("FolderStatus", folderStatus);
                        map.put("SeqNoFrefix", seqNoFrefix);
                        map.put("RunningSeqNo", runningSeqNo);
                        map.put("CreatedDate", createdDate);
                        map.put("LUDate", lUDate);
                        folder_array.add(map);

                        folderAdapter = new SimpleAdapter(
                                HDAddDeliveryItems.this, folder_array,
                                R.layout.ezeid_folder_row,
                                new String[]{"TID", "MasterID",
                                        "FolderTitle", "RuleFunction",
                                        "RuleType", "CountryIDs",
                                        "MatchAdminLevel", "MappedNames",
                                        "Latitude", "Longitude", "Proximity",
                                        "DefaultFolder", "FolderStatus",
                                        "SeqNoFrefix", "RunningSeqNo",
                                        "CreatedDate", "LUDate"}, new int[]{
                                R.id.folderTID, R.id.masterID,
                                R.id.folderTitle, R.id.ruleFunction,
                                R.id.ruleType, R.id.countryIDs,
                                R.id.matchAdminLevel, R.id.mappedNames,
                                R.id.latitude, R.id.longitude,
                                R.id.proximity, R.id.defaultFolder,
                                R.id.folderStatus, R.id.seqNoFrefix,
                                R.id.runningSeqNo, R.id.createdDate,
                                R.id.lUDate});
                        // folderFilter.setAdapter(folderAdapter);
                    }
                }
            } else {
                folder_array = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map_ = new HashMap<String, String>();
                map_.put("TID", "");
                map_.put("MasterID", "");
                map_.put("FolderTitle", "No folder rule added");
                map_.put("RuleFunction", "");
                map_.put("RuleType", "");
                map_.put("CountryIDs", "");
                map_.put("MatchAdminLevel", "");
                map_.put("MappedNames", "");
                map_.put("Latitude", "");
                map_.put("Longitude", "");
                map_.put("Proximity", "");
                map_.put("DefaultFolder", "");
                map_.put("FolderStatus", "");
                map_.put("SeqNoFrefix", "");
                map_.put("RunningSeqNo", "");
                map_.put("CreatedDate", "");
                map_.put("LUDate", "");
                folder_array.add(map_);
                folderAdapter = new SimpleAdapter(HDAddDeliveryItems.this,
                        folder_array, R.layout.ezeid_folder_row, new String[]{
                        "TID", "MasterID", "FolderTitle",
                        "RuleFunction", "RuleType", "CountryIDs",
                        "MatchAdminLevel", "MappedNames", "Latitude",
                        "Longitude", "Proximity", "DefaultFolder",
                        "FolderStatus", "SeqNoFrefix", "RunningSeqNo",
                        "CreatedDate", "LUDate"}, new int[]{
                        R.id.folderTID, R.id.masterID,
                        R.id.folderTitle, R.id.ruleFunction,
                        R.id.ruleType, R.id.countryIDs,
                        R.id.matchAdminLevel, R.id.mappedNames,
                        R.id.latitude, R.id.longitude, R.id.proximity,
                        R.id.defaultFolder, R.id.folderStatus,
                        R.id.seqNoFrefix, R.id.runningSeqNo,
                        R.id.createdDate, R.id.lUDate});
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetPrimaryDetails(String ezeid) {
        try {
            if (!ezeid.equalsIgnoreCase("")) {
                String token = ezeidPreferenceHelper
                        .GetValueFromSharedPrefs("Token");
                String url = EZEIDUrlManager.getAPIUrl()
                        + "ewtEZEIDPrimaryDetails?Token=" + token + "&EZEID="
                        + ezeid;
                if (!ezeidPreferenceHelper.GetValueFromSharedPrefs(
                        "ITEM_TO_ADD_USER").equalsIgnoreCase("business_mgr")) {
                    contactInfoProgress.setVisibility(View.VISIBLE);
                }
                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                        new Response.Listener<JSONArray>() {

                            @Override
                            public void onResponse(JSONArray response) {
                                PrimaryDetail(response);
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        PrimaryDetail(null);
                    }
                });
                jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                EzeidGlobal.getInstance().addToRequestQueue(jsonArrayRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void PrimaryDetail(JSONArray response) {
        try {
            if (null != response) {
                int len = response.length();
                if (len > 0) {
                    for (int i = 0; i < len; i++) {

                        JSONObject jsonObject = response.getJSONObject(i);
                        // String tID = jsonObject.getString("TID");
                        ezeid = jsonObject.getString("EZEID");
                        firstName = jsonObject.getString("FirstName");
                        lastName = jsonObject.getString("LastName");
                        mobileNumber = jsonObject.getString("MobileNumber");
                        latitude = jsonObject.getString("Latitude");
                        longitude = jsonObject.getString("Longitude");
                        salesRequester.setText("" + firstName + " " + lastName
                                + " - " + mobileNumber);

                    }
                    if (!latitude.equalsIgnoreCase("")
                            && !longitude.equalsIgnoreCase("")) {
                        // GetGeoAddress(Double.parseDouble(latitude),
                        // Double.parseDouble(longitude));
                        GetAddress(latitude, longitude);
                    }
                }
                checkEzeidStatusText.setVisibility(View.GONE);
            } else {
                // salesRequester.setHint("Requester");
                // salesRequester.setHint("Contact info");
                checkEzeidStatusText.setText("Invalid EZEID!");
            }
            contactInfoProgress.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void CheckSessionExpiredStatus() {
        String token = ezeidPreferenceHelper.GetValueFromSharedPrefs("Token");
        if (!token.equalsIgnoreCase("") && sessionRetry < 5) {
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                    EzeidGlobal.EzeidUrl + "ewtGetLoginCheck?Token=" + token,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            SessionStatus(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    sessionRetry += 1;
                    if (error instanceof TimeoutError) {
                        SessionTryAgain();
                    } else if (error instanceof ServerError
                            || error instanceof AuthFailureError) {
                        EzeidUtil.showToast(HDAddDeliveryItems.this,
                                "Network is unreachable!");
                    } else if (error instanceof NetworkError
                            || error instanceof NoConnectionError) {
                        SessionTryAgain();
                    } else {
                        SessionTryAgain();
                    }
                }
            });
            req.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(req, TAG);
        } else {
            // loader.setVisibility(View.INVISIBLE);
        }
    }

    private void SessionTryAgain() {
        CheckSessionExpiredStatus();
    }

    private void SessionStatus(JSONObject response) {

        if (!response.isNull("IsAvailable")) {
            try {
                String status = response.getString("IsAvailable");
                if (status.equalsIgnoreCase("true")) {
                    ActivityStatus();
                } else {
                    SessionDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void SessionDialog() {
        final MaterialDialog materialDialog = new MaterialDialog(this);
        materialDialog
                .setBackgroundResource(R.drawable.abc_cab_background_internal_bg);
        materialDialog.setTitle(R.string.sessionExpired)
                .setMessage("Your session has expired. Please login again!")
                .setPositiveButton(R.string.ok, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        materialDialog.dismiss();
                        ezeidPreferenceHelper.SaveValueToSharedPrefs("Token",
                                "");
                        startActivity(new Intent(HDAddDeliveryItems.this,
                                SplashScreen.class));
                        HDAddDeliveryItems.this.finish();
                    }
                });
        materialDialog.setCanceledOnTouchOutside(true).show();
    }

    protected void GetAddress(String lati, String longi) {

        try {

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    "http://maps.googleapis.com/maps/api/geocode/json?latlng="
                            + lati + "," + longi + "&sensor=true",
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            // TODO Auto-generated method stub
                            BindAddress(response);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    VolleyLog.e(TAG, "error:" + error);
                    BindAddress(null);
                }
            });

            // JSONObject jsonObj = EzeidUtil
            // .getJSONfromURL("http://maps.googleapis.com/maps/api/geocode/json?latlng="
            // + lati + "," + longi + "&sensor=true");
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            EzeidGlobal.getInstance().addToRequestQueue(jsonObjectRequest, TAG);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void BindAddress(JSONObject jsonObj) {
        // TODO Auto-generated method stub
        String address1 = "";
        // String address2 = "";
        // String PIN = "";
        // String full_address = "";
        try {
            if (null != jsonObj) {
                String Status = jsonObj.getString("status");
                if (Status.equalsIgnoreCase("OK")) {
                    JSONArray Results = jsonObj.getJSONArray("results");
                    JSONObject zero = Results.getJSONObject(0);
                    JSONArray address_components = zero
                            .getJSONArray("address_components");

                    JSONObject geoObj = zero.getJSONObject("geometry");
                    JSONObject locObj = geoObj.getJSONObject("location");
                    rlatitude = locObj.getString("lat");
                    rlongitude = locObj.getString("lng");

                    for (int i = 0; i < address_components.length(); i++) {
                        JSONObject zero2 = address_components.getJSONObject(i);
                        String long_name = zero2.getString("long_name");
                        JSONArray mtypes = zero2.getJSONArray("types");
                        String Type = mtypes.getString(0);

                        if (TextUtils.isEmpty(long_name) == false
                                || !long_name.equals(null)
                                || long_name.length() > 0 || long_name != "") {

                            if (Type.equalsIgnoreCase("street_number")) {
                                address1 = long_name + " ";
                            } else if (Type.equalsIgnoreCase("route")) {
                                address1 = address1 + long_name;
                            }
                            // else if (Type.equalsIgnoreCase("sublocality")) {
                            // address2 = long_name;
                            // }
                            else if (Type.equalsIgnoreCase("locality")) {
                                // Address2 = Address2 + long_name + ", ";
                                city = long_name;
                            } else if (Type
                                    .equalsIgnoreCase("administrative_area_level_2")) {
                                country = long_name;
                            } else if (Type
                                    .equalsIgnoreCase("administrative_area_level_1")) {
                                state = long_name;
                            } else if (Type.equalsIgnoreCase("country")) {
                                country = long_name;
                            }
                            // else if (Type.equalsIgnoreCase("postal_code")) {
                            // PIN = long_name;
                            // }

                        }

                    }
                }
            } else {
                // Log.i(TAG, "null response");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
