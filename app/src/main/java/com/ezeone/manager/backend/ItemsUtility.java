package com.ezeone.manager.backend;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class ItemsUtility {
	// convert from bitmap to byte array
	public static byte[] getBytes(Bitmap bitmap) {
		if (null != bitmap) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, 0, stream);
			return stream.toByteArray();
		} else {
			return null;
		}
	}

	// convert from byte array to bitmap
	public static Bitmap getPhoto(byte[] image) {
		return BitmapFactory.decodeByteArray(image, 0, image.length);
	}
}
