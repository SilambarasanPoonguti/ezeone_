package com.ezeone.manager.backend;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ezeone.manager.pojo.AddedItems;

import java.util.ArrayList;
import java.util.List;

public class ItemDBhelper {

	public static final String ITEM_ROW_ID = "item_row_id";
	public static final String ITEM_ID = "item_id";
	public static final String ITEM_TID = "item_tid";
	public static final String ITEM_NAME = "item_name";
	public static final String ITEM_PICTURE = "item_picture";
	public static final String ITEM_RATE = "item_rate";
	public static final String ITEM_AMOUNT = "item_amt";
	public static final String ITEM_QTY = "item_qty";
	public static final String ITEM_STATUS = "item_status";
	public static final String ITEM_DURATIONS = "item_durations";
	public static final String ITEM_MESSAGEID = "item_message_id";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	private static final String DATABASE_NAME = "sales_items_db.db";
	private static final int DATABASE_VERSION = 1;

	private static final String SALES_TABLE = "sales_items";

	// private static final String CREATE_SALES_ITEM_TABLE = "create table "
	// + SALES_TABLE + " (" + ITEM_ROW_ID
	// + " integer primary key autoincrement, " + ITEM_PICTURE
	// + " blob not null, " + ITEM_ID + " text not null unique, "
	// + ITEM_TID + " text not null," + ITEM_NAME + " text," + ITEM_RATE
	// + " text," + ITEM_AMOUNT + " text," + ITEM_QTY + " text ,"
	// + ITEM_STATUS + " text  );";

	private static final String CREATE_SALES_ITEM_TABLE = "create table "
			+ SALES_TABLE + " (" + ITEM_ROW_ID
			+ " integer primary key autoincrement, " + ITEM_PICTURE
			+ " blob not null, " + ITEM_ID + " text not null, " + ITEM_TID
			+ " text not null," + ITEM_NAME + " text," + ITEM_RATE + " text,"
			+ ITEM_AMOUNT + " text," + ITEM_QTY + " text ," + ITEM_STATUS
			+ " text," + ITEM_DURATIONS + " text," + ITEM_MESSAGEID
			+ " text, UNIQUE (" + ITEM_ROW_ID + "," + ITEM_ID
			+ ") ON CONFLICT IGNORE );";

	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_SALES_ITEM_TABLE);
		}

		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + SALES_TABLE);
			onCreate(db);
		}
	}

	public void Reset() {
		mDbHelper.onUpgrade(this.mDb, 1, 1);
	}

	public ItemDBhelper(Context ctx) {
		mCtx = ctx;
		mDbHelper = new DatabaseHelper(mCtx);
	}

	public ItemDBhelper open() throws SQLException {
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public void insertItemDetails(SaleItemModel itemModel) {
		ContentValues cv = new ContentValues();
		cv.put(ITEM_PICTURE, ItemsUtility.getBytes(itemModel.getItem_img()));
		cv.put(ITEM_ID, itemModel.getItem_id());
		cv.put(ITEM_TID, itemModel.getItem_tid());
		cv.put(ITEM_NAME, itemModel.getItem_name());
		cv.put(ITEM_RATE, itemModel.getItem_rate());
		cv.put(ITEM_AMOUNT, itemModel.getItem_amt());
		cv.put(ITEM_QTY, itemModel.getItem_qty());
		cv.put(ITEM_STATUS, itemModel.getItem_status());
		cv.put(ITEM_DURATIONS, itemModel.getItem_durations());
		cv.put(ITEM_MESSAGEID, itemModel.getItem_message_id());
		mDb.insert(SALES_TABLE, null, cv);

	}

	public SaleItemModel retriveItemDetails() throws SQLException {
		Cursor cur = mDb.query(true, SALES_TABLE, new String[] { ITEM_PICTURE,
				ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE, ITEM_AMOUNT, ITEM_QTY,
				ITEM_STATUS, ITEM_DURATIONS, ITEM_MESSAGEID }, null, null,
				null, null, null, null);
		if (cur.moveToFirst()) {
			byte[] blob = cur.getBlob(cur.getColumnIndex(ITEM_PICTURE));
			String item_id = cur.getString(cur.getColumnIndex(ITEM_ID));
			String item_tid = cur.getString(cur.getColumnIndex(ITEM_TID));
			String item_name = cur.getString(cur.getColumnIndex(ITEM_NAME));
			String item_rate = cur.getString(cur.getColumnIndex(ITEM_RATE));
			String item_amt = cur.getString(cur.getColumnIndex(ITEM_AMOUNT));
			String item_qty = cur.getString(cur.getColumnIndex(ITEM_QTY));
			String item_status = cur.getString(cur.getColumnIndex(ITEM_STATUS));
			String item_durations = cur.getString(cur
					.getColumnIndex(ITEM_DURATIONS));
			String item_messagge_id = cur.getString(cur
					.getColumnIndex(ITEM_MESSAGEID));

			cur.close();
			return new SaleItemModel(item_id, item_tid, item_name,
					ItemsUtility.getPhoto(blob), item_rate, item_amt, item_qty,
					item_status, item_durations, item_messagge_id);

		}
		cur.close();
		return null;
	}

	/**
	 * Getting All Contacts
	 * 
	 * @return
	 */
	public List<SaleItemModel> GetAllItems() {
		List<SaleItemModel> itemModels = new ArrayList<SaleItemModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + SALES_TABLE + " ORDER BY "
				+ ITEM_NAME;
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				byte[] blob = cursor.getBlob(cursor
						.getColumnIndex(ITEM_PICTURE));
				String item_id = cursor.getString(cursor
						.getColumnIndex(ITEM_ID));
				String item_tid = cursor.getString(cursor
						.getColumnIndex(ITEM_TID));
				String item_name = cursor.getString(cursor
						.getColumnIndex(ITEM_NAME));
				String item_rate = cursor.getString(cursor
						.getColumnIndex(ITEM_RATE));
				String item_amt = cursor.getString(cursor
						.getColumnIndex(ITEM_AMOUNT));
				String item_qty = cursor.getString(cursor
						.getColumnIndex(ITEM_QTY));
				String item_status = cursor.getString(cursor
						.getColumnIndex(ITEM_STATUS));
				String item_durations = cursor.getString(cursor
						.getColumnIndex(ITEM_DURATIONS));
				String item_messagge_id = cursor.getString(cursor
						.getColumnIndex(ITEM_MESSAGEID));
				// cursor.close();
				// Adding contact to list
				itemModels
						.add(new SaleItemModel(item_id, item_tid, item_name,
								ItemsUtility.getPhoto(blob), item_rate,
								item_amt, item_qty, item_status,
								item_durations, item_messagge_id));
			} while (cursor.moveToNext());
		}
		// close inserting data from database
		// cursor.close();
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		// return contact list
		return itemModels;

	}

	/**
	 * Getting All Items
	 * 
	 * @return SaleItemModel list
	 */
	public List<SaleItemModel> GetAllFilteredItems(String name) {
		List<SaleItemModel> itemModels = new ArrayList<SaleItemModel>();
		// Select All Query

		// "SELECT  * FROM sales_items where item_id like '"
		// + id + "'";
		String selectQuery = "SELECT  * FROM " + SALES_TABLE
				+ " WHERE item_name LIKE'" + name + "'";
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		// Cursor cursor = db.rawQuery(selectQuery, null);
		// Cursor cursor = db.query(true, SALES_TABLE, new String[] {
		// ITEM_PICTURE, ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE,
		// ITEM_AMOUNT, ITEM_QTY, ITEM_STATUS }, ITEM_NAME + "=?",
		// new String[] { name }, null, null, null, null);
		Cursor cursor = db.query(true, SALES_TABLE, new String[] {
				ITEM_PICTURE, ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE,
				ITEM_AMOUNT, ITEM_QTY, ITEM_STATUS, ITEM_DURATIONS,
				ITEM_MESSAGEID }, ITEM_NAME + " LIKE ?", new String[] { name
				+ "%" }, null, null, null, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				byte[] blob = cursor.getBlob(cursor
						.getColumnIndex(ITEM_PICTURE));
				String item_id = cursor.getString(cursor
						.getColumnIndex(ITEM_ID));
				String item_tid = cursor.getString(cursor
						.getColumnIndex(ITEM_TID));
				String item_name = cursor.getString(cursor
						.getColumnIndex(ITEM_NAME));
				String item_rate = cursor.getString(cursor
						.getColumnIndex(ITEM_RATE));
				String item_amt = cursor.getString(cursor
						.getColumnIndex(ITEM_AMOUNT));
				String item_qty = cursor.getString(cursor
						.getColumnIndex(ITEM_QTY));
				String item_status = cursor.getString(cursor
						.getColumnIndex(ITEM_STATUS));
				String item_durations = cursor.getString(cursor
						.getColumnIndex(ITEM_DURATIONS));
				String item_messagge_id = cursor.getString(cursor
						.getColumnIndex(ITEM_MESSAGEID));
				// cursor.close();
				// Adding contact to list
				itemModels
						.add(new SaleItemModel(item_id, item_tid, item_name,
								ItemsUtility.getPhoto(blob), item_rate,
								item_amt, item_qty, item_status,
								item_durations, item_messagge_id));
			} while (cursor.moveToNext());
		}
		// close inserting data from database
		// cursor.close();
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		// return contact list
		return itemModels;

	}

	/**
	 * Getting All Items
	 * 
	 * @return SaleItemModel list
	 */
	public List<SaleItemModel> GetSelectedItem(String item_id) {
		List<SaleItemModel> itemModels = new ArrayList<SaleItemModel>();
		// Select All Query

		// "SELECT  * FROM sales_items where item_id like '"
		// + id + "'";
//		String selectQuery = "SELECT  * FROM " + SALES_TABLE
//				+ " WHERE item_id LIKE'" + item_id + "'";
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		// Cursor cursor = db.rawQuery(selectQuery, null);
		// Cursor cursor = db.query(true, SALES_TABLE, new String[] {
		// ITEM_PICTURE, ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE,
		// ITEM_AMOUNT, ITEM_QTY, ITEM_STATUS }, ITEM_NAME + "=?",
		// new String[] { name }, null, null, null, null);
		Cursor cursor = db.query(true, SALES_TABLE, new String[] {
				ITEM_PICTURE, ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE,
				ITEM_AMOUNT, ITEM_QTY, ITEM_STATUS, ITEM_DURATIONS,
				ITEM_MESSAGEID }, ITEM_ID + " LIKE ?", new String[] { item_id
				+ "%" }, null, null, null, null);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				byte[] blob = cursor.getBlob(cursor
						.getColumnIndex(ITEM_PICTURE));
				String itemId = cursor
						.getString(cursor.getColumnIndex(ITEM_ID));
				String item_tid = cursor.getString(cursor
						.getColumnIndex(ITEM_TID));
				String item_name = cursor.getString(cursor
						.getColumnIndex(ITEM_NAME));
				String item_rate = cursor.getString(cursor
						.getColumnIndex(ITEM_RATE));
				String item_amt = cursor.getString(cursor
						.getColumnIndex(ITEM_AMOUNT));
				String item_qty = cursor.getString(cursor
						.getColumnIndex(ITEM_QTY));
				String item_status = cursor.getString(cursor
						.getColumnIndex(ITEM_STATUS));
				String item_durations = cursor.getString(cursor
						.getColumnIndex(ITEM_DURATIONS));
				String item_messagge_id = cursor.getString(cursor
						.getColumnIndex(ITEM_MESSAGEID));
				itemModels
						.add(new SaleItemModel(itemId, item_tid, item_name,
								ItemsUtility.getPhoto(blob), item_rate,
								item_amt, item_qty, item_status,
								item_durations, item_messagge_id));
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return itemModels;

	}

	public void DeleteAllItems() {
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		db.execSQL("DELETE FROM " + SALES_TABLE);
		db.close();
	}

	// Updating single data
	// public static int updateUserData(UserData data) {
	//
	// SQLiteDatabase db = mDbHelper.getWritableDatabase();
	//
	// ContentValues values = new ContentValues();
	// values.put(KEY_USER_NAME, data.getName());
	// values.put(KEY_USER_EMAIL, data.getEmail());
	// db.update(SALES_TABLE, values, ITEM_ID + " = ?", whereArgs)
	// // updating row
	// return db.update(USER_TABLE, values, KEY_ID + " = ?",
	// new String[] { String.valueOf(data.getID()) });
	// }

	public int UpdateItemData(SaleItemModel itemModel, String item_id) {
		ContentValues cv = new ContentValues();
		cv.put(ITEM_PICTURE, ItemsUtility.getBytes(itemModel.getItem_img()));
		cv.put(ITEM_ID, itemModel.getItem_id());
		cv.put(ITEM_TID, itemModel.getItem_tid());
		cv.put(ITEM_NAME, itemModel.getItem_name());
		cv.put(ITEM_RATE, itemModel.getItem_rate());
		cv.put(ITEM_AMOUNT, itemModel.getItem_amt());
		cv.put(ITEM_QTY, itemModel.getItem_qty());
		cv.put(ITEM_STATUS, itemModel.getItem_status());
		cv.put(ITEM_DURATIONS, itemModel.getItem_durations());
		cv.put(ITEM_MESSAGEID, itemModel.getItem_message_id());
		return mDb.update(SALES_TABLE, cv, ITEM_ID + " = ?",
				new String[] { String.valueOf(item_id) });
	}

	/**
	 * Getting All selcted Items
	 * 
	 * @return SaleItemModel list
	 */
	public List<AddedItems> GetAllAddedItems() {
		List<AddedItems> addedItems = new ArrayList<AddedItems>();

		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		// Cursor cursor = db.rawQuery(selectQuery, null);
		Cursor cursor = db.query(true, SALES_TABLE, new String[] {
				ITEM_PICTURE, ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE,
				ITEM_AMOUNT, ITEM_QTY, ITEM_STATUS, ITEM_DURATIONS,
				ITEM_MESSAGEID }, ITEM_STATUS + "=?", new String[] { "edit" },
				null, null, null, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				byte[] blob = cursor.getBlob(cursor
						.getColumnIndex(ITEM_PICTURE));
				String item_id = cursor.getString(cursor
						.getColumnIndex(ITEM_ID));
				String item_tid = cursor.getString(cursor
						.getColumnIndex(ITEM_TID));
				String item_name = cursor.getString(cursor
						.getColumnIndex(ITEM_NAME));
				String item_rate = cursor.getString(cursor
						.getColumnIndex(ITEM_RATE));
				String item_amt = cursor.getString(cursor
						.getColumnIndex(ITEM_AMOUNT));
				String item_qty = cursor.getString(cursor
						.getColumnIndex(ITEM_QTY));
				String item_status = cursor.getString(cursor
						.getColumnIndex(ITEM_STATUS));
				String item_durations = cursor.getString(cursor
						.getColumnIndex(ITEM_DURATIONS));
				String item_message_id = cursor.getString(cursor
						.getColumnIndex(ITEM_MESSAGEID));

				AddedItems items = new AddedItems();
				items.item_id = item_id;
				items.item_tid = item_tid;
				items.item_name = item_name;
				items.item_rate = item_rate;
				items.item_amt = item_amt;
				items.item_qty = item_qty;
				items.item_status = item_status;
				items.item_img = ItemsUtility.getPhoto(blob);
				items.item_durations = item_durations;
				items.item_message_id = item_message_id;
				addedItems.add(items);

			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return addedItems;

	}

	/**
	 * Getting All new Items
	 * 
	 * @return SaleItemModel list
	 */
	public List<SaleItemModel> GetAllNewItems() {
		List<SaleItemModel> saleItemModels = new ArrayList<SaleItemModel>();

		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		// Cursor cursor = db.rawQuery(selectQuery, null);
		Cursor cursor = db.query(true, SALES_TABLE, new String[] {
				ITEM_PICTURE, ITEM_ID, ITEM_TID, ITEM_NAME, ITEM_RATE,
				ITEM_AMOUNT, ITEM_QTY, ITEM_STATUS, ITEM_DURATIONS,
				ITEM_MESSAGEID }, ITEM_STATUS + "=?", new String[] { "new" },
				null, null, null, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				byte[] blob = cursor.getBlob(cursor
						.getColumnIndex(ITEM_PICTURE));
				String item_id = cursor.getString(cursor
						.getColumnIndex(ITEM_ID));
				String item_tid = cursor.getString(cursor
						.getColumnIndex(ITEM_TID));
				String item_name = cursor.getString(cursor
						.getColumnIndex(ITEM_NAME));
				String item_rate = cursor.getString(cursor
						.getColumnIndex(ITEM_RATE));
				String item_amt = cursor.getString(cursor
						.getColumnIndex(ITEM_AMOUNT));
				String item_qty = cursor.getString(cursor
						.getColumnIndex(ITEM_QTY));
				String item_status = cursor.getString(cursor
						.getColumnIndex(ITEM_STATUS));
				String item_durations = cursor.getString(cursor
						.getColumnIndex(ITEM_DURATIONS));
				String item_messagge_id = cursor.getString(cursor
						.getColumnIndex(ITEM_MESSAGEID));

				saleItemModels.add(new SaleItemModel(item_id, item_tid,
						item_name, ItemsUtility.getPhoto(blob), item_rate,
						item_amt, item_qty, item_status, item_durations,
						item_messagge_id));

			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return saleItemModels;

	}

	public float GetTotalAmount() {

		float amt = 0, amt1 = 0;
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		Cursor cursor = db.query(true, SALES_TABLE,
				new String[] { ITEM_AMOUNT }, ITEM_STATUS + "=?",
				new String[] { "edit" }, null, null, null, null);
		// int count = cursor.getCount();
		if (cursor.moveToFirst()) {
			do {
				String item_amt = cursor.getString(cursor
						.getColumnIndex(ITEM_AMOUNT));
				// item_amt = item_amt.substring(4, item_amt.length());
				amt1 = Float.parseFloat(item_amt);
				amt = (amt1) + (amt);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return amt;
	}

	public int GetTotalAddedItems() {

		int count = 0;
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		Cursor cursor = db.query(true, SALES_TABLE, null, ITEM_STATUS + "=?",
				new String[] { "edit" }, null, null, null, null);
		count = cursor.getCount();
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return count;
	}
}
